const fs = require('fs');

var config = {};

var del = function(type, filename) {
    var path = config.local.basePath + config.local.typePaths[type] + filename;
    return new Promise(function(resolve, reject) {
        fs.unlink(path, err => {
            if (err) return reject(err);
            return resolve(true);
        });
    })
    .then(function() {
        console.log('horray file delete');
    })
    .catch(function(e) {
        console.log('unable delete . maybe no file?');
    });
};

var uploadBuffer = function(type, filename, buffer) {
    var path = config.local.basePath + config.local.typePaths[type] + filename;
    return new Promise(function(resolve, reject) {
        fs.writeFile(path, buffer, function(err) {
            if (err) return reject(err);
            return resolve(true);
        });
    })
    .then(function() {
        console.log('succes uupload file');
    });
}

exports.init = function(configParams) {
    config = configParams;

    return {
        delete: del,
        uploadBuffer: uploadBuffer
    };
};