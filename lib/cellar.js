const config  = require('../../config'),
      Promise = require('bluebird'),
      request = require('request');

var callPlugin = function(method, path, search, data, cookie, isOnionRequest) {
    return new Promise(function(resolve, reject) {
        let headers = { 'Content-Type': 'application/json' };
        if (isOnionRequest) {
            headers['X-Onion-Request'] = 'true';
        }
        var options = {
            url: config.deimos.url + '/' + path + (search ? search : ''),
            headers: headers,
            method: method
        };
        if (cookie) options.headers.Cookie = cookie;
        if (data) options.formData = data;
        request(options, function(err, response, body) {
            if (err) return reject(err);
            try { var parsed = JSON.parse(body); }
            catch (err) { return reject(err); }
            if (response.statusCode != 200) reject({
                pluginErr:  parsed,
                status:     response.statusCode
            });
            return resolve(parsed);
        })
    });
}

exports.getPlugin = function(path, search, cookie, isOnionRequest) {
    return callPlugin('GET', path, search, null, cookie, isOnionRequest);
}

exports.postPlugin = function(path, search, data, cookie, isOnionRequest) {
    return callPlugin('POST', path, search, data, cookie, isOnionRequest);
}

exports.pipe = function(req, res, path, subPath) {
    var url = config.deimos.url + '/' + path + '/' + subPath;
    return req.pipe(request(url)).pipe(res);
}