const config  = require('../../config'),
      sitemap = [];

exports.add = (path, lastmod) => {
    let page = { path: config.baseURL + path };
    if (lastmod) page.lastmod = lastmod;
    sitemap.push(page);
};

exports.dump = () => sitemap;

exports.dumpXML = () => {
    let output = '<?xml version="1.0" encoding="UTF-8"?>\n';
    output += '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n';
    for (let page of sitemap) {
        output += '    <url>\n';
        output += '        <loc>' + page.path + '</loc>\n';
        if (page.lastmod) {
            output += '        <loc>' + page.lastmod + '</loc>\n';
        }
        output += '    </url>\n';
    }
    output += '</urlset>';

    return output;
}