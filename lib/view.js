var Promise   = require('bluebird');
var nunjucks  = require('nunjucks');
var fse       = require('fs-extra');
var cache     = require('./cache');
var util      = require('./util');
var config    = require('../../config');
var sri       = require('../sri.json');

var getPublicConfig = (req) => {
  return {
    ugly: config.ugly,
    cdn_surface: util.isOnionRequest(req) ? null : config.cdn.surface,
    cdn_plugins: util.isOnionRequest(req) ? null : config.cdn.plugins,
    sri: sri
  };
}

exports.render = function(req, path, params, overrideMeta) {
  var meta = {},
      content = '',
      pageType = path.replace(/\//g, '-');

  return exports.getMeta(path, overrideMeta)
    .then(function(_meta) {
      meta = _meta;
      if (util.isOnionRequest(req)) {
        params.onion_request = true;
      }

      params.config = getPublicConfig(req);

      return new Promise(function(resolve, reject) {
        nunjucks.render(path + '/index.html', params, function(err, template) {
          if (err) reject(err);
          else resolve(template);
        });
      });
    })

    .then(function(_content) {
      content = _content;
      return exports.layout(req, content, meta, pageType);
    })

    .then(function(html) {
      var data = {
        html: html,
        content: content,
        meta: meta,
        pageType: pageType
      }
      return Promise.resolve(data);
    })

    .catch(function(err) {
      return Promise.reject(err);
    });
};

exports.renderPartial = function(req, partial, params) {
  params.config = getPublicConfig(req);
  return new Promise(function(resolve, reject) {
    nunjucks.render(partial + '.html', params, function(err, template) {
      if (err) reject(err);
      else resolve(template);
    });
  });
};

exports.renderComponent = function(req, component, params) {
  params.config = getPublicConfig(req);
  return new Promise(function(resolve, reject) {
    nunjucks.render('_components/'+ component + '.html', params,
      function(err, template) {
        if (err) reject(err);
        else resolve(template);
      }
    );
  });
}

exports.layout = function(req, content, meta, pageType) {
  return new Promise(function(resolve, reject) {
    var viewData = {
      content: content,
      meta: meta,
      path: req.path,
      pageType: pageType,
      config: getPublicConfig(req),
      user: {
        id: req.user.id,
        username: req.user.username,
        avatar: req.user.avatar
      }
    };
    if (util.isOnionRequest(req)) {
        viewData.onion_request = true;
      }
    if (util.isJSON(req)) return resolve(null);
    nunjucks.render('_layouts/default.html', viewData, function(err, html) {
      if (err) reject(err);
      else resolve(html);
    });
  });
}

exports.getMeta = function(path, overrideMeta) {
  var metaPath    = __dirname + '/../views/' + path + '/meta.json',
      cachedData  = cache.memoryGet('files', metaPath);

  var promise = Promise.resolve();

  if (cachedData)
    promise = promise.then(function() { return cachedData; });
  else 
    promise = promise.then(function() { return fse.readJson(metaPath) })
      .then(function(json) {
        cache.memorySet('files', metaPath, json);
        return Promise.resolve(json);
      })
      .catch(function(err) {
        var fakeJson = { meta: [], scripts: [], title: path };
        cache.memorySet('files', metaPath, fakeJson);
        return Promise.resolve(fakeJson);
      });

  return promise
    .then(function(meta) {
      if (overrideMeta) {
        meta = JSON.parse(JSON.stringify(meta));
        if (overrideMeta.title) {
          meta.title = overrideMeta.title;
        }
        if (overrideMeta.meta) {
          if (!meta.meta) {
            meta.meta = [];
          }
          for (var i=0; i<overrideMeta.meta.length; i++) {
            var found = false,
                tag = overrideMeta.meta[i];
                
            for (var j=0; j<meta.meta.length; j++) {
              if (
                (tag.name && meta.meta[j].name && tag.name == meta.meta[j].name)
                ||
                (tag.property && meta.meta[j].property && tag.property == meta.meta[j].property)
                ||
                (tag.httpEquiv && meta.meta[j].httpEquiv && tag.httpEquiv == meta.meta[j].httpEquiv)
              ) {
                found = true;
                if (tag.content) {
                  meta.meta[j].content = tag.content;
                }
              }
            }
            if (found == false) {
              meta.meta.push(tag);
            }
          }
        }
      }
      if (meta.stylesheets) {
        for (stylesheet of meta.stylesheets) {
          let integrity = sri['@static/css/'+stylesheet+'.css'];
          if (integrity) {
            if (typeof meta.sri === "undefined") meta.sri = {};
            meta.sri['css/'+stylesheet] = {hashes: integrity.hashes, integrity: integrity.integrity};
          }
        }
      }
      if (meta.scripts) {
        for (script of meta.scripts) {
          let integrity = sri['@static/js/'+script+ (config.ugly ? '.min.js' : '.js')];
          if (integrity) {
            if (typeof meta.sri === "undefined") meta.sri = {};
            meta.sri['js/'+script] = {hashes: integrity.hashes, integrity: integrity.integrity};
          }
        }
      }
      if (meta.libs) {
        for (lib of meta.libs) {
          let integrity = sri['@static/js/_lib/'+lib+'.js'];
          if (integrity) {
            if (typeof meta.sri === "undefined") meta.sri = {};
            meta.sri['js/_lib/'+lib] = {hashes: integrity.hashes, integrity: integrity.integrity};
          }
        }
      }
      return meta;
    })
};