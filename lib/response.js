var util      = require('./util'),
    view      = require('./view'),
    config    = require('../../config');

exports.send = function(data, req, res, flags) {
  flags || (flags = {});
  res.set('X-Frame-Options', config.security.xFrameOptions);
  res.set('Content-Security-Policy', config.security.csp);
  res.set('Access-Control-Allow-Origin', config.security.accessControlAllowOrigin);
  res.set('X-Content-Type-Options', 'nosniff');
  res.set('Referrer-Policy', 'strict-origin');
  res.set('Cross-Origin-Resource-Policy', 'same-site');
  res.set('Permissions-Policy', 'interest-cohort=()'); // fuck google's IE6 browser.
  if (config.security.hsts && !util.isOnionRequest(req)) {
    res.set('Strict-Transport-Security', config.security.hsts);
  }
  if (config.onionURL) {
    res.set('Onion-Location', config.onionURL);
  }
  if (util.isJSON(req) || flags.json) return res.json(data);
  if (flags.raw) return res.send(data.raw);
  return res.send(data.html);
};

exports.error = function(err, req, res) {
  console.log('ERROR: ', err);
  return res.status(500).send('Internal Server Error');
};

exports.redirect = function(url, req, res) {
  if (util.isJSON(req)) return res.json({redirect: url});
  return res.redirect(url);
};

exports.show404 = function(req, res) {
  if (!util.isJSON(req)) res.status(404);
  return view.render(req, '404', {})
    .then(function(data) { exports.send(data, req, res); })
    .catch(function(err) { exports.error(err, req, res); });
}

exports.HTTPError = (req, res, code) => {
  let error = 'Internal Server Error';
  if (code == 403) error = 'Forbidden';
  res.status(code).send(`<html>
<head><title>${code} ${error}</title></head>
<body>
<center><h1>${code} ${error}</h1></center>
<hr><center>obsessive facts</center>
</body>
</html>`);
}
