const config  = require('../../config'),
      crypto  = require('crypto'),
      Promise = require('bluebird');

exports.isJSON = function(req) {
    if (
        typeof req.headers !== "undefined"
        &&
        typeof req.headers["content-type"] !== "undefined"
        &&
        req.headers["content-type"] == "application/json"
    )
    return true;
    return false;
}

exports.isOnionRequest = (req) => {
    return !!req.headers['x-onion-request'];
}

exports.makeId = function(bytes) {
    if (!bytes) bytes = 8;
    return new Promise(function(resolve, reject) {
        crypto.randomBytes(bytes, function(err, buff) {
            if (err) return reject(err);
            return resolve(buff.toString('hex'));
        });
    });
}

exports.makeIdCheap = function() {
    return Math.random().toString(36).substr(2, 9); // good enough
}

exports.form = {
    truthy: function(field) {
        if (field instanceof Array) field = field[0];
        return field && (field == true || field.toLowerCase() == "true");
    },

    flatten: function(form) {
        var flat = {};
        for (var field in form)
            if (form.hasOwnProperty(field))
                if (form[field] instanceof Array && form[field].length == 1)
                    flat[field] = form[field][0];
                else
                    flat[field] = form[field];

        return flat;
    },

    isEmail: function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    isUrl: function(str) {
        var pattern = new RegExp('^https?:\\/\\/'
            + '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'
            + '((\\d{1,3}\\.){3}\\d{1,3}))'
            + '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'
            + '(\\?[;&a-z\\d%_.~+=-]*)?'
            + '(\\#[-a-z\\d_]*)?$','i');
        return !!pattern.test(str);
    }
}

const key = config.users.tmpEncryptionPassphrase;

exports.encrypt = function(obj) {
    let iv = crypto.randomBytes(16)
    const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
    var encrypted = cipher.update(JSON.stringify(obj), 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return iv.toString('hex') + '-' + encrypted;
}

exports.decrypt = function(hex) {
    let components = hex.split('-');
    let iv = Buffer.from(components[0], 'hex')
    var decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    let decrypted = decipher.update(components[1], 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return JSON.parse(decrypted);
}

exports.sha256 = function(str) {
    return crypto.createHash('sha256').update(str).digest('hex')
}

exports.getHashedIPFromReq = function(req) {
    return req.headers['x-forwarded-for'] ?
        exports.sha256(req.headers['x-forwarded-for'])
        :
        exports.sha256(req.connection.remoteAddress);
}