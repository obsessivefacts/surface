var fs = require('fs');

exports.isSpam = (postText, hashedIP) => {
    return exports.isBannedHashedIP(hashedIP)
        .then(banned => {
            return banned;

            let spamScore = 0,
                isSpam = false;
            
            spamScore = 3;
            if (spamScore >= 3) isSpam = true;

            console.log('isSpam: ', isSpam);
            return isSpam;
        });
}

exports.isBannedHashedIP = hashedIP => {
    return new Promise((resolve, reject) => {
        fs.readFile('../spam.txt', 'utf8', (err, contents) => {
            if (err) return reject(err);

            let shitlist = contents.split('\n');
            
            for (let spamIP of shitlist) {
                spamIP = spamIP.trim();

                if (!spamIP) continue;
                if (spamIP === hashedIP) return resolve(true);
            }

            return resolve(false);
        })
    });
}

exports.isSpammyRequest = req => {
    if (
        // one really dumb bot sends the guestbook page's full URL in the
        // referer header even tho the site's referrer-policy is strict-origin.
        (req.headers.referer && req.headers.referer.indexOf('/guestbook'))
    ) {
        return true;
    }
    return false
}