const util  = require('./util');

var CACHING = true;

var memoryCache = {
    files: {},
    posts: {},
};

var redisIsInitialized = false,
    redis = null;

exports.memoryGet = function(bucket, key) {
    if (!CACHING) {
        console.log('Caching is disabled.')
        return;
    }
    return memoryCache[bucket][key] ? memoryCache[bucket][key] : null;
}

exports.memorySet = function(bucket, key, data) {
    memoryCache[bucket][key] = data;
    return data;
}

exports.redisInit = function(redisClient) {
    if (redisIsInitialized) return false;
    redis = redisClient;
    redisIsInitialized = true;
}

exports.redisGet = function(key, namespace) {
    return Promise.resolve()
        .then(function() {
            if (!redisIsInitialized)
                return redisError();

            return true;
        })
        .then(function() {
            if (!namespace) return '';
            else return redisGetNamespace(namespace);
        })
        .then(function(nsVal) {
            return new Promise(function(resolve, reject) {
                redis.get(nsVal + key, function(err, data) {
                    if (err) reject(err);
                    else resolve(data);
                })
            })
            .catch(function(err) {
                return redisError(err);
            });
        })
}

exports.redisSet = function(key, val, seconds, namespace) {
    seconds || (seconds = 300);

    return Promise.resolve()
        .then(function() {
            if (!redisIsInitialized)
                return redisError();
            
            return true;
        })
        .then(function() {
            if (!namespace) return '';
            else return redisGetNamespace(namespace);
        })
        .then(function(nsVal) {
            return new Promise(function(resolve, reject) {
                redis.set(nsVal + key, val, function(err, reply) {
                    if (err) reject(err);
                    redis.expire(nsVal + key, seconds);
                    resolve(true);
                });
            });
        })
        .catch(function(err) {
            return redisError(err);
        });
}

exports.redisDelete = function(key, namespace) {
    return Promise.resolve()
        .then(function() {
            if (!redisIsInitialized)
                return redisError();

            return true;
        })
        .then(function() {
            if (!namespace) return '';
            else return redisGetNamespace(namespace);
        })
        .then(function(nsVal) {
            return new Promise(function(resolve, reject) {
                redis.del(nsVal + key, function(err, data) {
                    if (err) reject(err);
                    else resolve(data);
                })
            })
            .catch(function(err) {
                return redisError(err);
            });
        })
}

exports.redisDestroyNamespace = function(namespace) {
    return Promise.resolve()
        .then(function() {
            return redisGetNamespace(namespace, true);
        });
}

var redisGetNamespace = function(namespace, destroy) {
    var nsKey = 'NS:' + namespace,
        nsVal;

    return exports.redisGet(nsKey)
        .then(function(result) {
            if (result && !destroy) {
                nsVal = result;
                return result;
            }
            return util.makeId()
                .then(function(hex) {
                    nsVal = 'NS:' + namespace + ':' + hex + ':';
                    return exports.redisSet(nsKey, nsVal, 86400, null);
                })
        })
        .then(function() {
            return nsVal;
        })
        .catch(function(err) {
            return redisError(err);
        });
}

var redisError = function(err) {
    throw Error(err || 'redis not initialized dumbass');
}