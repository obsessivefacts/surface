var Promise  = require('bluebird');
var util     = require('./util');
var config   = require('../../config'),
    crypto   = require('crypto'),
    models   = {},
    app      = null,
    db       = null,
    redis    = null;

module.exports = function(providers) {
// ==

app           = providers.app;
db            = providers.db;
redis         = providers.redis;

models.User = require('../models/user')(db);
models.Role = require('../models/role')(db);

var populateAuthMethods = function(user) {
    user.hasRole = function(role) {
        for (var i=0; i<this.roles.length; i++)
            if (this.roles[i].name == role)
                return true;
        return false;
    }.bind(user);

    user.isAdmin = function() {
        return this.hasRole('angel');
    }.bind(user);

    user.encryptedAccessToken = function() {
        var token = {access_token: user.access_token};
        return util.encrypt(token);
    },

    user.isLoggedIn = function() { return user.id > 0; }
};

return function(req, res, next) {

    if (req.path.indexOf('fmiffel') !== -1) {
        console.log('req(fmiffel): ', req.path, req.headers['user-agent']);
    } else if (req.path.indexOf('gemini-proxy') !== -1) {
        console.log('req(gemini-proxy): ', req.path, ';', (req.query.uri ? decodeURIComponent(req.query.uri) : '(no uri)'), ';', req.headers['user-agent']);
    } else {
        console.log('req: ', req.path, req.headers['referer']);
    }

    req.user = {
        id:         0,
        username:   'anonymous',
        title:      'anonymous',
        avatar:     null,
        roles:      []
    }
    if (!req.signedCookies || !req.signedCookies.access_token) {
        populateAuthMethods(req.user);
        return next();
    }

    var token = req.signedCookies.access_token;

    return new Promise(function(resolve, reject) {
        redis.get(token, function(err, user) {
            if (err) reject(err);
            else resolve(user);
        })
    })
    .then(function(user) {
        if (user) {
            req.user = JSON.parse(user);
            return null;
        }
        return models.User.findOne({
            where: { access_token: token },
            include: [ models.Role ]
        });
    })
    .then(function(user) {
        if (user) {
            var user = user.dataValues;
            redis.set(token, JSON.stringify(user), function(err, reply) {
                if (!err) redis.expire(token, 86400);
            });
            req.user = user;
        }
        populateAuthMethods(req.user);

        if (!req.user.id) {
            console.log('CLEARING BAD COOKIE');
            res.clearCookie('access_token');
        }        
        return next();
    })
    .catch(function(err) {
        console.log('USER AUTHENTICATION ERROR: ', err);
        return next();
    });
}

// ==
}