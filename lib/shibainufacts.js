exports.getTheFacts = () => {
    for (let fact of facts)
        if (fact.text)
            fact.text = fact.text
                            .replace(/([>\s])(#)(\w+)/img, '$1<a href="/fmiffel/tag/$3">$2$3</a>')
                            .replace(/\n/mg, '<br/>')
                            .replace(/(((https?:\/\/)|(www\.))[^\s]+)/g, function (url) {
                                var hyperlink = url;
                                if (!hyperlink.match('^https?:\/\/')) {
                                  hyperlink = 'http://' + hyperlink;
                                }
                                return '<a href="' + hyperlink + '" target="_blank" rel="noopener noreferrer">' + url + '</a>'
                            });
    return facts;
}

const facts = [
    {
        "pinned": true,
        "prettyDate": "May 15, 2016",
        "timestamp": "2016-05-15T23:00:17.000Z",
        "id": "731982565830238209",
        "text": "It's time to take drugs",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CiiGPNLUkAEZaWW?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "52",
        "likes": "147"
    },
    {
        "prettyDate": "Jan 14, 2021",
        "timestamp": "2021-01-15T02:35:33.000Z",
        "id": "1349908048849100809",
        "text": "punish",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ErvWNPBXIAAHasX?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 5, 2021",
        "timestamp": "2021-01-05T22:25:55.000Z",
        "id": "1346583733378428929",
        "text": "Shiba inu decertify the election result",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ErAGw21XYAMYPZX?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 5, 2021",
        "timestamp": "2021-01-05T16:10:30.000Z",
        "id": "1346489257184141312",
        "text": "Covid is a:",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "hoax",
                    "percent": 0
                },
                {
                    "option": "failed hoax",
                    "percent": 0
                },
                {
                    "option": "scamdemic hoax",
                    "percent": 0
                },
                {
                    "option": "all of the above",
                    "percent": 100,
                    "winner": true
                }
            ],
            "stats": "1 vote"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 3, 2021",
        "timestamp": "2021-01-04T01:45:21.000Z",
        "id": "1345909150744768513",
        "text": "Protip: don’t sit in spiny burrs",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Eq2hPGVW8AE--ZU?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 31, 2020",
        "timestamp": "2020-12-31T15:34:17.000Z",
        "id": "1344668206036873216",
        "text": "is punishment the right choice for my entire family",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 29, 2020",
        "timestamp": "2020-12-30T04:44:21.000Z",
        "id": "1344142258166226944",
        "text": "Sick inu needs a nap",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EqdaQUjXYAIdKGi?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 25, 2020",
        "timestamp": "2020-12-26T00:09:22.000Z",
        "id": "1342623502575267841",
        "text": "You will learn about punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 25, 2020",
        "timestamp": "2020-12-26T00:01:05.000Z",
        "id": "1342621420061339660",
        "text": "Road trip intensifies",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EqHzD60W4AEH3iv?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 25, 2020",
        "timestamp": "2020-12-25T23:58:31.000Z",
        "id": "1342620773517811713",
        "text": "Yes. You must punish.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 25, 2020",
        "timestamp": "2020-12-25T23:36:59.000Z",
        "id": "1342615354435117059",
        "text": "My dog is Asian. Should I punish my entire family?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 25, 2020",
        "timestamp": "2020-12-25T15:18:11.000Z",
        "id": "1342489824759054336",
        "text": "Shiba inu road trip",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EqF7YLGXAAAHpfw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 23, 2020",
        "timestamp": "2020-12-24T02:20:00.000Z",
        "id": "1341931601786068992",
        "text": "Shiba inu cal me a leftist beta incel soyboy manlet cuck",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 22, 2020",
        "timestamp": "2020-12-23T02:30:30.000Z",
        "id": "1341571857418342400",
        "text": "In Cyberpunk 2077 I am RP’ing a pacifist bisexual cyberslut who goes around “stealthing” people",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 22, 2020",
        "timestamp": "2020-12-22T20:15:46.000Z",
        "id": "1341477553521844224",
        "text": "i have a huge cock",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 22, 2020",
        "timestamp": "2020-12-22T19:58:21.000Z",
        "id": "1341473170356805633",
        "text": "Should all shiba inus be punished or do all shiba inus already punish? How far would you be willing to go to get punished? #VoteThemAllOut",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 21, 2020",
        "timestamp": "2020-12-22T02:35:00.000Z",
        "id": "1341210601855447041",
        "text": "o wise shiba inu, give me the crack. to smoke meth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 21, 2020",
        "timestamp": "2020-12-21T14:40:40.000Z",
        "id": "1341030833759535104",
        "text": "Why the long face",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EpxMbrrUwAAIwWx?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 20, 2020",
        "timestamp": "2020-12-21T04:31:38.000Z",
        "id": "1340877565548871680",
        "text": "Get the chicken.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/1340877528496369665/pu/img/V3qUnACSIcZ_BLnc.jpg"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 20, 2020",
        "timestamp": "2020-12-21T00:19:35.000Z",
        "id": "1340814132879060992",
        "text": "What if I’m not a real person",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EpuHWKcUcAA-UdZ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 20, 2020",
        "timestamp": "2020-12-21T00:03:50.000Z",
        "id": "1340810172466229248",
        "text": "Gay Effect Gaydromeda",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 20, 2020",
        "timestamp": "2020-12-20T18:37:04.000Z",
        "id": "1340727937356611586",
        "text": "Golden retrievers are stupid.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 20, 2020",
        "timestamp": "2020-12-20T12:58:47.000Z",
        "id": "1340642806201634816",
        "text": "Covid is a hoax",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EprrhiDU0AIi2hB?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 20, 2020",
        "timestamp": "2020-12-20T12:57:38.000Z",
        "id": "1340642514840088579",
        "text": "Instead of Mass Effect Andromeda it should be called Gay Effect Gaydromeda",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 19, 2020",
        "timestamp": "2020-12-19T23:35:05.000Z",
        "id": "1340440546049814528",
        "text": "I got all achievements in Mass Effect Andromeda. Now I’m officially “dumb and gay”",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 16, 2020",
        "timestamp": "2020-12-17T00:19:15.000Z",
        "id": "1339364499212517379",
        "text": "I play Fallout 76 with a butt plug.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 16, 2020",
        "timestamp": "2020-12-17T00:18:20.000Z",
        "id": "1339364267112374272",
        "text": "reeeee",
        "avatar": "https://pbs.twimg.com/profile_images/1278949838009233408/TZCI_acb_normal.png",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 16, 2020",
        "timestamp": "2020-12-16T21:41:52.000Z",
        "id": "1339324891691012097",
        "text": "You are beautiful",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EpY843gVQAEghuP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-13T00:02:28.000Z",
        "id": "1337910725604347904",
        "text": "Asians are Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-12T23:59:40.000Z",
        "id": "1337910018209763328",
        "text": "fuck fuck motherfuck motherfuck fucker motherfuck fuck motherfuck motherfucker fuck fuck motherfuck motherfuck fucker mother mother fuck mother fuck mother fucker",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-12T23:58:30.000Z",
        "id": "1337909724289748993",
        "text": "Legalize meth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-12T23:57:48.000Z",
        "id": "1337909548053454849",
        "text": "Fuck",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-12T23:57:37.000Z",
        "id": "1337909503342170117",
        "text": "I will be forced to punish.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-12T23:57:18.000Z",
        "id": "1337909424984182784",
        "text": "Fuck I must punish",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-12T23:56:23.000Z",
        "id": "1337909192556859393",
        "text": "good",
        "avatar": "https://pbs.twimg.com/profile_images/1344693818722017281/PImN4FrG_normal.jpg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 12, 2020",
        "timestamp": "2020-12-12T23:47:27.000Z",
        "id": "1337906946704244736",
        "text": "Why is No Man’s Sky sexist it should be called No Woman’s Sky",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 10, 2020",
        "timestamp": "2020-12-10T18:12:30.000Z",
        "id": "1337097876241977344",
        "text": "Which U.S. President is EXACTLY LIKE HITLER",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Trump",
                    "percent": 33.3
                },
                {
                    "option": "All of the above",
                    "percent": 66.7,
                    "winner": true
                }
            ],
            "stats": "3 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 8, 2020",
        "timestamp": "2020-12-08T14:57:41.000Z",
        "id": "1336324073765314565",
        "text": "The time has come for severe punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EouTqLKVEAAjtv7?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 5, 2020",
        "timestamp": "2020-12-05T22:22:57.000Z",
        "id": "1335348964128935958",
        "text": "My doctor said I have too much buttsex",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 5, 2020",
        "timestamp": "2020-12-05T22:03:37.000Z",
        "id": "1335344099969347586",
        "text": "I sharted",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 5, 2020",
        "timestamp": "2020-12-05T21:47:55.000Z",
        "id": "1335340147613818887",
        "text": "Send nudes",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EogUyR0XcAEqWUv?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Nov 26, 2020",
        "timestamp": "2020-11-27T04:53:10.000Z",
        "id": "1332185674607325184",
        "text": "My Asian dog is just like a baby",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Enzfzx9VkAAN8dn?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Nov 26, 2020",
        "timestamp": "2020-11-26T21:36:18.000Z",
        "id": "1332075732898443265",
        "text": "This claim about election fraud is disputed.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Enx70P_VkAAE6LH?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 26, 2020",
        "timestamp": "2020-11-26T17:06:04.000Z",
        "id": "1332007726109450240",
        "text": "Donald Drumpf AIDS",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Enw99xNUUAEUqf3?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Nov 26, 2020",
        "timestamp": "2020-11-26T15:51:10.000Z",
        "id": "1331988880397004803",
        "text": "Thanksgiving inu give me a horrific inflammation",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Enws0uEVkAAgDmm?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 24, 2020",
        "timestamp": "2020-11-25T04:40:10.000Z",
        "id": "1331457627008561155",
        "text": "Thanks for your participation",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EnpJp4tUwAEA9Ao?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 23, 2020",
        "timestamp": "2020-11-23T22:20:04.000Z",
        "id": "1330999583812382721",
        "text": "Who did you vote for 2020",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Trump",
                    "percent": 0
                },
                {
                    "option": "Trump",
                    "percent": 0
                },
                {
                    "option": "Trump",
                    "percent": 0
                },
                {
                    "option": "Trump",
                    "percent": 100,
                    "winner": true
                }
            ],
            "stats": "1 vote"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Nov 19, 2020",
        "timestamp": "2020-11-19T16:12:05.000Z",
        "id": "1329457429587832832",
        "text": "Great news! Covid-19 is a scamdemic demonrat hoax! And it is over",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EnMue3pVoAA1NZS?format=jpg&name=large"
        ],
        "replies": "2",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 2, 2020",
        "timestamp": "2020-11-02T21:24:54.000Z",
        "id": "1323375555878678530",
        "text": "Punish this fucking sunflower",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/El2TDQHU8AAD74m?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 29, 2020",
        "timestamp": "2020-08-30T00:41:40.000Z",
        "id": "1299869865184882688",
        "text": "Should I punish my whole family?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Whole family",
                    "percent": 66.7,
                    "winner": true
                },
                {
                    "option": "Just partial",
                    "percent": 33.3
                }
            ],
            "stats": "3 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Aug 21, 2020",
        "timestamp": "2020-08-22T03:17:26.000Z",
        "id": "1297009960727928832",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ef_nrriUYAEyAqo?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Aug 21, 2020",
        "timestamp": "2020-08-22T03:16:58.000Z",
        "id": "1297009841198649344",
        "text": "THE TIME HAS COME FOR SEVERE PUNISHMENT",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "Aug 20, 2020",
        "timestamp": "2020-08-21T01:28:08.000Z",
        "id": "1296620065803010048",
        "text": "Gandalf",
        "avatar": "https://pbs.twimg.com/profile_images/1278949838009233408/TZCI_acb_normal.png",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Aug 10, 2020",
        "timestamp": "2020-08-11T00:22:30.000Z",
        "id": "1292979669038374912",
        "text": "Shiba inus understand Japanese (because they are from Japan)",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EfGWJxaVoAIX-c_?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Aug 6, 2020",
        "timestamp": "2020-08-06T21:51:18.000Z",
        "id": "1291492066724155394",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EexNMAuVoAAbO3o?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jul 31, 2020",
        "timestamp": "2020-07-31T20:30:14.000Z",
        "id": "1289297341564850176",
        "text": "I’m a butt slut who is gay",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jul 16, 2020",
        "timestamp": "2020-07-16T14:46:36.000Z",
        "id": "1283775042627633153",
        "text": "I’m a gay butt slut",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jul 8, 2020",
        "timestamp": "2020-07-08T23:44:52.000Z",
        "id": "1281011399095799808",
        "text": "Me IRL",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EccRE1IVcAAMDEK?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jul 7, 2020",
        "timestamp": "2020-07-07T13:45:43.000Z",
        "id": "1280498233056169984",
        "text": "It just dawned on me that no one will be safe from the Very Real ScaryFlu until Blompf is out of office. Now I’m literally shaking",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EcU-WXSWAAUQZrq?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Jun 26, 2020",
        "timestamp": "2020-06-27T00:22:34.000Z",
        "id": "1276672231699972097",
        "text": "Shiba inu illegal drugs garden",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EbemmJoVAAE1A0S?format=jpg&name=large",
            "https://pbs.twimg.com/media/EbemmJoVAAAGDDx?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 9, 2020",
        "timestamp": "2020-06-10T03:48:34.000Z",
        "id": "1270563482472034304",
        "text": "join antifa",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 6, 2020",
        "timestamp": "2020-06-06T05:21:43.000Z",
        "id": "1269137369917095936",
        "text": "Look at this dog",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EZzhtSwUMAAb7wj?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "May 30, 2020",
        "timestamp": "2020-05-31T03:29:27.000Z",
        "id": "1266934791208759298",
        "text": "Surely democrats smashing up their own cities will be the end for Drumpf",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 30, 2020",
        "timestamp": "2020-05-31T03:24:39.000Z",
        "id": "1266933582347427841",
        "text": "Thanks realdonaldblompf plz smash antifa once and for all",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EZUNX9pU8AAIyzk?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "May 29, 2020",
        "timestamp": "2020-05-29T04:04:36.000Z",
        "id": "1266218862397157376",
        "text": "Shiba inu riot",
        "avatar": "https://pbs.twimg.com/profile_images/1289984813890404354/DLZXFUED_normal.jpg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "May 25, 2020",
        "timestamp": "2020-05-26T03:38:44.000Z",
        "id": "1265125188548493313",
        "text": "The punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EY6gprgU0AA1C7W?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "May 25, 2020",
        "timestamp": "2020-05-26T02:42:44.000Z",
        "id": "1265111094739128320",
        "text": "Shiba inu nightmare",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EY6T1VzUEAAbZYO?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 19, 2020",
        "timestamp": "2020-05-20T02:04:58.000Z",
        "id": "1262927264372584449",
        "text": "Shiba inu 8th birthday",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EYbRpiLXQAAW4AT?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 19, 2020",
        "timestamp": "2020-05-19T08:39:06.000Z",
        "id": "1262664061679886337",
        "text": "Gay of Gibraltar",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "May 12, 2020",
        "timestamp": "2020-05-12T04:00:55.000Z",
        "id": "1260057339471687680",
        "text": "Shiba inus hate energy drinks",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EXyfd8CVAAIZcTH?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "May 11, 2020",
        "timestamp": "2020-05-12T03:57:40.000Z",
        "id": "1260056523142619136",
        "text": "The time for punishment has now arrived",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EXyeueaUEAA1phk?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 28, 2020",
        "timestamp": "2020-04-29T02:19:31.000Z",
        "id": "1255320782718279682",
        "text": "Legalize Meth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EWvLmTVXsAMHcpR?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 24, 2020",
        "timestamp": "2020-04-24T21:05:50.000Z",
        "id": "1253792288133844992",
        "text": "Did shiba inus create the coronavirus hoax to destroy the Americans economy #openthe #america #shiba #inu #reee",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 24, 2020",
        "timestamp": "2020-04-24T21:04:28.000Z",
        "id": "1253791944309739520",
        "text": "Only on hardwood floor",
        "avatar": "https://pbs.twimg.com/profile_images/1400252501039976448/Z9YYmLm0_normal.jpg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 23, 2020",
        "timestamp": "2020-04-23T04:43:41.000Z",
        "id": "1253182734991650816",
        "text": "Firefox",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EWQzDXYUEAAkbm_?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 23, 2020",
        "timestamp": "2020-04-23T04:01:31.000Z",
        "id": "1253172122215542784",
        "text": "Hentacles was the Greek god of hentai",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 22, 2020",
        "timestamp": "2020-04-23T02:54:04.000Z",
        "id": "1253155147410075648",
        "text": "#AnimalCrossing #ACNH #NintendoSwitch",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EWQZ9yHUYAA2geV?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 22, 2020",
        "timestamp": "2020-04-23T02:47:42.000Z",
        "id": "1253153546289115136",
        "text": "Shiba inu indentured servant",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EWQYglcUcAEng0r?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 21, 2020",
        "timestamp": "2020-04-21T21:25:33.000Z",
        "id": "1252710085471563778",
        "text": "Coronavirus SARS AIDS (shiba inu)",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EWKFLjLUcAAvC9P?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 21, 2020",
        "timestamp": "2020-04-21T13:09:07.000Z",
        "id": "1252585156457533442",
        "text": "Meth is not a crime",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EWITjs9VcAEUM8f?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 15, 2020",
        "timestamp": "2020-04-15T20:54:09.000Z",
        "id": "1250527858486849537",
        "text": "Shiba inu gave me COVAIDS-69",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EVrEc95XsAMKdJz?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 14, 2020",
        "timestamp": "2020-04-14T15:32:57.000Z",
        "id": "1250084636232843269",
        "text": "CHECKMATE NORMIES",
        "avatar": "https://pbs.twimg.com/profile_images/1278949838009233408/TZCI_acb_normal.png",
        "attachedImages": [
            "https://pbs.twimg.com/media/EVkxWR8UcAAW5WP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 13, 2020",
        "timestamp": "2020-04-13T13:28:34.000Z",
        "id": "1249690945622167552",
        "text": "Do not misgender shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EVfLSbEVAAIthv4?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 12, 2020",
        "timestamp": "2020-04-12T19:34:02.000Z",
        "id": "1249420531419381761",
        "text": "shiba inu Easter",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EVbVWRiU4AI4Bpx?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 8, 2020",
        "timestamp": "2020-04-08T22:09:44.000Z",
        "id": "1248010164084469763",
        "text": "IT’S NOT OVER #WriteInBernieSanders",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EVHSoN5VAAAw_2F?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 4, 2020",
        "timestamp": "2020-04-05T01:46:57.000Z",
        "id": "1246615275564183552",
        "text": "Most people think rape is sexy.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 4, 2020",
        "timestamp": "2020-04-05T01:41:56.000Z",
        "id": "1246614013389058048",
        "text": "Shiba inu gave me COVID-19",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EUzc1eIU8AA2I6g?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 4, 2020",
        "timestamp": "2020-04-04T21:08:26.000Z",
        "id": "1246545186416541696",
        "text": "Get woke bro",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EUyePKMUcAEtwMw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 31, 2020",
        "timestamp": "2020-03-31T21:28:49.000Z",
        "id": "1245100764160086016",
        "text": "I may be forced to punish my entire family while smoking tons and tons and tons and tons and tons of meth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EUd8ivBUEAAhijy?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Mar 31, 2020",
        "timestamp": "2020-03-31T08:27:09.000Z",
        "id": "1244904050589446144",
        "text": "Yoga pose",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EUbJoiVUYAAAVDn?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 26, 2020",
        "timestamp": "2020-03-26T23:21:06.000Z",
        "id": "1243317082202103809",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EUEmSyvUMAA7vus?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 26, 2020",
        "timestamp": "2020-03-26T23:19:52.000Z",
        "id": "1243316769613217792",
        "text": "Herpes",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 7, 2019",
        "timestamp": "2019-12-07T14:53:23.000Z",
        "id": "1203326645794295809",
        "text": "It's a 2 Xanax + Red Bull morning",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 1, 2019",
        "timestamp": "2019-12-01T23:58:24.000Z",
        "id": "1201289474874806272",
        "text": "Shiba Inu unfinished art",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EKvWZshU8AA6Spw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 1, 2019",
        "timestamp": "2019-12-01T23:46:28.000Z",
        "id": "1201286472688521216",
        "text": "Checkmate liberals",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/EKvTrGiUEAQ7brN?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Aug 17, 2019",
        "timestamp": "2019-08-18T00:09:29.000Z",
        "id": "1162879149905592320",
        "text": "shiba inu japan",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 19, 2019",
        "timestamp": "2019-05-19T21:21:57.000Z",
        "id": "1130222081512235010",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D69a_VXW4AEUi1U?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "May 3, 2019",
        "timestamp": "2019-05-03T20:07:19.000Z",
        "id": "1124405092164165632",
        "text": "Count the asians",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D5qwdg3UIAAXTBt?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 2, 2019",
        "timestamp": "2019-05-03T03:01:12.000Z",
        "id": "1124146863479779328",
        "text": "One thing many people don't realize before traveling to Asia is how many Asians live there",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D5nFmZ4U8AEKiqy?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 8, 2019",
        "timestamp": "2019-03-08T06:33:43.000Z",
        "id": "1103906624203157506",
        "text": "Do not carrot all",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D1HdOHjUwAAUHDw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Mar 4, 2019",
        "timestamp": "2019-03-05T01:19:05.000Z",
        "id": "1102740279205998595",
        "text": "All the cool kids are getting asbestos-flavored chlamydia",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D024bvsXcAA50GL?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Mar 4, 2019",
        "timestamp": "2019-03-05T00:54:52.000Z",
        "id": "1102734185733767168",
        "text": "Is punishment the right choice for your entire family?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D02y5HiW0AAQPwp?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Mar 2, 2019",
        "timestamp": "2019-03-03T01:47:36.000Z",
        "id": "1102022680428060672",
        "text": "shiba inu ocasio cortez",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D0sryMTX0AIZDPX?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Mar 2, 2019",
        "timestamp": "2019-03-03T01:38:25.000Z",
        "id": "1102020370004103168",
        "text": "shiba inus love punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/D0sprH2WkAACLgj?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Mar 1, 2019",
        "timestamp": "2019-03-02T04:32:13.000Z",
        "id": "1101701722290954241",
        "text": "Face the punishment. Legalize meth.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 24, 2018",
        "timestamp": "2018-12-24T19:12:31.000Z",
        "id": "1077280880140472327",
        "text": "t'was the asian before asian\nand all through the asian\nnot an asian was asian\nnot even an asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DvNFP5HUUAAl9at?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 22, 2018",
        "timestamp": "2018-12-23T04:36:49.000Z",
        "id": "1076698115095314432",
        "text": "Drugs calendar",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DvEzOQGWkAAGtJV?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Nov 18, 2018",
        "timestamp": "2018-11-19T00:20:12.000Z",
        "id": "1064312347714568192",
        "text": "Cleanu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DsUycA9WsAAgwi0?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 9, 2018",
        "timestamp": "2018-11-09T23:16:17.000Z",
        "id": "1061034774532444160",
        "text": "fuck this pinecone",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DrmNfZpU8AAl7GC?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "7"
    },
    {
        "prettyDate": "Nov 2, 2018",
        "timestamp": "2018-11-03T01:36:10.000Z",
        "id": "1058533259561365504",
        "text": "Shiba Inu dreaming about chasing rabbits",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DrCqYL0U4AA7MwY?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "8"
    },
    {
        "prettyDate": "Oct 19, 2018",
        "timestamp": "2018-10-19T13:30:12.000Z",
        "id": "1053277136663003139",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Dp399qMX4AItDJG?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Oct 18, 2018",
        "timestamp": "2018-10-19T01:23:32.000Z",
        "id": "1053094261355397120",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Dp1Xo7eX0AAJa1J?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 3, 2018",
        "timestamp": "2018-10-03T14:30:11.000Z",
        "id": "1047494022858772480",
        "text": "make america great again",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DolyLGJU4AAe8Gv?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Jul 31, 2018",
        "timestamp": "2018-08-01T02:00:15.000Z",
        "id": "1024474861236879360",
        "text": "> Doctor, I can't stop drinking what should I do\n\n> Have you tried punishing your entire family?\n\n> No\n\n> Ok, try punishing your entire family.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DjeqcpnWsAAdHWB?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "5"
    },
    {
        "prettyDate": "Jun 17, 2018",
        "timestamp": "2018-06-17T19:08:16.000Z",
        "id": "1008426118305734656",
        "text": "Shiba Inu gardener",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Df6mMv_W4AAIPdG?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "2"
    },
    {
        "prettyDate": "May 11, 2018",
        "timestamp": "2018-05-12T01:43:34.000Z",
        "id": "995117248124739584",
        "text": "shiba inu .exe has stopped",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Dc9d2ARWsAAq0pV?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "9"
    },
    {
        "prettyDate": "May 7, 2018",
        "timestamp": "2018-05-08T01:39:32.000Z",
        "id": "993666681166876673",
        "text": "Shiba Inu called me an Obama-loving libtard",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Dco2fnfXUAAdkik?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "May 7, 2018",
        "timestamp": "2018-05-08T00:44:59.000Z",
        "id": "993652952933036032",
        "text": "I was going to watch hentai but I felt like it would be cheating on my waifu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 6, 2018",
        "timestamp": "2018-05-06T04:10:36.000Z",
        "id": "992979921818570753",
        "text": "My Asian dog is Asian (is Asian), My Asian dog is a dog (is a dog)",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 25, 2018",
        "timestamp": "2018-04-26T02:02:47.000Z",
        "id": "989323875342323712",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/989323803070222337/pu/img/qjiuNRPu8m76GWZt.jpg"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 25, 2018",
        "timestamp": "2018-04-26T01:37:43.000Z",
        "id": "989317566630940672",
        "text": "Shiba Inu muscle car",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DbrDE2aWkAAVMGJ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 2, 2018",
        "timestamp": "2018-04-03T02:41:12.000Z",
        "id": "980998623839342593",
        "text": "Which one is the real shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DZ01CHQWAAAmHAz?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "7"
    },
    {
        "prettyDate": "Mar 15, 2018",
        "timestamp": "2018-03-16T00:29:19.000Z",
        "id": "974442452982599681",
        "text": "Hentai hentai hentai",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/974442388595896321/pu/img/_ADmOt0nTUgELEgx.jpg"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "5"
    },
    {
        "prettyDate": "Feb 27, 2018",
        "timestamp": "2018-02-28T01:08:34.000Z",
        "id": "968654123368263680",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DXFZyQ2U0AAymHt?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 20, 2018",
        "timestamp": "2018-02-20T05:02:20.000Z",
        "id": "965813850711961600",
        "text": "Wireless charging inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DWdCknkXUAA4sug?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 17, 2018",
        "timestamp": "2018-02-17T05:57:55.000Z",
        "id": "964740675274240001",
        "text": "Shiba Inu subreddit",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DWNyheNW4AAlyFP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Feb 14, 2018",
        "timestamp": "2018-02-14T16:26:45.000Z",
        "id": "963811762016571393",
        "text": "How to be annoying step 1",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DWAlrmZXcAEtFCM?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 7, 2018",
        "timestamp": "2018-02-08T02:05:51.000Z",
        "id": "961420783493955585",
        "text": "Forgive me father, for I am a gigantic slut",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 28, 2018",
        "timestamp": "2018-01-28T23:19:31.000Z",
        "id": "957755044862025728",
        "text": "Super comfy",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DUqhIMyXcAAY1CP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 28, 2018",
        "timestamp": "2018-01-28T20:59:36.000Z",
        "id": "957719832203538434",
        "text": "The ibu pator",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DUqBGDDXkAEq7Up?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Jan 26, 2018",
        "timestamp": "2018-01-26T17:39:17.000Z",
        "id": "956944646504099840",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DUfAE89XcAARui1?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 26, 2018",
        "timestamp": "2018-01-26T06:30:42.000Z",
        "id": "956776392032702464",
        "text": "How to be cold, starring shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DUcnDeKX4AAs1q6?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "12"
    },
    {
        "prettyDate": "Jan 26, 2018",
        "timestamp": "2018-01-26T05:34:54.000Z",
        "id": "956762349712564224",
        "text": "I am Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DUcaQjPU8AAQrsz?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 22, 2018",
        "timestamp": "2018-01-23T00:50:49.000Z",
        "id": "955603692614508545",
        "text": "My Asian dog is Asian is Asian.\nMy Asian dog is a dog is a dog.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DUL8e5KX0AEujuN?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Jan 15, 2018",
        "timestamp": "2018-01-16T01:13:09.000Z",
        "id": "953072596987334661",
        "text": "Republic of Gamers inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DTn-dbUW0AUuu8g?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Jan 15, 2018",
        "timestamp": "2018-01-16T01:11:13.000Z",
        "id": "953072112134221824",
        "text": "Fun fact: The CIA killed MLK Jr., Bill Clinton is a rapist, http://infowars.com #MLKJrDay",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DTn-BG5W0AUn2h3?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 4, 2018",
        "timestamp": "2018-01-05T04:35:32.000Z",
        "id": "949137262788296704",
        "text": "Why are shiba inus so funny",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DSwDTcUV4AAenPM?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "6"
    },
    {
        "prettyDate": "Jan 4, 2018",
        "timestamp": "2018-01-05T04:32:06.000Z",
        "id": "949136400397451264",
        "text": "Oh fuck oh shit oh fuck",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DSwChF5U8AAAuro?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 4, 2018",
        "timestamp": "2018-01-04T18:52:23.000Z",
        "id": "948990509946720257",
        "text": "My Asian dog is also Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "ok",
                    "percent": 45.5
                },
                {
                    "option": "shiba inu",
                    "percent": 54.5,
                    "winner": true
                }
            ],
            "stats": "11 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 3, 2018",
        "timestamp": "2018-01-04T03:17:07.000Z",
        "id": "948755142685417473",
        "text": "The violent, crazy inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DSqnxIkW4AAntGb?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "12"
    },
    {
        "prettyDate": "Dec 29, 2017",
        "timestamp": "2017-12-29T21:15:33.000Z",
        "id": "946852210998202368",
        "text": "PSA: don't share your bitcoin private key, if someone asks for that, it might be a scam.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 23, 2017",
        "timestamp": "2017-12-24T04:54:34.000Z",
        "id": "944793400322220032",
        "text": "The time for punishment has now arrived.\nThe time for punishment is here.\nThe time for punishment is here and now.\nIt will not be next year.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DRyUk7aX0AAYsOZ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "8"
    },
    {
        "prettyDate": "Dec 20, 2017",
        "timestamp": "2017-12-21T01:27:24.000Z",
        "id": "943654100356489216",
        "text": "on a scale of 1 to 10, how Asian would you rate shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "1",
                    "percent": 0
                },
                {
                    "option": "2",
                    "percent": 0
                },
                {
                    "option": "3",
                    "percent": 7.1
                },
                {
                    "option": "Asian",
                    "percent": 92.9,
                    "winner": true
                }
            ],
            "stats": "14 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 19, 2017",
        "timestamp": "2017-12-19T21:55:40.000Z",
        "id": "943238428333936640",
        "text": "Yea use my chair caster as a pillow, what could go wrong",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DRcOVzeX0AY7kmQ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 14, 2017",
        "timestamp": "2017-12-14T05:33:34.000Z",
        "id": "941179334563368960",
        "text": "Shiba Inu fact",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/941179266317914112/pu/img/-E48xomIbWlnvHlU.jpg"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 28, 2017",
        "timestamp": "2017-10-29T00:00:52.000Z",
        "id": "924425765445341184",
        "text": "The time for punishment has arrived \nThe time for punishment is here\nThe time for punishment has arrived\nIt will not be next year",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DNQ4U-jXkAAck6D?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Oct 27, 2017",
        "timestamp": "2017-10-27T15:09:24.000Z",
        "id": "923929629928443905",
        "text": "life begins at conception :)",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Oct 27, 2017",
        "timestamp": "2017-10-27T04:06:23.000Z",
        "id": "923762775456751616",
        "text": "strašidelný inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DNHdV1HWsAAV9y0?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 27, 2017",
        "timestamp": "2017-10-27T04:04:45.000Z",
        "id": "923762365899849728",
        "text": "spooky inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DNHc9zSXkAAZ5TB?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 25, 2017",
        "timestamp": "2017-10-25T21:30:39.000Z",
        "id": "923300799299567616",
        "text": "wait it's not fucking thursday. god damn it",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Oct 25, 2017",
        "timestamp": "2017-10-25T21:30:10.000Z",
        "id": "923300678373482496",
        "text": "#tbt",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DNA5DpUXUAAlx1D?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Oct 17, 2017",
        "timestamp": "2017-10-18T00:41:29.000Z",
        "id": "920449720908238850",
        "text": "Why are there so many facts about inus, and what's on the other side?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Oct 13, 2017",
        "timestamp": "2017-10-14T03:44:05.000Z",
        "id": "919046121502146560",
        "text": "Inu gives zero fucks",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DMEbkN2UMAAV2xM?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Oct 12, 2017",
        "timestamp": "2017-10-12T05:19:18.000Z",
        "id": "918345310107328513",
        "text": "Sometimes when you’re feeling down, you just need a pick-me-up. Sometimes when you’re feeling down, you just need some meth!",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DL6eLh2X4AA7PAT?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "8",
        "likes": "22"
    },
    {
        "prettyDate": "Oct 6, 2017",
        "timestamp": "2017-10-06T04:20:03.000Z",
        "id": "916156072401960961",
        "text": "Should I take my niece’s college money so I can buy graphics cards?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Oct 6, 2017",
        "timestamp": "2017-10-06T04:07:50.000Z",
        "id": "916152996681146369",
        "text": "Sweatshirt warm activate",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DLbUR_-W0AA0rCj?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "6"
    },
    {
        "prettyDate": "Oct 4, 2017",
        "timestamp": "2017-10-05T00:47:31.000Z",
        "id": "915740198461460480",
        "text": "Why does @apple support white supremacy?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DLVc2XnU8AEkwkw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Oct 4, 2017",
        "timestamp": "2017-10-05T00:46:31.000Z",
        "id": "915739947675594752",
        "text": "My review of iOS 11: bad operation system, F-",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Oct 4, 2017",
        "timestamp": "2017-10-04T21:06:32.000Z",
        "id": "915684587006767104",
        "text": "High definition inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DLUqRbrUIAA8UHp?format=jpg&name=large"
        ],
        "replies": "2",
        "retweets": "3",
        "likes": "13"
    },
    {
        "prettyDate": "Oct 4, 2017",
        "timestamp": "2017-10-04T12:46:26.000Z",
        "id": "915558730531573760",
        "text": "Prostitutes",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DLS3ytzVoAEvh8t?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 3, 2017",
        "timestamp": "2017-10-04T03:13:20.000Z",
        "id": "915414505554882560",
        "text": "An inu.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DLQ0kB3XcAALFuO?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "4"
    },
    {
        "prettyDate": "Sep 27, 2017",
        "timestamp": "2017-09-28T01:47:55.000Z",
        "id": "913218684004573185",
        "text": "How to be Asian? Starring a shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DKxni2PUIAAYJFA?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Sep 26, 2017",
        "timestamp": "2017-09-27T03:56:27.000Z",
        "id": "912888642577846274",
        "text": "Someone give me a fucking blue checkmark already. 100% very true verified shiba inu facts. Like all Asians are good at math @TwitterSupport",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DKs7X42VwAA7XdC?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Sep 26, 2017",
        "timestamp": "2017-09-27T03:48:32.000Z",
        "id": "912886649197154304",
        "text": "Is Donald Trump exactly like Hitler?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Yes",
                    "percent": 33.3
                },
                {
                    "option": "Yes",
                    "percent": 8.3
                },
                {
                    "option": "Yes",
                    "percent": 33.3,
                    "winner": true
                },
                {
                    "option": "Yes",
                    "percent": 25
                }
            ],
            "stats": "12 votes"
        },
        "replies": "1",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Sep 25, 2017",
        "timestamp": "2017-09-25T04:56:47.000Z",
        "id": "912179050403319808",
        "text": "What to do if you find out you are Asian, step 1: don't panic! Many people are Asian and live completely normal lives",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DKi2ALnVYAAVxMG?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "3",
        "likes": "10"
    },
    {
        "prettyDate": "Sep 21, 2017",
        "timestamp": "2017-09-21T04:34:15.000Z",
        "id": "910723826236960768",
        "text": "Solution found! Solution found! Solution found! I'm a lucky boy.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DKOKfCkWAAEZ1XA?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Sep 19, 2017",
        "timestamp": "2017-09-19T23:04:43.000Z",
        "id": "910278509628723201",
        "text": "I'm never fucking using you again @DwarfPool",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Sep 17, 2017",
        "timestamp": "2017-09-18T02:59:38.000Z",
        "id": "909612851999932416",
        "text": "If I ever have grandchildren, I'm giving them all AIDS",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Sep 17, 2017",
        "timestamp": "2017-09-17T22:51:39.000Z",
        "id": "909550444808146944",
        "text": "A good dog",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DJ9fS8HVwAAPD7x?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Sep 16, 2017",
        "timestamp": "2017-09-16T04:39:16.000Z",
        "id": "908913151919476736",
        "text": "How to know if you are Asian and what to do if you find out you are Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Sep 15, 2017",
        "timestamp": "2017-09-16T03:57:49.000Z",
        "id": "908902720446312448",
        "text": "Drop out of school, become an alcoholic instead.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DJ0SMu7U8AAOjOx?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Sep 15, 2017",
        "timestamp": "2017-09-16T00:43:55.000Z",
        "id": "908853924576337920",
        "text": "Abortion is murder 🙂",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DJzl0UaVwAAGtnJ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Sep 15, 2017",
        "timestamp": "2017-09-16T00:38:44.000Z",
        "id": "908852616570048513",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DJzkoPmUMAIjng_?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Sep 13, 2017",
        "timestamp": "2017-09-13T18:20:56.000Z",
        "id": "908032766838788096",
        "text": "Introducing the Apple Galaxy S8",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DJn68siUMAAB8sN?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Sep 11, 2017",
        "timestamp": "2017-09-12T00:35:59.000Z",
        "id": "907402374561648640",
        "text": "How to not be Gandalf",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DJe9pnEX0AEkC7W?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Sep 10, 2017",
        "timestamp": "2017-09-10T17:44:33.000Z",
        "id": "906936447252529153",
        "text": "How to pubes?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Bush",
                    "percent": 10
                },
                {
                    "option": "Trimmed",
                    "percent": 70,
                    "winner": true
                },
                {
                    "option": "Shaved",
                    "percent": 20
                }
            ],
            "stats": "10 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Sep 5, 2017",
        "timestamp": "2017-09-05T21:38:53.000Z",
        "id": "905183480379781121",
        "text": "Using Windows 10 is so triggering for me. I went on WebMD and self-diagnosed with PTSD. How could Microsoft make such a bigoted OS? #resist",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Sep 5, 2017",
        "timestamp": "2017-09-05T04:15:18.000Z",
        "id": "904920851363004416",
        "text": "Shiba inu mining for doge coins",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DI7ss8zXoAAOfhY?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "3"
    },
    {
        "prettyDate": "Sep 4, 2017",
        "timestamp": "2017-09-04T18:07:35.000Z",
        "id": "904767916939374592",
        "text": "I sprayed shiba inu in the face with the hose after he rolled in raccoon shit. Then he tore around the house shaking water everywhere whoops",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Sep 1, 2017",
        "timestamp": "2017-09-02T01:04:17.000Z",
        "id": "903785618035048448",
        "text": "Asked Siri if I'm the world's gayest butt slut (shiba inu says yes)",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DIrkOI5W0AEnojA?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "4"
    },
    {
        "prettyDate": "Sep 1, 2017",
        "timestamp": "2017-09-02T00:58:48.000Z",
        "id": "903784238071328768",
        "text": "shibespreading",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DIri9daW0AEh8YX?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "Aug 31, 2017",
        "timestamp": "2017-09-01T02:24:39.000Z",
        "id": "903443454801432576",
        "text": "I didn't want to have to be forced to punish my entire family, while smoking meth, but you've left me with no choice.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DImtBudW4AEm790?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Aug 27, 2017",
        "timestamp": "2017-08-27T15:58:37.000Z",
        "id": "901836358272024577",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DIP3YGFVAAEPth4?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Aug 26, 2017",
        "timestamp": "2017-08-26T19:46:35.000Z",
        "id": "901531337500360704",
        "text": "Pls let me out",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DILh8l9XgAAp-Gz?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "1",
        "likes": "7"
    },
    {
        "prettyDate": "Aug 26, 2017",
        "timestamp": "2017-08-26T12:48:09.000Z",
        "id": "901426036818235392",
        "text": "You're the good boy",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DIKCL3oV0AARsKC?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "34"
    },
    {
        "prettyDate": "Aug 25, 2017",
        "timestamp": "2017-08-25T21:42:37.000Z",
        "id": "901198152971190273",
        "text": "Do not misgender my shiba inu you bigot",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "6"
    },
    {
        "prettyDate": "Aug 24, 2017",
        "timestamp": "2017-08-24T23:54:44.000Z",
        "id": "900869013907099649",
        "text": "Pepper balls",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DICHllPU0AAAOEY?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 24, 2017",
        "timestamp": "2017-08-24T23:54:24.000Z",
        "id": "900868928817246209",
        "text": "Shiba inu vs. wall",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/900868819446415361/pu/img/9vzppWM-rNlt8Eip.jpg"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 23, 2017",
        "timestamp": "2017-08-23T20:31:38.000Z",
        "id": "900455513661612032",
        "text": "You must punish your friends and teacher #AdviceForBackToSchool",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DH8PgJpW0AALhkJ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "8"
    },
    {
        "prettyDate": "Aug 20, 2017",
        "timestamp": "2017-08-20T04:26:15.000Z",
        "id": "899125401699614720",
        "text": "In life there are only four types of facts: true facts, fake facts, hate facts and shiba inu facts",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DHpVxvIW0AAziYe?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "4"
    },
    {
        "prettyDate": "Aug 19, 2017",
        "timestamp": "2017-08-20T02:53:07.000Z",
        "id": "899101963681505281",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DHpAc9cXkAACpxv?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Aug 19, 2017",
        "timestamp": "2017-08-20T02:48:41.000Z",
        "id": "899100848265080832",
        "text": "Find the Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DHo_cWoW0AAFKv7?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Aug 19, 2017",
        "timestamp": "2017-08-20T02:46:39.000Z",
        "id": "899100338971693057",
        "text": "How to be a shiba inu fact",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DHo-8ZxXoAAz0yd?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "1",
        "likes": "6"
    },
    {
        "prettyDate": "Jul 10, 2017",
        "timestamp": "2017-07-10T06:59:27.000Z",
        "id": "884306055731204096",
        "text": "Sweet dreams, inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DEWvqp8UIAAbEMk?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jul 8, 2017",
        "timestamp": "2017-07-08T20:01:37.000Z",
        "id": "883778115079720960",
        "text": "fuck your rules",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DEPPgLcXcAAHJyX?format=jpg&name=large"
        ],
        "replies": "2",
        "retweets": "75",
        "likes": "301"
    },
    {
        "prettyDate": "Jun 29, 2017",
        "timestamp": "2017-06-29T05:01:46.000Z",
        "id": "880290172264488960",
        "text": "I'll have to be forced to punish you,.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 25, 2017",
        "timestamp": "2017-06-26T01:25:24.000Z",
        "id": "879148555432230914",
        "text": "Shiba inu pajamas",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DDNc8VsXUAAhAeh?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Jun 23, 2017",
        "timestamp": "2017-06-23T04:22:10.000Z",
        "id": "878105877273657345",
        "text": "Get more drugs.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DC-on3uXgAAbDM8?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 20, 2017",
        "timestamp": "2017-06-20T09:16:18.000Z",
        "id": "877092736003952640",
        "text": "The time for being an inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DCwPMCTXsAEjqKW?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Jun 19, 2017",
        "timestamp": "2017-06-19T23:20:12.000Z",
        "id": "876942722774560769",
        "text": "How to know if your shiba inu is running a drug empire",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DCuGv0rWsAAt_9h?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "7"
    },
    {
        "prettyDate": "Jun 16, 2017",
        "timestamp": "2017-06-17T00:00:05.000Z",
        "id": "875865597736873985",
        "text": "Visiting German Shepard loves punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DCezEg1U0AEMjtB?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 6, 2017",
        "timestamp": "2017-06-06T06:09:33.000Z",
        "id": "871972309116104704",
        "text": "Shiba inu bedtime",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DBneL6mXUAIFgv4?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jun 4, 2017",
        "timestamp": "2017-06-05T01:54:17.000Z",
        "id": "871545680271216640",
        "text": "Fuck.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_normal.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jun 3, 2017",
        "timestamp": "2017-06-04T01:06:27.000Z",
        "id": "871171253817798656",
        "text": "Islam is peaceful so dont say anything mean about them or they might suicide bomb a concert full of kids or drive a van into u #LondonBridge",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DBcFnjpXYAE6LZW?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 3, 2017",
        "timestamp": "2017-06-03T23:48:36.000Z",
        "id": "871151661879894017",
        "text": "You will inevitably die, but your choice of motor vehicle will determine how worthwhile your life was",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Ford Edge",
                    "percent": 0
                },
                {
                    "option": "Chevy Equinox",
                    "percent": 0
                },
                {
                    "option": "Honda CRV",
                    "percent": 60,
                    "winner": true
                },
                {
                    "option": "Dodge Challenger SRT",
                    "percent": 40
                }
            ],
            "stats": "5 votes"
        },
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "May 29, 2017",
        "timestamp": "2017-05-30T01:09:55.000Z",
        "id": "869360188154892289",
        "text": "How to know if I am an alcoholic for",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 28, 2017",
        "timestamp": "2017-05-29T03:15:02.000Z",
        "id": "869029285838848000",
        "text": "Shiba inu eating a cucumber",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/869029108642172928/pu/img/KpEexA6ElshIvgSR.jpg"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "May 28, 2017",
        "timestamp": "2017-05-28T23:37:03.000Z",
        "id": "868974430827020292",
        "text": "Friends of shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DA83oP_XsAARWmB?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 26, 2017",
        "timestamp": "2017-05-27T00:29:45.000Z",
        "id": "868262916126117888",
        "text": "Learn about punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DAywgr2XUAEzsi9?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "May 25, 2017",
        "timestamp": "2017-05-26T01:48:36.000Z",
        "id": "867920372653584384",
        "text": "Game dev: \"Hey boss, look how fast my game starts up!\"\nBoss: \"Put 10 fucking loud intro videos in.\"\nDev: \"wat\"\nBoss: \"DO IT OR YOU'RE FIRED\"",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "May 23, 2017",
        "timestamp": "2017-05-24T00:13:25.000Z",
        "id": "867171642669137920",
        "text": "Fantastic inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DAjP_6UW0AAOp01?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "3"
    },
    {
        "prettyDate": "May 23, 2017",
        "timestamp": "2017-05-23T22:26:00.000Z",
        "id": "867144609931169793",
        "text": "Charizard tentacle impregnation hentai",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "May 22, 2017",
        "timestamp": "2017-05-23T03:34:52.000Z",
        "id": "866859951121203201",
        "text": "Shibespreading",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/DAe0gspXcAALwpL?format=jpg&name=large",
            "https://pbs.twimg.com/media/DAe0gsqXoAE9hBw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "5"
    },
    {
        "prettyDate": "May 9, 2017",
        "timestamp": "2017-05-09T20:26:10.000Z",
        "id": "862041021181513728",
        "text": "There once was a dog named Ibu\nHe had a very large tail\nHe went for my snack when I turned my back\nBut his efforts were doomed to fail",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C_aVVw2XsAM8rUx?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "7"
    },
    {
        "prettyDate": "May 6, 2017",
        "timestamp": "2017-05-06T23:44:41.000Z",
        "id": "861003819873972224",
        "text": "First rule of fire safety: do not drink from the fire hose",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C_LmZTtXcAAPGLz?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "May 6, 2017",
        "timestamp": "2017-05-06T20:36:54.000Z",
        "id": "860956559500877824",
        "text": "found the stick",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C_K7bWdWsAASheZ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "May 6, 2017",
        "timestamp": "2017-05-06T13:48:14.000Z",
        "id": "860853717142306816",
        "text": "If another Trump supporter makes derogatory comments about my shiba inu I will call the FBI",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "May 6, 2017",
        "timestamp": "2017-05-06T05:04:01.000Z",
        "id": "860721793178456064",
        "text": "Is Macron a cocaine addict? #MacronLeaks",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C_Hl5qVWsAALnsW?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "2",
        "likes": "6"
    },
    {
        "prettyDate": "May 6, 2017",
        "timestamp": "2017-05-06T04:00:59.000Z",
        "id": "860705928554958848",
        "text": "DO NOT FAT SHAME MY SHIBA INU. He chose his own gender.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "May 5, 2017",
        "timestamp": "2017-05-06T03:56:44.000Z",
        "id": "860704860039237632",
        "text": "i refuese to walk",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C_HWf2SXgAEq1xL?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "32",
        "likes": "54"
    },
    {
        "prettyDate": "May 4, 2017",
        "timestamp": "2017-05-05T00:19:36.000Z",
        "id": "860287828785344512",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C_BbNmKXoAMCJO4?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "4",
        "likes": "8"
    },
    {
        "prettyDate": "May 4, 2017",
        "timestamp": "2017-05-05T00:19:15.000Z",
        "id": "860287741296345089",
        "text": "Is Donald Trump JUST LIKE HITLER",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Yes",
                    "percent": 55.6,
                    "winner": true
                },
                {
                    "option": "Yes",
                    "percent": 44.4
                }
            ],
            "stats": "9 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 22, 2017",
        "timestamp": "2017-04-22T19:50:27.000Z",
        "id": "855871439525863424",
        "text": "Ask Siri where can I get AIDS",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C-CqiE6XYAUOZwG?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Apr 21, 2017",
        "timestamp": "2017-04-21T12:19:38.000Z",
        "id": "855395603056123905",
        "text": "Morning inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C975wecXoAALNNF?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "1",
        "likes": "4"
    },
    {
        "prettyDate": "Apr 20, 2017",
        "timestamp": "2017-04-21T01:10:19.000Z",
        "id": "855227164345737220",
        "text": "TRUMP IS LITERALLY HITLER IF ENOUGH PEOPLE RETWEET THIS BERNIE CAN STILL WIN",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C95gj6dW0AAL0jK?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "5",
        "likes": "10"
    },
    {
        "prettyDate": "Apr 19, 2017",
        "timestamp": "2017-04-19T04:38:32.000Z",
        "id": "854554783910432769",
        "text": "Shiba inu took over my bed, won't budge.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C9v9CasXoAAkU_d?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Apr 17, 2017",
        "timestamp": "2017-04-17T06:05:48.000Z",
        "id": "853851973073997824",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C9l91e4XsAAohj1?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 15, 2017",
        "timestamp": "2017-04-15T06:10:11.000Z",
        "id": "853128299589095424",
        "text": "Drugs",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C9brpRIV0AAtTJR?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "6"
    },
    {
        "prettyDate": "Apr 14, 2017",
        "timestamp": "2017-04-14T04:45:49.000Z",
        "id": "852744680219463681",
        "text": "Shiba inus love crates",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C9WOowwUIAYmlX5?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 10, 2017",
        "timestamp": "2017-04-10T05:57:00.000Z",
        "id": "851313041102831616",
        "text": "prostitutes prostitutes",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 10, 2017",
        "timestamp": "2017-04-10T05:43:03.000Z",
        "id": "851309532177457152",
        "text": "Make meth legal and affordable for children #legalizemeth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 10, 2017",
        "timestamp": "2017-04-10T05:08:29.000Z",
        "id": "851300832347901953",
        "text": "Sleep inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C9Btlm7WAAA9L8V?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "6"
    },
    {
        "prettyDate": "Apr 9, 2017",
        "timestamp": "2017-04-09T22:06:40.000Z",
        "id": "851194680268206080",
        "text": "ok good idea",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C9ANCt2XoAEIh6u?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Apr 7, 2017",
        "timestamp": "2017-04-08T02:13:28.000Z",
        "id": "850532011399950336",
        "text": "How to get rid of herpes outbreak fast",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 6, 2017",
        "timestamp": "2017-04-07T00:24:29.000Z",
        "id": "850142198301683714",
        "text": "OK Google how to be a butt slut",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Mar 31, 2017",
        "timestamp": "2017-04-01T02:02:36.000Z",
        "id": "847992563743293440",
        "text": "Fun fact: shiba inus sleep all day and all night. Unless there's a cat",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C8SsvebW0Ag374o?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Mar 28, 2017",
        "timestamp": "2017-03-28T04:46:04.000Z",
        "id": "846584149242757125",
        "text": "How to be a shiba inu step 1",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C7-rxxcVMAESfTG?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "6"
    },
    {
        "prettyDate": "Mar 20, 2017",
        "timestamp": "2017-03-20T04:17:41.000Z",
        "id": "843677901149102080",
        "text": "Protip from a real shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C7VYk56U8AMXFC_?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Mar 19, 2017",
        "timestamp": "2017-03-20T02:37:38.000Z",
        "id": "843652722771591170",
        "text": "Shiba inu loopholes. (\"Go to your crate\")",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C7VBrMiV4AAkz9K?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "10",
        "likes": "22"
    },
    {
        "prettyDate": "Mar 18, 2017",
        "timestamp": "2017-03-18T05:12:51.000Z",
        "id": "842967009302319104",
        "text": "Play time is over",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C7LSBbBV0AAfGBs?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "4",
        "likes": "3"
    },
    {
        "prettyDate": "Mar 18, 2017",
        "timestamp": "2017-03-18T04:17:37.000Z",
        "id": "842953109034995712",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 18, 2017",
        "timestamp": "2017-03-18T04:12:19.000Z",
        "id": "842951778471755776",
        "text": "I still don't understand why @NintendoAmerica would release such a sexist game. Wake up. It's 2016",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C7LEK0UVAAEdAnQ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 18, 2017",
        "timestamp": "2017-03-18T04:00:00.000Z",
        "id": "842948677765349376",
        "text": "In order to come back inside you must answer three riddles",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C7LBWUdVoAAVKnT?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "4"
    },
    {
        "prettyDate": "Mar 15, 2017",
        "timestamp": "2017-03-15T04:22:50.000Z",
        "id": "841867258452402176",
        "text": "You know who else paid taxes? Hitler.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C67pzgeXEAEanZ0?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "7"
    },
    {
        "prettyDate": "Mar 12, 2017",
        "timestamp": "2017-03-12T05:39:33.000Z",
        "id": "840799403933872128",
        "text": "Playing Zelda BotW. Link is sexist",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C6semOuU8AIOKc5?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Mar 12, 2017",
        "timestamp": "2017-03-12T05:34:55.000Z",
        "id": "840798235480469504",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C6sdiMZV0AA95J-?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "1",
        "likes": "8"
    },
    {
        "prettyDate": "Mar 7, 2017",
        "timestamp": "2017-03-08T00:41:51.000Z",
        "id": "839274932802633730",
        "text": "Bernie could still win this",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C6W0GOyVMAEn7G7?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "5"
    },
    {
        "prettyDate": "Mar 4, 2017",
        "timestamp": "2017-03-04T12:08:35.000Z",
        "id": "837998203693846528",
        "text": "I'll start worrying about how addicted I am to these pills after I take some more pills",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C6Eq6UKWgAA2n3L?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "6"
    },
    {
        "prettyDate": "Mar 4, 2017",
        "timestamp": "2017-03-04T06:20:34.000Z",
        "id": "837910623472222209",
        "text": "Smoking meth mixed with crack is not a crime, unless you're Hitler, in which case you should kill your entire family #protip",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C6DbQvMWAAAYGt7?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "24"
    },
    {
        "prettyDate": "Mar 3, 2017",
        "timestamp": "2017-03-04T04:07:40.000Z",
        "id": "837877176015941632",
        "text": "I now own http://shibainufacts.com what should I do with it",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Mar 3, 2017",
        "timestamp": "2017-03-04T03:59:11.000Z",
        "id": "837875040423137286",
        "text": "Darkness my old friend",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C6C63YOWQAAXE73?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "7"
    },
    {
        "prettyDate": "Feb 18, 2017",
        "timestamp": "2017-02-19T02:27:41.000Z",
        "id": "833140972200534016",
        "text": "punish this sprinkler",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C4_pSyuUYAIKUj9?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "41",
        "likes": "135"
    },
    {
        "prettyDate": "Feb 17, 2017",
        "timestamp": "2017-02-17T15:22:03.000Z",
        "id": "832611073958227968",
        "text": "Shiba inus won't even watch CNN #MediaLiesAgain",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C44HV_PUkAEXESE?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 16, 2017",
        "timestamp": "2017-02-16T07:57:01.000Z",
        "id": "832136688021405696",
        "text": "Why must I cry",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 15, 2017",
        "timestamp": "2017-02-15T07:25:57.000Z",
        "id": "831766480547086336",
        "text": "That is really how he sleeps",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C4sHMxeWMAE5Pcf?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "19",
        "likes": "67"
    },
    {
        "prettyDate": "Feb 14, 2017",
        "timestamp": "2017-02-15T01:57:04.000Z",
        "id": "831683714610561027",
        "text": "Won't somebody please punish me? #meth #crack",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C4q77NHVUAAxnVP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "3"
    },
    {
        "prettyDate": "Feb 11, 2017",
        "timestamp": "2017-02-11T21:18:45.000Z",
        "id": "830526512768155652",
        "text": "Glorious inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C4afc-VW8AA2iv-?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "7",
        "likes": "21"
    },
    {
        "prettyDate": "Feb 10, 2017",
        "timestamp": "2017-02-10T06:58:34.000Z",
        "id": "829947650384474112",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C4SQ-3YUEAA4jW9?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 8, 2017",
        "timestamp": "2017-02-09T01:09:03.000Z",
        "id": "829497305124446209",
        "text": "Keep your shiba inus close. They are planning to punish you.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C4L3YZpVMAEdEIP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "7",
        "likes": "19"
    },
    {
        "prettyDate": "Feb 3, 2017",
        "timestamp": "2017-02-03T06:29:24.000Z",
        "id": "827403596920532995",
        "text": "You don't get anything. You get nothing.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C3uHLgSUYAAW1t5?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Jan 24, 2017",
        "timestamp": "2017-01-25T02:44:04.000Z",
        "id": "824085398942908418",
        "text": "Muscle car inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C2-9S_IUsAAtyQ5?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "6",
        "likes": "18"
    },
    {
        "prettyDate": "Jan 20, 2017",
        "timestamp": "2017-01-21T04:21:35.000Z",
        "id": "822660390064689156",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C2qtPxFXgAAshnz?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 20, 2017",
        "timestamp": "2017-01-20T22:01:45.000Z",
        "id": "822564801742327808",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C2pWT9QWEAE-qEF?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "11"
    },
    {
        "prettyDate": "Jan 11, 2017",
        "timestamp": "2017-01-11T05:07:46.000Z",
        "id": "819048132717711360",
        "text": "I might be forced to punish my entire family",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C13X7ZhUQAAn4IZ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 7, 2017",
        "timestamp": "2017-01-08T03:33:42.000Z",
        "id": "817937293709340673",
        "text": "it's fun to smoke crack. while also smoking meth!",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 6, 2017",
        "timestamp": "2017-01-07T02:30:40.000Z",
        "id": "817559043350925316",
        "text": "Learn about brushed",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C1iNnGQUoAE0GXZ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "10",
        "likes": "19"
    },
    {
        "prettyDate": "Jan 5, 2017",
        "timestamp": "2017-01-05T06:37:45.000Z",
        "id": "816896450965676032",
        "text": "Get the ball.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/816894493047672832/pu/img/gsHV7FVXqKqBnAoh.jpg"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Jan 3, 2017",
        "timestamp": "2017-01-04T02:01:25.000Z",
        "id": "816464522168909825",
        "text": "invisible inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C1SqJLGVEAA8HGg?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "3"
    },
    {
        "prettyDate": "Jan 1, 2017",
        "timestamp": "2017-01-02T03:01:51.000Z",
        "id": "815754951565737984",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C1Iky2AXUAAVAYh?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 1, 2017",
        "timestamp": "2017-01-01T06:10:25.000Z",
        "id": "815440018017763328",
        "text": "Thank you for pre-warming my flannel",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C1EGXbGXgAA9enS?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Dec 28, 2016",
        "timestamp": "2016-12-29T04:29:26.000Z",
        "id": "814327443683151872",
        "text": "how to build a PC, with shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 28, 2016",
        "timestamp": "2016-12-29T04:23:36.000Z",
        "id": "814325976071110657",
        "text": "how to be an owl step 1",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/814325793094414337/pu/img/5bTCJCZKC_C7mNV_.jpg"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "5"
    },
    {
        "prettyDate": "Dec 28, 2016",
        "timestamp": "2016-12-28T22:59:09.000Z",
        "id": "814244324359761920",
        "text": "punishment mode activate",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C0zG3cQUQAArKyz?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Dec 26, 2016",
        "timestamp": "2016-12-27T04:49:54.000Z",
        "id": "813607819597512704",
        "text": "Might I recommend the punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C0qD_XRUcAAD_Ss?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "8"
    },
    {
        "prettyDate": "Dec 25, 2016",
        "timestamp": "2016-12-25T18:43:15.000Z",
        "id": "813092759506714624",
        "text": "What's the fuckin point of anything",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C0iviO_WEAE4a9M?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "4",
        "likes": "8"
    },
    {
        "prettyDate": "Dec 24, 2016",
        "timestamp": "2016-12-24T06:00:39.000Z",
        "id": "812538460825387009",
        "text": "Shiba inu ready to punish you",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C0a3Xd0WQAASXuk?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Dec 23, 2016",
        "timestamp": "2016-12-23T08:07:19.000Z",
        "id": "812207945861754880",
        "text": "What's the point of making your bed when I'm gonna fuck it up right afterwards",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C0WK0QbUUAA9ZxH?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "19"
    },
    {
        "prettyDate": "Dec 22, 2016",
        "timestamp": "2016-12-23T03:47:07.000Z",
        "id": "812142464253489152",
        "text": "When I wake up you're all punished",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C0VPQXoXAAEjvnn?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 21, 2016",
        "timestamp": "2016-12-21T23:04:54.000Z",
        "id": "811709056180219908",
        "text": "Building your own computer is a bad idea. You should buy an Alienware",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 19, 2016",
        "timestamp": "2016-12-20T01:22:47.000Z",
        "id": "811018978709696512",
        "text": "Find out if Trump really won",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/C0FRdUoUcAAv9w8?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Dec 18, 2016",
        "timestamp": "2016-12-18T21:18:31.000Z",
        "id": "810595120743665669",
        "text": "You must punish your teachers",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cz_P9x3VIAAM9ZM?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Dec 13, 2016",
        "timestamp": "2016-12-13T06:12:04.000Z",
        "id": "808555063883034624",
        "text": "The time has come",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CziQiN1WQAAkG1x?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 13, 2016",
        "timestamp": "2016-12-13T06:09:40.000Z",
        "id": "808554462260424704",
        "text": "Shiba inu must sleep",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CziP_LtXEAAHDNF?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 12, 2016",
        "timestamp": "2016-12-13T03:25:55.000Z",
        "id": "808513254125342720",
        "text": "an asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CzhqgOnVIAA9For?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 10, 2016",
        "timestamp": "2016-12-10T06:37:46.000Z",
        "id": "807474369018662912",
        "text": "HELLO, LADIES",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CzS5pLGVEAAs6Wf?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "8"
    },
    {
        "prettyDate": "Dec 8, 2016",
        "timestamp": "2016-12-08T23:39:50.000Z",
        "id": "807006807256350721",
        "text": "Learn about feet clean",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CzMQaHfVEAA7ez-?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "4"
    },
    {
        "prettyDate": "Dec 7, 2016",
        "timestamp": "2016-12-08T04:26:11.000Z",
        "id": "806716478103912448",
        "text": "Darkness fills my heart with hate",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "2",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 3, 2016",
        "timestamp": "2016-12-03T19:03:07.000Z",
        "id": "805125226967302144",
        "text": "Shiba inu carrot",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CyxhIC_XUAU5Q_v?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 1, 2016",
        "timestamp": "2016-12-02T01:22:40.000Z",
        "id": "804495967194071045",
        "text": "I just want everyone to be punished",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Dec 1, 2016",
        "timestamp": "2016-12-02T00:00:14.000Z",
        "id": "804475225773576196",
        "text": "Shiba inu called me a leftist beta cuck",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CyoR8eGWEAAb6cH?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "4"
    },
    {
        "prettyDate": "Nov 25, 2016",
        "timestamp": "2016-11-25T23:30:20.000Z",
        "id": "802293373851860992",
        "text": "Shiba inu emoji 🐕",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 24, 2016",
        "timestamp": "2016-11-24T05:07:21.000Z",
        "id": "801653409526988800",
        "text": "I wish upon a star to go back in time and vote for Donald Trump again",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CyALdKiUkAA8TIA?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 23, 2016",
        "timestamp": "2016-11-24T04:57:26.000Z",
        "id": "801650911881887744",
        "text": "Shiba inu bottle opener, recommended for extreme alcoholism",
        "avatar": "https://pbs.twimg.com/profile_images/605554158343700481/-C3qlE83_normal.jpg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Nov 23, 2016",
        "timestamp": "2016-11-23T11:05:40.000Z",
        "id": "801381193887469568",
        "text": "Shiba inu take my spot",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cx8T6XBXAAAWuPv?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "5"
    },
    {
        "prettyDate": "Nov 19, 2016",
        "timestamp": "2016-11-20T04:32:51.000Z",
        "id": "800195174806614016",
        "text": "Is your dog Asian? Find out",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CxrdQouUAAE9ZJm?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Nov 17, 2016",
        "timestamp": "2016-11-17T06:01:31.000Z",
        "id": "799130326677200897",
        "text": "Shit, I fucking hate everyone",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "2",
        "likes": "3"
    },
    {
        "prettyDate": "Nov 15, 2016",
        "timestamp": "2016-11-16T02:29:49.000Z",
        "id": "798714663403982848",
        "text": "I rate my shiba inu 10/10",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CxWav4LUUAAyaA9?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "2",
        "likes": "7"
    },
    {
        "prettyDate": "Nov 14, 2016",
        "timestamp": "2016-11-15T03:19:30.000Z",
        "id": "798364778532048896",
        "text": "#protip - if you don't want a bath, don't roll around in raccoon shit.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CxRchhBVQAAmM1J?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Nov 14, 2016",
        "timestamp": "2016-11-14T21:42:21.000Z",
        "id": "798279929712570368",
        "text": "important, and related.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/tweet_video_thumb/CxQPSqYUcAEPZfS.jpg"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Nov 14, 2016",
        "timestamp": "2016-11-14T21:37:12.000Z",
        "id": "798278636197543936",
        "text": "how to punish your entire family",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/tweet_video_thumb/CxQOI2bUcAAZd1Y.jpg"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "5"
    },
    {
        "prettyDate": "Nov 13, 2016",
        "timestamp": "2016-11-13T20:50:09.000Z",
        "id": "797904405299396608",
        "text": "Skyrim inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CxK50lPVQAAOp-A?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "5"
    },
    {
        "prettyDate": "Nov 9, 2016",
        "timestamp": "2016-11-09T07:42:39.000Z",
        "id": "796256672721305600",
        "text": "Shiba inus love smoking crack under a Trump presidency",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CwzfNnXUoAA_CKI?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "12",
        "likes": "19"
    },
    {
        "prettyDate": "Nov 6, 2016",
        "timestamp": "2016-11-07T02:05:28.000Z",
        "id": "795447044156444672",
        "text": "Shibutt",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cwn-3ICVIAEKqDY?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Oct 30, 2016",
        "timestamp": "2016-10-31T00:58:56.000Z",
        "id": "792893584450031616",
        "text": "ALL NUDE SHIBA INU PICS. ALL THE TIME. #TRUMP",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CwDsgPZXgAEPB5i?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Oct 23, 2016",
        "timestamp": "2016-10-23T20:12:03.000Z",
        "id": "790284673553432577",
        "text": "#Trump",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/790284595224649728/pu/img/Scu4D7ZIGqb3R4B1.jpg"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 19, 2016",
        "timestamp": "2016-10-19T05:35:05.000Z",
        "id": "788614424533827584",
        "text": "Who are you voting for in the presidential election",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Trump",
                    "percent": 33.3
                },
                {
                    "option": "Trump",
                    "percent": 16.7
                },
                {
                    "option": "Donald Trump",
                    "percent": 33.3,
                    "winner": true
                },
                {
                    "option": "#Trump2016",
                    "percent": 16.7
                }
            ],
            "stats": "6 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 9, 2016",
        "timestamp": "2016-10-09T18:12:31.000Z",
        "id": "785181162788495360",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CuWGFydWYAA-tPi?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Oct 7, 2016",
        "timestamp": "2016-10-07T06:16:16.000Z",
        "id": "784276134657609728",
        "text": "hentai hentai hentai",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Oct 4, 2016",
        "timestamp": "2016-10-05T03:46:11.000Z",
        "id": "783513591345516544",
        "text": "It's time to smoke crack, while punishing my entire family, while on drugs.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Oct 3, 2016",
        "timestamp": "2016-10-04T00:30:49.000Z",
        "id": "783102035478446080",
        "text": "Shiba inu [censored by Twitter]",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Sep 30, 2016",
        "timestamp": "2016-09-30T22:43:44.000Z",
        "id": "781987926020239360",
        "text": "Which Game of Thrones character",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Francis Underwood",
                    "percent": 0
                },
                {
                    "option": "Jean Luc Picard",
                    "percent": 0
                },
                {
                    "option": "Vin Diesel",
                    "percent": 0
                },
                {
                    "option": "Pepe the Frog",
                    "percent": 100,
                    "winner": true
                }
            ],
            "stats": "2 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Sep 27, 2016",
        "timestamp": "2016-09-27T12:07:38.000Z",
        "id": "780740682189185024",
        "text": "Shiba inu give me asperger syndrom",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CtW_gZgUIAIaAAb?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Sep 22, 2016",
        "timestamp": "2016-09-23T03:36:52.000Z",
        "id": "779162590698102784",
        "text": "Shiba inu flood",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CtAkN7qUIAIKeFf?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Sep 13, 2016",
        "timestamp": "2016-09-13T08:16:33.000Z",
        "id": "775609097634865156",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CsOEVsxVMAA8_oh?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "11"
    },
    {
        "prettyDate": "Sep 12, 2016",
        "timestamp": "2016-09-13T02:14:48.000Z",
        "id": "775518060346171392",
        "text": "What",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CsMxjlwVIAEIU0N?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Sep 12, 2016",
        "timestamp": "2016-09-13T01:04:43.000Z",
        "id": "775500420546453504",
        "text": "how many calories in chicken tendies",
        "avatar": "https://pbs.twimg.com/profile_images/1174646499172446208/uWcbhbCB_normal.jpg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Sep 12, 2016",
        "timestamp": "2016-09-12T22:33:55.000Z",
        "id": "775462473746948096",
        "text": "Shiba inu salad",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CsL-_7VVUAAch3d?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "5"
    },
    {
        "prettyDate": "Sep 8, 2016",
        "timestamp": "2016-09-08T05:06:36.000Z",
        "id": "773749356994318336",
        "text": "Vigilant ibu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Crzo7upUAAEdKtx?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "8"
    },
    {
        "prettyDate": "Sep 2, 2016",
        "timestamp": "2016-09-02T04:07:55.000Z",
        "id": "771560261303939072",
        "text": "SATAN INU RISE AGAIN",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CrUh9XkUEAAJaKZ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 31, 2016",
        "timestamp": "2016-08-31T22:02:21.000Z",
        "id": "771105874169307136",
        "text": "Shiba inus hate door-to-door solicitors",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "3",
        "likes": "1"
    },
    {
        "prettyDate": "Aug 30, 2016",
        "timestamp": "2016-08-30T16:21:32.000Z",
        "id": "770657715748085760",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CrHtFQ6XgAAv_R5?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "5"
    },
    {
        "prettyDate": "Aug 29, 2016",
        "timestamp": "2016-08-29T19:13:35.000Z",
        "id": "770338626630619136",
        "text": "Shiba inu canister",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CrDK5BpUAAA1lpw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Aug 29, 2016",
        "timestamp": "2016-08-29T16:52:24.000Z",
        "id": "770303096513671168",
        "text": "Top 10 sign your shiba inu is a drugs kingpin",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CrCqjZLUkAAvPix?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 28, 2016",
        "timestamp": "2016-08-29T03:20:00.000Z",
        "id": "770098648797552644",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cq_wm6IWEAEiFCX?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Aug 28, 2016",
        "timestamp": "2016-08-29T03:19:04.000Z",
        "id": "770098416890314752",
        "text": "Shiba inu caught me sharting into my own mouth again",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cq_wXDwWcAAtvSe?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Aug 25, 2016",
        "timestamp": "2016-08-25T05:29:33.000Z",
        "id": "768681699714539520",
        "text": "Snorting meth, waiting for the FedEx guy (to bark at)",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cqrn7NAUsAAtPLX?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "16"
    },
    {
        "prettyDate": "Aug 25, 2016",
        "timestamp": "2016-08-25T05:13:04.000Z",
        "id": "768677553766662145",
        "text": "I only follow back if you're @0kbps",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "7"
    },
    {
        "prettyDate": "Aug 25, 2016",
        "timestamp": "2016-08-25T05:09:06.000Z",
        "id": "768676553576189953",
        "text": "#tbt in my timezone",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CqrjPrBVMAIVQND?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 23, 2016",
        "timestamp": "2016-08-23T22:24:30.000Z",
        "id": "768212345868787712",
        "text": "One time my nose was bleeding, so I wrote \"HELP ME\" in blood on the bathroom mirror. The school called in a haz-mat team to clean it up.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 21, 2016",
        "timestamp": "2016-08-22T01:49:25.000Z",
        "id": "767539137758932992",
        "text": "Shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CqbYxTyUsAAfwAR?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Aug 20, 2016",
        "timestamp": "2016-08-20T06:02:14.000Z",
        "id": "766877986188464129",
        "text": "Should I punish my entire family",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "yes",
                    "percent": 83.3,
                    "winner": true
                },
                {
                    "option": "no",
                    "percent": 16.7
                }
            ],
            "stats": "6 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Aug 18, 2016",
        "timestamp": "2016-08-18T23:57:04.000Z",
        "id": "766423701826830336",
        "text": "Today shiba inu encountered two large grasshoppers engaged in sexual intercourse. He ate the one that couldn't hop away in time. Crunchy!",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 17, 2016",
        "timestamp": "2016-08-18T02:36:52.000Z",
        "id": "766101528625250305",
        "text": "I was recently diagnosed with chlamydia! #YOLO",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Aug 16, 2016",
        "timestamp": "2016-08-17T03:32:01.000Z",
        "id": "765753020176928774",
        "text": "How to know if your shiba inu is punished",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CqCATucUsAA5D_m?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Aug 13, 2016",
        "timestamp": "2016-08-13T18:30:10.000Z",
        "id": "764529496208793600",
        "text": "Shiba inu webcam #enhance",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CpwnhTrUEAAUmgp?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Aug 11, 2016",
        "timestamp": "2016-08-12T03:42:54.000Z",
        "id": "763943817196081153",
        "text": "lol",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CpoS13NUMAAej-l?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "1",
        "likes": "4"
    },
    {
        "prettyDate": "Aug 11, 2016",
        "timestamp": "2016-08-11T19:07:04.000Z",
        "id": "763814003583750144",
        "text": "You won't believe #5!",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cpmcx1ZUkAAVW3h?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "14"
    },
    {
        "prettyDate": "Aug 9, 2016",
        "timestamp": "2016-08-09T06:58:25.000Z",
        "id": "762905859919785984",
        "text": "Sleepy inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CpZi09rVMAAycc8?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "11",
        "likes": "24"
    },
    {
        "prettyDate": "Aug 7, 2016",
        "timestamp": "2016-08-08T03:16:24.000Z",
        "id": "762487600162615299",
        "text": "Punish!",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CpTmbLsUIAAhtbm?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Aug 6, 2016",
        "timestamp": "2016-08-07T03:02:42.000Z",
        "id": "762121761085272068",
        "text": "The time for punishment has arrived.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CpOZs6LUEAE6VWg?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Aug 3, 2016",
        "timestamp": "2016-08-03T04:03:20.000Z",
        "id": "760687470652559360",
        "text": "\"Yeah, I'm just going to shed all over your bed.\"",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Co6BMJ5WAAEhFv3?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "3"
    },
    {
        "prettyDate": "Aug 2, 2016",
        "timestamp": "2016-08-02T06:12:29.000Z",
        "id": "760357582930411520",
        "text": "Find the Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Co1VLx6UEAAaUw3?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Aug 1, 2016",
        "timestamp": "2016-08-01T22:47:55.000Z",
        "id": "760245703486517249",
        "text": "You know who else liked the Trans-Pacific Partnership? Hitler.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CozvbqXUAAAUDW-?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "3"
    },
    {
        "prettyDate": "Jul 29, 2016",
        "timestamp": "2016-07-29T05:11:14.000Z",
        "id": "758892619795615744",
        "text": "People who say smoking isn't cool aren't cool because smoking is cool.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CoggzkiUIAA9xWq?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jul 29, 2016",
        "timestamp": "2016-07-29T04:47:36.000Z",
        "id": "758886672721334272",
        "text": "Why do you do that stuff?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CogbZkaUkAEuypR?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "4"
    },
    {
        "prettyDate": "Jul 28, 2016",
        "timestamp": "2016-07-28T19:42:29.000Z",
        "id": "758749489804955655",
        "text": "Find out if your shiba inu has an eating disorder",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CoeeoWYUAAU4WA4?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "4",
        "likes": "11"
    },
    {
        "prettyDate": "Jul 24, 2016",
        "timestamp": "2016-07-24T05:46:00.000Z",
        "id": "757089427441070080",
        "text": "Horrific and excessive punishment await in your miserable future.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jul 23, 2016",
        "timestamp": "2016-07-23T17:30:59.000Z",
        "id": "756904456424488960",
        "text": "Top 10 reasons",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CoEQlahUEAAUY5u?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jul 23, 2016",
        "timestamp": "2016-07-23T17:12:58.000Z",
        "id": "756899920938881024",
        "text": "My Asian dog is Asian.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CoEMddCUsAAeNNQ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jul 21, 2016",
        "timestamp": "2016-07-21T05:32:52.000Z",
        "id": "755998959902928896",
        "text": "The sky is black. There's no way back. Nothing but mutants, looking for crack.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cn3ZCZpUMAADoN0?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "5"
    },
    {
        "prettyDate": "Jul 20, 2016",
        "timestamp": "2016-07-20T18:20:45.000Z",
        "id": "755829814578155520",
        "text": "Meth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cn0_NQ7UEAA4rus?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Jul 19, 2016",
        "timestamp": "2016-07-19T05:56:44.000Z",
        "id": "755280192109543424",
        "text": "If I get 100 followers I can finally smoke crack",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jul 13, 2016",
        "timestamp": "2016-07-13T05:38:38.000Z",
        "id": "753101309801357313",
        "text": "Crack.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CnONohbWIAAw-FW?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jul 13, 2016",
        "timestamp": "2016-07-13T05:11:16.000Z",
        "id": "753094420967329792",
        "text": "Thanks for being an inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CnOHYTVWYAIKMT5?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "9"
    },
    {
        "prettyDate": "Jul 10, 2016",
        "timestamp": "2016-07-10T04:47:37.000Z",
        "id": "752001304491233280",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cm-lMwQUkAA94fX?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jul 1, 2016",
        "timestamp": "2016-07-01T05:09:40.000Z",
        "id": "748745363184492544",
        "text": "I am miserable, but at least shiba inu is happy and taken care of",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jun 30, 2016",
        "timestamp": "2016-07-01T03:56:09.000Z",
        "id": "748726863992721408",
        "text": "Tonight I un-quit smoking",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CmQDG2zUEAAhDtY?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "6",
        "likes": "6"
    },
    {
        "prettyDate": "Jun 29, 2016",
        "timestamp": "2016-06-29T05:31:01.000Z",
        "id": "748025961363439616",
        "text": "Wake up, sheeple. #illuminati #illuminati",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CmGFo-lXIAE7L_X?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 28, 2016",
        "timestamp": "2016-06-28T04:31:55.000Z",
        "id": "747648700873314304",
        "text": "Find the Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CmAuhLtWAAAa0jy?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 25, 2016",
        "timestamp": "2016-06-25T23:19:58.000Z",
        "id": "746845418072354816",
        "text": "Shiba inu power outage",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cl1T8ILVYAE7EpS?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "6",
        "likes": "13"
    },
    {
        "prettyDate": "Jun 25, 2016",
        "timestamp": "2016-06-25T23:18:05.000Z",
        "id": "746844944334127104",
        "text": "hentai hentai hentai",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jun 24, 2016",
        "timestamp": "2016-06-25T03:30:14.000Z",
        "id": "746546014199263232",
        "text": "How many more people will have to be punished",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ClxDoz2UoAIwfmq?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "3"
    },
    {
        "prettyDate": "Jun 24, 2016",
        "timestamp": "2016-06-24T04:18:14.000Z",
        "id": "746195704545083393",
        "text": "New GTX 1070 build lol",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ClsFB-bUoAALtMi?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 20, 2016",
        "timestamp": "2016-06-20T20:41:47.000Z",
        "id": "744993670479286272",
        "text": "Ask shiba inu: is punishment the right choice for my entire family?\nAnswer: yes.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cla_yNOUoAAtwTc?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Jun 18, 2016",
        "timestamp": "2016-06-18T05:10:44.000Z",
        "id": "744034588821946369",
        "text": "Dear diary should I smoke crack or play Overwatch",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 16, 2016",
        "timestamp": "2016-06-17T03:34:44.000Z",
        "id": "743648042604072960",
        "text": "If you need an extra inu, try brushing your shiba",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ClH38rVVAAAxhfj?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "4"
    },
    {
        "prettyDate": "Jun 16, 2016",
        "timestamp": "2016-06-17T01:59:54.000Z",
        "id": "743624179329601536",
        "text": "Tips on furniture maintenance, with shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ClHiPh9UkAAGNTF?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "5"
    },
    {
        "prettyDate": "Jun 12, 2016",
        "timestamp": "2016-06-12T20:53:25.000Z",
        "id": "742097495472377857",
        "text": "Inus like going outside.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ckx1ux8VAAEtLOX?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Jun 12, 2016",
        "timestamp": "2016-06-12T04:53:47.000Z",
        "id": "741855996780249088",
        "text": "The time has come",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CkuaFoYVAAEpw03?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Jun 11, 2016",
        "timestamp": "2016-06-12T03:40:25.000Z",
        "id": "741837535098212354",
        "text": "punishment is absolute. punishment is necessary. punishment is good.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 11, 2016",
        "timestamp": "2016-06-12T03:34:43.000Z",
        "id": "741836100243689472",
        "text": "Shiba inu vs. vacuum the sequel",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/741835832152182784/pu/img/fosxf7tYkx_ETvNN.jpg"
        ],
        "replies": "0",
        "retweets": "4",
        "likes": "10"
    },
    {
        "prettyDate": "Jun 11, 2016",
        "timestamp": "2016-06-12T03:29:25.000Z",
        "id": "741834766094192640",
        "text": "Return of Satan Inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CkuGyAZUUAIA61G?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "8"
    },
    {
        "prettyDate": "Jun 11, 2016",
        "timestamp": "2016-06-12T01:33:22.000Z",
        "id": "741805561897848832",
        "text": "Shiba inus learn to lick their noses from an early age.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CktsOYAUkAElImB?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 9, 2016",
        "timestamp": "2016-06-10T02:13:45.000Z",
        "id": "741090949091102725",
        "text": "If your life is boring you should try taking drugs!",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CkjiSHwVEAAqLLc?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "16",
        "likes": "27"
    },
    {
        "prettyDate": "Jun 8, 2016",
        "timestamp": "2016-06-09T02:25:03.000Z",
        "id": "740731403025981440",
        "text": "Some shiba inus look like a potato.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "6"
    },
    {
        "prettyDate": "Jun 8, 2016",
        "timestamp": "2016-06-09T02:14:00.000Z",
        "id": "740728622927994880",
        "text": "My new shipment of drugs is here!",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CkeYvA0XEAArCFW?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "6",
        "likes": "9"
    },
    {
        "prettyDate": "Jun 4, 2016",
        "timestamp": "2016-06-04T06:59:37.000Z",
        "id": "738988561697886208",
        "text": "Shiba inu just rolled off the bed 🙄",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jun 4, 2016",
        "timestamp": "2016-06-04T06:33:32.000Z",
        "id": "738981997607981056",
        "text": "How to be an inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CkFkMyVUkAE1exT?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "12"
    },
    {
        "prettyDate": "Jun 4, 2016",
        "timestamp": "2016-06-04T04:30:27.000Z",
        "id": "738951021708214273",
        "text": "420 drugs. It's time to take drugs. 420 drugs, there's drugs.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jun 3, 2016",
        "timestamp": "2016-06-03T07:59:24.000Z",
        "id": "738641218251292672",
        "text": "Wear a scarf for maximum fucking pointlessness",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CkAuRdHUoAA9Hmb?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "7",
        "likes": "12"
    },
    {
        "prettyDate": "Jun 3, 2016",
        "timestamp": "2016-06-03T07:14:19.000Z",
        "id": "738629874160537601",
        "text": "It might be fun to punish a whole lot of people",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CkAj8viUYAAD4R3?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "9"
    },
    {
        "prettyDate": "May 28, 2016",
        "timestamp": "2016-05-28T07:47:26.000Z",
        "id": "736463879073472512",
        "text": "I didn't want it to have to come to this, but I may be forced to punish a lot of people.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 24, 2016",
        "timestamp": "2016-05-24T04:21:34.000Z",
        "id": "734962518473474048",
        "text": "Shiba inu Tinder pic",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CjMcf2MWsAA5ksA?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "8"
    },
    {
        "prettyDate": "May 18, 2016",
        "timestamp": "2016-05-18T17:49:44.000Z",
        "id": "732991577069494272",
        "text": "Shiba inus can't relax until you're punished",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ciwb5E6UUAEK5rZ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "9"
    },
    {
        "prettyDate": "May 18, 2016",
        "timestamp": "2016-05-18T05:55:26.000Z",
        "id": "732811815885897729",
        "text": "How to jack off",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cit4dAqWEAAjlGJ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "7"
    },
    {
        "prettyDate": "May 17, 2016",
        "timestamp": "2016-05-18T03:50:15.000Z",
        "id": "732780313877852162",
        "text": "Shiba inu will punish you",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CitbzhfXIAAotNt?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "6"
    },
    {
        "prettyDate": "May 17, 2016",
        "timestamp": "2016-05-18T01:59:09.000Z",
        "id": "732752351933693953",
        "text": "trying this again you cunt",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CitCYUzWkAA2cqV?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "2"
    },
    {
        "prettyDate": "May 17, 2016",
        "timestamp": "2016-05-17T04:46:12.000Z",
        "id": "732432006018031616",
        "text": "Sent from my iPad",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CiofBScUgAEdyF0?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "May 16, 2016",
        "timestamp": "2016-05-16T22:38:55.000Z",
        "id": "732339575234953217",
        "text": "Taking drugs is just like science, with tools that blend and baste.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CinKtiAW0AcBDhP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "May 7, 2016",
        "timestamp": "2016-05-07T04:33:21.000Z",
        "id": "728804894132473856",
        "text": "Truth is punishment. Punishment is love.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ch08JT0VAAAuUCu?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "May 5, 2016",
        "timestamp": "2016-05-05T04:57:21.000Z",
        "id": "728086155141373954",
        "text": "Horrific and excessive punishment await in your miserable future.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "May 5, 2016",
        "timestamp": "2016-05-05T04:45:55.000Z",
        "id": "728083280461860866",
        "text": "I made an autobiographical graphic novel",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Chqr4qBVEAAzKEF?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "May 5, 2016",
        "timestamp": "2016-05-05T04:27:35.000Z",
        "id": "728078664638697472",
        "text": "How to give yourself Super Physician-assisted syphil-herp-AIDS while on crack",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ChqnrR5UUAA1U1T?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "4",
        "likes": "5"
    },
    {
        "prettyDate": "May 2, 2016",
        "timestamp": "2016-05-03T02:17:23.000Z",
        "id": "727321122664910848",
        "text": "Shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Chf2sStUkAAaOUq?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "May 2, 2016",
        "timestamp": "2016-05-03T02:11:12.000Z",
        "id": "727319568490749952",
        "text": "Kill.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Chf1R0NUgAA7AQd?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Apr 26, 2016",
        "timestamp": "2016-04-27T03:53:02.000Z",
        "id": "725170868737835008",
        "text": "Shiba watches with a concerned look on his face as I take drugs, then falls asleep",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/ChBTDgGUgAE1ZdL?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "15"
    },
    {
        "prettyDate": "Apr 23, 2016",
        "timestamp": "2016-04-24T03:41:58.000Z",
        "id": "724080920609714176",
        "text": "I sharted into my own mouth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CgxzvBeUcAAnY95?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 23, 2016",
        "timestamp": "2016-04-24T01:07:29.000Z",
        "id": "724042040858660865",
        "text": "Forgive me father for I have sharted",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 23, 2016",
        "timestamp": "2016-04-23T20:41:16.000Z",
        "id": "723975045509353472",
        "text": "Santa Claus double penetration",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 23, 2016",
        "timestamp": "2016-04-23T05:02:11.000Z",
        "id": "723738719530807297",
        "text": "In order to come back inside you must answer three riddles",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cgs8hS0U8AAVicy?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "6",
        "likes": "12"
    },
    {
        "prettyDate": "Apr 21, 2016",
        "timestamp": "2016-04-22T00:00:45.000Z",
        "id": "723300471247511552",
        "text": "Top 10 signs of Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cgmt8EYUoAASDLs?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 16, 2016",
        "timestamp": "2016-04-16T04:01:10.000Z",
        "id": "721186647480512514",
        "text": "#falseflag #crackhitler",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 15, 2016",
        "timestamp": "2016-04-16T03:58:14.000Z",
        "id": "721185909568180225",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CgIqwY8UsAApkCa?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "7"
    },
    {
        "prettyDate": "Apr 15, 2016",
        "timestamp": "2016-04-16T03:57:54.000Z",
        "id": "721185825757601792",
        "text": "Dear shiba inu: how can I smoke enough crack to make my entire family be punished?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 12, 2016",
        "timestamp": "2016-04-12T05:56:56.000Z",
        "id": "719766231205548032",
        "text": "Effective tomorrow I quit abusing drugs and alcohol",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "1",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 10, 2016",
        "timestamp": "2016-04-11T03:18:51.000Z",
        "id": "719364058835582977",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CfuxyqyUYAA1DXQ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 10, 2016",
        "timestamp": "2016-04-11T03:14:06.000Z",
        "id": "719362863001968640",
        "text": "I didn't want to have to be forced to punish you... but you've left me no choice.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 8, 2016",
        "timestamp": "2016-04-08T05:47:37.000Z",
        "id": "718314332665331712",
        "text": "You can never stop me.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cff3FCTW4Agdu-O?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 8, 2016",
        "timestamp": "2016-04-08T05:35:12.000Z",
        "id": "718311210668699648",
        "text": "help me punish",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cff0PWTWcAA4T0s?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 8, 2016",
        "timestamp": "2016-04-08T04:38:37.000Z",
        "id": "718296968624238592",
        "text": "The only way your entire family won't be punished is if I don't punish them.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CffnR2nXEAAA21A?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Apr 6, 2016",
        "timestamp": "2016-04-07T01:41:41.000Z",
        "id": "717890053251342337",
        "text": "Which political candidate is the biggest gayslut",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "poll": {
            "options": [
                {
                    "option": "Hillary Sanders",
                    "percent": 0
                },
                {
                    "option": "Crank",
                    "percent": 0
                },
                {
                    "option": "Fuck",
                    "percent": 0
                }
            ],
            "stats": "0 votes"
        },
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Apr 5, 2016",
        "timestamp": "2016-04-05T06:51:00.000Z",
        "id": "717243120744681472",
        "text": "Some people say there's feces in her vomit",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 4, 2016",
        "timestamp": "2016-04-04T04:59:49.000Z",
        "id": "716852754614865920",
        "text": "Punishing",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 4, 2016",
        "timestamp": "2016-04-04T04:26:17.000Z",
        "id": "716844313674579969",
        "text": "Shiba inu uber",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CfK-FtUUkAAWEoe?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Apr 1, 2016",
        "timestamp": "2016-04-02T02:12:04.000Z",
        "id": "716085761901260801",
        "text": "Miraculous inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CfAMMqPXEAA6EuE?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "6"
    },
    {
        "prettyDate": "Apr 1, 2016",
        "timestamp": "2016-04-01T04:04:11.000Z",
        "id": "715751588082630657",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ce7cQuxXIAEfCbC?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 31, 2016",
        "timestamp": "2016-03-31T04:03:00.000Z",
        "id": "715388904501694464",
        "text": "Fuck the system",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 31, 2016",
        "timestamp": "2016-03-31T04:02:29.000Z",
        "id": "715388774625062913",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ce2SRRLUMAMzUAV?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 30, 2016",
        "timestamp": "2016-03-30T04:50:02.000Z",
        "id": "715038352995696642",
        "text": "Am so sad? Or am I a drama queen",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 29, 2016",
        "timestamp": "2016-03-30T03:42:32.000Z",
        "id": "715021363648434176",
        "text": "Drunk driving rules",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CexEF5DW8AA8VhC?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "5"
    },
    {
        "prettyDate": "Mar 27, 2016",
        "timestamp": "2016-03-27T06:12:59.000Z",
        "id": "713972063317262336",
        "text": "Shiba inus are master manipulators.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 27, 2016",
        "timestamp": "2016-03-27T06:07:21.000Z",
        "id": "713970644203872257",
        "text": "d';oy",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 27, 2016",
        "timestamp": "2016-03-27T04:51:40.000Z",
        "id": "713951597777190916",
        "text": "Bye",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ceh3KY9W8AA57mc?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 26, 2016",
        "timestamp": "2016-03-27T03:34:14.000Z",
        "id": "713932112143917056",
        "text": "LOOOOOLL",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CehlePUWEAAweo6?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 26, 2016",
        "timestamp": "2016-03-27T03:29:48.000Z",
        "id": "713930998476484608",
        "text": "Butt Sluts 69",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 22, 2016",
        "timestamp": "2016-03-22T06:27:46.000Z",
        "id": "712163844034920448",
        "text": "My Asian dog is Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CeIdPCYUEAAJBss?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 20, 2016",
        "timestamp": "2016-03-20T07:21:03.000Z",
        "id": "711452479553015809",
        "text": "Better call inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cd-WPT8UsAEwORw?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Mar 20, 2016",
        "timestamp": "2016-03-20T06:22:26.000Z",
        "id": "711437726583656448",
        "text": "It's always a good time for punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 20, 2016",
        "timestamp": "2016-03-20T04:50:12.000Z",
        "id": "711414515641012224",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cd9ztc3UAAAG71E?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "10"
    },
    {
        "prettyDate": "Mar 19, 2016",
        "timestamp": "2016-03-19T09:09:27.000Z",
        "id": "711117369163132928",
        "text": "Even though I don't want to have to punish you",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 18, 2016",
        "timestamp": "2016-03-18T04:24:00.000Z",
        "id": "710683146392502272",
        "text": "Let inus run free",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CdzaizmUIAAr_89?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "7"
    },
    {
        "prettyDate": "Mar 18, 2016",
        "timestamp": "2016-03-18T04:17:10.000Z",
        "id": "710681426597793792",
        "text": "Find the Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CdzY_dYUkAA-iP4?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 16, 2016",
        "timestamp": "2016-03-17T02:54:23.000Z",
        "id": "710298205406126081",
        "text": "I like to drink wine out of the bottle because fuck you",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cdt8cosUkAIS-65?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "5"
    },
    {
        "prettyDate": "Mar 15, 2016",
        "timestamp": "2016-03-15T04:14:10.000Z",
        "id": "709593508747845632",
        "text": "is it weird to keep a loaded shotgun by your computer/",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 12, 2016",
        "timestamp": "2016-03-13T00:35:28.000Z",
        "id": "708813695443271680",
        "text": "Anything goes when you're a shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CdY2SWgUYAAAzKE?format=jpg&name=large"
        ],
        "replies": "1",
        "retweets": "5",
        "likes": "13"
    },
    {
        "prettyDate": "Mar 9, 2016",
        "timestamp": "2016-03-10T00:11:23.000Z",
        "id": "707720469961506816",
        "text": "Shiba inu in the garden eating rabbit shit, should I punish",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 8, 2016",
        "timestamp": "2016-03-09T04:17:54.000Z",
        "id": "707420120927182849",
        "text": "BYE ICE",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CdFC0E7VAAAtdI2?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 8, 2016",
        "timestamp": "2016-03-08T05:14:12.000Z",
        "id": "707071899503734785",
        "text": "Crack babies for Trump",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 7, 2016",
        "timestamp": "2016-03-08T04:41:35.000Z",
        "id": "707063692530196480",
        "text": "SPRING. INU.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cc_-rWkUsAATZB7?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "7"
    },
    {
        "prettyDate": "Mar 7, 2016",
        "timestamp": "2016-03-08T04:17:32.000Z",
        "id": "707057639495053312",
        "text": "Spring inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cc_5JqoUAAA1ouO?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 5, 2016",
        "timestamp": "2016-03-06T04:52:01.000Z",
        "id": "706341541535612928",
        "text": "What would you do if you found out your entire family is Asian?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cc1t4eFVAAAlY-c?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 3, 2016",
        "timestamp": "2016-03-03T05:07:44.000Z",
        "id": "705258331737227265",
        "text": "Punish",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CcmUtLLUYAE-yzK?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Mar 2, 2016",
        "timestamp": "2016-03-02T06:23:33.000Z",
        "id": "704915025639636992",
        "text": "An Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cchccy6W8AAcmnT?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 2, 2016",
        "timestamp": "2016-03-02T05:46:37.000Z",
        "id": "704905732207022085",
        "text": "Shiba inu dinner",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CchUBN9XEAAynR9?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "4"
    },
    {
        "prettyDate": "Mar 2, 2016",
        "timestamp": "2016-03-02T05:27:09.000Z",
        "id": "704900832815403010",
        "text": "The correct choice for your entire family is horrific and excessive punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 2, 2016",
        "timestamp": "2016-03-02T05:18:01.000Z",
        "id": "704898532025704448",
        "text": "Hey kids it's never too early to start smoking",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 1, 2016",
        "timestamp": "2016-03-02T04:59:30.000Z",
        "id": "704893873940766721",
        "text": "Job description must have experience doing a lot of drugs",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Mar 1, 2016",
        "timestamp": "2016-03-02T04:47:46.000Z",
        "id": "704890920274161664",
        "text": "Dear diary how can I insufflate grams and grams of cocaine while still punishing my entire family",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Mar 1, 2016",
        "timestamp": "2016-03-02T04:33:25.000Z",
        "id": "704887310039388160",
        "text": "Help me shart into my own mouth h",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 29, 2016",
        "timestamp": "2016-02-29T05:31:14.000Z",
        "id": "704177082872385536",
        "text": "Find the Asian 2",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CcW9TvpUkAAaTze?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 26, 2016",
        "timestamp": "2016-02-27T04:06:08.000Z",
        "id": "703430893600268288",
        "text": "Need some extra cash? Sell your used condoms on Craigslist",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 26, 2016",
        "timestamp": "2016-02-27T04:02:13.000Z",
        "id": "703429904914890752",
        "text": "Shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CcMVw10UYAARcR0?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "7"
    },
    {
        "prettyDate": "Feb 26, 2016",
        "timestamp": "2016-02-27T03:48:09.000Z",
        "id": "703426364930588672",
        "text": "I'm no longer in control over my own actions",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CcMSizMUMAEVITf?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "9"
    },
    {
        "prettyDate": "Feb 26, 2016",
        "timestamp": "2016-02-27T02:27:00.000Z",
        "id": "703405945984921601",
        "text": "If you're not Asian then fuck you",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 26, 2016",
        "timestamp": "2016-02-27T02:25:52.000Z",
        "id": "703405659203567616",
        "text": "All I wanted to do is punish my entire family. Is that so much to ask",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CcL_tpUUkAAEVAj?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Feb 20, 2016",
        "timestamp": "2016-02-20T06:21:26.000Z",
        "id": "700928225677934592",
        "text": "Nobody loves you",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 20, 2016",
        "timestamp": "2016-02-20T05:40:34.000Z",
        "id": "700917942314147840",
        "text": "You shouldn't be afraid to smoke crack if it means punishing your entire family.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 20, 2016",
        "timestamp": "2016-02-20T05:35:51.000Z",
        "id": "700916753748987904",
        "text": "In order to punish my entire family I must take drugs",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 20, 2016",
        "timestamp": "2016-02-20T05:30:06.000Z",
        "id": "700915307410432000",
        "text": "Murder is not a crime.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 19, 2016",
        "timestamp": "2016-02-20T01:14:37.000Z",
        "id": "700851013402697729",
        "text": "I literally fuck farm animals",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 19, 2016",
        "timestamp": "2016-02-20T01:14:06.000Z",
        "id": "700850882758496260",
        "text": "Big booty sluts",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 19, 2016",
        "timestamp": "2016-02-20T01:11:35.000Z",
        "id": "700850249049329664",
        "text": "Punish your mom and dad by becoming addicted to painkillers",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CbnrlHVUUAES6fO?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Feb 18, 2016",
        "timestamp": "2016-02-19T02:13:03.000Z",
        "id": "700503332440268800",
        "text": "My dog is Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 18, 2016",
        "timestamp": "2016-02-18T05:01:09.000Z",
        "id": "700183247213371392",
        "text": "Help me punish my entire family while on drugs",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 17, 2016",
        "timestamp": "2016-02-17T06:35:29.000Z",
        "id": "699844597187358721",
        "text": "Shiba the Hedgehog 2",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 16, 2016",
        "timestamp": "2016-02-17T03:21:15.000Z",
        "id": "699795718798602240",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CbYsfX2UsAApS0E?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "16"
    },
    {
        "prettyDate": "Feb 16, 2016",
        "timestamp": "2016-02-17T03:20:59.000Z",
        "id": "699795650993479680",
        "text": "I didn't want it to have to come to this, but I will be literally forced to punish literally everyone I have ever loved",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 11, 2016",
        "timestamp": "2016-02-11T06:31:48.000Z",
        "id": "697669345535774720",
        "text": "taking a break from fallout 4 to jack off",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 11, 2016",
        "timestamp": "2016-02-11T06:31:22.000Z",
        "id": "697669235439484928",
        "text": "Fuck you I will punish #TBT",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Ca6eePMWcAAnc_8?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 11, 2016",
        "timestamp": "2016-02-11T06:29:54.000Z",
        "id": "697668867536060416",
        "text": "I didn't want this to happen, but soon I will be forced to punish again",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 9, 2016",
        "timestamp": "2016-02-09T05:21:44.000Z",
        "id": "696926936531079172",
        "text": "My dog is Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/Cav7WSEW8AEgXZk?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 6, 2016",
        "timestamp": "2016-02-07T04:15:09.000Z",
        "id": "696185404672741376",
        "text": "Mirror mirror on the wall who's the most Asian of them all",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Feb 6, 2016",
        "timestamp": "2016-02-06T09:08:19.000Z",
        "id": "695896792265678848",
        "text": "Drink away the pain",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CahScQbUsAAI1GP?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Feb 3, 2016",
        "timestamp": "2016-02-03T06:38:54.000Z",
        "id": "694772029917138944",
        "text": "I sharted",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 30, 2016",
        "timestamp": "2016-01-31T02:27:36.000Z",
        "id": "693621624512184322",
        "text": "Learn how to shart",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 29, 2016",
        "timestamp": "2016-01-30T02:56:53.000Z",
        "id": "693266605371838464",
        "text": "I love shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZ76TKZUAAAatZE?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "3"
    },
    {
        "prettyDate": "Jan 28, 2016",
        "timestamp": "2016-01-29T02:52:13.000Z",
        "id": "692903043935768576",
        "text": "Who wants to give their whole family hepatitis AIDS",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZ2vpDYUYAIQ9-V?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 27, 2016",
        "timestamp": "2016-01-28T03:07:41.000Z",
        "id": "692544547432329216",
        "text": "I smashed my fist and it hurt my wrist",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 27, 2016",
        "timestamp": "2016-01-28T01:41:42.000Z",
        "id": "692522906472026112",
        "text": "Shiba inu vs. Swiffer mop",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/ext_tw_video_thumb/692522835143704576/pu/img/DRuKcpi2Re3pvjNF.jpg"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "6"
    },
    {
        "prettyDate": "Jan 27, 2016",
        "timestamp": "2016-01-28T00:47:58.000Z",
        "id": "692509388054097921",
        "text": "#MyLastFourWords Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 27, 2016",
        "timestamp": "2016-01-27T05:23:39.000Z",
        "id": "692216376480636929",
        "text": "The human eye can't see more than 30 fps",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 24, 2016",
        "timestamp": "2016-01-24T06:56:55.000Z",
        "id": "691152686008369152",
        "text": "Shiba inus like to be close by, but they don't like to be coddled.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 24, 2016",
        "timestamp": "2016-01-24T06:54:23.000Z",
        "id": "691152047438237696",
        "text": "I could drive to my heroin dealer, buy heroin, come home and inject heroin in the time it takes Destiny to update",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZd3H1pWkAAidqn?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 24, 2016",
        "timestamp": "2016-01-24T06:53:00.000Z",
        "id": "691151697649999872",
        "text": "Super shiba inu",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZd2zYcWQAAt3yj?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 23, 2016",
        "timestamp": "2016-01-24T02:23:33.000Z",
        "id": "691083888026714112",
        "text": "Most people suck dicks for crack at least 4 times in their life. It's normal",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "3",
        "likes": "3"
    },
    {
        "prettyDate": "Jan 23, 2016",
        "timestamp": "2016-01-24T01:48:00.000Z",
        "id": "691074940724875264",
        "text": "Lactate all over my face",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 22, 2016",
        "timestamp": "2016-01-23T04:51:44.000Z",
        "id": "690758793790029825",
        "text": "Balls cunt",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 22, 2016",
        "timestamp": "2016-01-23T04:38:09.000Z",
        "id": "690755375000190976",
        "text": "How many pills should I take",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 22, 2016",
        "timestamp": "2016-01-23T04:26:29.000Z",
        "id": "690752437473280000",
        "text": "Murder is not a crime.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 22, 2016",
        "timestamp": "2016-01-23T02:14:28.000Z",
        "id": "690719214886203393",
        "text": "AIDS",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZXtdneUAAApmOJ?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 22, 2016",
        "timestamp": "2016-01-23T01:38:14.000Z",
        "id": "690710096716943362",
        "text": "I will give you AIDS",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 22, 2016",
        "timestamp": "2016-01-22T05:42:08.000Z",
        "id": "690409090388918272",
        "text": "Shiba inus are difficult to train because they’re very strong-willed. If they don’t see a good reason to do a command, they won’t do it.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 21, 2016",
        "timestamp": "2016-01-22T04:02:20.000Z",
        "id": "690383973650939909",
        "text": "Search for \"Asians\" on Siri",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 21, 2016",
        "timestamp": "2016-01-22T03:48:55.000Z",
        "id": "690380598255448064",
        "text": "Punish me Santa",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZS5fgHWIAAwPYr?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 21, 2016",
        "timestamp": "2016-01-21T06:11:40.000Z",
        "id": "690054133773131776",
        "text": "Learning to draw all over again",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 21, 2016",
        "timestamp": "2016-01-21T06:09:41.000Z",
        "id": "690053634873159680",
        "text": "He moved before I could finish drawing him not even once",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZOQH2kUsAIDVVY?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 20, 2016",
        "timestamp": "2016-01-21T00:34:30.000Z",
        "id": "689969283183566849",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CZNDZ3LWcAIob1B?format=png&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 18, 2016",
        "timestamp": "2016-01-18T06:11:41.000Z",
        "id": "688966975104548864",
        "text": "Extreme painsluts",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CY-zzfEUEAAPU9D?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 18, 2016",
        "timestamp": "2016-01-18T06:05:21.000Z",
        "id": "688965381080219649",
        "text": "Only violent and extreme punishment will suffice",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CY-yW0dUQAAmtMe?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 17, 2016",
        "timestamp": "2016-01-17T09:01:58.000Z",
        "id": "688647437842399232",
        "text": "Punishment for its own sake",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 16, 2016",
        "timestamp": "2016-01-17T03:08:49.000Z",
        "id": "688558565678682112",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CY5AXLjUEAEcWno?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 15, 2016",
        "timestamp": "2016-01-16T04:06:34.000Z",
        "id": "688210712330555394",
        "text": "I'm never drinking again. After tonight.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 13, 2016",
        "timestamp": "2016-01-14T00:03:33.000Z",
        "id": "687424777296965632",
        "text": "Snort large quantity of crack meth",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 13, 2016",
        "timestamp": "2016-01-13T23:49:18.000Z",
        "id": "687421193255809026",
        "text": "Satan inu coming to your town",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CYo17KCWMAAOlnu?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "4"
    },
    {
        "prettyDate": "Jan 11, 2016",
        "timestamp": "2016-01-12T04:46:52.000Z",
        "id": "686771302129086464",
        "text": "How to know if you will be punished",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 11, 2016",
        "timestamp": "2016-01-12T04:35:21.000Z",
        "id": "686768403655204864",
        "text": "We can all be punished",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 11, 2016",
        "timestamp": "2016-01-12T02:58:23.000Z",
        "id": "686744000158547968",
        "text": "Fato Pato",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 11, 2016",
        "timestamp": "2016-01-12T01:55:07.000Z",
        "id": "686728077796192256",
        "text": "Punishment is the only punishment",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 9, 2016",
        "timestamp": "2016-01-09T07:34:37.000Z",
        "id": "685726354214535168",
        "text": "fuck you, anti-vaxxers, you cunt cunt cunts",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "3"
    },
    {
        "prettyDate": "Jan 9, 2016",
        "timestamp": "2016-01-09T07:08:51.000Z",
        "id": "685719869589827584",
        "text": "The only way to punish my entire family is to punish my entire family.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Jan 9, 2016",
        "timestamp": "2016-01-09T06:35:16.000Z",
        "id": "685711416221798401",
        "text": "Soon I will be forced to punish everyone I have ever loved.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 1, 2016",
        "timestamp": "2016-01-01T21:45:39.000Z",
        "id": "683041418391064577",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CXqmhuGUEAAp4h1?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "2"
    },
    {
        "prettyDate": "Jan 1, 2016",
        "timestamp": "2016-01-01T19:57:17.000Z",
        "id": "683014147831799808",
        "text": "Mirror mirror on the wall. who is the most punished of them all?",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CXqNvcTUoAAa9Ru?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 1, 2016",
        "timestamp": "2016-01-01T06:32:45.000Z",
        "id": "682811682075217922",
        "text": "fuck you im triggered",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Jan 1, 2016",
        "timestamp": "2016-01-01T06:28:17.000Z",
        "id": "682810557313908736",
        "text": "Trigger warning",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 30, 2015",
        "timestamp": "2015-12-31T01:14:37.000Z",
        "id": "682369230885355521",
        "text": "Shiba inu with glasses, who is also addicted to meth and crack cocaine",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CXhDMUCUwAAf4sH?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "21",
        "likes": "29"
    },
    {
        "prettyDate": "Dec 29, 2015",
        "timestamp": "2015-12-30T01:51:02.000Z",
        "id": "682016007532183552",
        "text": "Drink away the pain.",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 25, 2015",
        "timestamp": "2015-12-25T06:17:25.000Z",
        "id": "680271108168224768",
        "text": "Tis the season to PUNISH",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CXDO9p5VAAElnBn?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Dec 25, 2015",
        "timestamp": "2015-12-25T06:10:49.000Z",
        "id": "680269447727153152",
        "text": "Fuck",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 25, 2015",
        "timestamp": "2015-12-25T06:05:07.000Z",
        "id": "680268010980950021",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CXDMJdMUwAItJ_H?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 25, 2015",
        "timestamp": "2015-12-25T05:48:25.000Z",
        "id": "680263810490351616",
        "text": "Shiba inu will punish you for your crime",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CXDIU4OUsAABUn_?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "5",
        "likes": "6"
    },
    {
        "prettyDate": "Dec 24, 2015",
        "timestamp": "2015-12-25T00:55:07.000Z",
        "id": "680189999636062212",
        "text": "Is your dog Asian or is faking it",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 24, 2015",
        "timestamp": "2015-12-24T20:58:10.000Z",
        "id": "680130367257358336",
        "text": "How to know if Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 24, 2015",
        "timestamp": "2015-12-24T05:35:51.000Z",
        "id": "679898260685733888",
        "text": "Asians",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "0"
    },
    {
        "prettyDate": "Dec 19, 2015",
        "timestamp": "2015-12-19T07:48:30.000Z",
        "id": "678119703152201728",
        "text": "My dog is an Asian",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CWkqRsaUEAAJKym?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "0",
        "likes": "2"
    },
    {
        "prettyDate": "Nov 7, 2015",
        "timestamp": "2015-11-07T09:04:23.000Z",
        "id": "662918507298840576",
        "text": "Is punishment the right choice for your shiba inu? (scroll down for answer)\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\n-\n\nYes",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "0",
        "likes": "0"
    },
    {
        "prettyDate": "Oct 31, 2015",
        "timestamp": "2015-10-31T04:12:32.000Z",
        "id": "660308345522290688",
        "text": "Death to America",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CSni776VAAEgDNc?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    },
    {
        "prettyDate": "Oct 31, 2015",
        "timestamp": "2015-10-31T04:05:53.000Z",
        "id": "660306671516778496",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CSnhaayVAAAxomT?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "2",
        "likes": "3"
    },
    {
        "prettyDate": "Oct 31, 2015",
        "timestamp": "2015-10-31T04:02:39.000Z",
        "id": "660305859545690113",
        "text": "Almost time to punish my entire fucking family",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [
            "https://pbs.twimg.com/media/CSngrLsVAAAbg65?format=jpg&name=large"
        ],
        "replies": "0",
        "retweets": "3",
        "likes": "5"
    },
    {
        "prettyDate": "Oct 30, 2015",
        "timestamp": "2015-10-30T04:35:26.000Z",
        "id": "659951721133776896",
        "text": "FML",
        "avatar": "https://pbs.twimg.com/profile_images/3734166388/e7c4db36f9fff78e8d29741130c2016f_x96.jpeg",
        "attachedImages": [],
        "replies": "0",
        "retweets": "1",
        "likes": "1"
    }
]