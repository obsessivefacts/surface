var Promise  = require('bluebird');
var util     = require('./util');
var config   = require('../../config'),
    crypto = require('crypto'),
    models   = {},
    app      = null,
    db       = null,
    redis    = null;

module.exports = function(providers) {
// ==

app           = providers.app;
db            = providers.db;
redis         = providers.redis;

return function(req, res, next) {

    if (req.user && req.user.isLoggedIn())
        return next();

    const headers = req.headers,
          dnt = headers.dnt && headers.dnt.toString() === "1" ? true : false;

    if (req.signedCookies && req.signedCookies.insidious_tracking_cookie) {
        try {
            let cookie = req.signedCookies.insidious_tracking_cookie;
            let notMasked = util.decrypt(cookie);

            // ignore+destroy cookie if DNT is on and no explicit opt-in exists!
            if (dnt && !notMasked.optIn) {
                res.clearCookie('insidious_tracking_cookie');
            } else {
                if (dnt) res.set({'Tk': 'C'});    // send explicit opt-in header
                req.user.tmpIdentity = notMasked;
                req.user.tmpIdentityEncrypted = cookie;
                return next();
            }
        } catch(err) {
            console.warn('someone is trying to hack their cookie: ', cookie);
        }
    }

    // if it's not cryptographically secure enough then register a real account.
    let notMasked = {
        name: generateUsername(),
        symbol: Math.floor(Math.random() * 33),
        snowflake: +Math.random().toFixed(15) // postgres precision workaround
    }
    req.user.tmpIdentity = notMasked;
    
    let encrypted = util.encrypt(notMasked);

    req.user.tmpIdentityEncrypted = encrypted;

    if (!dnt) res.cookie('insidious_tracking_cookie', encrypted, config.cookie);    

    return next();
}

// ==
}

var generateUsername = function() {
    var genders = [
        'Abimegender',
        'Adamasgender',
        'Aerogender',
        'Aesthetigender',
        'Affectugender',
        'Agender',
        'Agenderflux',
        'Alexigender',
        'Aliusgender',
        'Amaregender',
        'Ambigender',
        'Ambonec',
        'Amicagender',
        'Androgyne',
        'Anesigender',
        'Angenital',
        'Anogender',
        'Anongender',
        'Antegender',
        'Anxiegender',
        'Apagender',
        'Apconsugender',
        'Astergender',
        'Astralgender',
        'Autigender',
        'Autogender',
        'Axigender',
        'Bigender',
        'Biogender',
        'Blurgender',
        'Boyflux',
        'Burstgender',
        'Caelgender',
        'Cassgender',
        'Cassflux',
        'Cavusgender',
        'Cendgender',
        'Ceterofluid',
        'Ceterogender',
        'Cisgender',
        'Cloudgender',
        'Collgender',
        'Colorgender',
        'Commogender',
        'Condigender',
        'Deliciagender',
        'Demifluid',
        'Demiflux',
        'Demigender',
        'Domgender',
        'Demi-vapor',
        'Demi-smoke',
        'Duragender',
        'Egogender',
        'Epicene',
        'Espigender',
        'Exgender',
        'Existigender',
        'Female',
        'Femfluid',
        'Femgender',
        'Fluidflux',
        'Gemigender',
        'Genderblank',
        'Genderflow',
        'Genderfluid',
        'Genderflux',
        'Genderfuzz',
        'Gender Neutral',
        'Genderpunk',
        'Genderqueer',
        'Genderwitched',
        'Girlflux',
        'Glassgender',
        'Glimragender',
        'Greygender',
        'Gyragender',
        'Healgender',
        'Heliogender',
        'Hemigender',
        'Horogender',
        'Hydrogender',
        'Imperigender',
        'Intergender',
        'Juxera',
        'Libragender',
        'Male',
        'Magigender',
        'Mascfluid',
        'Mascgender',
        'Maverique',
        'Mirrorgender',
        'Molligender',
        'Multigender',
        'Nanogender',
        'Neutrois',
        'Nonbinary',
        'Omnigender',
        'Oneirogender',
        'Pangender',
        'Paragender',
        'Perigender',
        'Polygender',
        'Proxvir',
        'Quoigender',
        'Subgender',
        'Surgender',
        'Systemgender',
        'Tragender',
        'Transgender',
        'Transneutral',
        'Trigender',
        'Vapogender',
        'Venngender',
        'Verangender',
        'Vibragender',
        'Vocigender',
    ];
    var gender = genders[Math.floor(Math.random() * genders.length)];
    var extra = false;
    if (gender.indexOf('gender') !== -1 && Math.random() < .25) {
        var identities = ['boy', 'girl', 'nonbinary'],
            variant = identities[Math.floor(Math.random() * identities.length)];

        gender = gender.replace('gender', variant);
    }
    var therians = [
        'Therian',
        'Elf',
        'Fae',
        'Dragon',
        'Angel',
        'Hybrid',
        'Polymorph',
        'Galaxy-kin',
        'Bear',
        'God-kin',
        'Mermaid',
        'Robot',
        'Shadow',
        'Wolf',
        'Fox',
        'Bird',
        'Dingo',
        'Elemental',
        'Otherkin',
        'Feline',
        'Ghost',
        'Plant',
        'Orc',
        'Android',
        'Farmer',
        'Carpenter',
        'Blacksmith',
        'Doge',
        'Inu',
        'Coyote',
        'Raccoon',
        'Squirrel',
        'Rabbit',
        'Bunny',
        'Clown',
        'Sparrow',
        'Robin',
        'Lizard',
        'Moth',
        'Butterfly',
        'Velociraptor',
        'Flower',
        'Tree',
        'Star',
        'Moon',
        'Asteroid',
        'Wyvern',
        'Asari',
        'Gek',
        'Korvax',
        'Vy\'keen',
        'Turtle',
        'Chain Chomp',
        'Horse-kin',
        'Kangaroo',
        'Platypus',
        'Penguin',
        'Hobbit',
        'Dolphin',
        'Manatee',
        'Seal',
        'Noble',
        'Wolverine',
        'Moose',
        'Elk',
        'Faun',
        'Deer',
        'Hatchling',
        'Beetle',
        'Giraffe',
        'Chameleon',
        'Duckling',
        'Hedgehog',
        'Kitten',
        'Panda',
        'Octopus',
        'Owl',
        'Hamster',
        'Walrus',
        'Otter',
        'Tortoise',
        'Goose',
        'Waterfowl',
        'Pumpkin',
        'Eggplant',
        'Falcon',
        'Super Mutant',
        'Robobrain',
        'Player Character',
        'Chipmunk',
        'Jackal',
        'Super Hacker',
    ];
    gender += ' ' + therians[Math.floor(Math.random() * therians.length)];
    if (extra == false && gender.indexOf('kin') === -1 && Math.random() < .2) {
        gender += '-kin';
        extra = true;
    }
    if (extra == false && Math.random() < .05) {    // LEGENDARY
        gender += ' Master';
    }
    return gender;
}