# Obsessive Facts

## The anti-social network.

We were drawn to technology, the Internet, the promise of a decentralized
realtime communication medium of unprecedented scale, the collective
subconscious of humanity, a great equalizing infrastructure that could spur
social change and affect solutions to long-standing real world problems. These
hopes were naïve. Instead we got shopping malls, microtransactions, a massively
multiplayer circlejerk on social media, self driving cars for the wealthy, all
the relevant valuations, IPOs, catered lunches, corporate and government
surveillance, and an increasingly-organized cabal of entrenched media and state
actors working to destroy what little good is left in the whole thing. But truly
we wanted to "make the world a better place," and in yet another naïve move we
were drawn to nonprofit civil rights advocacy.

Alas, the indefinitely pending revolution has not been televised nor would it be
if it existed in practice. The widely-accepted authorities on questioning
authority are themselves compromised to the point of being controlled
opposition. This is not because their intentions were originally impure, but
regrettably, when put into practice, the pursuit of an ideal almost invariably
runs into scaling issues surmountable only through the creation of
infrastructure. But infrastructure needs maintenance, maintenance requires
people, people create bureaucracy, bureaucracy sucks up lots of money, and money
always demands compromise. Seldom does an ideal survive the final link in this
chain and break the mold of the status quo. Thus traditional models of advocacy
are broken. But there is a better way.

The siren song of perfection, the illusive wings of hubris, the oily shadows of
corruption; the singular pursuit of an ideal is itself corruptive, because an
ideal is a guide, not a goal. Like a decision, an ideal without a test of its
merits is boring. To decide and then rationalize ex-post-facto is base
animalism masked by ego. To ideate in intellectual isolation is cowardice masked
by self-righteousness, the tradecraft of zealots. In either case, if what lies
beneath the mask is weak, trivial or wrong, then why be so vehement in its
defense? If it's strong, if it's right, then
a
challenge
should
be
welcome.
Thus, ironically, to avoid being compromised within we seek compromise without.
We seek to learn and understand those ideas that give us pause, to reject and
replace broken patterns, and to create our own model of reality based on what we
understand, not what we are told.

We seek those who wish to join us in this pursuit of discourse, understanding
and critical thought, regardless of whether our conclusions align. The best
medium to enable this intercourse already exists, but it is under threat.
Commercial interests have centralized and now serve as gatekeepers to large
portions of the Internet, where they have commoditized our interests, mining for
ad revenue tokens on our spare mental cycles and imposing control over what we
see and say. But beyond their control are the advanced persistent threats they
have unleashed in the process: the rise of mass surveillance, disinformation and
manipulation, fake news and the resultant civil unrest—the Senate can cajole as
many tech CEOs as they'd like to "do better next time," but the Pandora's Box
has been opened and none of them know what the fuck is going on.

The real threat to the Internet comes not from those listed above but rather the
actions of those Senators and CEOs as they desperately try to fight all the
monsters they've created in the aftermath of a political upheaval. Not being
evil becomes an afterthought when your shareholder value and grip on power are
under threat. And as with any control system caught off-guard and blindly
reacting to a threat to its power, their only available response is to exert
more control. Because, we are told, the free exchange of information is too
dangerous, and certainly cannot be monetized. And so they have deputized their
controlled opposition to convince the masses that restricting the Internet is
the morally just response to a nebulous threat of fascism—the Internet must
become a safe space where a benevolant technocracy serves us only the
information we need.

We reject very little but we do not compromise in our rejection of these visions
of the Internet as a walled garden, where monopolistic social media corporations
are free to mass-manipulate, enjoying indemnity from the misdeeds of their users
yet restricting any user-generated content that disrupts their commercial
narratives; where domain registrars and infrastructure providers censor entire
websites at the behest of angry mobs; where self-anointed arbiters of the moral
monoculture stupidly trample over human rights in their misguided efforts to
protect them. People who claimed moral authority have perpetrated the worst
human rights abuses throughout history—but this time, supposedly, things are
different? No. We will not accept the rule of corporate exploiters, their puppet
propagandists or the neo-puritan mobs. Nobody but the individual can be a moral
authority.

So instead, we propose an alternative model of art, activism and actualization,
to re-imagine the Internet as a medium of unyielding Free Expression, and
harness it to empower individual actors as vanguards of independent thought and
discourse. In contrast to centralized platforms, we envision federations of
standalone websites as the primary conveyors of online creation, whether they be
the products of individuals, startups, cooperatives, or forums of many, topical
or general. Fully idealized, this **Federated Model** is a revitalization of the
"Wild West," the ghost of Internet past, prior to its deranged commercialization
and corporate consolidation. But beyond simply reverting to older and quainter
times, we seek to learn from two decades of successes and failures, implement
new technology and engage in civic action to strengthen the Internet against the
threats that corrupt it.

## Re-decentralize the web.

Building individual websites makes the Internet stronger. When content is
spread across the web and controlled by people, rather than centralized on the
servers of a few large corporations, it is harder for bad actors to restrict or
manipulate peoples' access to information, or engage in widespread invasions of
privacy. The creation and sharing of content becomes less about instant
notifications, perfectly plastic alter-egos living fairy tale lives, and
semi-hypnotic states induced by continuous scrolling over algorithmically
curated content feeds. These things exist to make us better consumers, but at
best they add little value to the content itself. We suggest that social media
has crested, is increasingly irrelevant, and can be completely deprecated from
our lives. Any void left by its absense can be filled by our own creations.

Any person can create a website. The primary difficulty in doing so is the
paralyzing assumption that it is difficult. Today it's easier than ever to
create a web presence, thanks to decades of advances in technology,
infrastructure and documentation. And beyond the social incentives to (actually)
make the world a better place, there are strong market incentives that make
learning to code a smart idea, even at a basic level. You don't need bare metal
servers, fiber running out of your attic or anonymous corporate registrations.
You just need a willingness to learn, and a curiosity about what lies beneath
the surface of the things you took for granted. And in the process of exploring
and illuminating what was previously dark, you may find your voice, even if you
never thought you had anything interesting to say.

You will be in good company. Each day, more people are breaking out of
their filter bubbles and seeing the reality of the Internet as it exists
today—that humanity's once-great achievement of technical mastery and free
expression has been corrupted by manipulators who sought control but are
increasingly failing at it. Fortunately, the explosive growth of alt-media
portends a new generation of free thinkers with the agency and technical prowess
to speak for themselves, free of social media giants and content conglomerates.
Collaborating together we will re-decentralize the Internet, ending the
stranglehold of amoral corporations over our access to information, and breaking
the divide-and-conquer status quo that pits good, moral people against each
other for the ultimate benefit of the ruling class and to the detriment of our
culture.

The future is federated. The Internet of tomorrow is not a web but a torrent of
creative energy that cannot be controlled or restricted, for to restrict
any thing would be to restrict everything. We seek not to destroy but to
deconstruct and recreate, to inspire the strong and strengthen the weak. We
believe this pursuit is virtuous, because at its core, humanity is basically
good. But what do you believe?
