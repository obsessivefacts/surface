const config = require('../../config'),
      Sequelize = require('sequelize'),
      db = new Sequelize(
        config.db.db,
        config.db.user,
        config.db.pass,
        config.db.options
      );

qry = `
    SELECT
        fu.id
    FROM
        fmiffel_user fu
    WHERE
        fu.created_at < now() - interval '1 hour'
    AND
        fu.user_id IS NULL
    AND
        fu.post_count = 0`;

db.query(qry).then(results => {
    for (let result of results[0]) {
        console.log('STALE USER: ', result.id);
        deleteFavorites(result)
            .then(() => deleteFollowing(result))
            .then(() => deleteFollowers(result))
            .then(() => deleteUser(result))
    }
})

const deleteFavorites = result => {
    let qry = 'DELETE FROM fmiffel_favorite WHERE fmiffel_user_id = :id';
    return db.query(qry, { replacements: result })
        .then(() => console.log(' - deleted favorites: ', result.id));
};

const deleteFollowing = result => {
    let qry = 'DELETE FROM fmiffel_follow WHERE fmiffel_user_id = :id';
    return db.query(qry, { replacements: result })
        .then(() => console.log(' - deleted following: ', result.id));
};

const deleteFollowers = result => {
    let qry = 'DELETE FROM fmiffel_follow WHERE fmiffel_user_followed_id = :id';
    return db.query(qry, { replacements: result })
        .then(() => console.log(' - deleted followers: ', result.id));
};

const deleteUser = result => {
    let qry = 'DELETE FROM fmiffel_user WHERE id = :id';
    return db.query(qry, { replacements: result })
        .then(() => console.log(' - deleted user: ', result.id));
};