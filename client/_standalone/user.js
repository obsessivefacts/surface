/**
 *
 * @source: https://gitlab.com/obsessivefacts/surface
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) Obsessive Network LLC
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

userControl.handleLogin = function() {
    log('handling login');
    var subnav = e('header nav ul.main li ul.subnav'),
        userLi = q(subnav, 'li.user'),
        cdn    = ( CONFIG.cdnSurface ? CONFIG.cdnSurface : '');

    if (!userLi) {
        userLi = c('li');
        var box = c('div'), avatar = c('div'), status = c('div'),
            strong = c('strong'), a1 = c('a'), a2 = c('a'), span = c('span');

        userLi.className = 'user';
        avatar.className = 'avatar';
        status.className = 'status';
        a1.textContent = 'View Profile';
        a1.addEventListener('click', function() { header.hideSubnav(); });
        span.textContent = ' / ';
        a2.textContent = 'Logout';
        a2.href = '/user/auth/logout';
        a2.addEventListener('click', userControl.clickLogout);
        status.appendChild(strong);
        status.appendChild(a1);
        status.appendChild(span);
        status.appendChild(a2);
        box.appendChild(avatar);
        box.appendChild(status);
        userLi.appendChild(box);
        subnav.insertBefore(userLi, subnav.querySelector('.n1'));
    }

    if (user.avatar) var avatar = cdn + '/avatars/' + user.avatar;
    else var avatar = cdn + '/images/user/profile/anonymous.png';

    var avEl    = e('nav .subnav .user .avatar'),
        strong  = e('nav .subnav .user .status strong'),
        profile = e('nav .subnav .user .status a:first-of-type');

    avEl.style.backgroundImage = 'url('+avatar+')';
    avEl.style.backgroundSize  = '100% auto';
    strong.textContent = 'Hi, ' + user.username + ' ';
    profile.href = '/user/' + user.username;
};

userControl.clickLogout = function(e) {
    stop(e);
    userControl.logout();
};

userControl.logout = function() {
    notifier.notify('You have been logged out.');
    userControl.anonymous();
    var userLi = e('header nav ul.main li ul.subnav li.user');
    if (userLi) userLi.parentNode.removeChild(userLi);
    loader.go('/user/auth/logout');
};

var userScript = id('user');
if (userScript && userScript.dataset.id) {
    user = {
        id: userScript.dataset.id,
        username: userScript.dataset.username,
        avatar: userScript.dataset.avatar
    }
}
var profile = e('header nav ul.main li ul.subnav .status a:first-of-type');

var logout = e('header nav ul.main li ul.subnav .status a:last-of-type');
if (logout)
    logout.addEventListener('click', userControl.clickLogout);