var DEBUG = true,
    MASTER_AUDIO_CTX = new (window.AudioContext || window.webkitAudioContext)(),
    CONFIG = {};

var user, userControl = {
    anonymous: function() { user = {id: 0, username: 'anonymous', avatar: null}}
}
userControl.anonymous();

var inherit = function(child, parent) {
    child.prototype = Object.create(parent.prototype);
}

var log = function() {
    if (DEBUG) {
        console.log.apply(console, arguments);
    }
}

var c    = document.createElement.bind(document),
    id   = document.getElementById.bind(document),
    e    = document.querySelector.bind(document),
    es   = document.querySelectorAll.bind(document),
    ev   = document.addEventListener.bind(document),
    q    = function(el, qry) { return el.querySelector(qry); },
    qs   = function(el, qry) { return el.querySelectorAll(qry); },
    stop = function(e) { if (e) { e.preventDefault(); e.stopPropagation(); } }

console.log('%cDON\'T BE STUPID', 'color: red; font-size: 40px; font-family: \'Impact\';');
console.log('%cIf someone told you to copy-paste something here then they are trying to hack you. Compromised accounts will be deleted and never reinstated.', 'color: red; font-size: 20px; font-weight: bold;');

switch(document.readyState) {
    case 'complete':
    case 'loaded':
    case 'interactive':
        loader.initialize();
        break;
    default:
        if (typeof document.addEventListener === 'function')
            ev('DOMContentLoaded', loader.initialize.bind(loader), false);
}