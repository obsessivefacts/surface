var AccessGrantedController = function(el) {
    this.init(el);
    this.initCanvases();    
    header.hide();
    q(this.el, '.matrix').classList.remove('animating');
    q(this.el, '.matrix').classList.remove('noJS');
    q(this.el, '.matrix').classList.remove('h4x0r');              // firefox hax

    return this;
}

inherit(AccessGrantedController, Page);
loader.registerController('access_granted', AccessGrantedController);

AccessGrantedController.prototype.initCanvases = function() {
    this.WORDS     = [
        'I\'m afraid I will be forced to punish you',
        'I didn\'t want to have to be forced to punish you, but you\'ve left me no choice',
        'Face the punishment',
        'Punishment is absolute. Punishment is necessary. Punishment is good.',
        'You will learn to love your punishment.',
        'In Australia, birthplace of the shiba inu, the national pastime is smoking crack.',
        'Teledildonics is a promising new field for enterprise solution big data machine learning'
    ];
    this.FONT_SIZE          = 28;
    this.LINE_HEIGHT        = 24;
    this.EXTRA_ROWS         = 10;
    this.FADE_FACTOR        = .05;
    this.MAX_QUEUE          = 200;
    this.DELAY              = 100;
    this.CONCURRENCY        = 80;
    this.COLLIDE_OK         = true;
    this.COLOR              = 'red';
    this.rainCanvas         = null;
    this.rainCtx            = null;
    this.canvases           = [];
    this.ctx                = [];
    this.grid               = [];
    this.wordQueue          = [];
    this.letters            = {};
    this.animating          = true;
    this.gridNeedsUpdate    = true;
    this.gridStartTime      = 0;
    this.tmpCanvas          = null;
    this.tmpCtx             = null;
    this.lockCtx            = null;
    this.lockBody           = null;
    this.lockShackle        = null;
    this.lockShackleStart   = 0;
    this.lockShackleMs      = 500;
    this.lockDoneAnimating  = false;
    this.interval           = null;
    this.pollTimeout        = null;
    this.animateTimeout     = null;
    this.redirectTimeout    = null;
    this.h4x0rTimeout       = null; // wacky browser hack
    this.width              = 0;
    this.height             = 0;
    this.fontWidth          = 0;
    this.rows               = 0;
    this.cols               = 0;

    var lockCanvas = q(this.el, 'canvas.lock');
    lockCanvas.width = 700;
    lockCanvas.height = 820;
    this.lockCtx = lockCanvas.getContext('2d');
    this.lockCtx.webkitImageSmoothingEnabled = false;
    this.lockCtx.imageSmoothingEnabled = false;
    this.initLock();

    this.rainCanvas = q(this.el, 'canvas.rain');
    this.width  = this.rainCanvas.offsetWidth;
    this.height = this.rainCanvas.offsetHeight;
    this.rainCanvas.width = this.width;
    this.rainCanvas.height = this.height;
    this.rainCtx = this.rainCanvas.getContext('2d');
    this.rainCtx.imageSmoothingEnabled = false;
    this.rainCtx.webkitImageSmoothingEnabled = false;
    this.rainCtx.mozImageSmoothingEnabled = false;

    this.tmpCanvas = c('canvas');
    this.tmpCanvas.width = this.width;
    this.tmpCanvas.height = this.height;
    this.tmpCtx = this.tmpCanvas.getContext('2d');
    this.tmpCtx.imageSmoothingEnabled = false;
    this.tmpCtx.webkitImageSmoothingEnabled = false;
    this.tmpCtx.mozImageSmoothingEnabled = false;

    for (var i=0; i<6; i++) {
        var gridCanvas = c('canvas');       
        gridCanvas.width  = this.width;
        gridCanvas.height = this.height;

        this.canvases.push(gridCanvas);

        this.ctx.push(gridCanvas.getContext('2d'));
        this.ctx[i].imageSmoothingEnabled = false;
        this.ctx[i].webkitImageSmoothingEnabled = false;
        this.ctx[i].mozImageSmoothingEnabled = false;
    }

    this.initFont(0);
}

AccessGrantedController.prototype.cleanup = function() {
    if (this.interval) clearInterval(this.interval);
    if (this.pollTimeout) clearTimeout(this.pollTimeout);
    if (this.animateTimeout) clearTimeout(this.animateTimeout);
    if (this.redirectTimeout) clearTimeout(this.redirectTimeout);
    if (this.h4x0rTimeout) clearTimeout(this.h4x0rTimeout);
    this.animating = false;
    this.ctx = [];
    this.grid = [];
    this.wordQueue = [];
    this.canvases = [];
    this.letters = {};
}

AccessGrantedController.prototype.initFont = function(i) {
    this.ctx[0].font = this.FONT_SIZE + 'px serif';
    var text         = this.ctx[0].measureText('1');
    this.ctx[0].font = this.FONT_SIZE + 'px "whitrabt"';
    var text2        = this.ctx[0].measureText('1');

    // only proceed to the next step if the webfont is loaded
    if (text2.width != text.width || i == 20) {
        this.fontWidth = text2.width;

        // pre-cache mini-canvases for all the unique letters
        for (var j=0; j<this.WORDS.length; j++) {
            for (var k=0; k<this.WORDS[j].length; k++) {
                var letter = this.WORDS[j].charAt(k);
                if (!this.letters[letter]) {
                    var pre     = c('canvas'),
                        preCtx  = pre.getContext('2d');
                    pre.width           = this.fontWidth;
                    pre.height          = this.LINE_HEIGHT;
                    preCtx.font         = this.FONT_SIZE + 'px "whitrabt"';
                    preCtx.fillStyle    = this.COLOR;
                    preCtx.textBaseline = 'top';
                    preCtx.fillText(letter, 0, 0);
                    this.letters[letter] = pre;
                }
            }
        }
        // now start the grid animation
        return this.initGrid();
    }

    // maybe call this again if the webfont hasn't loaded yet.
    this.pollTimeout = setTimeout(function() {
        i++;
        this.initFont(i);
    }.bind(this), 100);
}

AccessGrantedController.prototype.initLock = function() {
    this.lockBody = c('canvas');
    this.lockBody.width = 700;
    this.lockBody.height = 490;

    var ctx = this.lockBody.getContext('2d');
    ctx.webkitImageSmoothingEnabled = false;
    ctx.imageSmoothingEnabled = false;

    var x = 50,
        y = 50,
        width = 600,
        height = 390,
        radius = 40,
        fill = 'red';

    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();
    ctx.fillStyle = 'red';
    ctx.shadowColor = 'red';
    ctx.shadowBlur = 40;
    ctx.fill();

    this.lockShackle = c('canvas');
    this.lockShackle.width = 700;
    this.lockShackle.height = 380;

    var ctx = this.lockShackle.getContext('2d');
    ctx.webkitImageSmoothingEnabled = false;
    ctx.imageSmoothingEnabled = false;

    ctx.beginPath();
    ctx.lineWidth = 75;
    ctx.arc(350, 260, 180, 0, Math.PI, true);
    ctx.lineTo(170, 380);
    ctx.strokeStyle = 'red';
    ctx.shadowColor = 'red';
    ctx.shadowBlur = 40;
    ctx.stroke();

    this.lockCtx.drawImage(this.lockBody, 0, 330);
    this.lockCtx.drawImage(this.lockShackle, 0, 120);
}

AccessGrantedController.prototype.animateLock = function() {
    var elapsedTime = new Date().getTime() - this.lockShackleStart,
        offset = 120 * (elapsedTime / this.lockShackleMs);

    if (offset > 120) offset = 120;

    this.lockCtx.clearRect(0, 0, 700, 820);
    this.lockCtx.drawImage(this.lockBody, 0, 330);
    this.lockCtx.drawImage(this.lockShackle, 0, 120 - offset);

    if (offset == 120) this.lockDoneAnimating = true;
}

AccessGrantedController.prototype.initGrid = function(fontWidth) {
    this.cols = Math.round(this.width/this.fontWidth);
    this.rows = Math.round(this.height/this.LINE_HEIGHT)+(this.EXTRA_ROWS*2);

    for (var i=0; i<this.ctx.length; i++) {
        var x = [];
        for (var j=0; j<this.cols; j++) {
            var y = [];
            for (var k=0; k<this.rows; k++) {
                y.push(0);
            };
            x.push(y);
        }
        this.grid.push(x);
    }

    this.interval = setInterval(this.signalGridUpdate.bind(this), this.DELAY);
    this.animateTimeout = setTimeout(function() {
        this.redirectTimeout = setTimeout(function() {
            if (user.id) notifier.notify('Welcome, '+user.username+'.');
            loader.go('/')
        }, 3000);
    }.bind(this), this.DELAY);

    this.gridStartTime = new Date().getTime();
    this.animateGridLoop();
}

AccessGrantedController.prototype.signalGridUpdate = function() {
    this.gridNeedsUpdate = true;
}

AccessGrantedController.prototype.animateGridLoop = function() {
    if (!this.animating) return false;
    requestAnimationFrame(function() {
        this.animateGrid();
        this.animateGridLoop();
    }.bind(this))
}

AccessGrantedController.prototype.animateGrid = function() {
    if (!this.animating) return false;

    var tmpCanvas  = this.tmpCanvas,
        tmpCtx     = this.tmpCtx,
        width      = this.width,
        height     = this.height;

    // only update the grid letters at the interval managed by gridNeedsUpdate
    if (this.gridNeedsUpdate) {
        this.gridNeedsUpdate = false;

        // "decay" each grid by fading to FADE_FACTOR
        for (var i=0; i<this.ctx.length; i++) {
            tmpCtx.clearRect(0, 0, width, height)
            tmpCtx.drawImage(this.canvases[i], 0, 0);
            this.ctx[i].clearRect(0, 0, width, width);
            this.ctx[i].globalAlpha = 1 - this.FADE_FACTOR;
            this.ctx[i].drawImage(tmpCanvas, 0, 0);
            this.ctx[i].globalAlpha = 1;

            // if (this.COLLIDE_OK == false) {
            //     for (var j=0; j<this.grid[i].length; j++) {
            //         for (var k=0; k<this.grid[i][j].length; k++) {
            //             if (this.grid[i][j][k] > 0) {
            //                 this.grid[i][j][k] = this.grid[i][j][k]-this.FADE_FACTOR
            //             }
            //         }
            //     }
            // }
        }
        // add a word to the queue if the queue is not full
        if (this.wordQueue.length < this.MAX_QUEUE) {
            for (var i=0; i<this.CONCURRENCY; i++) {
                var grid    = Math.floor(Math.random() * this.ctx.length),
                    wordIdx = Math.floor(Math.random() * this.WORDS.length),
                    word    = this.WORDS[wordIdx],
                    letters = [],
                    coords  = this.getOpenCoordinates(grid, 0);

                if (coords === -1) {
                    // log('recursion fail');
                    continue;
                }

                for (var j=0; j<word.length; j++) {
                    letters.push({
                        grid:   grid,
                        x:      coords[0],
                        y:      coords[1] + j,
                        letter: word.charAt(j)
                    });
                    this.grid[grid][coords[0]][coords[1] + j] = 1 + (j * this.FADE_FACTOR);
                }
                this.wordQueue.push(letters);
            }
        }

        // process our word queue
        var i = this.wordQueue.length;
        while (i--) {
            var letter = this.wordQueue[i].shift();
            var x = letter.x * this.fontWidth;
            var y = (-1 * this.EXTRA_ROWS * this.LINE_HEIGHT) + (letter.y * this.LINE_HEIGHT);
            this.ctx[letter.grid].drawImage(this.letters[letter.letter], x, y);

            if (this.wordQueue[i].length == 0) this.wordQueue.splice(i, 1);
        }
    }

    // now draw all the grids onto our main canvas, maybe apply a scale factor
    var elapsedTime = new Date().getTime() - this.gridStartTime,
        coefficient = 1 - (elapsedTime / 2000);

    if (coefficient < 0) coefficient = 0;

    var ctx = this.rainCtx;
    ctx.clearRect(0, 0, width, height);

    for (var i=0; i<6; i++) {
        if (coefficient) {
            switch (i) {
                case 0:
                    var scaleFactor = 2;
                    break;
                case 1:
                    var scaleFactor = 4;
                    break;
                case 2:
                    var scaleFactor = 8;
                    break;
                case 3:
                    var scaleFactor = 10;
                    break;
                case 4:
                    var scaleFactor = 12;
                    break;
                default:
                    var scaleFactor = 16;
                    break;
            };
            var multiplier = (coefficient * scaleFactor) + 1;
            ctx.setTransform(
                multiplier,
                0,
                0,
                multiplier,
                -1 * (multiplier - 1) * width / 2,
                -1 * (multiplier - 1) * height / 2
            );
        }
        ctx.drawImage(this.canvases[i], 0, 0);
        if (coefficient) {
            ctx.setTransform(1, 0, 0, 1, 0, 0);
        }
    }

    // if we're done zooming in and enhancing, then animate the lock opening up.
    if (coefficient == 0) {
        if (!this.lockShackleStart) {
            this.lockShackleStart = new Date().getTime();
            q(this.el, '.matrix').classList.add('h4x0r');
        }
        if (!this.lockDoneAnimating) {
            this.animateLock();
        }
    }
}

AccessGrantedController.prototype.getOpenCoordinates = function(grid, depth) {
    if (depth >= 10) return -1;
    var x = Math.floor(Math.random() * this.grid[grid].length),
        y = Math.floor(Math.random() * this.grid[grid][0].length);

    if (this.COLLIDE_OK) return [x, y];

    var i = grid;
    while (i < this.ctx.length) {
        if (this.grid[i][x][y] > 0)
            return this.getOpenCoordinates(grid, depth + 1);
        
        i++;
    }
    return [x, y];
}