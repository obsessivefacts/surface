var MemespeechController = function(el) {
    this.init(el);

    this.logoLastChanged = null;
    this.animatingPlaceholder = false;
    this.preventPlaceholderAnimation = false;
    this.constitutionPos = 0;
    this.placeholderQueueTimeout = null;
    this.plaintextBoxPos = 0;
    this.plaintextBoxFocused = false;
    this.placeholderText = '|';
    this.placeholderAnimationDirection = 'forward';
    this.memeIndex = -1;
    this.memes = [
        'Epstein didn\'t kill himself.',
        'Harambe had dirt on the Clintons.',
        'Ted Cruz is the Zodiac Killer.',
        'His name was S3th R!ch',
        'REEEEEEEEEEEEEEEEEEEEE'
    ];
    this.canvas = null;
    this.canvasCtx = null;
    this.ms = null;
    this.msBudgets = null;

    this.plaintextBox = q(this.el, 'input[name="plaintext"]');
    this.form = q(this.el, 'form[name="pride-and-accomplishment"]');
    this.constitution = q(this.el, 'div.constitution');
    this.carrierTextarea = q(this.el, 'textarea[name="carrier"]');

    this.sideLinks = qs(this.el, 'nav a');
    this.highlightedLink = '';

    // don't lose this lol
    this.ORIGINAL_CARRIER_TEXT = this.carrierTextarea.value;

    this.bind(this.form, 'submit', this.encrypt.bind(this));
    this.bind(this.plaintextBox, 'focus', this.boxFocus.bind(this));
    this.bind(this.plaintextBox, 'blur', this.boxBlur.bind(this));
    this.bind(this.plaintextBox, 'keyup', this.hasByteBudgetError.bind(this));
    this.bind(this.plaintextBox, 'change', this.hasByteBudgetError.bind(this));
    this.bind(window, 'resize', this.computeElementPositions.bind(this));

    this.initScrollEvents();
    this.initSideLinks();
    this.doHighlightLinks();

    // workaround for Safari's buggy position:sticky support...
    if (window.safari)
        for (let div of qs(this.el, '.side-nav nav'))
            div.classList.add('safari-hack');

    // handle chrome extension inline installation
    if (window.chrome) {
        this.handleChromeInlineInstaller();
    }

    return this;
}

inherit(MemespeechController, Page);
loader.registerController('memespeech', MemespeechController);

MemespeechController.prototype.computeElementPositions = function() {
    this.elOffsetHeight = this.el.offsetHeight;
    this.plaintextBoxPos = this.plaintextBox.offsetTop + 50;
    this.constitutionPos = this.constitution.offsetTop;
    if (window.safari) return; // hack to save cpu cuz Safari nav is sad anyway
    this.anchorMemespeechPos = q(this.el, 'h1.INFOWARS-DOT-COM').offsetTop - 40;
    this.anchorDemoPos = q(this.el, 'div.demo').offsetTop - 40;
    this.anchorExtensionPos = this.constitutionPos;
    this.anchorFAQPos = q(this.el, 'h3.faq').offsetTop + q(this.el, 'div.bottom').offsetTop - 40;
}

MemespeechController.prototype.initSideLinks = function() {
    for (let link of this.sideLinks) {
        this.bind(link, 'click', function(e) {
            if (e.target.hash === window.location.hash) {
                util.scrollToAnchor(this.el);
            }
        }.bind(this));
    }
}

MemespeechController.prototype.initScrollEvents = function() {
    this.computeElementPositions();
    this.bind(this.el, 'scroll', this.scrollHandler.bind(this), false, true);
}

MemespeechController.prototype.scrollHandler = function() {
    let scroll = this.el.scrollTop + this.elOffsetHeight;

    // if scrolled into view of text box, start animating placeholder text
    if (scroll > this.plaintextBoxPos && !this.animatingPlaceholder) {
        log('starting placeholder animation');
        this.queuePlaceholderAnimation();
    }

    this.doHighlightLinks();

    // animate the memespeech logo, with a debounce delay
    let now = new Date().getTime();
    if (this.logoLastChanged && now - this.logoLastChanged < 150)
        return;

    var infowars = qs(this.el, 'h1.INFOWARS-DOT-COM b');
    for (frog of infowars) {
        if (Math.random() > .5) {
            frog.classList.add('gay');
        } else {
            frog.classList.remove('gay');
        }
    }
    this.logoLastChanged = new Date().getTime();
}

MemespeechController.prototype.doHighlightLinks = function() {
    if (window.safari) return;
    
    let scrollTop = this.el.scrollTop,
        sideLinks = this.sideLinks;

    const _highlightLink = function(name) {
        if (name == this.highlightedLink) return;
        this.highlightedLink = name;

        for (const link of sideLinks) {
            if (link.hash == name) {
                link.parentNode.classList.add('sel');
            } else {
                link.parentNode.classList.remove('sel');
            }
        }
    }.bind(this);

    if (scrollTop >= this.anchorFAQPos) {
        _highlightLink('#faq');
    } else if (scrollTop >= this.anchorExtensionPos) {
        _highlightLink('#extension');
    } else if (scrollTop >= this.anchorDemoPos) {
        _highlightLink('#demo');
    } else if (scrollTop >= this.anchorMemespeechPos) {
        _highlightLink('#introducing-memespeech');
    } else {
        _highlightLink('#intro');
    }
};

MemespeechController.prototype.queuePlaceholderAnimation = function() {
    if (this.preventPlaceholderAnimation) return false;

    this.animatingPlaceholder = true;
    this.placeholderAnimationDirection = 'forward';

    this.memeIndex++;
    if (this.memeIndex === this.memes.length) this.memeIndex = 0;

    this.placeholderQueueTimeout = setTimeout(this.doPlaceholderAnimation.bind(this), 3000);
}

MemespeechController.prototype.doPlaceholderAnimation = function() {
    let meme = this.memes[this.memeIndex],
        tmpStr;

    if (this.placeholderAnimationDirection === 'forward') {
        let placeholderLength = this.placeholderText.length - 1,
            slicePos = Math.min(meme.length, placeholderLength + 1);

        tmpStr = meme.substr(0, slicePos) + '|';
    } else {
        tmpStr = this.placeholderText.substr(0, this.placeholderText.length - 2) + '|';
    }
    this.placeholderText = tmpStr;
    if (this.plaintextBoxFocused === false) {
        this.plaintextBox.placeholder = tmpStr;
    }

    if (this.placeholderAnimationDirection === 'forward') {
        if (tmpStr.length - 1 == meme.length) {
            this.placeholderAnimationDirection = 'backward';
            this.placeholderQueueTimeout = setTimeout(
                this.doPlaceholderAnimation.bind(this),
                2000
            );
        } else {
            this.placeholderQueueTimeout = setTimeout(
                this.doPlaceholderAnimation.bind(this),
                100
            );
        }
    } else {
        if (tmpStr.length - 1 == 0) {
            // start over
            this.queuePlaceholderAnimation();
        } else {
            this.placeholderQueueTimeout = setTimeout(
                this.doPlaceholderAnimation.bind(this),
                25
            );
        }
    }
}

MemespeechController.prototype.boxFocus = function() {
    this.plaintextBoxFocused = true;
    this.plaintextBox.placeholder = '';
}

MemespeechController.prototype.boxBlur = function() {
    this.plaintextBoxFocused = false;
    this.plaintextBox.placeholder = this.placeholderText;
}

MemespeechController.prototype.cleanup = function() {
    log('cleanup');

    if (this.placeholderQueueTimeout) {
        clearTimeout(this.placeholderQueueTimeout)
    }
    if (this.canvas) {
        this.canvas.parentNode.remove(this.canvas);
        this.canvas = null;
        this.canvasCtx = null;
    }
    let encryption = document.querySelector('._ms-encryption');
    if (encryption) {
        document.body.removeChild(encryption);
        this.el.classList.remove('blur');
    }
}

MemespeechController.prototype.hasByteBudgetError = function(e) {
    if (!this.ms) {
        this.ms = new Memespeech({ carrierText: this.ORIGINAL_CARRIER_TEXT });
        this.msBudgets = this.ms.computeAESByteBudget();
    }
    let error = q(this.el, '.encryption-error'),
        plaintext = this.plaintextBox.value,
        plaintextLength = Memespeech.DataUtils.stringToBytes(plaintext).length,
        budget = this.msBudgets.byteBudgetCompact - plaintextLength;

    if (budget > 69) {
        error.classList.add('hidden');
        return false;
    } else if (budget >= 0 && budget <= 69) {
        error.textContent = budget + ' bytes remaining for encryption';
        error.classList.remove('hidden');
        error.classList.add('warn');
        return false;
    } else {
        error.textContent = 'Please shorten your text by ' + (-1 * budget) + ' bytes';
        error.classList.remove('hidden', 'warn');
        return true;
    }
}

MemespeechController.prototype.encrypt = function(e) {
    stop(e);
    if (this.hasByteBudgetError()) return false;
    if (!this.plaintextBox.value) {
        let error = q(this.el, '.encryption-error');
        error.textContent = 'Please enter a message to encrypt!';
        error.classList.remove('hidden', 'warn');
        return false;
    }

    this.preventPlaceholderAnimation = true;

    return this.rerouteTheEncryption();
}

MemespeechController.prototype.rerouteTheEncryption = async function() {
    header.hide();

    let div = c('div'),
        wait  = ms => new Promise(resolve => setTimeout(() => resolve(), ms)),
        frame = cb => new Promise(resolve => requestAnimationFrame(() => {
                if (cb) cb();
                resolve();
            })),
        dumbLog = (text, centered) => {
            let el = c('strong');
            el.textContent = text;
            el.className = centered ? 'centered' : '';
            div.appendChild(el);
            div.scrollTop = div.scrollHeight;
            return el;
        },
        shuffle = (array) => {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
        };

    div.className = '_ms-encryption';
    document.body.appendChild(div);
    this.el.classList.add('blur');

    let msg = 'Your message was encrypted into the Bill of Rights with password: SANIC';
    q(this.el, '.constitution span').textContent = msg;

    dumbLog('Encrypting #YOLO');
    await wait(50);
    dumbLog('.');
    await wait(50);
    dumbLog('.');
    await wait(50);
    dumbLog('.');
    await wait(50);

    this.ms = new Memespeech({ carrierText: this.ORIGINAL_CARRIER_TEXT });

    let budgets = this.ms.computeAESByteBudget(),
        plaintext = this.plaintextBox.value,
        plaintextLength = Memespeech.DataUtils.stringToBytes(plaintext).length,
        mode = 'compact';

    if (budgets.byteBudgetStandard - plaintextLength >= 0)
        mode = 'standard'

    await this.ms.initCipherFromPassphrase('SANIC', mode);
    await this.ms.encrypt(plaintext);

    let payload = this.ms.cryptographicPayload || [],
        carrierText = this.ms.carrierText,
        matches = this.ms.carrierTextABCMatches,
        carrierChars = [];

    for (var i=0; i<carrierText.length; i++) {
        carrierChars.push({
            str: carrierText.charAt(i),
            carrierPos: i
        });
    }
    for (var i=0; i<matches.length; i++) {
        carrierChars[matches[i].index].matchPos = i;
    }
    shuffle(carrierChars);

    var carrierPos = 0,
        ciphertextPos = 0,
        binaryStr = '';

    for (var i=0; i<matches.length; i++) {
        let match = matches[i];

        for (; carrierPos <= match.index; carrierPos++) {
            if (carrierPos < match.index) {
                binaryStr += carrierText[carrierPos];
            } else {
                if (ciphertextPos < payload.length) {
                    binaryStr += payload[ciphertextPos] == 0 ? 0 : 1;
                    ciphertextPos++
                } else {
                    binaryStr += Math.random() < .5 ? 0 : 1;
                }
            }
        }
    }
    for (; carrierPos < carrierText.length; carrierPos++) {
        binaryStr += carrierText[carrierPos];
    }
    this.carrierTextarea.value = binaryStr;

    let ciphertext = payload.join(''),
        charsPerFrame = charLimit = 16,
        text = dumbLog(ciphertext.substr(0, charLimit));

    while (charLimit < ciphertext.length) {
        await frame();
        charLimit += charsPerFrame;
        text.textContent = ciphertext.substr(0, charLimit);
        div.scrollTop = div.scrollHeight;
    }

    await wait(50);
    dumbLog('.', true);
    await wait(50);
    dumbLog('.', true);
    await wait(50);
    dumbLog('.', true);
    await wait(50);
    dumbLog('Encryption complete!', true);
    await wait(50);
    dumbLog('\xa0', true);
    await wait(50);
    dumbLog('\xa0', true);
    await wait(250);
    document.body.removeChild(div);
    this.el.classList.remove('blur');
    this.carrierTextarea.scrollTop = 0;

    await wait(50);

    this.computeElementPositions();

    let scroll = this.el.scrollTop,
        cp = this.constitutionPos,
        isSmoothScrollSupported = 'scrollBehavior' in this.el.style;

    if (isSmoothScrollSupported) {
        this.el.scrollTo({
            behavior: 'smooth',
            left: 0,
            top: cp
        });
    } else {
        // fake smoothscroll for browsers that don't support this yet
        while (scroll < cp) {
            await frame(() => {
                scroll = scroll + ((cp - scroll)/7.5);
                if (scroll > cp - 1) scroll = cp;
                this.el.scrollTop = scroll;
            });
        }
    }

    await wait(1000);

    const _replaceAt = (index, replacement) => {
        let val = this.carrierTextarea.value;
        this.carrierTextarea.value = val.substr(0, index)
            + replacement
            + val.substr(index + replacement.length);
    };

    const FRAMES = 120;
    
    let replacementsPerFrame = Math.ceil(carrierText.length / FRAMES),
        j = 0;

    while (carrierChars.length) {
        if (j++ % replacementsPerFrame == 0) await frame();

        let char = carrierChars.pop();

        if (
            typeof char.matchPos !== "undefined"
            &&
            char.matchPos < payload.length
        ) {
            if (payload[char.matchPos] == 0) {
                _replaceAt(char.carrierPos, char.str.toLowerCase());
            } else {
                _replaceAt(char.carrierPos, char.str.toUpperCase());
            }
        } else {
            if (Math.random() < .5) {
                _replaceAt(char.carrierPos, char.str.toLowerCase());
            } else {
                _replaceAt(char.carrierPos, char.str.toUpperCase());
            }
        }
    }
}

MemespeechController.prototype.handleChromeInlineInstaller = function() {
    for (let span of qs(this.el, 'span.browser-name'))
        span.textContent = 'Chrome';

    for (let button of qs(this.el, 'a.install-extension')) {
        let img = q(button, 'img'),
            span = q(button, 'span');

        button.href = 'https://chrome.google.com/webstore/detail/memespeech/mpabmihiimmhjiiloblccfeoomaojhof';
        img.src = (CONFIG.cdnSurface ? CONFIG.cdnSurface : '')
            + '/images/memespeech/chrome-logo.png';
        if (button.parentNode.classList.contains('install-area'))
            span.textContent = 'Install Memespeech for Chrome';
        else
            span.textContent = 'Add to Chrome';
    }
}