var FmiffelPage = function(el) {
    this.init(el);

    this.searchParams = null;

    this.posts = q(this.el, 'ol.posts');
    if (this.posts) { this.initializeSearchParams(); }

    this.searchForm = q(this.el, 'form[name="search"]');
    if (this.searchForm) { this.initializeSearchForm(); }

    this.postForm = q(this.el, 'form[name="twit"]');
    if (this.postForm) { this.initializePostForm(); }

    this.settingsForm = q(this.el, 'form[name="billy"]');
    if (this.settingsForm) { this.initializeSettingsForm(); }

    if (q(this.el, 'div.toggle')) { this.toggleFollowingInfo(true); }

    var unreadReplies = q(this.el, 'span.unread-replies');
    if (unreadReplies && unreadReplies.textContent) {
        notifier.notify('You have unread replies.');
    }

    this.bind(this.el, 'click', this.clickHandler.bind(this));
    
    return this;
}

inherit(FmiffelPage, Page);
loader.registerController('fmiffel', FmiffelPage);

FmiffelPage.prototype.initializeSearchParams = function() {
    var _bool = function(param) {
        return param.toLowerCase() == "true" ? true : false;
    };
    this.searchParams = {
        userId: parseInt(this.posts.dataset.userid),
        showUserPosts: _bool(this.posts.dataset.showuserposts),
        showFollowedPosts: _bool(this.posts.dataset.showfollowedposts),
        showAtPosts: _bool(this.posts.dataset.showatposts),
        showFavorites: _bool(this.posts.dataset.showfavorites),
        searchString: decodeURIComponent(this.posts.dataset.searchstring),
        limit: parseInt(this.posts.dataset.limit),
        pageSize: parseInt(this.posts.dataset.pagesize)
    };
}

FmiffelPage.prototype.initializeSearchForm = function() {
    this.bind(this.searchForm, 'submit', function(e) {
        stop(e);
        var searchStr = encodeURIComponent(q(this.searchForm, 'input.search').value);
        if (window.location.pathname == '/fmiffel/search') {
            history.replaceState({force_load: true}, document.title, window.location.href);
        }
        loader.go('/fmiffel/search?searchString=' + searchStr);
        history.replaceState({force_load: true}, document.title, window.location.href);
        return false;
    }.bind(this));
}

FmiffelPage.prototype.initializePostForm = function() {
    this.textarea = q(this.el, 'textarea');
    this.replyToUserId = q(this.el, 'input[name="replyToUserId"]');
    this.replyToPostId = q(this.el, 'input[name="replyToPostId"]');
    this.counter = q(this.el, 'p.counter');
    this.submitButton = q(this.postForm, 'input[type="submit"]');

    if (window.location.search.indexOf('replyToPostId') !== -1) {
        this.focusTextarea();
    }

    this.bind(this.textarea, 'keyup', this.updateCounter.bind(this));
    this.bind(this.textarea, 'change', this.updateCounter.bind(this));
    this.bind(this.textarea, 'keyup', this.updateTextareaSize.bind(this));
    this.bind(this.textarea, 'focus', this.updateTextareaSize.bind(this));
    this.bind(this.textarea, 'blur', this.blurTextarea.bind(this));

    this.counter.classList.remove('hidden');
    this.updateCounter();

    this.bind(this.postForm, 'submit', function(e) {
        stop(e);
        var xhr  = new XMLHttpRequest(),
            d    = new FormData(this.postForm);

        d.append('json', true);

        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                this.submitButton.disabled = false;
                var json = JSON.parse(xhr.response);

                if (json.error) {
                    var h1 = q(this.el, 'h1.box_caption');
                    h1.textContent = json.error;
                    h1.classList.add('error');
                } else {
                    if (json.path) {
                        loader.go(json.path);
                    } else {
                        var profileA = q(this.el, 'div.side div.pad.avatar a');
                        loader.go(profileA.href);
                    }
                }
            }
        }.bind(this);

        this.submitButton.disabled = true;
        xhr.open("post", this.postForm.action, true);
        xhr.send(d);
    }.bind(this));
}

FmiffelPage.prototype.initializeSettingsForm = function() {
    this.submitButton = q(this.settingsForm, 'button[type="submit"]');
    this.bind(this.settingsForm, 'submit', function(e) {
        stop(e);
        var xhr  = new XMLHttpRequest(),
            d    = new FormData(this.settingsForm);

        d.append('json', true);

        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                this.submitButton.disabled = false;
                var json = JSON.parse(xhr.response);
                if (json.npc) return this.showNPCError();
                if (json.errors) {
                    var possibleErrors = ['url', 'bio', 'location'];
                    for (var i=0; i<possibleErrors.length; i++) {
                        if (json.errors[possibleErrors[i]]) {
                            var p = q(this.el, 'p.' + possibleErrors[i]);
                            p.classList.add('error');
                            p.textContent = json.errors[possibleErrors[i]];
                        }
                    }
                } else {
                    notifier.notify('Successfully updated your profile!');
                    loader.go(json.path);
                }
            }
        }.bind(this);

        this.submitButton.disabled = true;
        xhr.open("post", this.settingsForm.action, true);
        xhr.send(d);
    }.bind(this));
}

FmiffelPage.prototype.clickHandler = function(e) {
    var a = util.findTarget(e.target, 'a');
    if (a) {
        if (a.classList.contains('more')) {
            stop(e);
            this.loadMorePosts();
        }
        else if (a.classList.contains('follow') || a.classList.contains('unfollow')) {
            stop(e);
            this.toggleFollow(a);
        }
        else if (a.classList.contains('favorite')) {
            stop(e);
            this.toggleFavorite(a);
        }
        else if (a.classList.contains('reply') && this.postForm) {
            stop(e);
            this.setupPostReply(a);
        }
        else if (a.classList.contains('delete')) {
            stop(e);
            this.deletePost(a);
        }
    }
    var div = util.findTarget(e.target, 'div');
    if (div) {
        if (div.classList.contains('toggle')) {
            stop(e);
            this.toggleFollowingInfo();
        }
    }
    return true;
}

FmiffelPage.prototype.loadMorePosts = function() {
    var lastPostId = parseInt(this.posts.lastChild.dataset.id.substr(5)),
        xhr = new XMLHttpRequest(),
        d   = new FormData();        

    d.append('userId', this.searchParams.userId);
    d.append('showUserPosts', this.searchParams.showUserPosts);
    d.append('showFollowedPosts', this.searchParams.showFollowedPosts);
    d.append('showAtPosts', this.searchParams.showAtPosts);
    d.append('showFavorites', this.searchParams.showFavorites);
    d.append('searchString', this.searchParams.searchString);
    d.append('afterId', lastPostId);
    d.append('json', true);      

    var queryString = new URLSearchParams(d).toString();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var json = JSON.parse(xhr.response);
            this.posts.innerHTML += json.posts;
            var loaded = qs(this.posts, 'li');
            if (loaded.length == json.count) {
                q(this.el, 'a.more').style.display = 'none';
            }
        }
    }.bind(this);
    xhr.open('get', '/fmiffel/search?' + queryString, true);
    xhr.send();
}

FmiffelPage.prototype.toggleFollowingInfo = function(initialHide) {
    var toggle = q(this.el, 'div.toggle');
    var info = q(this.el, 'div.info');
    toggle.classList.toggle('sel', initialHide ? false : undefined);
    info.classList.toggle('sel', initialHide ? false : undefined);
}

FmiffelPage.prototype.toggleFollow = function(a) {
    var url = a.href,
        action = url.indexOf('unfollow') !== -1 ? 'unfollow' : 'follow',
        xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var json = JSON.parse(xhr.response);
            if (json.error) return this.showNPCError();

            if (action == 'unfollow') {
                url = url.replace('unfollow', 'follow');
            } else {
                url = url.replace('follow', 'unfollow');
            }

            if (q(this.el, 'ol.follow')) {
                var span = a.parentNode,
                    username = span.dataset.fmiffelUsernameFormatted;

                while(span.firstChild) { span.removeChild(span.firstChild); }

                if (q(this.el, 'ol.follow.is-me')) {
                    var countSpan = q(this.el, '.following-count'),
                        count = parseInt(countSpan.textContent);
                    if (!isNaN(count)) {
                        if (action == 'follow') {
                            count += 1;
                        } else {
                            count -= 1;
                        }
                        countSpan.textContent = count;
                        var summaryEm = q(this.el, '.follow-summary');
                        if (summaryEm.textContent.indexOf('Follower') === -1) {
                            if (count == 1) {
                                var summary = 'You follow 1 person.';
                            } else {
                                var summary = 'You follow '+count+' people.';
                            }
                            summaryEm.textContent = summary;
                        }
                    }
                }

                if (action == 'unfollow') {
                    var newA = c('a');
                    newA.className = 'follow';
                    newA.href = url;
                    newA.textContent = 'follow';

                    span.appendChild(newA); 
                } else {
                    var img = c('img'), em = c('em'), newA = c('a');

                    img.className = 'check';
                    img.src = (CONFIG.cdnSurface ? CONFIG.cdnSurface : '')
                              + '/images/fmiffel/check_follow.png';
                    em.textContent = 'You are following ' + username + ' ';
                    newA.className = 'unfollow';
                    newA.href = url;
                    newA.textContent = 'remove';

                    span.appendChild(img);
                    span.appendChild(em);
                    span.appendChild(newA);
                }
            } else {
                if (action == 'unfollow') {
                    q(this.el, 'div.info').classList.remove('sel');
                    var toggle = q(this.el, 'div.toggle'),
                        toggleParent = toggle.parentNode;

                    toggleParent.removeChild(toggle);

                    var newA = c('a');
                    newA.className = 'follow';
                    newA.href = url;
                    newA.textContent = 'Follow';

                    toggleParent.appendChild(newA); 

                    var countSpan = q(this.el, '.follower-count'),
                        count = parseInt(countSpan.textContent);
                    if (!isNaN(count)) { countSpan.textContent = count - 1; }
                } else {
                    var toggle = q(this.el, 'a.follow'),
                        toggleParent = toggle.parentNode;

                    toggleParent.removeChild(toggle);

                    var div = c('div'), img = c('img'), span = c('span');

                    div.className = 'toggle';
                    img.src = (CONFIG.cdnSurface ? CONFIG.cdnSurface : '')
                              + '/images/fmiffel/check.png';
                    img.width = 8;
                    img.height = 8;
                    span.textContent = 'Following';

                    div.appendChild(img);
                    div.appendChild(span);
                    toggleParent.appendChild(div);

                    var countSpan = q(this.el, '.follower-count'),
                        count = parseInt(countSpan.textContent);
                    if (!isNaN(count)) { countSpan.textContent = count + 1; }
                }
            }
        }
    }.bind(this);
    xhr.open('get', url + '?json=true', true);
    xhr.send();
}

FmiffelPage.prototype.toggleFavorite = function(a) {
    var url = a.href,
        action = url.indexOf('e/fd') !== -1 ? 'unfavorite' : 'favorite',
        xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var json = JSON.parse(xhr.response),
                img = q(a, 'img');

            if (action == 'unfavorite') {
                a.href  = url.replace('e/fd', 'e/fa');
                img.src = (CONFIG.cdnSurface ? CONFIG.cdnSurface : '')
                              + '/images/fmiffel/favorite_off.png';
                img.classList.remove('sel');

                if (q(this.el, 'ol.posts.favorites.is-me')) {
                    var postId = a.dataset.id.substr(2),
                        postEl = q(this.el, 'li[data-id="post-' + postId + '"]');
                    postEl.parentNode.removeChild(postEl);
                }
            } else {
                a.href  = url.replace('e/fa', 'e/fd');
                img.src = (CONFIG.cdnSurface ? CONFIG.cdnSurface : '')
                              + '/images/fmiffel/favorite_on.png';
                img.classList.add('sel');
            }
        }
    }.bind(this);
    xhr.open('get', url + '?json=true', true);
    xhr.send();
}

FmiffelPage.prototype.showNPCError = function() {
    this.el.scrollTo(0, 0);
    var omg = q(this.el, '.omg');
    omg.classList.add('npc-fail');
    setTimeout(function() { omg.classList.remove('npc-fail'); }, 3000);
}

FmiffelPage.prototype.focusTextarea = function() {
    var _val = this.textarea.value;
    this.textarea.focus();
    this.textarea.value = '';
    this.textarea.value = _val;
    this.textarea.value.dirty = true;
}

FmiffelPage.prototype.setupPostReply = function(a) {
    var id = a.dataset.id.substr(2),
        username = a.dataset.username,
        userId = a.dataset.userId,
        postText = q(this.el, 'li[data-id="post-' + id + '"] div.post-text'),
        replyHintContainer = q(this.el, '.reply-hint-container');

    while (replyHintContainer.firstChild) {
        replyHintContainer.removeChild(replyHintContainer.firstChild);
    }

    var div = c('div'), p = c('p'), strong = c('strong'), a = c('a'), span = c('span');

    div.className = 'reply-hint post-text';
    a.href = '/fmiffel/' + username;
    a.textContent = username;
    span.textContent = ' wrote:';

    strong.appendChild(a);
    strong.appendChild(span);
    p.appendChild(strong);
    div.appendChild(p);

    for(var child=postText.firstChild; child!==null; child=child.nextSibling) {
        div.appendChild(child.cloneNode(true));
    }
    replyHintContainer.appendChild(div);

    this.replyToUserId.value = userId;
    this.replyToPostId.value = id;
    this.textarea.value = '@' + username + ' ';
    this.el.scrollTo(0, 0);
    this.focusTextarea();
    this.updateCounter();
}

FmiffelPage.prototype.updateCounter = function() {
    var counter = -140 + this.textarea.value.length;
    this.counter.textContent = util.numberWithCommas(counter);
    if (counter >= 0) {
        this.counter.classList.add('ok');
    } else {
        this.counter.classList.remove('ok');
    }
}

FmiffelPage.prototype.updateTextareaSize = function(e) {
    var textarea = e.target;
    textarea.style.height = 'inherit';

    var c = window.getComputedStyle(textarea);

    var height = parseInt(c.getPropertyValue('border-top-width'), 10)
                 + parseInt(c.getPropertyValue('padding-top'), 10)
                 + textarea.scrollHeight
                 + parseInt(c.getPropertyValue('padding-bottom'), 10)
                 + parseInt(c.getPropertyValue('border-bottom-width'), 10);

    textarea.style.height = height + 'px';
}

FmiffelPage.prototype.blurTextarea = function(e) {
    if (e.target.value == '') { e.target.style.removeProperty('height'); }
}

FmiffelPage.prototype.deletePost = function(a) {
    var postId = parseInt(a.dataset.id.substr(2)),
        postEl = q(this.el, 'li[data-id="post-' + postId + '"]');

    if (!confirm('Are you sure you wish to delete this post?\n\nThis cannot be undone!')) {
        return false;
    }

    var url = a.href,
        xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            var json = JSON.parse(xhr.response);
            if (json.error) return this.showNPCError();
            postEl.parentNode.removeChild(postEl);
        }
    }.bind(this);
    xhr.open('get', url + '?json=true', true);
    xhr.send();
}