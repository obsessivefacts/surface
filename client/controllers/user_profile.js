var UserProfileController = function(el) {
    this.init(el);
    this.initForm();

    var logout = q(this.el, '.logout');
    if (logout) 
        this.bind(logout, 'click', userControl.clickLogout);

    return this;
}

inherit(UserProfileController, Page);
loader.registerController('user-profile', UserProfileController);
loader.registerController('user-edit', UserProfileController);

UserProfileController.prototype.initForm = function() {
    this.form = q(this.el, 'form')
    if (!this.form) return;

    this.changePassword = qs(this.el, 'input[name="change_password"]');
    this.avatar = q(this.el, 'input[name="avatar"]');

    for (var i=0; i<this.changePassword.length; i++)
        this.bind(this.changePassword[i], 'change',
            this.handlePasswordChangeState.bind(this));

    this.handlePasswordChangeState();

    this.bind(this.avatar, 'change', this.handleAvatarChange.bind(this));
    this.avatar.style.display = 'none';

    this.bind(q(this.el, 'button[type="reset"]'), 'click',
        this.handleReset.bind(this));

    this.bind(this.form, 'submit', function(e) {
        stop(e);
        var xhr  = new XMLHttpRequest(),
            d    = new FormData(this.form),
            butt = q(this.el, 'button[type="submit"]');
        d.append('json', true);
        butt.disabled = true;
        butt.classList.add('disabled');
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                butt.disabled = false;
                butt.classList.remove('disabled');
                var json = JSON.parse(xhr.response);
                if (xhr.status != 200) {
                    var buttons = q(this.el, '.submit-thingy'),
                        errors = q(buttons, '.errors');
                    if (!errors) {
                        errors = c('ul');
                        errors.className = 'errors';
                        buttons.insertBefore(errors, buttons.firstChild);
                    }
                    while (errors.childNodes.length)
                        errors.removeChild(errors.childNodes[0]);
                    for (var i=0; i<json.errors.length; i++) {
                        var error = c('li');
                        error.textContent = json.errors[i];
                        errors.appendChild(error);
                    }
                } else {
                    notifier.notify('Your changes have been saved.');
                    loader.go(this.form.dataset.backUrl);
                }
            }
        }.bind(this);
        xhr.open("post", this.form.action, true);
        xhr.send(d);
        
    }.bind(this));
}

UserProfileController.prototype.handlePasswordChangeState = function(e) {
    var doDisable = !q(this.el, '#do_change').checked,
        table     = q(this.el, 'table#change_form'),
        inputs    = qs(table, 'input');
    
    if (doDisable)
        table.classList.add('disabled');
    else
        table.classList.remove('disabled');

    for (var i=0; i<inputs.length; i++) inputs[i].disabled = doDisable;
}

UserProfileController.prototype.handleAvatarChange = function(e) {
    var input = this.avatar;
    if (!input.files || !input.files[0]) return;

    var reader = new FileReader();
    reader.onload = function(e) {
        var target = q(this.el, 'div.avatar');
        target.style.backgroundImage = 'url('+e.target.result+')';
        target.style.backgroundPosition = 'center center';
        target.style.backgroundSize = 'auto 100%';
    }.bind(this);

    reader.readAsDataURL(input.files[0]);
}

UserProfileController.prototype.handleReset = function(e) {
    e.preventDefault();
    notifier.notify('Then just forget it.');
    loader.go(this.form.dataset.backUrl);
    return false;
};