var ContactController = function(el) {
    this.init(el);

    this.faqdrop = q(this.el, '.faqdrop');
    loader.registerInternalPath(/^\/contact$/, this.contactPage.bind(this));
    loader.registerInternalPath(/^\/contact\/faq$/, this.faqPage.bind(this));

    return this;
}

inherit(ContactController, Page);
loader.registerController('contact', ContactController);

ContactController.prototype.contactPage = function(args) {
    this.faqdrop.classList.remove('active');
}

ContactController.prototype.faqPage = function(args) {
    this.faqdrop.classList.add('active');
}