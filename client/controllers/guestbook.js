var GuestbookPage = function(el) {
    this.init(el);

    socket.subscribe('guestbook', this.receiveSocketMessage.bind(this));

    this.messageLastSentTimestamp = null;
    this.messageAutosendInterval = 500;
    this.readyToReceiveMessages = false;
    this.messageQueue = {
        radius: null,
        blur: null,
        r: null,
        g: null,
        b: null,
        coords: []
    };
    
    this.initializeDrawingState();
    this.initializeMenus();
    this.loadCanvasState();
    
    return this;
}

inherit(GuestbookPage, Page);
loader.registerController('guestbook', GuestbookPage);

GuestbookPage.prototype.initializeDrawingState = function() {
    this.down = false;
    this.radius = 5;
    this.blur = 0;
    this.red = 0;
    this.green = 0;
    this.blue = 0;
    this.swatch = q(this.el, 'a.color');
    this.inputColor = q(this.el, 'input[type="color"]');

    while (
        (this.red == 0 && this.green == 0 && this.blue == 0)
        ||
        (this.red == 255 && this.green == 255 && this.blue == 255)
    ) {
        this.red = [0, 128, 255][Math.floor(Math.random() * 3)];
        this.green = [0, 128, 255][Math.floor(Math.random() * 3)];
        this.blue = [0, 128, 255][Math.floor(Math.random() * 3)];
    }

    this.bind(document.body, 'mousedown', this.mousedown.bind(this));
    this.bind(document.body, 'mousemove', this.mousemove.bind(this));
    this.bind(document.body, 'mouseup', this.mouseup.bind(this));
    this.bind(this.swatch, 'click', this.selectColor.bind(this));
    this.bind(this.inputColor, 'change', this.setInputColor.bind(this));

    var hex = "#"+((1<<24)+(this.red<<16)+(this.green<<8)+this.blue).toString(16).slice(1);
    this.inputColor.value = hex;
    this.inputColor.setAttribute('value', hex);

    this.setSwatchColor();
}

GuestbookPage.prototype.loadCanvasState = function() {
    this.loadingCover = q(this.el, '.loading');

    var img = q(this.el, 'img.initial-canvas-state');
    if (!img) return this.initializeCanvas();

    this.canvasImg = img;
    this.beginTimestamp = img.src.replace(/^.*[\\\/]/, '').replace(/\..*$/, '');

    this.loadingCover.classList.add('active');

    var isLoaded = function() {
        this.initializeCanvas();
    }.bind(this);

    if (this.canvasImg.complete) return isLoaded();
    this.bind(this.canvasImg, 'load', isLoaded)
}

GuestbookPage.prototype.initializeCanvas = function() {
    this.canvas = q(this.el, 'canvas.guestbook');
    this.canvas.width = 1440;
    this.canvas.height = 900;
    
    this.ctx    = this.canvas.getContext('2d');

    if (typeof this.canvasImg !== "undefined") {
        this.ctx.drawImage(this.canvasImg, 0, 0, 1440, 900);
        this.canvasImg.classList.add('hidden');
    }

    this.canvas.classList.add('active');
    this.loadingCover.classList.remove('active');

    this.readyToReceiveMessages = true;

    if (typeof this.beginTimestamp !== "undefined") {
        socket.send('guestbook', 'q;'+this.beginTimestamp);
    }

    this.bind(this.canvas, 'touchstart', this.touchstart.bind(this));
    this.bind(this.canvas, 'touchmove', this.touchmove.bind(this));
    this.bind(this.canvas, 'touchend', this.touchend.bind(this));
}

GuestbookPage.prototype.cleanup = function() {
    socket.unsubscribe('guestbook');
}

GuestbookPage.prototype.queueSocketMessage = function(x, y, radius, blur, r, g, b) {
    var queue = this.messageQueue,
        timestamp = new Date().getTime();

    if (queue.coords.length && (radius != queue.radius || blur != queue.blur
        || r != queue.r || g != queue.g || b != queue.b)) {
        this.sendQueuedSocketMessages();
    }
    queue.radius = radius;
    queue.blur = blur;
    queue.r = r;
    queue.g = g;
    queue.b = b;
    queue.coords.push(x+','+y);

    if (!this.messageLastSentTimestamp)
        this.messageLastSentTimestamp = timestamp;

    if (timestamp - this.messageLastSentTimestamp > this.messageAutosendInterval)
        this.sendQueuedSocketMessages();
}

GuestbookPage.prototype.sendQueuedSocketMessages = function() {
    var q = this.messageQueue;

    if (!q.coords.length) return;

    var coords = q.coords.join(';')
        msg = 'd;'+q.radius+';'+q.blur+';'+q.r+';'+q.g+';'+q.b+';'+coords;

    socket.send('guestbook', msg);
    q.coords = [];
    this.messageLastSentTimestamp = new Date().getTime();
}

GuestbookPage.prototype.receiveSocketMessage = function(msg) {
    if (!this.readyToReceiveMessages) {
        log('readystate: ', this.readyToReceiveMessages);
        return console.error('guestbook discarding message (not ready): ', msg);
    }

    var components = msg.toString().split(/:(.+)/);
    if (components.length < 2)
        return console.error('GUESTBOOK BAD MESSAGE: ', msg);

    log('guestbook received message: ', components);

    switch (components[0]) {
        case 'bd':
            this.bulkDraw(components[1]);
            break;
        default:
            console.warn('guestbook unsupported message: ', msg);
            break;
    }
}

GuestbookPage.prototype.mousedown = function(e) {
    this.down = true;
    if (!util.findTarget(e.target, 'menu')) {
        this.sizeMenu.classList.remove('open');
        this.blurMenu.classList.remove('open');
    }
    this.handleMouseDrawEvent(e);
}

GuestbookPage.prototype.mousemove = function(e) {
    if (!this.down) return;
    if (typeof e.buttons !== "undefined" && e.buttons !== 1)
        return this.mouseup(e);
    this.handleMouseDrawEvent(e);
}

GuestbookPage.prototype.mouseup = function(e) {
    this.down = false;
    this.sendQueuedSocketMessages();
}

GuestbookPage.prototype.handleMouseDrawEvent = function(e) {
    if (e.target !== this.canvas) return;
    var bounds = this.canvas.getBoundingClientRect(),
        x = e.clientX - bounds.x,
        y = e.clientY;

    this.brush(this.ctx, x, y, this.radius, this.blur, this.red, this.green, this.blue);
    this.queueSocketMessage(x, y, this.radius, this.blur, this.red, this.green, this.blue);
}

GuestbookPage.prototype.touchstart = function(e) {
    e.preventDefault();
    this.sizeMenu.classList.remove('open');
    this.blurMenu.classList.remove('open');
    this.handleTouchDrawEvent(e);
}

GuestbookPage.prototype.touchmove = function(e) {
    e.preventDefault();
    this.handleTouchDrawEvent(e);
}

GuestbookPage.prototype.touchend = function(e) {
    e.preventDefault();
    this.sendQueuedSocketMessages();
}

GuestbookPage.prototype.handleTouchDrawEvent = function(e) {
    var bounds = this.canvas.getBoundingClientRect(),
        x = e.touches[0].clientX - bounds.x,
        y = e.touches[0].clientY;

    this.brush(this.ctx, x, y, this.radius, this.blur, this.red, this.green, this.blue);
    this.queueSocketMessage(x, y, this.radius, this.blur, this.red, this.green, this.blue);
}

GuestbookPage.prototype.selectColor = function(e) {
    stop(e);
    this.inputColor.click();
}

GuestbookPage.prototype.setInputColor = function(e) {
    var hex = this.inputColor.value,
        hexreg = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    this.red = parseInt(hexreg[1], 16);
    this.green = parseInt(hexreg[2], 16);
    this.blue = parseInt(hexreg[3], 16);

    this.setSwatchColor();
}

GuestbookPage.prototype.setSwatchColor = function() {
    this.swatch.style.backgroundColor = 'rgba('+this.red+','+this.green+','+this.blue+',1)';
}

GuestbookPage.prototype.initializeMenus = function() {
    var brushes = qs(this.el, '.select.size a'),
        blurs   = qs(this.el, '.select.blur a'),
        selects = qs(this.el, 'input[type="text"]');

    this.sizeMenu = q(this.el, 'menu.size');
    this.blurMenu = q(this.el, 'menu.blur');

    q(this.el, '.tools').classList.add('active');
    q(this.el, 'input[data-rel="size"]').value = this.radius + ' px';
    q(this.el, 'input[data-rel="blur"]').value = this.blur + '%';

    for (var i=0; i<brushes.length; i++) {
        var canvas1 = c('canvas'),
            canvas2 = c('canvas'),
            context1 = canvas1.getContext('2d'),
            context2 = canvas2.getContext('2d'),
            x = 18,
            y = 15;

        canvas1.width = canvas2.width = 38;
        canvas1.height = canvas2.height = 30;

        this.brush(context1, x, y, i+1, 0, 255, 255, 255);
        this.brush(context2, x, y, 10, blurs[i].dataset.blur, 255, 255, 255);

        brushes[i].appendChild(canvas1);
        blurs[i].appendChild(canvas2);

        if (parseInt(brushes[i].dataset.size) == this.radius)
            brushes[i].classList.add('sel');

        if (parseInt(blurs[i].dataset.blur) == this.blur)
            blurs[i].classList.add('sel');

        this.bind(brushes[i], 'click', this.selectSize.bind(this));
        this.bind(blurs[i], 'click', this.selectBlur.bind(this));
    }

    var _bindSelect = function(select) {
        this.bind(select, 'focus', function() {
            this.toggleMenu(select);
        }.bind(this))
    }.bind(this);
    for (var i=0; i<selects.length; i++) { _bindSelect(selects[i]) }
}

GuestbookPage.prototype.toggleMenu = function(input) {
    input.blur();
    var menu = q(this.el, 'menu.select.' + input.dataset.rel);
    if (menu.classList.contains('open'))
        menu.classList.remove('open');
    else
        menu.classList.add('open');
}

GuestbookPage.prototype.selectSize = function(e) {
    e.preventDefault();

    var brushes = qs(this.el, '.select.size a');
    for (var i=0; i<brushes.length; i++) brushes[i].classList.remove('sel');

    var a = util.findTarget(e.target, 'a'),
        val = parseInt(a.dataset.size);

    a.classList.add('sel');
    q(this.el, 'menu.select.size').classList.remove('open');

    if (!isNaN(val) && val >= 1 && val <= 10) {
        this.radius = val;
        q(this.el, 'input[data-rel="size"]').value = val + ' px';
    }
}

GuestbookPage.prototype.selectBlur = function(e) {
    e.preventDefault();

    var blurs = qs(this.el, '.select.blur a');
    for (var i=0; i<blurs.length; i++) blurs[i].classList.remove('sel');

    var a = util.findTarget(e.target, 'a'),
        val = parseInt(a.dataset.blur);

    a.classList.add('sel');
    q(this.el, 'menu.select.blur').classList.remove('open');

    if (!isNaN(val) && val >= 0 && val <= 100 && val % 10 == 0) {
        this.blur = val;
        q(this.el, 'input[data-rel="blur"]').value = val + '%';
    }
}

GuestbookPage.prototype.brush = function(context, x, y, radius, blur, r, g, b) {
    var blurSize = radius - (((blur/100) * radius) || (radius == 1 ? .001 : 1)),
        radgrad = context.createRadialGradient(x, y, blurSize, x, y, radius);
        radgrad.addColorStop(0, 'rgba('+r+','+g+','+b+',1)');
        radgrad.addColorStop(1, 'rgba('+r+','+g+','+b+',0)');
        radgrad.addColorStop(1, 'rgba(255,255,255,0)');
        context.fillStyle = radgrad;
        context.fillRect(x - radius, y - radius, radius * 2, radius * 2);
}

GuestbookPage.prototype.bulkDraw = function(commands) {
    var commands = commands.split(':');
    for (var i=0; i<commands.length; i++) {
        var command = commands[i].split(';'),
            radius = parseInt(command[1]),
            blur = parseInt(command[2]),
            r = parseInt(command[3]),
            g = parseInt(command[4]),
            b = parseInt(command[5]);

        for (var j=6; j<command.length; j++) {
             var xy = command[j].split(',');
             this.brush(this.ctx, xy[0], xy[1], radius, blur, r, g, b);
        }
    }
}