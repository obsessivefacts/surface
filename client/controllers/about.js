var AboutController = function(el) {
    this.init(el);

    this.article = q(this.el, 'article.about-us');
    this.obsess = q(this.el, 'section.obsess');
    this.social = q(this.el, 'section.social');
    this.obvert = q(this.el, '#obvert');
    this.bodyHeader = q(document.body, 'header');
    this.scrollPane = q(this.el, 'div.chrome-fix');

    this.pauseStatic = false;

    this.initAnimations();

    this.bind(window, 'resize', this.initAnimations.bind(this));
    this.bind(q(this.el, '.tv svg'), 'click', this.stayDetermined.bind(this));
    this.bind(q(this.el, 'a.volition'), 'click', stop);

    loader.registerInternalPath(/^\/about$/, this.aboutUsPage.bind(this));
    loader.registerInternalPath(/^\/about\/x$/, this.realAboutPage.bind(this));

    header.bindToEl(this.scrollPane);

    if (window.location.pathname.indexOf('/x') === -1)
        this.obvert.checked = false;
    else
        this.bodyHeader.style.backgroundColor = 'rgba(0, 0, 0, 1)';

    this.initStatic();

    return this;
}

inherit(AboutController, Page);
loader.registerController('about', AboutController);

AboutController.prototype.aboutUsPage = function() {
    this.obvert.checked = false;
    this.article.scrollTop = this.article.scrollHeight;
    setTimeout(function() { this.initAnimations(true); }.bind(this), 500);
}

AboutController.prototype.realAboutPage = function() {
    this.article.scrollTop = this.article.scrollHeight;
    this.bodyHeader.style.backgroundColor = 'rgba(0, 0, 0, 1)';
    this.pauseStatic = true;
    this.obvert.checked = true;
    setTimeout(function() { this.pauseStatic = false; }.bind(this), 1000);
}

AboutController.prototype.cleanup = function() {
    this.canvasCtx = null;
    this.tmpContext = null;
    this.bodyHeader.style.removeProperty('background-color');
}

AboutController.prototype.initAnimations = function(skipStaticInit) {

}

AboutController.prototype.handleScrollPosition = function(e) {

}

AboutController.prototype.maybeSpawnFilterBubble = function(e) {

}

AboutController.prototype.actuallySpawnFilterBubble = function(bottom) {

}

AboutController.prototype.initStatic = function() {
    var c = q(this.el, 'canvas'),
        w = 300;
        h = 226;

    c.width = 300;
    c.height = 226;

    var ocanvas = document.createElement("canvas");
    ocanvas.width = w<<1;
    ocanvas.height = h<<1;

    var octx = ocanvas.getContext("2d", {alpha: false});
    var d = octx.createImageData(ocanvas.width, ocanvas.height);
    var buffer32 = new Uint32Array(d.data.buffer);
    for (var i=0; i<d.data.length; i+=4) {
        d.data[i+0] = d.data[i+1] = d.data[i+2] = Math.random() < 0.5 ? 128 : 255;
        d.data[i+3] = 255;
    }

    octx.putImageData(d, 0, 0);

    this.canvasCtx      = c.getContext("2d", {alpha: false});
    this.canvasWidth    = w;
    this.canvasHeight   = h;
    this.tmpContext     = ocanvas;
    this.frameRequested = false;

    requestAnimationFrame(this.animateStatic.bind(this));
}

AboutController.prototype.animateStatic = function() {
    if (this.canvasCtx) {
        if (this.obvert.checked && !this.pauseStatic) {    // avoid too much cpu
            var x = (this.canvasWidth * Math.random())|0;                    
            var y = (this.canvasHeight * Math.random())|0;
            this.canvasCtx.drawImage(this.tmpContext, -x, -y);    
        }
        this.frameRequested = false;               
    }
    if (!this.frameRequested) {
        if (this.canvasCtx) this.frameRequested = true;
        requestAnimationFrame(this.animateStatic.bind(this));
    }
}

AboutController.prototype.stayDetermined = function() {
    loader.go('/volition');
}