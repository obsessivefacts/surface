var MusicController = function(el) {
    this.init(el);
    this.initAnimations();
    this.initCanvas();

    return this;
}

inherit(MusicController, Page);
loader.registerController('music', MusicController);

MusicController.prototype.initAnimations = function() {
    // immediately remove noscript version of design (since we are in a script!)
    var spokes = q(this.el, 'svg.wheel g.spokes-noscript');
    if (spokes) spokes.parentNode.removeChild(spokes);
    this.frameRequested = false;
    this.explode = false;
    this.explodeDegrees = 0;
    this.prevSpokeRotation = -1;

    this.background = q(this.el, '.psychotic');
    this.songs = q(this.el, 'ul');
    this.rotator = q(this.el, '.rotato');

    this.bind(this.songs, 'scroll', this.animate.bind(this), false, true);
    header.bindToEl(this.songs);

    var links       = qs(this.el, 'li a');

    for (var i=0; i<links.length; i++) {
        this.bind(links[i], 'mouseover', this.over.bind(this));
        this.bind(links[i], 'mouseout', this.out.bind(this));
        this.bind(links[i], 'click', this.play.bind(this));
    }
}

MusicController.prototype.animate = function(e) {
    if (this.frameRequested) return;
    else this.frameRequested = true;

    requestAnimationFrame(function() {
        var again = false;
        if (this.explode == true && this.explodeDegrees < 90) {
            this.explodeDegrees += 5;
            var again = true;
        } else if (this.explode == false && this.explodeDegrees > 0) {
            this.explodeDegrees -= 5;
            var again = true;
        }
        this.frameRequested = false;
        var scroll = this.songs.scrollTop;
        this.drawWheel(-(scroll/6), this.explodeDegrees);
        this.rotator.style.transform = 'rotate('+(scroll/6)+'deg) translate3d(0, 0, 0)';
        if (again) {
            this.animate();
        }
    }.bind(this));
}

MusicController.prototype.over = function(e) {
    e.preventDefault();
    this.background.classList.add('over');
    this.explode = true;
    this.animate();
}

MusicController.prototype.out = function(e) {
    e.preventDefault();
    this.background.classList.remove('over');
    this.explode = false;
    this.animate();
}

MusicController.prototype.play = function(e) {
    var a       = e.target,
        title   = a.textContent;
    stop(e);
    header.loadMP3(a.href, {title: title});
}

MusicController.prototype.initCanvas = function() {
    this.canvas = q(this.el, 'canvas');
    this.context = this.canvas.getContext('2d');
    
    this.canvas.width = 1000;
    this.canvas.height = 1000;
    
    var _generateFractal = function(ctx, maxRadius, heightBudget) {
        var steps = 0,
            radius = maxRadius,
            baseY = heightBudget / 2,
            baseX = 0;

        while (radius >= 2.5) {
            baseX += steps == 0 ? radius : radius * 2;

            if (radius > 5) baseX += 5;
            else if (radius > 2.5) baseX += 2.5;

            steps++;
            radius /= 2;
        }

        var _draw = function(x, y, radius, bright, fromSide) {
            if (x - radius < 0 || y - radius < 0 || y + radius > heightBudget)
                return;

            ctx.beginPath();
            ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = bright ? 'rgba(146, 133, 245, .69)' : 'rgba(136, 96, 247, .69)';
            ctx.fill();

            if (radius == 2.5) return;

            if (fromSide !== 'left')
                _draw(x + radius + (radius > 5 ? 5 : 2.5) + (radius/2), y, radius / 2, !bright, 'right')
            if (fromSide !== 'right')
                _draw(x - radius - (radius > 5 ? 5 : 2.5) - (radius/2), y, radius / 2, !bright, 'left')
            if (fromSide !== 'bottom')
                _draw(x, y + radius + (radius > 5 ? 5 : 2.5) + (radius/2), radius / 2, !bright, 'top')
            if (fromSide !== 'top')
                _draw(x, y - radius - (radius > 5 ? 5 : 2.5) - (radius/2), radius / 2, !bright, 'bottom')
        }
        _draw(baseX, baseY, maxRadius, steps % 2 == 1, null)
    }

    this.o = c('canvas');
    this.o.width = 265;
    this.o.height = 135;
    octx = this.o.getContext('2d');
    _generateFractal(octx, 40, 135);

    this.m = c('canvas');
    this.m.width = 135;
    this.m.height = 65;
    mctx = this.m.getContext('2d');
    _generateFractal(mctx, 20, 65);

    this.i = c('canvas');
    this.i.width = 65;
    this.i.height = 50;
    ictx = this.i.getContext('2d');
    _generateFractal(ictx, 10, 50);

    this.spoke = c('canvas');
    this.spoke.width = 1000;
    this.spoke.height = 265;
    this.spokeCtx = this.spoke.getContext('2d');
    this.spokeCtx.strokeStyle = 'rgba(0, 0, 0, .4)';
    
    this.drawWheel(0, 0);
}

MusicController.prototype.drawSpoke = function(deg) {
    if (this.prevSpokeRotation == deg) return;
    this.prevSpokeRotation = deg;
    var ctx = this.spokeCtx;
    ctx.clearRect(0, 0, 1000, 265);
    ctx.beginPath();
    ctx.moveTo(0,132.5);
    ctx.lineTo(1000, 132.5);
    ctx.stroke();

    if (deg) {
        ctx.translate(395.5 + (260 * (deg/90)), 132.5);
        ctx.rotate((Math.PI / 180) * deg);
        ctx.drawImage(this.o, -132.5, -67.5);
    } else {
        ctx.drawImage(this.o, 263 + (260 * (deg/90)), 65);
    }
    if (deg) {
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.translate(199.5 + (130 * (deg/90)), 132.5);
        ctx.rotate((Math.PI / 180) * deg);
        ctx.drawImage(this.m, -67.5, -32.5);
    } else {
        ctx.drawImage(this.m, 132 + (130 * (deg/90)), 100);
    }

    if (deg) {
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.translate(104.5 + (58 * (deg/90)), 132.5);
        ctx.rotate((Math.PI / 180) * deg);
        ctx.drawImage(this.i, -32.5, -25);
    } else {
        ctx.drawImage(this.i, 72 + (58 * (deg/90)), 107.5);
    }
    if (deg) {
        ctx.setTransform(1, 0, 0, 1, 0, 0);
    }
}

MusicController.prototype.drawWheel = function(wheelRotate, spokeRotate) {
    this.drawSpoke(spokeRotate);

    var deg = ((wheelRotate % 22.5) + 22.5) % 22.5,
        ctx = this.context;

    ctx.clearRect(0, 0, 1000, 1000);
    ctx.translate(0, 500);
    
    ctx.rotate((Math.PI / 180) * ((-112.5 + deg)));
    ctx.drawImage(this.spoke, 0, -132.5);
    for (var i=0; i<10; i++) {
        ctx.rotate((Math.PI / 180) * 22.5);
        ctx.drawImage(this.spoke, 0, -132.5);
    }
    ctx.setTransform(1, 0, 0, 1, 0, 0);
}