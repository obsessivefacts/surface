var FactsFakeAppleController = function(el) {
    this.init(el);
    log('We tried to tell you nicely.');
    return this;
}

inherit(FactsFakeAppleController, Page);
loader.registerController('facts-fake-apple', FactsFakeAppleController);