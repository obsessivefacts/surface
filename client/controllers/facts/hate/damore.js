var FactsHateDamoreController = function(el) {
    this.init(el);
    log('Your services are no longer required.');
    return this;
}

inherit(FactsHateDamoreController, Page);
loader.registerController('facts-hate-damore', FactsHateDamoreController);