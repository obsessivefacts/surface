var FactsTrueUndefinedController = function(el) {
    this.init(el);
    log('I want to play a game.');
    return this;
}

inherit(FactsTrueUndefinedController, Page);
loader.registerController('facts-true-undefined', FactsTrueUndefinedController);