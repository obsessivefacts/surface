var FactsShibainuProfileController = function(el) {
    this.init(el);

    this.populateNextUnloadedImg();
    this.bind(this.el, 'scroll', this.checkLazyLoad.bind(this), false, true);

    return this;
}

inherit(FactsShibainuProfileController, Page);
loader.registerController('facts-shibainu-profile', FactsShibainuProfileController);

FactsShibainuProfileController.prototype.populateNextUnloadedImg = function() {
    const imgButt = q(this.el, 'img.butt');
    if (imgButt) {
        this.nextImg = imgButt;
    } else {
        this.nextImg = null;
    }
}

FactsShibainuProfileController.prototype.checkLazyLoad = function() {
    if (!this.nextImg) return false;

    let nextImgTop = this.nextImg.parentNode.getBoundingClientRect().y;

    if (this.el.offsetHeight + 100>= nextImgTop) {
        this.loadNextImg();
    }
}

FactsShibainuProfileController.prototype.loadNextImg = function() {
    this.nextImg.loading = 'eager';
    this.nextImg.classList.remove('butt');
    this.nextImg.classList.add('slut');
    this.nextImg = null;
    this.populateNextUnloadedImg();
}