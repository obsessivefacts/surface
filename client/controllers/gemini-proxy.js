var GeminiProxyController = function(el) {
    this.init(el);

    this.HOMEPAGE = 'gemini://obsessivefacts.com/gemini-proxy';

    header.hide();
    el.classList.add('javascript-enabled'); // for that extra special flair ;)

    const pathRegex = /^\/gemini-proxy$/;
    loader.registerInternalPath(pathRegex, this.historyNav.bind(this), true);

    if (window.location.href.indexOf('uri=') === -1) {
        history.replaceState(
            {},
            document.title,
            '/gemini-proxy?uri=' + encodeURIComponent(this.HOMEPAGE)
        );
    }

    this.loading = '';

    this.windowedCheckbox   = id('gemini-windowed');
    this.window             = q(this.el, '.win.explorer');
    this.gemframe           = q(this.el, 'div.gemframe');
    this.statusBar          = q(this.el, 'div.status div.info');
    this.gmiContent         = q(this.el, 'article.gmi-content');
    this.location           = q(this.el, 'form.location input');
    this.prevButton         = q(this.el, 'button.prev');
    this.nextButton         = q(this.el, 'button.next');
    this.stopButton         = q(this.el, 'button.stop');
    this.homeButton         = q(this.el, '.button.home');
    this.menu               = q(this.el, 'label.menu');
    this.reloadButton       = q(this.el, '.button.reload');
    this.errorMessageSpan   = q(this.el, '.win.error span.prompt');
    this.toolbar            = q(this.el, 'div.toolbar');
    this.toolbarHr          = q(this.el, 'hr.toolbar-hr');
    this.locationBar        = q(this.el, 'form.location');
    this.locationBarHr      = q(this.el, 'hr.location-hr');
    this.errorCheckbox      = id('e');
    this.openCheckbox       = id('b');
    this.printCheckbox      = id('gemini-print');

    qs(this.el, 'form').forEach(this.bindFormEvents.bind(this));

    this.bind(this.prevButton, 'click', this.historyPrev.bind(this));
    this.bind(this.nextButton, 'click', this.historyNext.bind(this));
    this.bind(this.stopButton, 'click', this.stop.bind(this));
    this.bind(this.homeButton, 'click', this.navigateHome.bind(this));
    this.bind(this.reloadButton, 'click', this.reload.bind(this));

    this.initUriBar();
    this.initImages();
    this.initLinks();
    this.initHistory();
    this.initMenus();

    setTimeout(function() {
        this.initWindows();
        this.bind(this.el, 'mousedown', this.mousedown.bind(this));
        this.bind(window, 'mouseup', this.mouseup.bind(this));
        this.bind(window, 'mousemove', this.mousemove.bind(this));
        this.bind(this.windowedCheckbox, 'change', this.initWindows.bind(this));
        this.preloadImages();
    }.bind(this), 400);

    return this;
}

inherit(GeminiProxyController, Page);
loader.registerController('gemini-proxy', GeminiProxyController);

GeminiProxyController.prototype.cleanup = function() {
    this.stop();
    this.nextButton.disabled = true;
    this.nextButton.firstChild.src = this.ICON_FORWARD_DISABLED;
    this.prevButton.disabled = true;
    this.prevButton.firstChild.src = this.ICON_BACK_DISABLED;
}

GeminiProxyController.prototype.initUriBar = function() {
    this.bind(q(this.el, 'input[id=uri]'), 'focus', (e) => e.target.select()); 
}

GeminiProxyController.prototype.initLinks = function() {
    qs(this.el, 'a').forEach((link) => {
        const url = new URL(link.href);
        if (url.pathname.startsWith('/gemini-proxy')
            && link.target !== '_blank'
            && link.classList.contains('button') == false) {
            this.bind(
                link,
                'click',
                this.handleProxyLinkClick.bind(this),
                false,
                false,
                'gemini-proxy-link'
            );
        }
    });
}

GeminiProxyController.prototype.handleProxyLinkClick = function(e) {
    stop(e);
    // log('LINK CLICKED: ', e.target.href);
    this.navigate(e.target.href);
};

GeminiProxyController.prototype.bindFormEvents = function(form) {
    this.bind(form, 'submit', this.handleLocationSubmit.bind(this));
}

GeminiProxyController.prototype.navigateHome = function(e) {
    stop(e);
    this.navigate(this.HOMEPAGE);
    return false;
};

GeminiProxyController.prototype.reload = function(e) {
    stop(e);
    this.navigate(window.location.href, true);
    return false;
};

GeminiProxyController.prototype.handleLocationSubmit = function(e) {
    stop(e);
    let uri = q(e.target, 'input[type="text"]').value,
        urlMatchRegexp = new RegExp(/^(\w+:\/\/)/);

    let matches = uri.match(urlMatchRegexp);
    if (matches) {
        if (matches[0] !== 'gemini://') {
            return this.alert('This proxy only supports Gemini Protocol URIs.');
        }
    } else {
        uri = 'gemini://' + uri;
    }

    let sanityTest = new RegExp(/^gemini:\/\/\S+\.\S/); // not perfect but w/e
    if (!uri.match(sanityTest)) {
        return this.alert('Please enter a valid URI.');
    }

    this.openCheckbox.checked = false;
    this.navigate(uri);
};

GeminiProxyController.prototype.navigate = function(href, silent) {
    var xhr = new XMLHttpRequest();

    if (!href.startsWith('http'))
        href = '/gemini-proxy?uri=' + encodeURIComponent(href);

    href += (href.indexOf('?') !== -1 ? '&' : '?') + 'internal=true';

    // log('navigate:',  href);
    this.setLoading(href);

    xhr.onreadystatechange = function() {6
        if (xhr.readyState === 4) {
            if (this.loading !== href) return;
            this.stop();

            switch (xhr.status) {
                case 200:
                    let res = JSON.parse(xhr.response);
                    this.handleNavigateResponse(res, silent);
                    break;

                default:
                    alert('ERROR');
                    console.error('Error loading ' + href);
                    console.error('Status:       ' + xhr.status);
                    console.error('Response:     ' , xhr.response);
            }
        }
    }.bind(this);
    xhr.open("get", href, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
}

GeminiProxyController.prototype.handleNavigateResponse = function(res, silent) {
    // log('res:', res);

    let params = res.viewParams;

    this.unbindByTag('gemini-proxy-link');
    this.gmiContent.innerHTML = res.content;
    if (!silent) this.gmiContent.scrollTop = 0;
    this.initLinks();
    
    if (params.error) {
        this.gmiContent.classList.add('error');
    } else {
        this.gmiContent.classList.remove('error');
    }

    if (params.rawtext) {
        this.gmiContent.classList.add('raw');
    } else {
        this.gmiContent.classList.remove('raw');
    }

    this.location.value = params.uri;
    this.statusBar.innerHTML = 'Document: Done in ' + params.elapsed +'s';

    let uri = params.uri;

    if (uri.startsWith('http')) {
        uri = decodeURIComponent(uri.replace(/^.*\?/, ''));
    }
    let url = '/gemini-proxy?uri=' + encodeURIComponent(uri);

    if (!silent) {
        history.pushState({}, document.title, url);
        this.historyIndex++;
        this.history.splice(this.historyIndex, Infinity, window.location.href);
    }

    this.handleHistoryButtons();
}

GeminiProxyController.prototype.setLoading = function(href) {
    this.loading = href;
    this.gemframe.style.background = 'black url('+this.LOAD+')';
    this.stopButton.disabled = false;
    this.stopButton.firstChild.src = this.ICON_STOP;
}

GeminiProxyController.prototype.stop = function() {
    this.loading = '';
    this.gemframe.style.background = 'black url('+this.LOGO+')';
    this.stopButton.disabled = true;
    this.stopButton.firstChild.src = this.ICON_STOP_DISABLED;
}

GeminiProxyController.prototype.initHistory = function() {
    this.historyIndex = 0;
    this.history = [ window.location.href ];
}

GeminiProxyController.prototype.handleHistoryButtons = function() {
    // log('this.history: ', this.history);
    // log('this.historyIndex: ', this.historyIndex);

    if (this.historyIndex < this.history.length - 1) {
        this.nextButton.disabled = false;
        this.nextButton.firstChild.src = this.ICON_FORWARD;
    } else {
        this.nextButton.disabled = true;
        this.nextButton.firstChild.src = this.ICON_FORWARD_DISABLED;
    }

    if (this.historyIndex > 0) {
        this.prevButton.disabled = false;
        this.prevButton.firstChild.src = this.ICON_BACK;
    } else {
        this.prevButton.disabled = true;
        this.prevButton.firstChild.src = this.ICON_BACK_DISABLED;
    }
}

GeminiProxyController.prototype.historyNav = function(e) {
    // log('historyNav: ', window.location.href);

    for (let i=this.history.length-1; i>=0; i--) {
        if (this.history[i] == window.location.href) {
            this.historyIndex = i;
        }
    }
    this.handleHistoryButtons();
    this.navigate(window.location.href, true);
}

GeminiProxyController.prototype.historyPrev = function(e) {
    stop(e);
    // log('historyPrev: ', e);
    history.back();
}

GeminiProxyController.prototype.historyNext = function(e) {
    stop(e);
    // log('historyNext: ', e);
    history.forward();
}

GeminiProxyController.prototype.initImages = function() {
    this.CDN  = (CONFIG.cdnSurface ? CONFIG.cdnSurface : '');
    this.PATH = '/images/gemini-proxy/';

    const p = this.CDN + this.PATH;

    this.LOGO                   = p + 'gem.png';
    this.LOAD                   = p + 'gem_animated.gif';
    this.ICON_FORWARD           = p + 'icon_forward.png';
    this.ICON_FORWARD_DISABLED  = p + 'icon_forward_disabled.png';
    this.ICON_BACK              = p + 'icon_back.png';
    this.ICON_BACK_DISABLED     = p + 'icon_back_disabled.png';
    this.ICON_STOP              = p + 'icon_stop.png';
    this.ICON_STOP_DISABLED     = p + 'icon_stop_disabled.png';
}

GeminiProxyController.prototype.preloadImages = function(e) {
    const p = this.CDN + this.PATH;

    const preloadList = [
        this.LOAD,
        this.LOGO,
        this.ICON_FORWARD,
        this.ICON_FORWARD_DISABLED,
        this.ICON_BACK,
        this.ICON_BACK_DISABLED,
        this.ICON_STOP,
        this.ICON_STOP_DISABLED,
        p+'go_button_background_active.png',
        p+'resize.png',
        p+'win_border_corner.png',
        p+'win_border_left.png',
        p+'win_border_top.png',
        p+'win_button.png',
        p+'win_button_active.png',
        p+'win_button_disabled.png',
        p+'win_button_focus.png',
        p+'win_button_heavy.png',
        p+'win_icon_computer_sel.png',
        p+'win_icon_documents_sel.png',
        p+'win_icon_ie_big_sel.png',
        p+'win_icon_netscape_sel.png',
        p+'win_icon_network_sel.png',
        p+'win_icon_recycle_sel.png',
        p+'win_maximize.png',
        p+'win_maximize_active.png',
        p+'win_minimize_active.png',
        p+'win_progbar_corner.png',
        p+'win_progbar_left.png',
        p+'win_progbar_top.png',
        p+'win_start_sel.png',
        p+'win_taskbar_ql_hover.png',
        p+'win_taskbar_ql_click.png',
        p+'win_unmaximize.png',
        p+'win_unmaximize_active.png',
        p+'win_unmaximize_disabled.png',
        p+'win_x_active.png',
        p+'win_x_gray.png',
        p+'menu_border_top.png',
        p+'menu_border_left.png',
        p+'menu_border_right.png',
        p+'menu_border_bottom.png',
        p+'gemini.png',
        p+'johnny.gif'
    ];

    let preloads = [],
        isLoaded = function(e) {
        // log('PRELOAD!', e.target.src);
    }.bind(this);

    for (let i=0; i<preloadList.length; i++) {
        preloads[i] = new Image();
        preloads[i].src = preloadList[i];
        this.bind(preloads[i], 'load', isLoaded, true);
        // if (preloads[i].complete) log('cached: ', preloadList[i]);
    }
}

GeminiProxyController.prototype.alert = function(msg) {
    this.errorCheckbox.checked = true;
    this.errorMessageSpan.textContent = msg;
}

////////////////////////////////////////////////////////////////////////////////
// w-w-w-WINDOWS THREE EIGHTY SIX!! ------------------------------------------//
////////////////////////////////////////////////////////////////////////////////

GeminiProxyController.prototype.initWindows = function() {
    this.DOUBLE_DEBOUNCE = false;

    if (this.windowedCheckbox.checked) {
        if (!this.windowWidth) {
            const coords = this.window.getBoundingClientRect();
            this.windowWidth = coords.width;
            this.windowHeight = coords.height;
            this.windowLeft = coords.left;
            this.windowBottom = window.innerHeight - coords.bottom;
        }
        this.setWindowPosition();
    } else {
        this.window.style.left = null;
        this.window.style.bottom = null;
        this.window.style.width = null;
        this.window.style.height = null;
        this.window.style.transformOrigin = null;
    }
    this.initDragState();
};

GeminiProxyController.prototype.initMenus = function() {
    this.menu.classList.remove('no-js');

    this.MENU_KEEP_OPEN = false;
    this.MENU_KEEP_OPEN_TIMER = false;

    this.MENUS = {
        'File': [
            {
                type: 'item',
                label: 'New Web Browser',
                click: function(e) { window.open('/gemini-proxy'); }
            },
            {
                type: 'item',
                label: 'New Mail Message',
                click: function(e) { window.open('https://mail.proton.me'); }
            },
            {
                type: 'item',
                label: 'Mail Document...',
                disabled: true
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Open Location...',
                click: function(e) { this.openCheckbox.checked = true; }
            },
            {
                type: 'item',
                label: 'Open File...',
                click: function(e) { this.openCheckbox.checked = true; }
            },
            {
                type: 'item',
                label: 'Save As...',
                click: function(e) { this.alert('Save yourself!'); }
            },
            {
                type: 'item',
                label: 'Upload File...',
                disabled: true
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Page Setup...',
                click: function(e) { window.print(); }
            },
            {
                type: 'item',
                label: 'Print...',
                click: function(e) { window.print(); }
            },
            {
                type: 'item',
                label: 'Print Preview...',
                click: function(e) { this.printCheckbox.checked = true; }
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Close',
                click: function(e) {
                    loader.go('/');
                }
            },
            {
                type: 'item',
                label: 'Exit',
                click: function(e) {
                    loader.go('/');
                }
            }
        ],
        'Edit': [
            {
                type: 'item',
                label: 'Undo',
                disabled: true
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Cut',
                disabled: true
            },
            {
                type: 'item',
                label: 'Copy',
                disabled: true
            },
            {
                type: 'item',
                label: 'Paste',
                disabled: true
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Select All',
                disabled: true
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Find...',
                click: function(e) {
                    this.alert('To find text on the page, use your browser\'s '
                             + 'built-in "Find" feature!', 'Helpful Hint');
                }
            },
            {
                type: 'item',
                label: 'Find Again',
                disabled: true
            },
        ],
        'View': [
            {
                type: 'item',
                label: 'Reload',
                click: function(e) { this.reload(); }
            },
            {
                type: 'item',
                label: 'Reload Frame',
                disabled: true
            },
            {
                type: 'item',
                label: 'Load Images',
                disabled: true
            },
            {
                type: 'item',
                label: 'Refresh',
                click: function(e) { this.reload(); }
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Document Source',
                click: function(e) {
                    window.open('https://gitlab.com/obsessivefacts/surface/-/blob/master/handlers/gemini-proxy.js');
                }
            },
            {
                type: 'item',
                label: 'Document Info',
                click: function(e) {
                    this.navigate('gemini://obsessivefacts.com/gemini-proxy/faq.gmi');
                }
            },
        ],
        'Go': [
            {
                type: 'item',
                label: 'Back',
                click: function(e) { history.back() },
                disabled: function() {
                    return this.historyIndex == 0;
                }
            },
            {
                type: 'item',
                label: 'Forward',
                click: function(e) { history.forward() },
                disabled: function() {
                    return this.historyIndex == this.history.length - 1;
                }
            },
            {
                type: 'item',
                label: 'Homepage',
                click: function(e) { this.navigate(this.HOMEPAGE); },
            },
            {
                type: 'item',
                label: 'Stop Loading',
                click: function(e) { this.stop() },
                disabled: function() {
                    return !!!this.loading;
                }
            },
            {
                type: 'hr'
            },
        ],
        'Bookmarks': [
            {
                type: 'item',
                label: 'Add Bookmark',
                click: function(e) {
                    this.alert('JavaScripts are (rightly) no longer allowed to '
                        + 'add bookmarks; please bookmark this page yourself!');
                }
            },
            {
                type: 'item',
                label: 'Manage Bookmarks...',
                click: function(e) {
                    this.alert('If you want your capsule on the default '
                        + 'bookmark list, email henriquez@protonmail.com')
                }
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Manifesto',
                click: function(e) {
                    this.navigate('gemini://obsessivefacts.com/manifesto.gmi');
                },
            },
            {
                type: 'item',
                label: 'Project Gemini',
                click: function(e) {
                    this.navigate('gemini://geminiprotocol.net/');
                },
            },
            {
                type: 'item',
                label: 'Yestercities',
                click: function(e) {
                    this.navigate('gemini://cities.yesterweb.org/');
                },
            },
            {
                type: 'item',
                label: 'Solderpunk\'s Realm',
                click: function(e) {
                    this.navigate('gemini://zaibatsu.circumlunar.space/~solderpunk/');
                },
            },
            {
                type: 'item',
                label: 'Tux Machines',
                click: function(e) {
                    this.navigate('gemini://gemini.tuxmachines.org/');
                },
            },
            {
                type: 'item',
                label: 'Techrights',
                click: function(e) {
                    this.navigate('gemini://gemini.techrights.org/');
                },
            },
            {
                type: 'item',
                label: 'LibreHacker\'s Capsule',
                click: function(e) {
                    this.navigate('gemini://librehacker.com/');
                },
            },
        ],
        'Options': [
            {
                type: 'item',
                label: 'Show Toolbar',
                click: function(e) {
                    if (this.toolbar.classList.contains('hidden')) {
                        this.toolbar.classList.remove('hidden');
                        this.toolbarHr.classList.remove('hidden');
                    } else {
                        this.toolbar.classList.add('hidden');
                        this.toolbarHr.classList.add('hidden');
                    }

                },
                checked: function() {
                    return !this.toolbar.classList.contains('hidden');
                }
            },
            {
                type: 'item',
                label: 'Show Location',
                click: function(e) {
                    if (this.locationBar.classList.contains('hidden')) {
                        this.locationBar.classList.remove('hidden');
                        this.locationBarHr.classList.remove('hidden');
                    } else {
                        this.locationBar.classList.add('hidden');
                        this.locationBarHr.classList.add('hidden');
                    }

                },
                checked: function() {
                    return !this.locationBar.classList.contains('hidden');
                }
            },
            {
                type: 'item',
                label: 'Show Java Console',
                click: function(e) {
                    window.open('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
                }
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Document Encoding: UTF-8',
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Save Options',
                click: function(e) { notifier.notify('ok saved lol'); }
            },
        ],
        'Directory': [
            {
                type: 'item',
                label: 'Netscape\'s Home',
                click: function(e) {
                    window.open('http://www.mcom.com/home/welcome.html');
                },
            },
            {
                type: 'item',
                label: 'What\'s New!',
                click: function(e) {
                    window.open('/blog');
                },
            },
            {
                type: 'item',
                label: 'What\'s Cool!',
                click: function(e) {
                    window.open('https://system76.com/laptops');
                },
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Netscape Galleria',
                click: function(e) {
                    window.open('https://www.versionmuseum.com/history-of/netscape-browser');
                },
            },
            {
                type: 'item',
                label: 'Internet Directory',
                click: function(e) {
                    window.open('https://en.wikipedia.org/wiki/Yahoo!_Directory');
                },
            },
            {
                type: 'item',
                label: 'Internet Search',
                click: function(e) { window.open('https://duckduckgo.com/'); },
            },
            {
                type: 'item',
                label: 'Internet White Pages',
                click: function(e) { window.open('https://craigslist.org/'); },
            },
            {
                type: 'item',
                label: 'About the Internet',
                click: function(e) {
                    window.open('https://en.wikipedia.org/wiki/Internet');
                },
            },
        ],
        'Help': [
            {
                type: 'item',
                label: 'About gemini-proxy...',
                click: function(e) { id('d').checked = true; },
            },
            {
                type: 'item',
                label: 'About Gemini...',
                click: function(e) {
                    this.navigate('gemini://geminiprotocol.net/');
                },
            },
            {
                type: 'item',
                label: 'Gemini FAQ...',
                click: function(e) {
                    this.navigate('gemini://geminiprotocol.net/docs/faq.gmi');
                },
            },
            {
                type: 'item',
                label: 'About Obsessive Facts...',
                click: function(e) {
                    this.navigate('gemini://obsessivefacts.com/manifesto.gmi');
                },
            },
            {
                type: 'hr'
            },
            {
                type: 'item',
                label: 'Security Info...',
                click: function(e) { id('c').checked = true; },
            },
        ],
    }

    qs(this.menu, 'span').forEach((menu) => {
        this.bind(menu, 'click', this.openMenu.bind(this));
        this.bind(menu, 'mouseenter', function(e) {
            if (this.MENU_KEEP_OPEN) { this.openMenu(e); }
        }.bind(this));
    });
};

GeminiProxyController.prototype.openMenu = function(e) {
    if (e.target.classList.contains('w386') === false) return false;
    if (q(e.target, 'div.submenu')) return false;
    clearTimeout(this.MENU_KEEP_OPEN_TIMER);

    const menu  = e.target,
          items = this.MENUS[menu.textContent],
          tag   = menu + menu.textContent;

    const close = function(e, force) {
        stop(e);

        if (!force) {
            this.MENU_KEEP_OPEN = true;
            this.MENU_KEEP_OPEN_TIMER = setTimeout(function() {
                this.MENU_KEEP_OPEN = false
            }.bind(this), 50);
        }

        this.unbindByTag(tag);
        menu.removeChild(panel);
    }.bind(this);

    const forceClose = function(e) { close(e, true); }.bind(this);

    let panel = c('div');
    panel.className = 'submenu';

    let blocker = c('div');
    blocker.className = 'blocker';
    blocker.textContent = menu.textContent;
    panel.appendChild(blocker);

    let borderTop = c('div');
    borderTop.className = 'border-top';
    panel.appendChild(borderTop);

    let itemList = c('ul');

    for (let i=0; i<items.length; i++) {
        var item = items[i];

        if (item.type == 'hr') {
            var li = c('li'), hr = c('hr');
                li.appendChild(hr);
            itemList.appendChild(li);
        } else {
            var li = c('li'), b = c('b');
                li.className = 'item';

            if (item.disabled) { 
                switch (typeof item.disabled) {
                    case 'boolean':
                        li.classList.add('disabled');
                        break;
                    case 'function':
                        if ((item.disabled.bind(this))())
                            li.classList.add('disabled');
                        break;
                }
            }

            if (typeof item.checked === 'function') {
                if ((item.checked.bind(this))()) {
                    li.classList.add('checked');
                }
            }

            b.textContent = item.label;
            li.appendChild(b);
            itemList.appendChild(li);

            if (item.click && li.classList.contains('disabled') == false) {
                this.bind(li, 'click', item.click.bind(this), true, false, tag);
                this.bind(li, 'click', close.bind(this), true, false, tag);
            }
        }
    }

    panel.appendChild(itemList);

    let borderBottom = c('div');
    borderBottom.className = 'border-bottom';
    panel.appendChild(borderBottom);

    menu.appendChild(panel);

    this.bind(panel, 'mouseleave', close, true, false, tag);
    this.bind(blocker, 'click', forceClose, true, false, tag);
};

GeminiProxyController.prototype.setWindowPosition = function() {
    this.window.style.left = this.windowLeft + 'px';
    this.window.style.bottom = this.windowBottom + 'px';
    this.window.style.width = this.windowWidth + 'px';
    this.window.style.height = this.windowHeight + 'px';
    this.window.style.transformOrigin = (150 - this.windowLeft)+'px '
        + (this.windowHeight + this.windowBottom)+'px ';
};

GeminiProxyController.prototype.initDragState = function() {
    this.clicked = false;
    this.action = '';
    this.startX = 0;
    this.startY = 0;
};

GeminiProxyController.prototype.mousedown = function(e) {
    let cls = e.target.classList;

    const setClicked = function(action) {
        this.clicked = true;
        this.deltaX = e.clientX;
        this.deltaY = e.clientY;
        this.action = action;
        stop(e);
    }.bind(this);

    if (cls.contains('main-window')) {
        if (!this.DOUBLE_DEBOUNCE) {
            this.DOUBLE_DEBOUNCE = true;
            window.setTimeout(function() {
                this.DOUBLE_DEBOUNCE = false;
            }.bind(this), 400);
        } else {
            this.windowedCheckbox.click();
        }
        setClicked('move');
    }

    if (!this.windowedCheckbox.checked) return;

    if (cls.contains('north')) {
        setClicked('resize-n');
    } else if (cls.contains('top-right')) {
        setClicked('resize-ne');
    } else if (cls.contains('east')) {
        setClicked('resize-e');
    } else if (cls.contains('grabber') || cls.contains('bottom-right')) {
        setClicked('resize-se');
    } else if (cls.contains('south')) {
        setClicked('resize-s');
    } else if (cls.contains('bottom-left')) {
        setClicked('resize-sw');
    } else if (cls.contains('west')) {
        setClicked('resize-w');
    } else if (cls.contains('top-left')) {
        setClicked('resize-nw');
    }
};

GeminiProxyController.prototype.mouseup = function(e) {
    if (!this.windowedCheckbox.checked) return;
    this.initDragState();
};

GeminiProxyController.prototype.mousemove = function(e) {
    if (this.clicked == false || !this.windowedCheckbox.checked) return;

    stop(e);

    const offsetX = e.clientX - this.deltaX;
    const offsetY = e.clientY - this.deltaY;

    const WINDOW_MIN_WIDTH = 500,
          WINDOW_MIN_HEIGHT = 300;

    const setWindowWidth = function(width) {
        if (width < WINDOW_MIN_WIDTH)
            return WINDOW_MIN_WIDTH;
        else if (this.windowLeft + width > window.innerWidth)
            return this.windowWidth;
        return width;
    }.bind(this);

    const setWindowHeight = function (height) {
        if (height < WINDOW_MIN_HEIGHT)
            return WINDOW_MIN_HEIGHT;
        else if (this.windowBottom + height > window.innerHeight)
            return this.windowHeight;
        return height;
    }.bind(this);

    const setWindowLeft = function(left) {
        if (left + this.windowWidth > window.innerWidth)
            return this.windowLeft;
        else if (left < 0)
            return this.windowLeft;
        else
            return left;
    }.bind(this);

    const setWindowBottom = function(bottom) {
        if (bottom + this.windowHeight > window.innerHeight)
            return this.windowBottom;
        else if (bottom < 0)
            return this.windowBottom;
        else
            return bottom;
    }.bind(this);

    switch (this.action) {
        case 'move':
            this.windowLeft = setWindowLeft(this.windowLeft + offsetX);
            this.windowBottom = setWindowBottom(this.windowBottom - offsetY);
            this.setWindowPosition();
            break;
        case 'resize-n':
            this.windowHeight = setWindowHeight(this.windowHeight - offsetY);
            this.setWindowPosition();
            break;
        case 'resize-ne':
            this.windowWidth = setWindowWidth(this.windowWidth + offsetX);
            this.windowHeight = setWindowHeight(this.windowHeight - offsetY);
            this.setWindowPosition();
            break;
        case 'resize-e':
            this.windowWidth = setWindowWidth(this.windowWidth + offsetX);
            this.setWindowPosition();
            break;
        case 'resize-se':
            this.windowWidth = setWindowWidth(this.windowWidth + offsetX);
            this.windowHeight = setWindowHeight(this.windowHeight + offsetY);
            this.windowBottom = setWindowBottom(this.windowBottom - offsetY);
            this.setWindowPosition();
            break;
        case 'resize-s':
            this.windowHeight = setWindowHeight(this.windowHeight + offsetY);
            this.windowBottom = setWindowBottom(this.windowBottom - offsetY);
            this.setWindowPosition();
            break;
        case 'resize-sw':
            this.windowWidth = setWindowWidth(this.windowWidth - offsetX);
            this.windowLeft = setWindowLeft(this.windowLeft + offsetX);
            this.windowHeight = setWindowHeight(this.windowHeight + offsetY);
            this.windowBottom = setWindowBottom(this.windowBottom - offsetY);
            this.setWindowPosition();
            break;
        case 'resize-w':
            this.windowWidth = setWindowWidth(this.windowWidth - offsetX);
            this.windowLeft = setWindowLeft(this.windowLeft + offsetX);
            this.setWindowPosition();
            break;
        case 'resize-nw':
            this.windowWidth = setWindowWidth(this.windowWidth - offsetX);
            this.windowLeft = setWindowLeft(this.windowLeft + offsetX);
            this.windowHeight = setWindowHeight(this.windowHeight - offsetY);
            this.setWindowPosition();
            break;
    }
    this.deltaX = e.clientX;
    this.deltaY = e.clientY;
};