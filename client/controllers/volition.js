var VolitionController = function(el) {
    this.init(el);

    header.hide();

    this.initCanvas();
    this.bind(window, 'resize', this.initCanvas.bind(this));
    this.bind(window, 'orientationChange', this.initCanvas.bind(this));

    return this;
}

inherit(VolitionController, Page);
loader.registerController('volition', VolitionController);

VolitionController.prototype.initCanvas = function() {
    var query = "(-webkit-min-device-pixel-ratio: 2), (min-device-pixel-ratio: 2), (min-resolution: 192dpi)",
        retina = matchMedia(query).matches ? true : false;

    // retina = false;
    // retina = true;

    var canvas = q(this.el, 'canvas'),
        width = canvas.offsetWidth * (retina ? 2 : 1),
        height = canvas.offsetHeight * (retina ? 2 : 1),
        ctx = canvas.getContext('2d');

    canvas.width = width;
    canvas.height = height;

    ctx.strokeStyle = 'black';
    ctx.lineWidth = retina ? 2 : 1;

    var centerX = width / 2,
        centerY = height / 2,
        i = 0,
        radius = 1,
        curLength = 1,
        skipper = 0,
        thiccener = 0,
        maxThickness = retina ? 94 : 47,
        markFinalIterator = 9999,
        markFinalIteratorSet = false;

    var startTime = new Date().getTime();

    var _getLineCoords = function(cx, cy, deg, r, l) {
        var cos = Math.cos((deg % 360) * Math.PI / 180),
            sin = Math.sin((deg % 360) * Math.PI / 180);

        return [cx+(r*cos), cy+(r*sin), cx+((r+l)*cos), cy+((r+l)*sin)];
    };

    while (radius < centerX && i < markFinalIterator) {
        if (skipper % 3 == 0) {
            if (retina) {
                if (thiccener == 0 && radius > (centerX / 5)) {
                    ctx.lineWidth = ctx.lineWidth * 2;
                    thiccener = 1;
                } else if (thiccener == 1 && radius > (centerX / 4)) {
                    ctx.lineWidth = ctx.lineWidth * 2;
                    thiccener = 2;
                }
            } else {
                ctx.lineWidth = curLength;
            }
            var length = markFinalIteratorSet == false ? curLength : 2000,
                line = _getLineCoords(centerX, centerY, i, radius, length);

            ctx.beginPath();
            ctx.moveTo(line[0], line[1]);
            ctx.lineTo(line[2], line[3]);
            ctx.stroke();
        }
        i += .1;
        skipper++;
        radius += retina ? .025 : .0125,
        curLength += retina ? .00125 : .000625;
        if (curLength > maxThickness && markFinalIteratorSet == false) {
            markFinalIterator = i + 360;
            markFinalIteratorSet = true;
        }
    }
    var endTime = new Date().getTime();
    log('took ' + (endTime - startTime) + ' ms');

    var download = q(this.el, 'a.download');
    if (download) download.href = canvas.toDataURL('application/octet-stream');

}

VolitionController.prototype.cleanup = function() {
    notifier.notify('Press F to Pay Respects');
}