var IndexPage = function(el) {
    this.init(el);
    this.bindHeroEvents();
    this.preloadImages();
    this.unhideKeyboard();
    loader.registerInternalPath(/^\/$/, this.rootPage.bind(this));
    loader.registerInternalPath(/^\/password$/, this.passwordPage.bind(this));
    loader.registerInternalPath(/^\/access_denied$/, this.accessDenied.bind(this));
    // loader.registerInternalPath(/^\/index\/(\d+)$/, this.testPath.bind(this));
    return this;
}

inherit(IndexPage, Page);
loader.registerController('index', IndexPage);

IndexPage.prototype.bindHeroEvents = function() {
    var as      = qs(this.el, 'a._'),
        over    = this.over.bind(this),
        out     = this.out.bind(this);
    for (var i=0; i<as.length; i++) {
        this.bind(as[i], 'mouseover', over);
        this.bind(as[i], 'mouseout', out);
    }
    this.bind(document, 'keyup', this.keyup.bind(this));
}

IndexPage.prototype.over = function(e) {
    q(this.el, '.backdrop').classList.add('over');
    stop(e);
}

IndexPage.prototype.out = function(e) {
    q(this.el, '.backdrop').classList.remove('over');
    stop(e);
}

IndexPage.prototype.keyup = function(e) {
    if (e.keyCode == 70 && !q(this.el, '.hand').classList.contains('password')) {
        loader.go('/password');
        notifier.notify('Press F to go ◂ back in time.');
    }
}

IndexPage.prototype.rootPage = function(args) {
    log('rootPage');
    q(this.el, '.villain').classList.remove('access-denied');
    q(this.el, '.backdrop').classList.remove('password');
    q(this.el, '.hand').classList.remove('password');
    q(this.el, '.keyboard').classList.remove('password');
    q(this.el, 'form').classList.remove('password');
    var hs = qs(this.el, 'a._');
    for (var i=0; i<hs.length; i++) {
        hs[i].href = '/password';
        hs[i].classList.remove('password');
    }
    this.TRIGGER('index/rootPage');
}

IndexPage.prototype.passwordPage = function(args, accessDenied) {
    if (accessDenied) q(this.el, '.villain').classList.add('access-denied');
    q(this.el, '.backdrop').classList.add('password');
    q(this.el, '.hand').classList.add('password');
    q(this.el, '.keyboard').classList.add('password');
    q(this.el, 'form').classList.add('password');
    var hs = qs(this.el, 'a._');
    for (var i=0; i<hs.length; i++) {
        hs[i].href = '/';
        hs[i].classList.add('password');
    }
    if (!accessDenied) this.TRIGGER('index/passwordPage', {test: true});
}

IndexPage.prototype.accessDenied = function(args) {
    this.passwordPage(null, true);
}

IndexPage.prototype.preloadImages = function() {
    // var light = new Image();
    // var urlPrefix = CONFIG.cdnSurface ? CONFIG.cdnSurface : '';
    // light.src = urlPrefix + '/images/index/light.png';
}

IndexPage.prototype.unhideKeyboard = function() {
    setTimeout(function() {
        q(this.el, '.keyboard').classList.remove('hidden');
    }.bind(this), 10);
}