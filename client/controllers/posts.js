var PostsController = function(el) {
    this.init(el);
    this.initDNT();
    this.initForm();
    this.initPagination();
    this.initDeleteLinks();
    this.initWriteLinks();

    if (window.location.hash) {
        header.suppress();
        header.hide();
    }
    return this;
}

inherit(PostsController, Page);
loader.registerController('blog', PostsController);
loader.registerController('write', PostsController);
loader.registerController('guestbook', PostsController);

PostsController.prototype.initDNT = function() {
    var dnt = q(this.el, '.dnt-notice');
    if (!dnt) return;

    this.bind(q(dnt, 'a.accept'), 'click', function(e) {
        stop(e);
        var xhr = new XMLHttpRequest();
            d   = new FormData();        
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status != 200) return loader.go('/access_denied');
                dnt.style.display = 'none';
                notifier.notify('You get a cookie!');
            }
        }.bind(this);
        xhr.open("get", e.target.href, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(d);
    });
    this.bind(q(dnt, 'label'), 'click', function(e) {
        notifier.notify('That\'s okay.');
    });
};

PostsController.prototype.initForm = function() {
    this.form = q(this.el, 'form')
    if (!this.form) return;

    var butt = q(this.el, 'button[type="submit"]'),
        originalButtText = butt.textContent;

    this.bind(this.form, 'submit', function(e) {
        stop(e);
        var xhr  = new XMLHttpRequest(),
            d    = new FormData(this.form);
        d.append('json', true);
        butt.disabled = true;
        butt.textContent = '…';

        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                
                var json = JSON.parse(xhr.response);

                if (xhr.status != 200) {
                    butt.disabled = false;
                    butt.textContent = originalButtText;

                    var p = q(this.form, 'p.msg');

                    if (p) {
                        p.classList.add('error');
                        p.textContent = json.errors[0];
                    } else {
                        p = c('p');
                        p.className = 'msg error';
                        p.textContent = json.errors[0];
                        var container = q(this.form, '.compose');
                        container.insertBefore(p, container.childNodes[0]);
                    }
                } else {
                    var postsContainer = q(this.el, '.posts-container');
                    var res = json.result;
                    if (postsContainer) {
                        
                        this.loadPage(null, res.cleanPath, res.page, res.id);
                    } else {
                        loader.go(res.finalPath);
                    }
                    
                }
            }
        }.bind(this);
        xhr.open("post", this.form.action, true);
        xhr.send(d);
        
    }.bind(this));
}

PostsController.prototype.initPagination = function() {
    var links = qs(this.el, '.pagination a');
    for (var i=0; i<links.length; i++) {
        this.bind(links[i], 'click', this.loadPage.bind(this));
    }
}

PostsController.prototype.loadPage = function(e, path, page, post) {
    if (e) {
        stop(e);
        var url = util.parseURL(e.target);
        path = url.pathname;
        page = 1;
        if (url.search) {
            var match = url.search.match(/page=(\d+)/);
            if (match.length) {
                page = parseInt(match[1])
            }
        }
    }
    var url = path + '?page=' + page;
    loader.go(url + '&posts_ajax=true', true, false, function(res) {

        var el = this.el,
            pendingText = null;

        var postsContainer = q(el, '.posts-container'),
            textarea = q(el, '.compose textarea');

        if (e && textarea && textarea.value)
            pendingText = textarea.value;

        loader.deactivateControllers();
        postsContainer.innerHTML = res.html;
        loader.activateControllers(el);

        if (pendingText) {
            var textarea = q(el, '.compose textarea');
            if (textarea) {
                textarea.value = pendingText;
            }
        }
        var anchor = null;
        if (post) {
            var anchor = q(el, '#POST-' + post);
            if (anchor) {
                anchor.classList.add('highlight');
            }
        }
        if (!anchor) {
            var anchor = q(el, 'section.posts');
        }
        var hash = anchor.id;

        history.replaceState({force_load: true}, document.title, window.location.href);
        history.pushState({force_load: true}, document.title, url + '#' + hash)

        el.scrollTop = anchor.offsetTop;
        header.suppress();
    }.bind(this));
}

PostsController.prototype.initDeleteLinks = function() {
    var links = qs(this.el, 'a.post-delete');
    for (var i=0; i<links.length; i++) {
        this.bind(links[i], 'click', function(e) {
            if (!confirm('Are you sure you wish to delete this post?')) {
                stop(e);
            }
        });
    }
}

PostsController.prototype.initWriteLinks = function() {
    var links = qs(this.el, 'a.post-write');
    for (var i=0; i<links.length; i++) {
        this.bind(links[i], 'click', function(e) {
            history.replaceState({force_load_after: true}, document.title, window.location.href);
        });
    }
}