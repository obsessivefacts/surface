var KeyboardController = function(el) {
    this.init(el);
    var form = q(this.el, 'form');
    form.classList.add('inactive');
    form.classList.add('no-transition');
    if (
        q(this.el, '.keyboard').classList.contains('password')
        &&
        !q(this.el, '.villain').classList.contains('access-denied')
    )
        this.initKeyboardUI();
    else
        this.listen('index/passwordPage', this.initKeyboardUI.bind(this), true);

    // hack for page transition effect
    var op = q(this.el, 'img.o.p');
    if (op) op.classList.remove('p');

    return this;
}

inherit(KeyboardController, Page);
loader.registerController('index', KeyboardController);

KeyboardController.prototype.initKeyboardUI = function(e) {
    this.initSynth();
    this.initCanvas();
    this.initMidi(e);
    this.initKnobs(e);
    this.initForm();
    this.bindKeyEvents();
    this.bind(window, 'resize', this.initCanvas.bind(this));
    this.bind(window, 'orientationChange', this.initCanvas.bind(this));
}

KeyboardController.prototype.cleanup = function() {
    if (this.context) {
        this.context = null;
        this.deleteControls();
        this.killOscillators();
        this.analyser = null;
        this.waveform = null;
    }
}

////////////////////////////////////////////////////////////////////////////////

KeyboardController.prototype.initSynth = function() {
    this.context    = MASTER_AUDIO_CTX;
    this.masterGain = this.context.createGain();
    this.analyser   = this.context.createAnalyser();
    this.waveform   = new Float32Array(this.analyser.frequencyBinCount);

    this.masterGain.gain.value = 1;
    this.masterGain.connect(this.context.destination);
    this.masterGain.connect(this.analyser);

    this.ENV_TIME_CONSTANT  = 2.5;
    this.gain               = .5;
    this.attack             = .0;
    this.decay              = .2;
    this.sustain            = .8;
    this.release            = .1;
    this.osc1Wave           = 'sine';
    this.filterType         = 'lowpass';
    this.filterFreq         = 1;
    this.filterQ            = .25;
}

KeyboardController.prototype.bindKeyEvents = function() {
    this.activeKeys = [];
    this.history    = [];
    this.clicking   = false;
    this.osc        = {};

    var keys    = qs(this.el, '.keys a'),
        down    = this.keyDown.bind(this),
        up      = this.keyUp.bind(this),
        enter   = this.mouseEnter.bind(this),
        mDown   = this.mouseDown.bind(this),
        mUp     = this.mouseUp.bind(this);

    for (var i=0; i<keys.length; i++) {
        this.bind(keys[i], 'click', stop);
        this.bind(keys[i], 'mousedown', mDown);
        this.bind(keys[i], 'mousedown', down);
        this.bind(keys[i], 'touchstart', down);
        this.bind(keys[i], 'mouseup', mUp);
        this.bind(keys[i], 'mouseup', up);
        this.bind(keys[i], 'mouseenter', enter);
        this.bind(keys[i], 'mouseleave', up);
        this.bind(keys[i], 'touchend', up);
    }
}

KeyboardController.prototype.mouseDown = function(e) {
    this.clicking = true;
}

KeyboardController.prototype.mouseUp = function(e) {
    this.clicking = false;
}

KeyboardController.prototype.mouseEnter = function(e) {
    if (this.clicking) return this.keyDown(e);
}

KeyboardController.prototype.keyDown = function(e, key, velocity) {
    if (e) e.preventDefault();
    if (key) {
        var keySel = '.k' + key;
    } else {
        var keySel = '.k' + e.target.dataset.key,
            key    = parseInt(e.target.dataset.key);
    }
    if (this.activeKeys.indexOf(key) === -1) {
        this.activeKeys.push(key);
        var el = q(this.el, keySel);
        if (el) el.classList.add('active');
        this.createOscillator(key);
        this.history.push(key);
    }
    if (this.resetCanvasTimer) {
        clearTimeout(this.resetCanvasTimer);
        this.resetCanvasTimer = null;
    }
    q(this.el, 'canvas.back').classList.add('playing');
}

KeyboardController.prototype.keyUp = function(e, key) {
    if (e) e.preventDefault();
    if (key) {
        var keySel       = '.k' + key;
    } else {
        var keySel       = '.k' + e.target.dataset.key,
            key         = parseInt(e.target.dataset.key);
    }
    var activeIndex = this.activeKeys.indexOf(key);
    if (activeIndex > -1) {
        var el = q(this.el, keySel);
        if (el) el.classList.remove('active');
        this.activeKeys.splice(activeIndex, 1);
        this.stopOscillators(key);
        if (this.activeKeys.length == 0 && !this.resetCanvasTimer)
            this.resetCanvasTimer = setTimeout(function () {
                q(this.el, 'canvas.back').classList.remove('playing');
                this.resetCanvasTempState();
                this.resetTinyCanvas();
                this.animateHand();
                var form = q(this.el, 'form');
                form.classList.remove('no-transition');
                form.classList.remove('inactive');
            }.bind(this), 100);
    }
}

KeyboardController.prototype.createOscillator = function(key) {
    var c = this.context,
        o = {
            key:        key,
            osc1:       null,
            noise1:     this.generateNoise(),
            osc1Mix:    c.createGain(),
            noise1Mix:  c.createGain(),
            filter:     c.createBiquadFilter(),
            env:        c.createGain(),
            releaseEnv: c.createGain(),
            amp:        c.createGain(),
            startTime:  c.currentTime
        };

    var id = util.makeId();

    var freq = 27.5 * Math.pow(2, (key - 21)/12);

    o.env.gain.value = 0;
    o.releaseEnv.gain.value = 1;
    o.amp.gain.value = this.gain;
    o.osc1 = c.createOscillator();
    o.osc1.frequency.value = freq;
    o.osc1.type = this.osc1Wave != 'noise' ? this.osc1Wave : 'sine';
    o.osc1.start(0);
    o.osc1.connect(o.osc1Mix);
    o.noise1.start(0);
    o.noise1.connect(o.noise1Mix);
    o.osc1Mix.connect(o.filter);
    o.noise1Mix.connect(o.filter)
    o.filter.connect(o.env);
    o.env.connect(o.releaseEnv);
    o.releaseEnv.connect(o.amp);
    o.amp.connect(this.masterGain);

    if (this.osc1Wave != 'noise') {
        o.osc1Mix.gain.value = 1;
        o.noise1Mix.gain.value = 0;
    } else {
        o.osc1Mix.gain.value = 0;
        o.noise1Mix.gain.value = 1;
    }

    o.filter.type = this.filterType;
    o.filter.frequency.value = this.computeFilterFreq();
    o.filter.Q.value = this.computeFilterQ();

    this.osc[id] = o;

    this.setEnvelope(id);
}

KeyboardController.prototype.setAmp = function(amount) {
    this.gain = amount;
    for (var oscKey in this.osc)
        if (this.osc.hasOwnProperty(oscKey))
            this.osc[oscKey].amp.gain.value = amount;
}

KeyboardController.prototype.setAttack = function(amount) {
    this.attack = amount;
    this.resetEnvelopes();
}

KeyboardController.prototype.setDecay = function(amount) {
    this.decay = amount;
    this.resetEnvelopes();
}

KeyboardController.prototype.setSustain = function(amount) {
    this.sustain = amount;
    this.resetEnvelopes();
}

KeyboardController.prototype.setRelease = function(amount) {
    this.release = amount;
    this.resetEnvelopes();
}

KeyboardController.prototype.osc1WaveToVal = function() {
    switch (this.osc1Wave) {
        case 'sine':
            return 0;
        case 'triangle':
            return .25;
        case 'square':
            return .5;
        case 'sawtooth':
            return .75;
        case 'noise':
            return 1;
    }
}

KeyboardController.prototype.oscValToWave = function(amount) {
    var val = (Math.round(amount * 4) / 4).toFixed(2);
    switch (parseFloat(val)) {
        case 0:
            return 'sine';
        case .25:
            return 'triangle';
        case .5:
            return 'square';
        case .75:
            return 'sawtooth';
        case 1:
            return 'noise';
    }
}

KeyboardController.prototype.setOsc1Wave = function(amount) {
    var wave    = this.oscValToWave(amount),
        symbols = qs(this.el, '.osc1 .waveform'),
        symbol  = q(this.el, '.osc1 .waveform.'+wave);

    this.osc1Wave = wave;

    for (var i=0; i<symbols.length; i++)
        symbols[i].classList.remove('visible');

    if (symbol) symbol.classList.add('visible');

    for (var oscKey in this.osc) {
        if (this.osc.hasOwnProperty(oscKey)) {
            if (wave != 'noise') {
                this.osc[oscKey].osc1.type = wave;
                this.osc[oscKey].osc1Mix.gain.value = 1;
                this.osc[oscKey].noise1Mix.gain.value = 0;
            } else {
                this.osc[oscKey].osc1Mix.gain.value = 0;
                this.osc[oscKey].noise1Mix.gain.value = 1;
            }
        }
    }
}

KeyboardController.prototype.filterTypeToVal = function() {
    switch (this.filterType) {
        case 'lowpass':     return 0;
        case 'highpass':    return .25;
        case 'bandpass':    return .5;
        case 'notch':       return .75;
        case 'allpass':     return 1;
    }
}

KeyboardController.prototype.filterTypeAmountToString = function(amount) {
    var val = (Math.round(amount * 4) / 4).toFixed(2);
    switch (parseFloat(val)) {
        case 0:     return 'lowpass';
        case .25:   return 'highpass';
        case .5:    return 'bandpass';
        case .75:   return 'notch';
        case 1:     return 'allpass';
    }
}

KeyboardController.prototype.setFilterType = function(amount) {
    var type  = this.filterTypeAmountToString(amount),
        label = q(this.el, '.filter-type h3');

    // log('setFilterType: ', type);

    this.filterType = type;
    switch (type) {
        case 'lowpass': label.textContent = 'lp'; break;
        case 'highpass': label.textContent = 'hp'; break;
        case 'bandpass': label.textContent = 'bp'; break;
        default: label.textContent = type; break;
    }

    for (var oscKey in this.osc) {
        if (this.osc.hasOwnProperty(oscKey)) {
            this.osc[oscKey].filter.type = type;
        }
    }
}

KeyboardController.prototype.setFilterFreq = function(amount) {
    // log('setFilterFreq: ', amount);
    
    this.filterFreq = amount;

    for (var oscKey in this.osc) {
        if (this.osc.hasOwnProperty(oscKey)) {
            this.osc[oscKey].filter.frequency.value = this.computeFilterFreq();
        }
    }
}

KeyboardController.prototype.computeFilterFreq = function() {
    if (this.filterFreq == 1)
        return this.context.sampleRate / 2;
    else
        return (this.context.sampleRate/16) * this.filterFreq;
}

KeyboardController.prototype.setFilterQ = function(amount) {
    // log('setFilterQ: ', amount);
    
    this.filterQ = amount;

    for (var oscKey in this.osc) {
        if (this.osc.hasOwnProperty(oscKey)) {
            this.osc[oscKey].filter.Q.value = this.computeFilterQ();
        }
    }
}

KeyboardController.prototype.computeFilterQ = function() {
    return this.filterQ * 100 || 0.01;
}

KeyboardController.prototype.resetEnvelopes = function() {
    for (var oscKey in this.osc) {
        if (this.osc.hasOwnProperty(oscKey)) {
            this.setEnvelope(oscKey);
        }
    }
}

KeyboardController.prototype.setEnvelope = function(oscId) {
    var c           = this.context,
        o           = this.osc[oscId],
        attackTime  = this.computeEnvParamTime(this.attack) || .001,
        decayTime   = this.computeEnvParamTime(this.decay) || .001;

    if (o.releaseTimer) return;

    o.env.gain.setTargetAtTime(1, o.startTime, attackTime);
    o.env.gain.setTargetAtTime(this.sustain, o.startTime+attackTime, decayTime);
}

KeyboardController.prototype.computeEnvParamTime = function(amount) {
    return this.ENV_TIME_CONSTANT * amount;
}

KeyboardController.prototype.stopOscillators = function(key) {
    for (var oscId in this.osc) {
        if (this.osc.hasOwnProperty(oscId)) {
            if (this.osc[oscId].key == key) {
                this.stopOscillator(oscId);
            }
        }
    }
}

KeyboardController.prototype.stopOscillator = function(oscId) {
    if (this.osc[oscId].stopping) return;
    this.osc[oscId].stopping = true;

    // var c = 0;
    // for (var osc in this.osc) if (this.osc.hasOwnProperty(osc)) c++;
    // log('active oscillators: ', c);

    var c       = this.context,
        curTime = c.currentTime,
        rTime   = this.computeEnvParamTime(this.release) || .015;

    this.osc[oscId].releaseEnv.gain.setTargetAtTime(0.001, curTime, rTime);   

    this.osc[oscId].releaseTimer = setTimeout(function() {
        this.killOscillator(oscId);
    }.bind(this), rTime * 4000);
}

KeyboardController.prototype.killOscillators = function() {
    for (var oscId in this.osc)
        if (this.osc.hasOwnProperty(oscId))
            this.killOscillator(oscId);
}

KeyboardController.prototype.killOscillator = function(oscId) {
    if (!this.osc || !this.osc[oscId]) return;
    this.osc[oscId].osc1.stop();
    this.osc[oscId].noise1.stop();
    delete this.osc[oscId];
}

KeyboardController.prototype.generateNoise = function() {
    var node    = this.context.createBufferSource();

    if (!this.noiseBuffer) {
        var b0 = 0, b1 = 0, b2 = 0, b3 = 0, b4 = 0, b5 = 0, b6 = 0
            sampleRate  = this.context.sampleRate,
            buffer      = this.context.createBuffer(1, sampleRate, sampleRate);
            data        = buffer.getChannelData(0);
        
        // for (var i = 0; i < sampleRate; i++) data[i] = Math.random(); // dumb

        // thnanks to Paul Kellet's pink noise approximation algorithm
        // http://www.musicdsp.org/files/pink.txt
        for (var i = 0; i < sampleRate; i++) {
            var white = Math.random() * 2 - 1;
            b0 = 0.99886 * b0 + white * 0.0555179;
            b1 = 0.99332 * b1 + white * 0.0750759;
            b2 = 0.96900 * b2 + white * 0.1538520;
            b3 = 0.86650 * b3 + white * 0.3104856;
            b4 = 0.55000 * b4 + white * 0.5329522;
            b5 = -0.7616 * b5 - white * 0.0168980;
            data[i] = b0 + b1 + b2 + b3 + b4 + b5 + b6 + white * 0.5362;
            data[i] *= 0.11;
            b6 = white * 0.115926;
        }
        this.noiseBuffer = buffer;               
    }
    
    node.buffer = this.noiseBuffer;
    node.loop = true;
    return node;
}

////////////////////////////////////////////////////////////////////////////////

KeyboardController.prototype.initKnobs = function(e) {
    this.KNOB_ORIGIN        = 60;
    this.KNOB_RADIUS        = 50;
    this.knobs              = {};
    this.knobY              = null;
    this.activeKnob         = null;

    var knobMove            = this.knobMove.bind(this),
        knobRelease         = this.knobRelease.bind(this);

    this.bind(document.body, 'mousemove', knobMove);
    this.bind(document.body, 'touchmove', knobMove);
    this.bind(document.body, 'mouseup', knobRelease);
    this.bind(document.body, 'touchend', knobRelease);

    var controlsContainer       = c('div'),
        controlsBackdrop        = c('div'),
        controlsPortal          = c('div'),
        controls                = c('div'),

        osc1                    = c('div'),
        osc1Title               = c('h2'),
        osc1Group               = c('div'),
        osc1WaveCtl             = c('div'),
        osc1WaveTitle           = c('h3'),

        filter                  = c('div'),
        filterTitle             = c('h2'),
        filterGroup             = c('div'),
        fTypeCtl                = c('div'),
        fTypeTitle              = c('h3'),
        fFreqCtl                = c('div'),
        fFreqTitle              = c('h3'),
        fQCtl                   = c('div'),
        fQTitle                 = c('h3'),

        env                     = c('div'),
        envTitle                = c('h2'),
        envGroup                = c('div'),
        attackControl           = c('div'),
        attackTitle             = c('h3'),
        decayControl            = c('div'),
        decayTitle              = c('h3'),
        sustainControl          = c('div'),
        sustainTitle            = c('h3'),
        releaseControl          = c('div'),
        releaseTitle            = c('h3'),

        amp                     = c('div'),
        ampTitle                = c('h2'),
        ampGroup                = c('div'),
        volControl              = c('div'),
        volTitle                = c('h3');

    controlsContainer.className = 'controls-container hidden faded';
    controlsBackdrop.className  = 'backdrop';
    controlsPortal.className    = 'portal';
    controls.className          = 'controls';

    osc1.className              = 'pane';
    osc1Group.className         = 'control-group';
    osc1WaveCtl.className       = 'control osc1';
    osc1Title.textContent       = 'Osc';
    osc1WaveTitle.textContent   = 'Waveform';

    controls.appendChild(osc1);
    osc1.appendChild(osc1Title);
    osc1.appendChild(osc1Group);
    osc1Group.appendChild(osc1WaveCtl);
    osc1WaveCtl.appendChild(osc1WaveTitle);

    filter.className        = 'pane filter';
    filterGroup.className   = 'control-group';
    fTypeCtl.className      = 'control filter-type';
    fFreqCtl.className      = 'control';
    fQCtl.className         = 'control';
    filterTitle.textContent = 'Filter';
    fTypeTitle.textContent  = 'Type';
    fFreqTitle.textContent  = 'Freq';
    fQTitle.textContent     = 'Q';

    controls.appendChild(filter);
    filter.appendChild(filterTitle);
    filter.appendChild(filterGroup);
    filterGroup.appendChild(fTypeCtl);
    fTypeCtl.appendChild(fTypeTitle);
    filterGroup.appendChild(fFreqCtl);
    fFreqCtl.appendChild(fFreqTitle);
    filterGroup.appendChild(fQCtl);
    fQCtl.appendChild(fQTitle);

    env.className               = 'pane env';
    envGroup.className          = 'control-group';
    attackControl.className     = 'control';
    decayControl.className      = 'control';
    sustainControl.className    = 'control';
    releaseControl.className    = 'control';
    envTitle.textContent        = 'Env';
    attackTitle.textContent     = 'Attack';
    decayTitle.textContent      = 'Decay';
    sustainTitle.textContent    = 'Sustain';
    releaseTitle.textContent    = 'Release';

    controls.appendChild(env);
    env.appendChild(envTitle);
    env.appendChild(envGroup);
    envGroup.appendChild(attackControl);
    envGroup.appendChild(decayControl);
    envGroup.appendChild(sustainControl);
    envGroup.appendChild(releaseControl);
    attackControl.appendChild(attackTitle);
    decayControl.appendChild(decayTitle);
    sustainControl.appendChild(sustainTitle);
    releaseControl.appendChild(releaseTitle);

    amp.className           = 'pane';
    ampGroup.className      = 'control-group';
    volControl.className    = 'control';
    ampTitle.textContent    = 'Amp';
    volTitle.textContent    = 'Volume';

    controls.appendChild(amp);
    amp.appendChild(ampTitle);
    amp.appendChild(ampGroup);
    ampGroup.appendChild(volControl);
    volControl.appendChild(volTitle);

    controlsPortal.appendChild(controls);
    controlsBackdrop.appendChild(controlsPortal);
    controlsContainer.appendChild(controlsBackdrop);
    q(this.el, '.villain').appendChild(controlsContainer);

    this.createKnob(
        osc1WaveCtl,
        this.osc1WaveToVal(),
        this.setOsc1Wave.bind(this),
        4
    );
    this.createKnob(
        fTypeCtl,
        this.filterTypeToVal(),
        this.setFilterType.bind(this),
        4
    );
    this.createKnob(fFreqCtl, this.filterFreq, this.setFilterFreq.bind(this));
    this.createKnob(fQCtl, this.filterQ, this.setFilterQ.bind(this));
    this.createKnob(attackControl, this.attack, this.setAttack.bind(this));
    this.createKnob(decayControl, this.decay, this.setDecay.bind(this));
    this.createKnob(sustainControl, this.sustain, this.setSustain.bind(this));
    this.createKnob(releaseControl, this.release, this.setRelease.bind(this));
    this.createKnob(volControl, this.gain, this.setAmp.bind(this));

    var svg = function(parent, curve, className) {
        var ns    = 'http://www.w3.org/2000/svg',
            svg   = document.createElementNS(ns, 'svg'),
            path  = document.createElementNS(ns, 'path'),
            show  = curOsc == className ? 'visible' : '';

        svg.setAttributeNS(null, 'class', 'waveform ' + className + ' ' + show);
        svg.setAttributeNS(null, 'viewBox', '0 0 220 110');
        path.setAttributeNS(null, 'd', curve);

        svg.appendChild(path);
        parent.appendChild(svg);
    }.bind(this);

    var curOsc      = this.osc1Wave,
        noisePath   = 'M10 40 H 50 V 10 H 90 V 30 H 130 V 90 H 170 V 60 H 210';

    svg(osc1WaveCtl, 'M10 55 C 45 -5, 75 -5, 110 55 S 160 115, 210 55', 'sine');
    svg(osc1WaveCtl, 'M10 60 L 60 10 L 160 100 L 210 60', 'triangle');
    svg(osc1WaveCtl, 'M10 10 H 110 V 100 H 210', 'square');
    svg(osc1WaveCtl, 'M15 100 L 15 10 L 200 100', 'sawtooth');
    svg(osc1WaveCtl, noisePath, 'noise');

    this.showControls(controlsContainer);
    this.listen('index/rootPage', this.hideControls.bind(this));
    this.listen('index/passwordPage', this.showControls.bind(this));
}

KeyboardController.prototype.showControls = function(el) {
    fade.in(el instanceof Element ? el : 0 || q(this.el, '.controls-container'))
}

KeyboardController.prototype.hideControls = function() {
    fade.out(q(this.el, '.controls-container'), 1000);
}

KeyboardController.prototype.deleteControls = function() {
    fade.out(q(this.el, '.controls-container'), 1000, true);
}

KeyboardController.prototype.createKnob = function(el, amount, callback, snap) {
    var ns        = 'http://www.w3.org/2000/svg',
        container = document.createElement('div'),
        svg       = document.createElementNS(ns, 'svg'),
        arcGroup  = document.createElementNS(ns, 'g'),
        knobGroup = document.createElementNS(ns, 'g'),
        arc       = document.createElementNS(ns, 'path'),
        knob      = document.createElementNS(ns, 'circle'),
        indicator = document.createElementNS(ns, 'circle'),
        id        = util.makeId(),
        touchKnob = this.touchKnob.bind(this);
    
    svg.setAttributeNS(null, 'preserveAspectRatio', 'xMinYMin meet');
    svg.setAttributeNS(null, 'viewBox', '0 0 120 120');
    svg.setAttributeNS(null, 'id', id);

    container.className = 'svg-container';
    knobGroup.setAttributeNS(null, 'class', 'knob');
    arcGroup.setAttributeNS(null, 'class', 'arc');

    knobGroup.setAttributeNS(null, 'draggable', false);
    knob.setAttributeNS(null, 'draggable', false);
    indicator.setAttributeNS(null, 'draggable', false);

    arcGroup.appendChild(arc);
    knobGroup.appendChild(knob);
    knobGroup.appendChild(indicator);
    svg.appendChild(arcGroup);
    svg.appendChild(knobGroup);
    container.appendChild(svg);

    knob.setAttributeNS(null, 'cx', this.KNOB_ORIGIN);
    knob.setAttributeNS(null, 'cy', this.KNOB_ORIGIN);
    knob.setAttributeNS(null, 'r', this.KNOB_RADIUS * .75);

    var xI = this.KNOB_ORIGIN + (this.KNOB_RADIUS*.75) * Math.cos(Math.Pi*.8),
        yI = this.KNOB_ORIGIN + (this.KNOB_RADIUS*.75) * Math.sin(Math.Pi*.8),
        xC = this.KNOB_ORIGIN - ((this.KNOB_RADIUS * .75) * .45),
        yC = this.KNOB_ORIGIN + ((this.KNOB_RADIUS * .75) * .45),
        tO = this.KNOB_ORIGIN + 'px ' + this.KNOB_ORIGIN + 'px';

    indicator.setAttributeNS(null, 'cx', xC);
    indicator.setAttributeNS(null, 'cy', yC);
    indicator.setAttributeNS(null, 'r', (this.KNOB_RADIUS * .75) * .2);
    knobGroup.style.transformOrigin = tO;

    this.knobs[id] = {
        arc: arc,
        knob: knobGroup,
        amount: amount,
        callback: callback,
        snap: snap ? snap : 0
    };

    el.appendChild(container);
    this.setKnob(id, amount);

    this.bind(knobGroup, 'mousedown', touchKnob);
    this.bind(knobGroup, 'touchstart', touchKnob);
}

KeyboardController.prototype.setKnob = function(knobId, amount) {
    var knob            = this.knobs[knobId],
        visibleAmount   = amount;

    if (knob.snap)
        visibleAmount = (Math.round(amount * knob.snap) / knob.snap).toFixed(2);

    if (visibleAmount <= .5) {
        var bigFlag = 0,
            radEnd  = Math.PI*(1-visibleAmount * 2)*-1;
    } else {
        var bigFlag = 1,
            radEnd  = Math.PI*((visibleAmount==1 ? .9999 : visibleAmount)-.5)*2;
    }

    var radStart    = Math.PI * 1,
        r           = this.KNOB_RADIUS,
        o           = this.KNOB_ORIGIN,
        xS          = o + r * Math.cos(radStart),
        yS          = o + r * Math.sin(radStart),
        xE          = o + r * Math.cos(radEnd),
        yE          = o + r * Math.sin(radEnd),
        path        = 'M'+xS+' '+yS+' A '+r+' '+r+' 0 '+bigFlag+' 1 '+xE+' '+yE,
        rotate      = visibleAmount*(knob.snap ? (360-360/(knob.snap+1)) : 270);

    knob.arc.setAttributeNS(null, 'd', path);
    knob.knob.style.transform = 'rotate('+rotate+'deg)';
    knob.amount = amount;

    knob.callback(amount);
}

KeyboardController.prototype.touchKnob = function(e) {
    var knob    = util.findTarget(e.target, 'svg'),
        id      = knob.id;

    if (this.activeKnob == id) return;
    else this.activeKnob = id;

    stop(e);
}

KeyboardController.prototype.knobMove = function(e) {
    if (!this.activeKnob) return;

    if (e.originalEvent && e.originalEvent.touches)
        var y = e.originalEvent.touches[0].pageY;
    else
        var y = e.pageY;

    var oldY = this.knobY;
    this.knobY = y;

    if (!oldY) return;

    var change = (oldY - y) / 150,
        knob   = this.knobs[this.activeKnob],
        amount = knob.amount;

    if (amount + change > 1) amount = knob.snap ? 0 : 1;
    else if (amount + change < 0) amount = knob.snap ? 1 : 0;
    else amount = amount + change;

    this.knobY = y;

    this.setKnob(this.activeKnob, amount);
}

KeyboardController.prototype.knobRelease = function(e) {
    if (this.activeKnob && !this.knobY && this.knobs[this.activeKnob].snap) {
        var knob        = this.knobs[this.activeKnob],
            newAmount   = knob.amount + (1 / knob.snap);

        if (newAmount > 1) newAmount = 0;
        this.setKnob(this.activeKnob, newAmount);
    }

    this.activeKnob = null;
    this.knobY      = null;
}

////////////////////////////////////////////////////////////////////////////////

KeyboardController.prototype.initMidi = function(e) {
    // log('initMidi: ', e ? e.detail : 'not triggered');
    if (!navigator.requestMIDIAccess) {
        // log('midi not supported :(');
        return false;
    }
    navigator.requestMIDIAccess({sysex: false})
        .then(this.bindMidiEvents.bind(this), this.noMidiAccess.bind(this));
}

KeyboardController.prototype.bindMidiEvents = function(midiAccess) {
    // log('got midi access: ', midiAccess);
    var inputs = midiAccess.inputs.values(),
        msgEv  = this.midiMessage.bind(this);
    for (var input=inputs.next(); input && !input.done; input=inputs.next()) {
        this.bind(input.value, 'midimessage', msgEv);
    }
}

KeyboardController.prototype.midiMessage = function(message) {
    // log('midi message: ', message.data);
    var type = message.data[0] & 0xf0,
        note = message.data[1],
        vel  = message.data[2];
    switch (type) {
        case 144:
            this.keyDown(null, note, vel);
            break;
        case 128:
            this.keyUp(null, note);
            break;
    }
}

KeyboardController.prototype.noMidiAccess = function(err) {
    // log('no midi access: ', err);
}

////////////////////////////////////////////////////////////////////////////////

KeyboardController.prototype.initCanvas = function() {
    this.canvasBaseState = null;
    this.canvasBaseStateTiny = null;
    this.canvasReady = false;

    if (this.canvasImgStatus == "loaded")
        return window.requestAnimationFrame(function() {
            this.drawCanvas();
        }.bind(this));

    if (!this.canvasImgStatus) {
        this.canvasImgStatus = "loading";
        this.canvasImg = q(this.el, '.pipes img');
        var isLoaded = function() {
            this.canvasImgStatus = 'loaded';
            this.drawCanvas();
        }.bind(this);

        if (this.canvasImg.complete) return isLoaded();

        this.bind(this.canvasImg, 'load', isLoaded)
    }
}

KeyboardController.prototype.drawCanvas = function() {
    this.canvas     = q(this.el, 'canvas.back');
    this.canvasWave = q(this.el, 'canvas.wave');

    var canvas      = this.canvas,
        ctx         = canvas.getContext('2d'),
        width       = canvas.offsetWidth,
        height      = Math.round(width * .403125),
        imgWidth    = width,
        img         = this.canvasImg;

    // force rightward pixel drift
    width = width % 2 == 1 ? width : width + 1;
    height = height % 2 == 0 ? height : height + 1;

    canvas.width = width;
    canvas.height = height;

    this.canvasWave.width = width;
    this.canvasWave.height = height;

    ctx.drawImage(img, (width - imgWidth) / 2, 0, imgWidth, imgWidth * .403125);

    this.canvasCtx          = ctx;
    this.canvasWidth        = width;
    this.canvasHeight       = height;
    this.canvasTimestamp    = new Date().getTime();
    this.canvasWaveCtx      = this.canvasWave.getContext('2d');
    this.frameRequested     = false;

    if (!this.canvasBaseState) {
        this.canvasBaseState = ctx.getImageData(0, 0, width, height);
        var tinyCanvas = c('canvas'),
            tinyCtx = tinyCanvas.getContext('2d');
        tinyCanvas.width = width/2;
        tinyCanvas.height = height/2;
        tinyCtx.drawImage(canvas, 0, 0, width/2, height/2);
        this.canvasBaseStateTiny = tinyCtx.getImageData(0, 0, width/2, height/2)
        this.tinyCanvas = tinyCanvas;
        this.tinyCtx = tinyCtx;
        this.resetCanvasTempState();
    }

    this.canvasReady = true;

    if (typeof this.analyser.getFloatTimeDomainData === "undefined")
        return loader.loadScript('keyboard.polyfill', false, function() {
            log('loaded polyfill');
            requestAnimationFrame(this.analyze.bind(this));
        }.bind(this), true);

    requestAnimationFrame(this.analyze.bind(this));
}

KeyboardController.prototype.analyze = function() {
    if (this.canvasReady && this.context) {
        this.analyser.getFloatTimeDomainData(this.waveform);
        this.animateCanvas();
    }
    
    if (!this.frameRequested) {
        if (this.context) this.frameRequested = true;
        requestAnimationFrame(this.analyze.bind(this));
    }
}

KeyboardController.prototype.animateCanvas = function() {
    var canvas      = this.canvas,
        ctx         = this.canvasCtx,
        ctxWave     = this.canvasWaveCtx,
        tinyCtx     = this.tinyCtx,
        width       = this.canvasWidth,
        height      = this.canvasHeight,
        waveform    = this.waveform,
        tmpCanvas   = this.tmpCanvas,
        smallCanvas = this.tinyCanvas,
        xMultiplier = width / waveform.length;

    // animate the background
    if (this.activeKeys.length == 0) {
        ctx.putImageData(this.canvasBaseState, 0, 0);
    } else {
        var curTime = new Date().getTime(),
            updated = false;

        // only run every 32 milliseconds to save cpu juice
        if (curTime > this.canvasTimestamp + 32) {
            this.canvasTimestamp = curTime;
            this.canvasRotate--;
            updated = true;
        }

        ctx.drawImage(smallCanvas, 0, 0, width, height);

        if (updated) {
            var rotateAmt = this.canvasRotate*Math.PI/180 * this.rotateCoeff;
            tinyCtx.drawImage(canvas, 0, 0, width / 2, height / 2);
            tinyCtx.save();
            tinyCtx.rotate(rotateAmt);
            tinyCtx.globalAlpha = .5;
            tinyCtx.globalCompositeOperation = 'overlay';
            tinyCtx.drawImage(tmpCanvas, 0, 0, width, height);
            tinyCtx.restore();
            tinyCtx.save();
            tinyCtx.rotate(rotateAmt * -1);
            tinyCtx.globalAlpha = .5;
            tinyCtx.globalCompositeOperation = 'soft-light';
            tinyCtx.drawImage(tmpCanvas, 0, 0, width, height);
            tinyCtx.restore();
            ctx.drawImage(smallCanvas, 0, 0, width, height);
        }
    }

    // animate the waveform
    ctxWave.clearRect(0, 0, width, height);
    ctxWave.strokeStyle = 'black';
    ctxWave.beginPath()

    var maxAmplitude = 0,
        heightMod = height * 1.6;

    for (var i=0; i<waveform.length; i++) {
        var x = i,
            y = ((0.5 + waveform[i] / 2) * heightMod) - (heightMod * .1);

        if (waveform[i] > maxAmplitude) maxAmplitude = waveform[i];

        if (i == 0) ctxWave.moveTo(x * xMultiplier, y);
        else ctxWave.lineTo(x * xMultiplier, y);
    }
    maxAmplitude = maxAmplitude * 2;

    ctxWave.lineWidth = 1 + maxAmplitude * 5;
    ctxWave.globalAlpha = maxAmplitude > 1 ? 1 : maxAmplitude;
    
    ctxWave.stroke();
    this.frameRequested = false;
}

KeyboardController.prototype.resetTinyCanvas = function() {
    this.tinyCtx.drawImage(this.canvas, 0, 0, this.canvasWidth / 2, this.canvasHeight / 2);
}

KeyboardController.prototype.resetCanvasTempState = function() {
    var ctx     = this.canvasCtx,
        width   = this.canvasWidth,
        height  = this.canvasHeight,
        tmp     = ctx.createImageData(width / 2, height / 2),
        black   = ctx.createImageData(width / 2, height / 2);

    tmp.data.set(this.canvasBaseStateTiny.data);

    var pixels      = tmp.data,
        blackData   = black.data;

    for (var i=0; i<pixels.length; i = i + 4) {
        if (pixels[i] == 0 && pixels[i+1] == 0 && pixels[i+2] == 0) {
            blackData[i]   = 255;
            blackData[i+1] = 255;
            blackData[i+2] = 255;
            blackData[i+3] = 255;
        } else {
            blackData[i]   = 255;
            blackData[i+1] = 0;
            blackData[i+2] = 0;
            blackData[i+3] = 0;
        }
    }

    this.canvasRotate = 0;
    this.rotateCoeff = Math.round(Math.random()) ? 1 : -1;

    var tmpCanvas = c('canvas');
    tmpCanvas.width = width / 2;
    tmpCanvas.height = height / 2;
    var ctx2 = tmpCanvas.getContext('2d');
    ctx2.putImageData(black, 0, 0);
    this.tmpCanvas = tmpCanvas;
}

KeyboardController.prototype.animateHand = function() {
    var h = qs(this.el, '._'),
        b = [];
        i = 1,
        spaz = function(el, delay) {
            if (el.dataset.animating == 1) return;
            el.dataset.animating = 1;
            setTimeout(function() {
                el.classList.add('keyup');
                setTimeout(function() {
                    el.classList.remove('keyup');
                    el.dataset.animating = 0;
                }, delay + 10);
            }, delay);
        };
    for (var k=0; k<h.length; k++) b.push(h[k]);
    while (b.length) {
        var j  = Math.floor(Math.random()*b.length),
            el = b.splice(j, 1);
        spaz(el[0], i * 4);
        i++;
    }
}

////////////////////////////////////////////////////////////////////////////////

KeyboardController.prototype.initForm = function() {
    this.bind(q(this.el, 'form a'), 'click', function(e) {
        stop(e);
        this.history = [];
        q(this.el, 'form').classList.add('inactive');
    }.bind(this));
    this.bind(q(this.el, 'form'), 'submit', function(e) {
        stop(e);
        var xhr = new XMLHttpRequest(),
            d   = new FormData();        
        d.append('code', this.history.join(','));  
        d.append('json', true);      
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status != 200) return this.accessDenied();
                var json = JSON.parse(xhr.response);
                return loader.go(json.url);
            }
        }.bind(this);
        xhr.open("post", '/password', true);
        xhr.send(d);
    }.bind(this));
}

KeyboardController.prototype.accessDenied = function() {
    this.history = [];
    q(this.el, 'form').classList.add('inactive');
    header.hide();
    var villain = q(this.el, '.villain');
    villain.classList.add('access-denied');
    setTimeout(function() {
        if (villain) villain.classList.remove('access-denied');
        header.show();
    }, 2000);
}