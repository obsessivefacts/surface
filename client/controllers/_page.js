var Page = function() {}

Page.prototype.init = function(el) {
    this.el = el;
    this.events = [];
}

Page.prototype.bind = function(el, ev, handler, once, passive, tag) {
    var id = util.makeId();
    if (once)
        var fn = function(e) {
            handler(e);
            for (var i=0; i<this.events.length; i++) {
                var event = this.events[i];
                if (event.id == id) {
                    event.el.removeEventListener(event.ev, event.fn);
                    this.events.splice(i, 1);
                }
            }
        }.bind(this);
    else
        var fn = handler;
    el.addEventListener(ev, fn, passive ? {passive: true} : false);
    this.events.push({ el, ev, fn, id, tag });
};

Page.prototype.unbindByTag = function(tag) {
    for (let i=0; i<this.events.length; i++) {
        if (this.events[i].tag == tag) {
            this.events[i].el.removeEventListener(
                this.events[i].ev, this.events[i].fn);
            this.events.splice(i, 1);
            i--;
        }
    }
}

Page.prototype.dispose = function() {
    while (this.events.length) {
        var event = this.events.pop();
        var derp = event.el.removeEventListener(event.ev, event.fn);
    }
    try { this.cleanup(); }
    catch (err) { console.error('failed to cleanup: ', err); }
};

Page.prototype.listen = function(trigger, fn, once) {
    this.bind(document.body, trigger, fn, once);
}

Page.prototype.TRIGGER = function(trigger, data) {
    var event =  new CustomEvent(trigger, {detail: data})
    document.body.dispatchEvent(event);
}

Page.prototype.cleanup = function() {}