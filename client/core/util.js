'use strict';
const util = {
    parseURL: function(url) {
        const a = c('a');
        a.href = url;
        return { protocol: a.protocol, hostname: a.hostname, port: a.port,
                 pathname: a.pathname, search: a.search, hash: a.hash,
                 host: a.host };
    },

    makeId: function() {
        return Math.random().toString(36).substr(2, 9); // good enough
    },

    findTarget: function(node, type) {
        if (!node || !node.tagName) return;
        else if (node.tagName.toLowerCase() == type.toLowerCase()) return node;
        else return this.findTarget(node.parentNode, type);
    },

    reflow: function(el) { return el.offsetWidth; },

    numberWithCommas: function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },

    scrollToAnchor: el => {
        let hash = window.location.hash;

        if (!hash) {
            el.scrollTop = 0;
            return;
        }

        let anchor = q(el, hash) || q(el, 'a[name="'+hash.substr(1)+'"]');
        
        if (anchor) {
            let parent = anchor.offsetParent,
                offsetTop = anchor.offsetTop;

            // hahaha FUCK YOU
            while (parent.offsetParent) {
                offsetTop += parent.offsetTop;
                parent = parent.offsetParent;
            }
            el.scrollTop = offsetTop;
        }
    }
}