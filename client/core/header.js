var header = {

    el: null,
    subnavToggleCheckboxEl: null,
    scrollEl: null,
    scrollPos: 0,
    ticking: false,
    boundScroller: null,
    suppressed: false,

    audioCtx: null,
    masterGain: null,
    analyser: null,
    bufferLength: null,
    waveform: null,
    canvasCtx: null,
    frameRequested: false,
    buffer: null,
    sourceNode: null,
    startedAt: null,
    pausedAt: null,
    paused: true,
    audioDuration: null,
    playerState: 'none',

    initialize: function() {
        var page = e('.page');
        this.el = e('header');
        this.subnavToggleCheckboxEl = id('subnav_toggle');
        this.boundScroller = this.scrollFn.bind(this);
        this.bindToEl(page, document.body.dataset.pageType);

        var subnav = q(this.el, '.subnav');

        e('#player .play').addEventListener('click', this.play.bind(this));
        e('#player .pause').addEventListener('click', this.pause.bind(this));

        subnav.addEventListener('mouseleave', this.hideSubnav.bind(this));
        subnav.addEventListener('click', this.hideSubnav.bind(this));

        const a = [['shiba','inu'],['/home'],['/music'],['/facts'],['/blog']];

        // this will make sense for future generations to come
        for (let i = 1; i < 5; i++) {
            const d = c('li'), b = c('a');
            d.className = 'n' + i;
            b.href = i == 1 ? '/' : a[i][0];
            b.textContent = a[i][0].charAt(1).toUpperCase() + a[i][0].slice(2);
            d.appendChild(b);
            subnav.insertBefore(d, subnav.querySelector('li.meth'));
        }

        if (!this.audioCtx) this.audioCtx = MASTER_AUDIO_CTX;
        this.masterGain = this.audioCtx.createGain();
        this.analyser   = this.audioCtx.createAnalyser();
        this.analyser.fftSize = 256;
        this.bufferLength = this.analyser.frequencyBinCount;
        this.waveform   = new Uint8Array(this.bufferLength);

        this.masterGain.gain.value = 1;
        this.masterGain.connect(this.audioCtx.destination);
        this.masterGain.connect(this.analyser);
    },

    bindToEl: function(el, pageType) {
        if (this.scrollEl)
            this.scrollEl.removeEventListener('scroll', this.boundScroller);

        var lis = qs(this.el, 'li a');

        if (pageType)
            for (var i=0; i<lis.length; i++)
                if (lis[i].dataset.pageType == pageType)
                    lis[i].classList.add('active');
                else
                    lis[i].classList.remove('active');
        
        this.scrollEl = el;
        this.scrollPos = el.scrollTop;
        this.scrollEl.addEventListener('scroll', this.boundScroller, {passive: true});
    },

    scrollFn: function(e) {
        var direction = 1;
        if (this.scrollEl.scrollTop < this.scrollPos)
            var direction = -1;

        this.scrollPos = this.scrollEl.scrollTop;
        if (!this.ticking) {
            window.requestAnimationFrame(function() {
                this.ticking = false;
                if (direction == 1 && this.scrollPos > 1) {
                    this.hide();
                } else {
                    if (this.suppressed) {
                        this.suppressed = false;
                    } else {
                        this.show();
                    }
                }
            }.bind(this));
        }
        this.ticking = true;
    },

    show: function() { this.el.classList.remove('hidden'); },
    hide: function() { this.el.classList.add('hidden'); },
    suppress: function() { this.suppressed = true; },

    hideSubnav: function(e) {this.subnavToggleCheckboxEl.checked = false},

    // player-related functionality below

    loadMP3: function(url, meta) {
        var player      = id('player');

        if (this.sourceNode) this.pause(true);

        e('header').classList.add('player-enabled');
        player.classList.add('visible');
        this.setPlayerState('loading');

        if (meta && meta.title) var title = meta.title;
        else var title = url.substring(url.lastIndexOf('/')+1);
        q(player, 'span').textContent = title;

        this.drawCanvas();

        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function() {
            this.audioCtx.decodeAudioData(
                xhr.response,
                this.bufferLoad.bind(this),
                this.bufferErr.bind(this)
            );
        }.bind(this);
        xhr.send();

        this.unmutePlayerHack();
        setTimeout(function() { this.show(); }.bind(this), 200);
    },

    setPlayerState: function(state) {
        var player      = id('player'),
            loader      = q(player, '.loading'),
            error       = q(player, '.error'),
            playBtn     = q(player, '.play'),
            pauseBtn    = q(player, '.pause');

        if (state == 'error') error.classList.add('visible');
        else error.classList.remove('visible');

        if (state == 'loading') loader.classList.add('visible');
        else loader.classList.remove('visible');

        if (state == 'playing') pauseBtn.classList.add('visible');
        else pauseBtn.classList.remove('visible');

        if (state == 'paused') playBtn.classList.add('visible');
        else playBtn.classList.remove('visible');

        this.playerState = state;
        this.paused = state == 'playing' ? false : true;
    },

    drawCanvas: function() {
        if (!this.canvasCtx) {
            var canvas      = q(this.el, 'canvas'),
                ctx         = canvas.getContext('2d'),
                width       = canvas.offsetWidth,
                height      = canvas.offsetHeight;

            canvas.width = width;
            canvas.height = height;

            this.canvasCtx          = ctx;
            this.canvasWidth        = width;
            this.canvasHeight       = height;
        } else {
            var ctx     = this.canvasCtx,
                width   = this.canvasWidth,
                height  = this.canvasHeight;
        }
        ctx.clearRect(0, 0, width, height);

        this.analyser.getByteFrequencyData(this.waveform);

        var waveform        = this.waveform,
            bufferLength    = this.bufferLength,
            barWidth        = (width / bufferLength) * 2,
            x               = 1,
            barHeight;
      
        ctx.fillStyle = 'red';
        ctx.globalAlpha = 0.25;
        
        for(var i=0; i<bufferLength; i++) {
            if (x > width - 1) break;
            barHeight = waveform[i]/2;
            ctx.fillStyle = 'rgb(255,128,128)';
            ctx.fillRect(x,height-(barHeight/4),barWidth,barHeight);
            x += barWidth + 1;
        }

        ctx.globalAlpha = 0.25;
        ctx.fillStyle = 'white';
        ctx.fillRect(10, 28, 205, 3);
        ctx.globalAlpha = 1;
        if (this.paused)
            var pct = ((this.pausedAt/1000)/this.audioDuration);
        else
            var pct = (((Date.now()-this.startedAt)/1000)/this.audioDuration);
        ctx.fillRect(10, 28, 205 * Math.min(pct, 1), 3);

        if (pct > 1) this.pause(true);

        if (this.playerState == 'playing')
            requestAnimationFrame(this.drawCanvas.bind(this));
    },

    bufferLoad: function(buffer) {
        this.buffer = buffer;
        this.audioDuration = buffer.duration;
        this.play();
    },

    bufferErr: function(err) {
        log('error loading audio buffer: ', err);
        this.setPlayerState('error');
        e('#player span').textContent = 'Unable to decode MP3 :(';

    },

    play: function() {
        if (this.sourceNode) this.sourceNode.stop();

        var ctx  = this.audioCtx,
            node = ctx.createBufferSource();

        node.connect(this.masterGain);
        node.buffer = this.buffer;

        if (this.pausedAt) {
            this.startedAt = Date.now() - this.pausedAt;
            node.start(0, this.pausedAt / 1000);
        } else {
            this.startedAt = Date.now();
            node.start(0);
        }
        this.sourceNode = node;
        this.setPlayerState('playing');
        this.drawCanvas();
    },

    pause: function(reset) {
        this.sourceNode.stop();
        this.pausedAt = reset === true ? 0 : Date.now() - this.startedAt;
        this.setPlayerState('paused');
    },

    unmutePlayerHack: function() {
        var osc     = this.audioCtx.createOscillator(),
            gain    = this.audioCtx.createGain();
        gain.gain.value = .01;                      // inaudible-ish
        osc.frequency.value = 16000;                // inaudible-ish
        osc.connect(gain);
        gain.connect(this.masterGain);
        osc.start(0);
        setTimeout(function() { osc.stop() }, 50);
    }
}
header.initialize();