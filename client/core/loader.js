let hashChanged = false;

window.onpopstate = e => {
    let state = e.state || {};
    if (hashChanged) {
        state = { hash_changed: true, cur_path: loader.curPath };
        history.replaceState(state, document.title, window.location);
        hashChanged = false;
    }
    loader.historyChange(state);
}

document.addEventListener('click', e => {
    let target = util.findTarget(e.target, 'A');
    if (target && !target.target) {
        let loc = window.location;
        if (target.hostname !== loc.hostname) return;
        if (target.pathname == loc.pathname && target.hash != loc.hash) {
            let state = { hash_changed: true, cur_path: loader.curPath };
            history.replaceState(state, document.title, loc);
            hashChanged = true;
            return;
        }
        stop(e);
        if (target.pathname != loc.pathname) loader.go(target.href);
    }
});

let loader = {
    curPath: null,            // tracks the current path
    availableControllers: {}, // holds refs to controller(s) for each pageType
    activeControllers: [],    // holds refs to controller instances for curPath
    internalPaths: [],        // maps URL paths to controller instance methods
    pageData: {},             // holds meta/static info for each path we've seen
    originalId: null,         // we won't remember why this is really important.

    // populate pageData from static tags on page load, activate any controllers
    initialize: function() {
        let page        = e('.page'),
            path        = window.location.pathname,
            type        = document.body.dataset['pageType'],
            title       = e('title').textContent,
            meta        = [],
            metaTags    = es('meta.dynamic'),
            eject       = page.dataset.eject ? true : false,
            cdnSurface  = e('meta[name="cdn-surface"]'),
            cdnPlugins  = e('meta[name="cdn-plugins"]');

        // set CONFIG based on static content
        if (id('core').src.indexOf('min') > 0) CONFIG.ugly = true;
        if (cdnSurface) CONFIG.cdnSurface = cdnSurface.content;
        if (cdnPlugins) CONFIG.cdnPlugins = cdnPlugins.content;

        for (let tag of metaTags) {
            let raw = {};
            if (tag.name) raw.name = tag.name;
            if (tag.content) raw.content = tag.content;
            meta.push(raw);
        }
        this.pageData[path] = {
            meta: { meta: meta, title: title, eject: eject },
            pageType: type,
            content: page.innerHTML
        }
        history.replaceState({test: true}, title, window.location.href);
        this.activateControllers(page);
    },

    // registers a controller to activate when loading a given pageType
    registerController: function (pageType, controller) {
        if (typeof this.availableControllers[pageType] === "undefined")
            this.availableControllers[pageType] = [];

        for (let existingController of this.availableControllers[pageType])
            if (existingController.name === controller.name)
                return false;
        
        this.availableControllers[pageType].push(controller);
    },

    // deactivates all controllers on the current page
    deactivateControllers: function() {
        let active = this.activeControllers;
        while (active.length) {
            let controller = active.pop();
            controller.dispose();
        }
    },

    // creates and tracks instances for all controllers registered to this page
    activateControllers: function(el) {
        let pageType    = document.body.dataset['pageType'],
            available   = this.availableControllers[pageType],
            active      = this.activeControllers;

        if (typeof available !== "undefined" && available.length)
            for (let controller of available)
                active.push(new controller(el));
        
        this.curPath = window.location.pathname;
    },

    // grabs JSON content for a new page, activates the page on receipt
    // (unless rawCb is true, then exec the callback with the raw JSON)
    go: function(path, silent, redirect, rawCb) {
        if (!silent && !redirect) history.pushState({}, "", path);
        if (!redirect && this.handleInternalPath(util.parseURL(path).pathname))
            return;
        if (redirect)
            history.replaceState({test: true}, "", path);
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                switch (xhr.status) {
                    case 200:
                        let res = JSON.parse(xhr.response);
                        if (res.redirect)
                            return loader.go(res.redirect, false, true, rawCb);
                        if (rawCb)
                            return rawCb(res);
                        return this.activatePage(path, res);
                    case 401:
                        return this.go('/access_denied');
                    case 404:
                        return this.go('/404');
                    default:
                        alert('ERROR');
                        console.error('Error loading '+path);
                        console.error('Status:   ' + xhr.status);
                        console.error('Response: ', xhr.response);
                }
            }
        }.bind(this);
        xhr.open('get', path, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send();
    },

    loadHTML: function(src, callback, sri) {
        if (id('html-' + src)) return callback();
        let cdn = CONFIG.cdnSurface,
            xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                let tag = c('div');
                tag.innerHTML = xhr.response;
                tag.id = 'html-' + src;
                document.body.appendChild(tag);
                callback();
            }
        }.bind(this);
        xhr.open("get", (cdn ? cdn : '') + '/html/' + src + '.html', true);
        xhr.send();
    },

    loadCSS: function(src, callback, sri) {
        if (id('css-' + src)) return callback();

        let cdn = CONFIG.cdnSurface,
            tag = c('link'),
            url = (cdn ? cdn : '') + '/css/' + src + '.css';

        if (sri && sri['css/' + src]) {
            url += '?cache=' + sri['css/' + src].hashes.sha256;
            tag.integrity = sri['css/' + src].integrity;
        }            
        tag.rel = 'stylesheet';
        tag.href = url;
        tag.id = 'css-' + src;
        tag.crossOrigin = 'anonymous';
        
        let loadListener = () => {
            tag.removeEventListener('load', loadListener);
            callback();
        }
        tag.addEventListener('load', loadListener);
        document.head.appendChild(tag);
    },

    loadScript: function(script, plugin, callback, standalone, sri) {
        if (id(script)) return callback();

        if (standalone && !sri) {
            let xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status == 200) {
                    let res = JSON.parse(xhr.response);
                    this.loadScript(script, plugin, callback, standalone, res);
                }
            }.bind(this);
            xhr.open("get", '/sri/standalone/?script=' + script, true);
            // xhr.setRequestHeader('Content-Type', 'application/json');
            return xhr.send();
        }

        let sriPath = (plugin ? 'basement/' : 'js/') + script,
            cacheQuery = '',
            integrity = null;

        if (sri && sri[sriPath]) {
            cacheQuery = '?cache=' + sri[sriPath].hashes.sha256;
            integrity = sri[sriPath].integrity;
        }

        if (plugin)
            var src = (CONFIG.cdnPlugins ? CONFIG.cdnPlugins : '/basement')
                      + '/plugins/' + script + '.js' + cacheQuery;
        else {
            if (!standalone)
                var file = script
                    + (CONFIG.ugly && script.indexOf('_lib') === -1 ? '.min':'')
                    + '.js' + cacheQuery;
            else
                var file = '_standalone/' + script + '.js' + cacheQuery;
            var src = (CONFIG.cdnSurface ? CONFIG.cdnSurface:'')+'/js/'+file;
        }

        let scriptTag = c('script');
        scriptTag.src    = src;
        scriptTag.id     = script;
        scriptTag.type   = 'text/javascript';
        scriptTag.onload = callback;
        scriptTag.crossOrigin = 'anonymous';
        if (integrity) scriptTag.integrity = integrity;

        // NOTE - i think this is only relevant for IE9. Probably not used.
        // scriptTag.onreadystatechange = function() {
        //     if (this.readyState == 'complete') callback();
        // }
        e('html').appendChild(scriptTag);
    },

    // activates a page, either from optional new data, or existing hidden html
    activatePage: function(path, data, fromButtonNavigation) {
        path = util.parseURL(path).pathname;

        let el              = id('page-'+path),
            old             = id('page-'+this.curPath),
            loadedChunks    = 0,

            chunkLoaded = function() {
                loadedChunks++;
                if (loadedChunks == chunks) srslyShowPage();
            }.bind(this),

            srslyShowPage = function() {
                if (el !== old) {
                    // NOTE ~ disable transition effect due to performance :(
                    
                    // if (old.classList.contains(pageData.pageType)) {
                    //     el.classList.remove('hidden', 'faded');
                    //     old.classList.add('hidden');
                    // } else {
                    //     fade.in(el);
                    //     fade.out(old, null, old.dataset.eject ? true : false);
                    // }
                    
                    el.classList.remove('hidden', 'faded');
                    old.classList.add('hidden');
                    if (this.originalId) old.id = this.originalId;
                }
                this.internalPaths = [];
                this.originalId = null;
                header.bindToEl(el, pageData.pageType);
                header.show();
                this.deactivateControllers();
                this.activateControllers(el);

                if (window.location.hash && !fromButtonNavigation) {
                    util.scrollToAnchor(el);
                } else if (!fromButtonNavigation) {
                    el.scrollTop = 0;
                }
            }.bind(this);

        if (!el) {
            el = c('div');
            el.id = 'page-'+path;
            el.className = 'page hidden';
            document.body.appendChild(el);
        }
        if (data) {
            this.pageData[path] = data;
            el.innerHTML = data.content;
        }
        let pageData = this.pageData[path];
        if (!pageData) return this.go(path, true);                       // edge
        if (el.childElementCount == 0) el.innerHTML = pageData.content;  // case
        document.body.dataset['pageType'] = pageData.pageType;
        el.classList.add(pageData.pageType);
        if (!old.classList.contains(pageData.pageType))
            el.classList.add('faded');

        var metas = es('meta.dynamic');
        for (let meta of metas) meta.parentNode.removeChild(meta);

        if (pageData.meta) {
            if (pageData.meta.title)
                e('title').textContent = pageData.meta.title;
            if (pageData.meta.meta) {
                for (let meta of pageData.meta.meta) {
                    let tag = c('meta');
                    if (meta.name) tag.name = meta.name;
                    if (meta.content) tag.content = meta.content;
                    tag.className = 'dynamic';
                    document.head.appendChild(tag);
                }
            }
            if (pageData.meta.eject) el.dataset.eject = pageData.meta.eject;
            var scripts  = pageData.meta.scripts || [],
                libs     = pageData.meta.libs || [], // checkmate libs
                plugins  = pageData.meta.plugins || [],
                css      = pageData.meta.stylesheets || [],
                html     = pageData.meta.chunks || [],
                sri      = pageData.meta.sri || {},
                chunks   = scripts.length
                           + libs.length
                           + plugins.length
                           + css.length
                           + html.length;
            for (let script of scripts)
                this.loadScript(script, false, chunkLoaded, false, sri);
            for (let lib of libs)
                this.loadScript('_lib/'+lib, false, chunkLoaded, false, sri);
            for (let plugin of plugins)
                this.loadScript(plugin, true, chunkLoaded, false, sri);
            for (let stylesheet of css)
                this.loadCSS(stylesheet, chunkLoaded, sri);
            for (let file of html)
                this.loadHTML(file, chunkLoaded);
            if (chunks) return;
        }
        srslyShowPage();
    },

    historyChange: function(e) {
        let path = window.location.pathname,
            search = window.location.search;
        if (e.hash_changed && this.curPath == e.cur_path)
            return util.scrollToAnchor(id('page-'+this.curPath));
        if (this.handleInternalPath(path))
            return;
        else if (this.pageData[path] && !e.force_load) {
            if (e.force_load_after) {
                history.replaceState(
                    {force_load: true},
                    document.title,
                    window.location.href
                );
            }
            this.activatePage(path, null, true);
        } else
            this.go(path + (search ? search : ''), true);
    },

    // registers an internal path (maps URL path to controller instance method)
    registerInternalPath: function(re, fn, noDelete) {
        this.internalPaths.push({ re, fn, noDelete });
    },

    // calls controller instance method for a given path, if one is registered
    handleInternalPath: function(path) {
        for (let registeredPath of this.internalPaths) {
            var match = path.match(registeredPath.re);
            if (match) {
                if (!this.originalId) this.originalId = 'page-'+this.curPath;
                let curPage  = id('page-'+this.curPath),
                    existing = id('page-'+path);
                curPage.id = 'page-'+path;
                this.curPath = path;
                if (existing && !registeredPath.noDelete)
                    existing.parentNode.removeChild(existing);                
                registeredPath.fn(match.slice(1));
                return true;
            }
        }
        return false;
    }
}