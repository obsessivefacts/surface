var notifier = {
    DELAY: 4000,

    notify: function(text) {
        var notifications = id('notifier');
        if (!notifications) {
            notifications = c('div');
            notifications.id = 'notifier';
            document.body.appendChild(notifications);
        }
        var bubble = c('div'), row = c('div');
        row.className = 'row';
        bubble.className = 'notification invisible';
        bubble.textContent = text;
        row.appendChild(bubble);
        notifications.appendChild(row);

        setTimeout(function() { bubble.classList.remove('invisible'); }, 25);

        var dismiss = function() {
            bubble.classList.add('dismiss');
            setTimeout(function() { notifications.removeChild(row); }, 500);
        };

        var timer = setTimeout(dismiss, notifier.DELAY);

        bubble.addEventListener('mouseenter', function() {
            clearTimeout(timer);
        });

        bubble.addEventListener('mouseleave', function() {
            timer = setTimeout(dismiss, notifier.DELAY);
        });        

        bubble.addEventListener('click', dismiss)
    }
}