var fade = {
    DELAY: 250,
    pendingFades: [],

    in: function(el) {
        this.cancelPending(el);

        el.classList.add('faded');
        el.classList.remove('hidden');
        util.reflow(el);
        el.classList.remove('faded');
    },

    out: function(el, delay, destroy) {
        el.classList.add('faded');

        var id      = util.makeId(),
            pending = this.pendingFades,
            hide    = function() {
                el.classList.add('hidden');
                if (destroy && el.parentNode) el.parentNode.removeChild(el);
                for (var i=0; i<pending.length; i++) {
                    if (pending[i].id == id) {
                        clearTimeout(pending[i].timer);
                        pending.splice(i, 1);
                    }
                }
            };

        this.cancelPending(el);
        
        return this.pendingFades.push({
            id: id,
            el: el,
            timer: setTimeout(hide, delay || this.DELAY),
            fn: hide
        });
    },

    cancelPending: function(el) {
        var pending = this.pendingFades;
        for (var i=0; i<pending.length; i++)
            if (pending[i].el === el)
                pending[i].fn();
    }
}