var socket = {
    opened: false,
    openInProcess: false,
    queue: [],
    ws: null,
    subscriptions: {
    },
    pingTimeout: null,
    pongTimeout: null,

    open: function() {
        if (this.openInProcess) return;
        this.openInProcess = true;

        var wl = window.location;
        this.ws = new WebSocket(
            (wl.protocol.indexOf('https') > -1 ? 'wss://' : 'ws://')
            + wl.hostname + (wl.port ? ':' + wl.port : '') + '/socket'
        );
        this.ws.onopen = function() {
            log('socket open');
            this.openInProcess = false;
            this.opened = true;
            this.executeQueue();
            this.heartbeat();
        }.bind(this);
        this.ws.onmessage = function(e) {
            this.executeProviderCallbacks(e.data);
            this.heartbeat();
        }.bind(this);
        this.ws.onclose = function(e) {
            this.opened = false;
        }.bind(this);
    },

    send: function(provider, message) {
        this.queue.push(provider + ':' + message);
        if (!this.opened) return this.open();
        this.executeQueue();
    },

    executeQueue: function() {
        while (this.queue.length) {
            var message = this.queue.shift();
            log('sending: ', message);
            this.ws.send(message);
        }
    },

    executeProviderCallbacks: function(msg) {
        var components = msg.toString().split(/:(.+)/);
        if (components.length < 2)
            return console.error('BAD MESSAGE FROM SOCKET: ', msg);

        var provider = components[0],
            msg = components[1];

        for (var key in this.subscriptions) {
            if (this.subscriptions.hasOwnProperty(key) && key == provider) {
                this.subscriptions[key](msg);
            }
        }
    },

    subscribe: function(provider, handler) {
        this.subscriptions[provider] = handler;
        this.send('subscribe', provider);
    },

    unsubscribe: function(provider) {
        if (typeof this.subscriptions[provider] !== "undefined") {
            delete this.subscriptions[provider];
            this.send('unsubscribe', provider);
        }
    },

    heartbeat: function() {
        if (this.pingTimeout) clearTimeout(this.pingTimeout);
        if (this.pongTimeout) clearTimeout(this.pongTimeout);
        this.pingTimeout = setTimeout(function() {
            this.send('heartbeat', 'ping');
            this.pongTimeout = setTimeout(function() {
                console.warn('socket heartbeat failed. terminating link');
                this.ws.close();
                this.opened = false;
            }.bind(this), 1000);
        }.bind(this), 30000);
    },
}