var Promise      = require('bluebird'),
    Sequelize    = require('sequelize'),
    compression  = require('compression'),
    express      = require('express'),
    cookieParser = require('cookie-parser'),
    nunjucks     = require('nunjucks'),
    redis        = require('redis'),
    nrp          = require('node-redis-pubsub'),
    view         = require('./lib/view'),
    response     = require('./lib/response'),
    util         = require('./lib/util'),
    files        = require('./lib/files'),
    shitlib      = require('./lib/shitlib'),
    sitemap      = require('./lib/sitemap'),
    config       = require('../config'),
    db           = config.db,
    app          = express(),
    expressWs    = require('express-ws')(app);

app.use(compression());
app.use(express.static('static'));
app.use(cookieParser(config.security.cookieSecret));

nunjucks.configure(['views', 'static'], { autoescape: false, express: app, noCache: true });

var deps = {
  db: new Sequelize(db.db, db.user, db.pass, db.options),
  app,
  redis: redis.createClient(config.redis),
  nrp: new nrp({
    emitter: redis.createClient(config.redis),
    receiver: redis.createClient(config.redis)
  }),
  files: files.init(config.storage),
  ws: expressWs,
  sitemap
};

// HACK ~ fix Sequelize count operation for queries with include relational data
//      ~ more info: https://github.com/sequelize/sequelize/issues/9477
deps.db.addHook('beforeCount', function (options) {
  if (this._scope.include && this._scope.include.length > 0) {
    throw new Error('A "count" operation executed with scope-based inclusions will return an incorrect result');
  }
  if (options.include && options.include.length > 0) { options.include = null; }
});

app.disable('x-powered-by');
app.use(require('./lib/auth')(deps));
app.use(require('./lib/single-serving-friend')(deps));

require('./handlers/index')(deps);
require('./handlers/socket')(deps);
require('./handlers/password')(deps);
require('./handlers/basement')(deps);
require('./handlers/user')(deps);
require('./handlers/blog')(deps);
require('./handlers/cookie')(deps);
require('./handlers/write')(deps);
require('./handlers/about')(deps);
require('./handlers/contact')(deps);
require('./handlers/sri')(deps);
require('./handlers/guestbook')(deps);
require('./handlers/fmiffel')(deps);
require('./handlers/memespeech')(deps);
require('./handlers/volition')(deps);
require('./handlers/facts')(deps);
require('./handlers/gemini-proxy')(deps);

const _static = (req, res, viewPath, viewParams) => {
  if (!viewParams) data = {};

  let promise = Promise.resolve(),
      spammer = false;

  // NOTE ~ disable shitlib for now
  /*
  if (shitlib.isSpammyRequest(req)) {
    let hashedIP = util.getHashedIPFromReq(req)
    promise = promise.then(() => {
      return shitlib.isBannedHashedIP(hashedIP).then(isBanned => {
        if (isBanned) {
          spammer = true;
          console.error('!BANNED!: ', hashedIP);
          throw new Error(hashedIP)
        }
        return false;
      })
    })
  }
  */

  return promise.then(() => view.render(req, viewPath, viewParams))
    .then(data => response.send(data, req, res))
    .catch(err => {
      if (spammer) return response.HTTPError(req, res, 403);
      response.error(err, req, res)
    });
};

const staticPages = [
  {path: '/music', view: 'music', viewParams: {}},
  {path: '/access_denied', view: 'index', viewParams: {access_denied: true}},
  {path: '/access_granted', view: 'access_granted', viewParams: {}},
  {path: '/sitemap', view: 'sitemap', viewParams: {}},
  {path: '/privacy', view: 'privacy', viewParams: {}},
  {path: '/terms', view: 'terms', viewParams: {}},
  {path: '/dmca', view: 'dmca', viewParams: {}},
]
for (const page of staticPages) {
  app.get(page.path, (req, res) => _static(req, res, page.view, page.viewParams));
  sitemap.add(page.path);
}

app.get('/sitemap.xml', (req, res) => {
  return response.send({raw: sitemap.dumpXML()}, req, res, {raw: true});
});

app.use(function(req, res, next){ response.show404(req, res); });

deps.redis.on('connect', function() {
  console.log('redis connected. app listening.');
  app.listen(9001);
});