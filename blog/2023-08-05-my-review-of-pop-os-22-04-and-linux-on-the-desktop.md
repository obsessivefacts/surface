---
title: 'My review of Pop!_OS 22.04 vs. MacOS and Windows'
subtitle: 'TL;DR: Pop!_OS > Windows > MacOS'
author: 'henriquez'
date: '2023-08-05 23:11:00 CST'
tags: 'Pop!_OS, Linux, Ubuntu, MacOS, Windows, Free Software'
---

I've been building and tinkering with computers since I was a small child.
Originally I would salvage old computer parts that schools and businesses were
throwing away, swapping broken parts for whatever working hardware I could find,
in the process converting my parents' family room into a junkyard of resurrected IBM PS/2s
and dot matrix printers, all of them beeping and clicking and running my
childish attempt at an artificial general intelligence.

Now that I'm grown up, my life is much the same, although instead of finding old
junk, I've blown altogether way too much money buying computers and experimenting
with new builds. This means I've never been a "PC guy" or a "Mac guy" or a
"Linux nerd" or anything else. I'm intimately familiar with Windows, MacOS and
many flavors of Linux and I appreciate all of them for what they are.

But recently, I sold off my Windows and Mac setups and made 2023 _my_ year
of Linux on the desktop. And so far I am loving it,
thanks in great part to Pop!\_OS 22.04, the only Linux distro I've used that
fits me like a glove. So in the following post, I will ramble on about
Pop!\_OS and why I took the plunge.

******

#### Drifting away from MacOS and Windows...

I like Macs for the quality of materials, the attention to detail and tight
integration between the hardware and software. This is only possible for
a massive company like Apple that controls all aspects of engineering,
manufacturing and software development. But they control too much. Recent
versions of MacOS have gotten steadily worse, bloated with half-baked features
like Stage Manager, and locked down more and more restrictively under the
hood.

Cory Doctorow spoke on [the 'coming war on general computation'](https://www.theverge.com/2012/1/2/2677097/cory-doctorow-28c3-war-on-general-computation),
and MacOS is headed in a direction that justifies his concern. Recent releases
have restricted parts of the "unixy" command line functionality (eg. try to place
a [symlink](https://en.wikipedia.org/wiki/Symbolic_link) in the root directory), and MacOS increasingly
makes you jump through hoops to run software downloaded from the Internet.
If the author didn't pay for Apple's [$99 permit to release software](https://developer.apple.com/support/enrollment/),
then MacOS assumes the software is untrustworthy, at least until you [answer
three riddles](https://support.apple.com/guide/mac-help/open-a-mac-app-from-an-unidentified-developer-mh40616/mac).

![Photo of Apple workstation](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/mac.jpg)

On the PC side, for less than the cost of [Apple's overpriced monitor](https://www.apple.com/pro-display-xdr/),
you can build a high end Windows gaming PC and still have enough money left over
to buy [Corsair's overpriced monitor](https://www.corsair.com/us/en/p/monitors/cm-9020003-na/corsair-xeneon-32uhd144-gaming-monitor-cm-9020003-na).
And in some ways, Windows is pretty cool, because using [Windows Subsystem for Linux 2](https://learn.microsoft.com/en-us/windows/wsl/install),
you can install a Linux VM CLI that integrates tightly with the core OS. It's like the best of
both worlds when you can run your full Linux stack including nginx, node and whatever
other daemons you want in [Windows Terminal](https://github.com/microsoft/terminal),
but also play AAA games that put the Playstation 5 to shame. What's not to love?

![Photo of Windows workstation with RGB lighting](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/windows.jpg)

Well one thing I didn't love is that Windows 11 cost me > $100 retail, but despite paying full price
it's still effectively adware. Everywhere you look in the OS is full of gallery links to
ad-infested celebrity gossip blogspam. And I always thought Google Chrome was bad, but Microsoft Edge
takes the Chrome codebase and adds tons of Windows 95 era toolbar bloat to it, with
unwanted chatbots and sketchy remote syncing features. What if I just want
to browse the web or watch videos in peace? Not possible, because everything I do syncs to my
Microsoft account whether I like it or not. Glance under the hood and there are tons
of processes sending data to the Internet all the time. I can't trust it and
ultimately I can't use the damn PC for more than 24 hours without some forced update
rebooting and resetting my privacy settings while I step away to sudo make a sandwich. 

Spending thousands of dollars on hardware only to be treated like a fucking
child when I want to use it just feels bad, man. Both Windows and MacOS infantilize
their users, and I just got sick of them.

#### So I had this beat up old Linux box...

I've been self-hosting my web sites for the better part of a decade, originally
using an old Dell laptop as an Ubuntu server until the motherboard failed. So in
2018 I pieced together a Linux server from 
old hardware I had laying around. The specs were nothing special but It was
just bomb proof. All it wanted to do was serve. When I was moving states, 
I unplugged it, threw it in the overhead compartment on an airplane, banged
it up a bit, and plugged it in on the other side after I landed. It just kept
going.

![My server waiting to board the aircraft](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/linux1.jpg)

Sure my server wasn't fancy or exciting, but it never complained when I ran it
in a stuffy closet, out of sight for years. I would install security updates, and
it would get out of my way and serve my websites. Shouldn't all computers be
like this? Could they be?

![Server in a closet](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/linux2.jpg)

#### Finding my perfect Linux

Linux has come a long way in the last ten years. [Ubuntu](https://ubuntu.com/)
made strides improving hardware compatibility and user experience to the point where I was
comfortable installing it as a daily driver OS on a work laptop. And then Ubuntu
ruined it by forcing [snapd](https://akashicseer.com/rants/why-ubuntu-snap-sucksssss/)
down everyone's throats, trading user control for Windows-style forced updates and
slow, glitchy (but sandboxed!) "app store" style package distribution. Some of my
coworkers swore by Arch or Manjaro Linux, but I noticed they spent about as much
time tweaking their OS and fixing usability issues as they spent actually working.
Not my cup of tea.

**What I want out of an operating system is simple:**

1. It should come preconfigured with sane default settings that allow me to...
2. ...work on my code and access UNIX-style [CLI tools.](https://en.wikipedia.org/wiki/Command-line_interface)
3. (Ideally) play games when I'm not working.
4. Allow me to customize the things I want, but otherwise get the hell out of my way.

Windows and MacOS more or less satisfy the first three points but fail horribly on point
number 4, and so does Ubuntu after recent developments. Thus for awhile I wrote Linux off,
settling for the ["least worst"](https://en.wikipedia.org/wiki/Joe_Biden)
commercial OS that fit my day-to-day workflow. That is, until I learned about System76
and their new Linux distribution.

#### Enter System76 and Pop!\_OS 22.04

[System76](https://system76.com/about) has been manufacturing custom
Linux PCs for a long time, but they recently started making waves with their
line of unique USA-made [Thelio Desktop PCs](https://system76.com/desktops), and
their launch of [Pop!\_OS](https://pop.system76.com/). Now originally I didn't think
much of Pop!\_OS, writing it off as a branded reskin of Ubuntu, but as I became
increasingly disillusioned with Ubuntu while seeing rave reviews of Pop!\_OS, I
took another look.

From [System76's site](https://pop.system76.com/):

> Pop!_OS is an operating system for STEM and creative professionals who use their computer as a tool to discover and create. Unleash your potential on secure, reliable open source software. Based on your exceptional curiosity, we sense you have a lot of it.

Well, golly gee, that sounds like me! I'm a STEM and creative professional! And
I'm curious! So on a whim, I backed up my Linux server and gave it a fresh
install of Pop!\_OS 22.04. And I ended up loving it so much that I made my server
my daily-driver desktop and selling off all my other computers!

#### Things I love about Pop!\_OS 22.04

Some of this stuff isn't unique to Pop!\_OS, but the totality of it makes for a
really slick package that nails my OS wish-list bullets from above.

**1. Super easy installation with NVIDIA driver support.**

The download link on the [Pop!\_OS](https://pop.system76.com/) homepage gives
you several options based on your hardware. I chose the NVIDIA .iso since my
server had an NVIDIA P2000 GPU. From there, they wrote a handy guide to
[creating a bootable USB installation drive](https://support.system76.com/articles/live-disk/),
which worked like a charm. The installer booted into a fully functioning Pop!\_OS
environment that allows you to test drive the system before reformatting any
drives, and the actual installation was super easy. I loved this compared
to [some other distros](https://wiki.archlinux.org/title/installation_guide).

**2. Not just an Ubuntu reskin!**

Like Ubuntu, Pop!\_OS uses Gnome components under the hood, but it's not Gnome
and it's definitely not Ubuntu either. By default, Pop!\_OS provides a desktop
environment similar to MacOS, with a familiar-looking dock and
an app launcher reminiscent of Spotlight search.

**3. No snapd — Pop!\_Shop offers dpkg and flatpak software packages.**

One of my biggest complaints with Ubuntu was their heavy-handed move to force snap
packages, where _sudo apt install firefox_ would give you a snap bundle that
takes many seconds to launch (seriously, Firefox was so bad on Ubuntu that the
snap devs [wrote a post on their attempts to fix it!](https://snapcraft.io/blog/improving-firefox-snap-performance-part-3))
Other gripes include things that should be easy actually being really hard, like
Minecraft mods breaking every time a forced update comes down the wire. And IMO
the forced updates go against the basic FOSS principles of user freedom.

Anyway, Pop!\_OS comes with none of this shit (unless you want it, then
_sudo apt install snapd_), and they offer an App Store with tons of useful
software. Honestly, this alone would be enough to sell me on Pop!\_OS if it were
otherwise no different than Ubuntu, but wait—there's more!

**4. Pop!\_OS' app launcher is really nice!**

By default, pressing the Super key brings up [Pop!\_OS' Launcher](https://support.system76.com/articles/pop-basics/)
which is similar to MacOS Spotlight but way better. Just start typing the first
few letters of the application you want and it narrows down a list of applications
with almost spooky clairvoyance as to what you're looking for. This is kind of how
Windows used to be before Microsoft added celebrity gossip blogspam results to
every system panel.

![Pop!\_OS Launcher Screenshot](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/launcher.png)

The Launcher is so psychic that I seldom have to use Pop's Applications
Panel, which also happens to be much more streamlined than the fullscreen
start menu UIs on Ubuntu and other systems. But it's there if you need it!

![Pop!\_OS Applications Panel](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/applications.png)

**5. Pop!\_OS offers an optional tiling window manager mode.**

Forget [i3-gaps](https://github.com/Airblader/i3) or whatever the Gen-Z kids are
using these days. Pop!\_OS allows you to flip a switch on the system menu that
turns on a tiling window manager, and they offer handy keyboard shortcuts to
make managing the layout really easy. Or you can just drag things around with
your mouse, getting realtime previews of the new layout, and optionally creating
stacks of applications similar to browser tabs. And you can adjust the gaps
in realtime, no editing of dotfiles required!

![Pop!\_OS Tiling Window Manager](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/tiles.png)

I'm mostly a luddite, but I still use the tiling window mode from time to time
when I have to manage and monitor a lot of applications at once. Switching modes
is super easy, so it's no trouble to move back and forth as needed.

**6. Yes, it can run Crysis! Proton runs Windows games with "RTX On™!"**

This isn't unique to Pop!\_OS, but one of the most amazing recent advancements
in Linux overall is its growing support for Windows games, driven in huge part
by Valve and their [SteamOS](https://store.steampowered.com/steamos) efforts.
Using ["Steam Play"](https://steamcommunity.com/games/221410/announcements/detail/1696055855739350561?snr=)
(aka Proton), you can simply install Windows games via [Steam](https://store.steampowered.com/)
and run them in Linux, in many cases with no trouble at all.

![Johnny Silverhand says let's do it!](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/johnny.jpg)

Compatibility with DirectX 12 and other cutting edge APIs is there, although some
games require tweaking to work perfectly (really tweaking is nothing new
to PC gaming). Fortunately there's the invaluable [protondb website](https://www.protondb.com/),
which lets you look up games and find the perfect compatibility settings for
your hardware. All in all I've found that most of my favorite games are working
with minimal effort, and usually with comparable to performance to running natively
in Windows. From a technical standpoint this is truly amazing.

Here are some of my favorite games that run well in Linux, many with full support for RTX and DLSS:

- Borderlands 3
- Control Ultimate Edition
- Crysis 1 / 2 / 3 Enhanced Editions
- Cyberpunk 2077
- Deathloop
- Fallout 76
- Far Cry 4 / 5 / 6
- Hogwarts Legacy
- Prey (2017)
- Watch Dogs Legion

**7. Stability is great; install system updates with confidence.**

You can't take this for granted in any OS, but in months of using Pop!\_OS
fulltime I've had no trouble downloading and installing kernel, security and GPU
driver updates. When my system goes down for reboot I can be confident that it
will come back up without a broken kernel and some arcane OS-recovery steps.
This is noteworthy because System76 has a much smaller dev team than
many of the bigger Linux distros, but they seem to be treating updates very
carefully and doing a great job.

#### Things I don't love about Pop!\_OS 22.04

**1. Customizing the Dock is a little more trouble than it needs to be.**

I'm happy the dock _is_ customizable, but one of the default behaviors I loved from
Windows and Ubuntu was the ability to hit Super + a number key and launch the
app in that position on the "taskbar." I have muscle memory that Super + 1 launches
Firefox, Super + 2 launches Sublime Text, etc. — In order to get this working in
Pop!\_OS you have to open an app called _dconf-editor_ and set the shortcuts up
manually. Someone on reddit wrote [a guide to setting this up](https://www.reddit.com/r/pop_os/comments/nsu5cu/launching_apps_with_keyboard_shortcuts_guide/),
but it shouldn't be this complicated, since the hooks to these keyboard shortcuts
are already present in the OS (but unused by default).

![Screenshot of dconf-editor](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/dconf-editor.png)

**2r. Pop!\_OS is abandoning GTK and I'm uncertain about the future.**

[Per Phoronix](https://www.phoronix.com/news/COSMIC-Desktop-Iced-Toolkit), System76
is planning to abandon [GTK](https://www.gtk.org/) in favor of ~its own~ a Rust-based UI framework known as 
["Iced."](https://blog.system76.com/post/more-on-cosmic-de-to-kick-off-2023)
I've read various accounts of the reasoning behind this, but from what I understand,
Gnome is making it increasingly difficult or impossible to develop UI extensions,
which is largely how Pop!\_OS provides such a vastly different user experience
than "vanilla" Gnome.

I have a lot of confidence in System76's engineers, but replacing GTK is a _huge undertaking_,
not unlike reinventing the wheel, and involves replacing many deeply-rooted user
interface APIs with totally new code. System76 is [documenting their progress on their blog](https://blog.system76.com/),
but it remains unclear when they plan to launch their refactored COSMIC Desktop Environment
and whether it will provide all of the features that Gnome / GTK has built up over more than
two decades. I don't think this is a reason to avoid Pop!\_OS, but it's something I'm watching
closely when it's time to do a major version upgrade.

**2023-08-11: Clarifications from System76 re: Iced Toolkit**

> Iced does not belong to us or anything like that, it's a community project that we are helping with for COSMIC and Rust in general.
> Iced is very popular in Rust's open source ecosystem. We're building our libcosmic platform around it.

#### In summary,

As usual this got way too long (the joys of unemployment)... I guess the main takeaway here is that I liked
Pop!\_OS so much that I sold off my fancy Mac setup and
tore down my Windows gaming PC to upgrade my Linux box, selling off what
was left of the donor box. And for once I don't feel like I'm compromising in any way
using Linux as a daily driver. I haven't experienced that with any other distribution
and I think System76 deserves props for an awesome OS!

![Ever-so-slightly upgraded Linux box](/images/blog/2023-08-05-my-review-of-pop-os-22-04-and-linux-on-the-desktop/bigbox-optimized.jpg)