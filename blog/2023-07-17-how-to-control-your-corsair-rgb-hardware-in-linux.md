---
title: 'How to control your Corsair RGB hardware in Linux'
author: 'henriquez'
image: 'desperate_fans_call_for_desperate_measures.jpg'
date: '2023-07-17 14:00:00 CST'
tags: 'Corsair, RGB, OpenRGB, iCUE, ckb-next, FOSS, Linux'
---

I've been a Corsair fanboy for awhile now. Back in the day Corsair made a name
for themselves by selling premium memory kits with lifetime warranties. Then
they started releasing computer cases, mice, keyboards, SSDs and water cooling
kits, and all of it was really good. Now they're even selling monitors and
fully-built gaming PCs. Wow! Take my money, Corsair!

Anyway Corsair has become notorious for putting RGB LEDs on pretty much
everything they sell. (They even offer [RGB RAM kits with no RAM!](https://www.corsair.com/us/en/p/pc-components-accessories/cmwlekit2/vengeance-rgb-pro-light-enhancement-kit-aca-a-black-cmwlekit2))
All of this stuff can be controlled through Corsair's [iCUE Application](https://www.corsair.com/us/en/s/icue),
which is sadly Windows- and MacOS-only. But what about us Linux users? Are we out
of luck? The answer may shock you!

***

#### Install Windows in a Virtual Machine to run iCUE in Linux!

You could argue that this defeats the purpose of Linux, but for the vast
majority of Corsair's USB-controlled hardware (except sadly their [RGB mousepad](https://www.corsair.com/us/en/p/mousepads/ch-9440020-na/mm800-rgb-polaris-gaming-mouse-pad-ch-9440020-na)),
you are able to save RGB lighting profiles to the hardware itself, meaning you
can just use Windows to initially get your RGB lighting and fans set up the way
you want, and then never use it again.

To do this, simply install the FOSS [VirtualBox](https://www.virtualbox.org/)
and then install your Windows of choice in a Virtual Machine (VM). You will need
a genuine Windows License Key, but there are some (apparently) [gray-market sites](https://www.g2a.com/microsoft-windows-10-home-microsoft-key-global-i10000083914003?aid=12202012&er=6bff7abd0c5aa5ba9faf093dccd2516d&___language=en&utm_source=google&utm_medium=surfaces&utm_campaign=gshopping_US&utm_content=surfaces_across_google)
selling them for pretty cheap.

![Screenshot of VirtualBox application](/images/blog/2023-07-17-how-to-control-your-corsair-rgb-hardware-in-linux/virtualbox.png)

Once you have Windows installed, you just need to "grab" the USB devices for all
the Corsair accessories you want to control, via the _Devices_ menu in
Virtualbox. This assigns the USB devices to the Windows VM, allowing them to
show up when you install iCUE.

For example, by grabbing the "CORSAIR iCUE COMMANDER Core" USB device, I'm able
to monitor and control the fan speeds, as well as customize the RGB colors on my
water cooler. I can save these settings to the hardware and then never run the
Windows VM again unless I want to tweak something.

![Screenshot of iCue application](/images/blog/2023-07-17-how-to-control-your-corsair-rgb-hardware-in-linux/icue.png)

**NOTE: if your water cooler has an LCD screen (eg. [iCUE H150i Elite LCD XT](https://www.corsair.com/us/en/p/cpu-coolers/cw-9060075-ww/icue-h150i-elite-lcd-xt-display-liquid-cpu-cooler)),
you will need to install the [Oracle VM Extension Pack](https://www.virtualbox.org/wiki/Download_Old_Builds_6_1) to get USB 3.0 running for
whichever version of VirtualBox you have installed.**

In order to save animated GIFs to your totally-useful and not-idiotic water
cooler LCD, you need USB 3.0 enabled for your Windows VM. Find your precise
version of VirtualBox in the "About Virtualbox..." menu and then download and
install the appropriate Extension Pack. Then enable USB 3.0 in your Windows VM's
settings panel. (This will require accepting a [non-FOSS Oracle License Agreement](https://www.virtualbox.org/wiki/VirtualBox_PUEL)
but I guess if we already installed Windows we've already jumped off that
bridge.)

Without USB 3.0 you can still control the LED ring around your LCD display, but
the "Screen Setup" panel will not show up in iCUE.

#### Install OpenRGB to customize your Corsair RGB memory

Using the FOSS [OpenRGB app](https://openrgb.org/) you can control RGB settings
for many memory modules, motherboards and video cards. Although you might be
able to find it in your Linux distro's "App Store", I would highly recommend
compiling it from source ([see the README for instructions](https://gitlab.com/CalcProgrammer1/OpenRGB/-/blob/master/README.md)).
This will ensure you have all the system dependencies installed which may not be
bundled with whatever you might find on Flathub.

Another crucial detail is to [enable kernel extensions for SMBus Access](https://gitlab.com/CalcProgrammer1/OpenRGB/-/blob/master/README.md#smbus-access-1),
which is how OpenRGB talks to many different hardware RGB devices, including
Corsair Vengeance and Dominator Pro RGB memory modules.

![Screenshot of OpenRGB application](/images/blog/2023-07-17-how-to-control-your-corsair-rgb-hardware-in-linux/openrgb.png)

Aside from setting it up, there are a couple of annoyances with OpenRGB
you should be aware of:

- You will need to enable user access to your SMBus if you don't want to run
  OpenRGB from the console as root. Again, the instructions are in the README
  but it's kind of a pain in the ass.

- You will probably want to disable OpenRGB for any Corsair USB devices that you
  could otherwise control via the Windows VM. I've noticed that if OpenRGB
  "touches" my Corsair M65 RGB Elite gaming mouse that it unbinds the
  keyboard shortcuts I've mapped to its various buttons in the hardware profiles.

#### Alternate solution: ckb-next

One alternative to both OpenRGB and running Windows in a VM is the FOSS
[ckb-next app](https://github.com/ckb-next/ckb-next). While I haven't personally
used it, it looks to support many Corsair devices via USB (but not memory
modules). In theory it could be a cleaner alternative if you aren't worried
about RAM and don't need the extra features offered by iCUE.

![Screenshot of ckb-next application](/images/blog/2023-07-17-how-to-control-your-corsair-rgb-hardware-in-linux/ckb-next.png)

There is also the [OpenCorsairLink project](https://github.com/audiohacked/OpenCorsairLink),
but it has been discontinued by the author. It's worth a mention because it still
technically works for older Corsair hardware in Linux, and I may have contributed
code to it in an alternate universe ;)

#### Wrapping up...

No computer build is complete unless it is crammed full of bright and distracting
RGB LEDs. Historically support for this has been a Windows-only affair but
recent advancements in Free and Open Source Software have opened new horizons
in Linux-controlled RGB bling. And if you want to have your cake and eat it too,
running Windows in VirtualBox can give you the power of Linux with the comfort
of your familiar Windows applications.