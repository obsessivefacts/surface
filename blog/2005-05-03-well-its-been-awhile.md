---
title: 'well it's been awhile...'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2005-05-03 00:00:00 PST'
tags: 'javascript, programming, deprecated'
---

I've been kind of slammed with work. I plan to redesign _undefined_ very
soon... right now I have a review of the _undefined_ posted here.

You probably have no idea what that means. Anyway, the new site should look
similar to that and be advanced to a new level of standards compliance. And
there will actually be content.

It will probably be stuff for geeks though since I am now a full time computer
nerd. So probably no more videos of crazy shit, although you can still get the
final Scream Team video here. (37 megabytes).

Unfortunately, I doubt I'll ever be coming back to _undefined_. Too much has
changed. I never expected to be making money out here. I never expected to enjoy
my job. But the unlikely happened: I have a reason to stay, and my family is
following me out here.

Just a news flash. It's been so long I doubt anyone reads this old site.