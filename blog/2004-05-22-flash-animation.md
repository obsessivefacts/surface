---
title: 'flash animation'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-05-22 00:00:00 PST'
tags: 'deprecated'
---

Check out the new page called "snoino."

So now I regret buying that _Macromedia Flash MX 2004 for Dummies_ book. I
figured out how to use the software on my own, and now I'm out $20. Oh well.
Maybe spending money gave me more motivation to learn or something.

I think I have a pretty good grasp on Flash, which isn't bad for only just a few
days of struggling. There's still a lot left to learn, (like how to make an
awesome computer game). Maybe I'll do that over the summer.

Alright, peace out.