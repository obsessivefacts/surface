---
title: 'happy birthday to me'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-09-01 00:00:00 PST'
tags: 'deprecated'
---

Yipee! I'm not a teenager anymore. Is that something to be happy about? I dunno.
Maybe it means I'm getting old and obsolete. I hope to be about a quarter done
with life now, (plan to die at 81) and so far so good.

I just got back from the Canadian Rockies road trip... it was amazing! And
apparently I'm not half bad at photography. Check out the new photos page.
Maybe I'm just full of myself again...

So Monday is the big move. I'm not sure if I'll have Internet access anymore...
Jesus! That thought scares me! I would take Internet over proper nutrition. So,
guess I'll shell out $$$ for DSL and only eat Perkins and Starbucks every day
instead of Aqua and Red Lobster. And I'll have to get a BMW instead of a
Porsche. Too bad. Santa Cruz, here I come!!!