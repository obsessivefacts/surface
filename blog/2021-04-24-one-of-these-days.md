---
title: 'One of these days I'm going to drop out.'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
image: 'lizard.jpg'
date: '2021-04-24 04:20:00 CST'
tags: 'fake, facts, debunked, reeeee'
---

I said it last year, and the year before, and it's still actually true. One of
these days I'm really going to drop out. I'll quit my job and focus on my art.
I'll become a full time weirdo and stop being a poser; I'll stop being a
hypocrite.

I'm just making stupid money and it would be stupid to quit right now. One
of these days my niche is going to collapse, like everything else. Time and time
we're told again _"it's not inflation—it's just that the stuff you want to buy
is all coincidentally more expensive than ever due to **[not inflation]**"_

I remember first it was food, then it was rent. Back when I was freelancing, the
pace of life was slowly increasing around me. All I could do was run to keep up,
faster, desperately trying to avoid being chucked off the back of the runaway
treadmill.

Our dear leaders told us to suck it up. They declined to prosecute the
architects of the Great Recession; instead our Department of Justice accepted
billion-plus dollar settlements, aka bribes. As I ran, I watched my friends
get chucked off the treadmill, never to recover. We were told they were just
lazy, entitled millenials. I had to reinvent myself out of necessity (did I?)

In any case, I'm not complaining. I'm happy that I did not crash and burn. Now
I'm like my old pal the big fat lizard, sleeping in a compost pile and eating
every bug and worm that comes my way. Sleeping and shitting and thinking lizard
thoughts. But I still dream.







