---
title: 'mononucleosis'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-06-27 00:00:00 PST'
tags: 'deprecated'
---

Well if it seems as if there haven't been any updates for awhile, it's because
I've been sick in bed with mono for the last week. I'm a bit better now, on the
recovering side (as opposed to the semi-suicidal immobile side) and it's back to
work.

Coming real soon is ZChess- the online multiplayer chess extravaganza. I know
I've been saying that for awhile, but now I'm actually well enough to finish it.
Give me a week. The non-fuctional version is already posted!