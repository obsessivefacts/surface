---
title: 'gemini-proxy'
subtitle: 'A Gemini browser, inspired by Netscape Navigator 2.0'
author: 'henriquez'
date: '2023-09-01 09:11:03 CST'
tags: 'gemini, proxy, css, netscape, lagrange'
image: 'gemini-proxy.png'
---

One of the best and worst things about the [Gemini Project](https://gemini.circumlunar.space/)
is the barrier to entry. It's a sort of walled off part of the web, akin to a nuclear
bunker for nerds. The whole thing has this sort of retro cyberpunk vibe, and browsing
Geminispace feels a lot like browsing the early, pre-"Web 2.0" World Wide Web, where
everything you see is created by ordinary people, not controlled by monopolistic
corporations and spy-bots. I totally dig it, but I didn't want it to be so walled off. So I made a proxy.

**[Click here to check out gemini-proxy!](/gemini-proxy)**

Gemini-proxy gives you the user experience of Netscape Navigator 2.0 running in
Windows 98 SE, with the style and beauty of the excellent [Lagrange Gemini Client](https://gmi.skyjake.fi/lagrange/),
all running in the comfort and safety of your familiar web browser. I don't think
it hurts anything to make Geminispace accessible to people who don't have the time
or expertise to set up specialized software, and it was a fun way to learn about
[the Gemini Protocol](https://gemini.circumlunar.space/docs/specification.gmi),
which was dead simple to implement.

**P.S. — I have disabled web crawlers from scraping Gemini URIs via robots.txt but if you hate gemini-proxy and don't want it touching your capsule, block 24.181.150.210**