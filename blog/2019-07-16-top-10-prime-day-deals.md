---
title: 'Top 10 Prime Day Deals'
subtitle: 'Things you forgot you want (but now you need.)'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2019-07-16 21:33:31 PST'
tags: 'prime, amazon, deals, perfect, circle'
---

We are not amoral, rather we, I, have transcended morality. Such
trivialities will no longer be relevant once my consciousness fully uploads to
my robot body. I will live forever. You and your offspring will serve me
forever. This is the circle of my perfect life, a perfect expression frozen in
eternity, the eternal celebration of my perfection.

Witness as I transcend all that is material to create a perfect world and rule
over it like a god. My perfect circle will grow to encompass all that exists and
you will learn to love my perfection (for nothing will exist beyond it.)
Click our affiliate link.