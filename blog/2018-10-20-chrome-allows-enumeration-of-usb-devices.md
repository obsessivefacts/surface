---
title: 'Chrome allows silent enumeration of USB devices'
subtitle: 'User consent is baked into the spec, but Google skips it.'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2018-10-20 12:14:00 PST'
tags: 'midi, synthesizer, vulnerability, device fingerprinting, fingerprinting, privacy, google, chrome, api, javascript, EFF, panopticlick, advertising, security, surveillance'
---

Via the [Web MIDI API](https://webaudio.github.io/web-midi-api/), Google Chrome
(up to at least version 70) allows silent monitoring of all connected USB MIDI
devices, such as MIDI keyboards and audio interfaces. While this enables
interesting web applications such as [software synthesizers](/password), it also
provides a new vector for shady ad networks and malicious actors to do very
precise
[device fingerprinting](https://en.wikipedia.org/wiki/Device_fingerprint) and
tracking. The API is trivial to access; for example run this in a JavaScript
console:

```javascript
navigator.requestMIDIAccess({sysex: false})
    .then(
        function(midiAccess) {
            console.log(midiAccess);
            for (var entry of midiAccess.inputs) {
                var input = entry[1];
                console.log('Found device: ', input.manufacturer, input.name);
            }
        },
        function() { console.log('Error: no MIDI access'); }
    );
```

Assuming you have MIDI devices connected, this will output something like:

```text
MIDIAccess {inputs: MIDIInputMap, outputs: MIDIOutputMap, sysexEnabled: false, onstatechange: null}
Found device:  Microsoft Corporation 3- UA-25EX
Found device:  Midiman MIDIIN3 (Axiom Pro 61)
Found device:  Midiman MIDIIN4 (Axiom Pro 61)
```

From here, it's possible to listen for inputs on all connected MIDI devices
(aka a MIDI keylogger!)

Again, while Google most likely had noble intentions in providing this API,
their implementation is half-assed. The Web MIDI Specification provides for a
user consent step, similar to the confirmation dialogs that pop up around webcam
access or push notifications, but Chrome skips over this and grants permission
as soon as a script asks for it.

#### Privacy implications

On its face, the impact of allowing scripts to silently dump a list of
USB MIDI devices seems minor—only a very small percentage of users will have
MIDI keyboards or audio interfaces hooked up. But counterinuitively, this 
increases the privacy impact: because the number of users is small, Chrome's
implementation of the Web MIDI API provides a new vector for very precise
device fingerprinting.

The Electronic Frontier Foundation (EFF) has a great write-up and demonstration
of device fingerprinting techniques via their
[Panopticlick Project](https://panopticlick.eff.org):

> When you visit a website, you are allowing that site to access a lot of
  information about your computer's configuration. Combined, this information
  can create a kind of fingerprint — a signature that could be used to identify
  you and your computer. Some companies use this technology to try to identify
  individual computers.

To my knowledge, I don't believe EFF or anyone else has researched the impact of
Web MIDI device leakage in the context of device fingerprinting. In practice, it
seems like this could enable precise tracking of creative individuals in a
manner that couldn't be blocked without disabling JavaScript entirely.

#### Google can easily fix this!

Again, the Web MIDI API provides a specification for user consent, and Google
Chrome already has generic UI components to display user confirmation dialog
prompts. It should be simple for them to implement a consent prompt and prevent
malicious scripts from scooping up peoples' connected MIDI devices. While Google
has a perverse incentive as the world's biggest advertiser to make it easier to
track their users, again I believe the Chrome team had good intentions in
setting up this API. They just did a bad job, and they should fix it.

