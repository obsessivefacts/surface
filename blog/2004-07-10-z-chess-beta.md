---
title: 'Z-Chess Beta'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-07-10 00:00:00 PST'
tags: 'deprecated, coming soon'
---

Well, I finally buckled down and finished it. Z-Chess is ready to go! It's a
real-time two player chess game. Just find a friend, sign up and get a game
going. Right now it's still in beta testing and may be a little buggy, but it's
playable (when the server isn't being all erratic).

I wrote it because I like playing chess with my friends. We're all spread out
across the country now, so there's no way to play in person, and Yahoo chess is
ugly and distracting (banners). I've made Z-Chess to be aesthetically simple,
yet extensively functional. I hope you'll agree.

It should work fine, but if something DOES go wrong, try closing your browser
and coming back. As long as one person stays in the game, it won't disappear off
the list.

Oh, and another thing: if you find that it won't let you move a piece to a place
you normally could, IT'S NOT A BUG! You'd be moving yourself into check, and the
game treats it the same way as if you tried to move a rook diagonally.

Anyway, be on the lookout for bugs (and a man called X) and give me a holler if
you find any. Enjoy! :-)