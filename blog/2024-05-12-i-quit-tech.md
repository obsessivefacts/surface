---
title: 'I quit tech.'
author: 'henriquez'
date: '2024-05-12 14:49:00 CST'
---

I pulled the RTX 4080 from the server and sold it for ETH. I've had this
[midlife crisis](/blog/2023-04-26-my-disappearing-act.html) on a
[slow burn](/blog/2021-06-02-i-got-bored-of-being-a-lizard.html) for a 
[very long time now](/blog/2021-04-24-one-of-these-days.html). Three years ago
I said I would drop out and I finally did. My disappearing act is complete.

Working for tech companies never made me feel good. The tech industry is not
making the world a better place. The Internet is mostly destroyed and our top
innovators are focused on putting more people out of jobs with their stupid AI
language models. It felt good to burn bridges on my way out.

Luckily I found an infinite money hack so I'm set for life as long as I keep
playing the game. I live in your critical infrastructure now, maintaining,
advancing, defending. It feels good to
do something useful, that helps people. Nobody around me knows what a psycho I
really am. I charm, blend in, fade to gray.

Now I am finally living life on my own terms and it feels good.
