---
title: 'I got bored of being a lizard'
subtitle: 'but I still like hanging out in the swamp.'
author: 'henriquez'
image: 'swamp.jpg'
author_email: 'henriquez@protonmail.com'
date: '2021-06-02 05:43:00 EST'
tags: 'lizard, swamp, ambitions'
---

About a month ago I wistfully recounted my enviousness of [the compost lizard
and his life of sedentary corpulence.](/blog/2021-04-24-one-of-these-days.html)
I resolved to be more lizard-like in my own ambitions and have since given it
an honest try. However, upon reflection of my life as a lizard, I've
realized that it's not a life for me.

The lizard exists solely to consume, become obese, and to proliferate
reptilianism upon the world around it. These Lizard Imperitives map
precisely to the set of ambitions that define the lizard-life. There is no place
in the mind of a lizard for ambitions of the human soul; art, empathy, the
pursuit of knowledge, friendship: all of these are irrelevant to the lizard.
The raison d'etre for a lizard is to be a lizard, nothing more, nothing less.

I knew all of this going into my experiment and hypothesized that it would be
therapeutic. Since many of my own soul ambitions are difficult to fulfill while
my creative energy is monopolized by a career I eventually want to leave, I
figured that switching to the mindset of a lizard would provide comfort and
resolve as I push forward towards the next phase of my life, a new life that
hopefully provides me more creative space to realize my true ambitions.

But while I did find comfort in the lizard-life, I did not find resolve, for the
Lizard Imperitives leave no room for resolution beyond satisfying the narrow
requirements of the lizard. Latent ambitions have no place or relevance; they
simply do not exist. Certainly no true lizard resolves to eventually
not be a lizard anymore. Thus, to become one with the lizard it is necessary
to fully abandon your humanity and be a lizard, nothing more, nothing less.

I am now interested in the chain of neuroticisms that lead me down this path.
My shame at being a sell-out, a corporate shill who abandoned his art for
money, a prostitute, a hypocrite, _a poser_; I could make the pain go away by
becoming a lizard! Except, even as a lizard I was still a poser, for I refused
to fully commit to the Lizard Imperitives and abandon altogether the ambitions
of my soul.

So the experiment was interesting but it also failed. I do have some other
experiments to try, but ta ta for now.






