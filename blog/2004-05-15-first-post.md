---
title: 'first post'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-05-15 00:00:00 PST'
tags: 'deprecated'
---

Ooh, you may notice this blog thing wasn't here before. Big whoop. It's actually
kind of nifty. I thought having the word "blog" in the corner like that was
distracting and detracted from the design, so I made it only flash if there's a
new entry since your last visit. Otherwise it's very discreet.

I put this here because I decided I'd like to be able to sound off on
_undefined_. It *is* my _undefined_ site, so I might as well make it somewhat
_undefined_. My life isn't particularly exciting or special. I don't see why anyone
should be too interested in what I have to say, but that's not gonna stop me.

Anyway, here's what I have to say: I'm somewhat pissed off. I got in last night
after a really relaxing hang-out session to find a note reading 
*"Go to Hell Jeff"* shoved under my door. I'm not as upset about someone in
these dorms hating me as I am about the fact that somewhere near me lurks a
pitiful coward who lacks the balls to confront me face to face. This person is a
pathetic waste of organic material and a blight upon humanity. At this point,
whoever is responsible for this act of cowardice would be wise to remain in
hiding, because if I ever find out who did it, _I WILL undefined_

That's all. Thank you for your time.