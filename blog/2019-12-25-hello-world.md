---
title: 'Obsessive Facts'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2019-12-25 00:35:00 PST'
tags: 'the moment,under construction'
---

Welcome to our newly relaunched site. We hope you will stay awhile and 
[learn about us.](/about)

Obsessive Facts is our website. Obsessive Network LLC is our organization. We
exist to build new technologies and engage in advocacy to promote 
and protect freedom of expression on the Internet. Our secondary objective is to
sustain ourselves.

There's more we could say, but words are boring
when weighed against actions, and we are compelled to act. So maybe just stay
tuned, smash that RSS button for notifications, and watch the pieces come
together.
