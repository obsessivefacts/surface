---
title: 'My experience being rejected from thatgamecompany'
subtitle: '"Take home test" was basically two days of free onboarding work for them.'
author: 'henriquez'
date: '2023-07-27 14:44:00 CST'
unlisted: true
---

I've been a gamer my whole life, so I was super excited when I saw an
engineering position open up at [thatgamecompany](https://thatgamecompany.com/),
the studio responsible for some of my favorite games (Flow and Flower) from the
Playstation 3 era. Of course I applied, and two interviews later I was
advanced to their "take home test."

Without going into too much detail, this take
home test was really just an exercise in training and onboarding for their
specific tech stack. I've avoided some of the frameworks a lot of newer
companies use because I prefer to roll my own minimal / zero-dependency
frameworks that are specifically tailored to the projects I work on.
Nevertheless, I learned TGC's codebase and (IMO) completely nailed the test
requirements. I was feeling really confident when I submitted my work to the
hiring manager.

But two days later I received a canned "Regarding your application" rejection email
with this gem:

> While we are unable to provide detailed feedback on the specific reasons for
> our decision, we encourage you to continue pursuing your career goals and
> building upon your skills and experiences.

I found this response to be unprofessional and frankly insulting.
After doing 16 hours of work that would be required of any new-hire, they flatly
rejected me with no feedback on my work or advice as to what I could have done
better. I'm confident in my skills as an engineer but am always trying to learn
and improve myself; not providing any feedback after the numerous interviews and
amount of time I invested into their hiring process gives me no opportunity to
improve myself. Feels bad, man.

Going forward in my job hunt I will be certain to be cautious of these sorts of
take home exercises; thatgamecompany appears to be using their take home test to
externalize the costs of employee training and onboarding to unpaid candidates.
That's the only conclusion I can come away from this experience with.