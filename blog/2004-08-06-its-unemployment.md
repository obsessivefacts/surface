---
title: 'it's unemployment!'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-08-06 00:00:00 PST'
tags: 'deprecated'
---

Long time, no blog, eh? Perhaps I've been feeling more than a little sheepish at
having pansied out of my truck unloading job. Having heavy machinery fall on
your head and getting thrown into piles of damp, filthy laundry does tend to
make a person less enthusiastic. But the temp agency has gotten me various other
crappy manual labor jobs since. Whatever.

In any case, I've saved enough up to return to Santa Cruz and get a place, so
that's good. Speaking of Santa Cruz, the Univerity (which I can no longer
afford) sent me a packet with some forms the other day. But it wasn't just ANY
packet of forms. No, it was super deluxe. The forms arrived in a high quality,
full-color printed folder that looked like it had been designed by a
professional graphic designer. "WOW," said I, "this is eye-catching and
aesthetically pleasing!" And it only probably cost $3 to print the damn thing.
Multiply that by 15,000 resident students (forget postage and graphic designer
fees), and the whole project still doesn't cost as much as it would cost me to
go to school there this fall as an out-of-stater. But that's not my point.

What really bugs me is that the place spends tens of thousands of dollars
designing, printing and mailing total frivolities when they're broke, their
students are broke and they have to raise the cost of tuition faster than
inflation to keep from totally collapsing. It just doesn't make sense! I tell
you, it's crazy!

Um, as for the site, no major updates lately. I've been thinking about making a
computer game in Flash and even designed a few sprites. Here's our hero, the
fabulous _undefined_:

![will](/images/blog/2004-08-06-its-unemployment/blog3-2.gif)
![you](/images/blog/2004-08-06-its-unemployment/blog3.gif)
![be](/images/blog/2004-08-06-its-unemployment/blog3-3.gif)
![punished](/images/blog/2004-08-06-its-unemployment/blog3-4.gif)

Hope you like. Ta ta for now.