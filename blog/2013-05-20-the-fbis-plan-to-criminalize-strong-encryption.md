---
title: 'The FBI’s plan to criminalize strong encryption'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2013-05-20 00:00:00 PST'
tags: 'FBI, CALEA, wiretap, wiretapping, going dark, encryption, cryptography, terrorism, gmail, conspiracy, backdoor, privacy, law, CFAA, CISPA, regulation, cybersecurity, hacking, surveillance'
---

Now that Congress has quietly backed away from
[CISPA](https://en.wikipedia.org/wiki/Cyber_Intelligence_Sharing_and_Protection_Act)
and
[expansion of the CFAA](https://www.eff.org/deeplinks/2013/04/huffington-post-credits-internet-activists-major-victory-stopping-bad-cfaa-bill),
the Federal Government has wasted no time in introducing new half-baked Internet
regulations. The latest comes courtesy of the FBI. Under their proposed
expansion of
[CALEA](https://en.wikipedia.org/wiki/Communications_Assistance_for_Law_Enforcement_Act),
a federal wiretapping law, online service providers would be required to build
wiretapping capabilities into their software, allowing law enforcement to
secretly monitor user communications.

[According to the FBI](https://archives.fbi.gov/archives/news/testimony/going-dark-lawful-electronic-surveillance-in-the-face-of-new-technologies),
these expanded monitoring capabilities are required because child
pornographers and terrorists are increasingly “going dark;” meaning that
instead of calling each other on their wiretapped iPhones, they’re sending
encrypted messages over the Internet, and the FBI can’t read them. By
expanding wiretapping requirements to include online service providers, the
FBI reasons, these tech-savvy villains can be brought to justice.

On the surface, this looks like a reasonable proposition. But, as is the case
with all technical regulations, the devil is in the details.

For a typical online service provider, like Gmail, complying with a wiretapping
order would be little to no trouble. Because a user’s messages are stored
centrally on Google’s servers, Google could simply give the FBI access to their
Gmail servers and be done with it.

But some online service providers allow for the exchange of encrypted messages
between users. Although the service providers may run central servers that
facilitate the exchange of these messages, they are not readable by the service
providers due to their encryption. Only the intended recipients can decrypt and
read them. For these service providers, the only way to comply with a government
wiretapping mandate is to bundle secret monitoring capabilities, or “backdoors,”
into the actual apps that run on users’ computers or smartphones.

While this might sound like a crazy conspiracy theory, it is the primary concern
of a leading group of computer security researchers, including cryptography
legends Bruce Schneier and Phil Zimmerman. Last Friday, the Center for Democracy
and Technology
[released a report condemning the FBI’s plan.](https://www.cdt.org/files/pdfs/CALEAII-techreport.pdf)
They warn that requiring software providers to install backdoors on peoples’
devices would “lower the already low barriers to successful cybersecurity
attacks,” by giving hackers an easy way to attack apps while remaining
undetected. 

But this could be exactly what the FBI wants. The FBI’s plan effectively gives
developers a choice:

- Install sketchy, easily-hacked monitoring software on your users’ devices.

- Pay
  [$25,000 a day](https://www.nytimes.com/2013/05/08/us/politics/obama-may-back-fbi-plan-to-wiretap-web-users.html)
  in non-compliance fines. (unpaid fines
  [double daily](https://www.washingtonpost.com/world/national-security/proposal-seeks-to-fine-tech-companies-for-noncompliance-with-wiretap-orders/2013/04/28/29e7d9d8-a83c-11e2-b029-8fb7e977ef71_story.html)
  after 90 days, quickly paying off the national debt) 

- —or— just don’t bother offering end-to-end encrypted software.

By making the first two options morally reprehensible and unrealistically
burdensome, the FBI might hope that companies will just stop offering encrypted
software for their users, making it much easier for them to centrally wiretap
peoples’ communication.

Certainly, investors will be less willing to fund startups that are required to
install backdoors on users’ devices. If such a backdoor is exploited by hackers,
where does the liability fall? It is arguably negligent to include a feature
that is unquestionably adverse to every user, regardless of whether the service
provider was required by law to do so. Unless the FBI also gives service
providers immunity for any damages related to the backdoors, it’s quite likely
these companies will be on the losing end of lawsuits when they inevitably get
hacked. 

More importantly than concerns about stifling Silicon Valley innovation,
the FBI’s proposed regulation raises questions about the rights of government
to eavesdrop on its citizens. Since the 1990s, law-abiding people have taken for
granted their ability to exchange encrypted digital communication with complete
(or at least [pretty good](https://en.wikipedia.org/wiki/Pretty_Good_Privacy))
privacy. Are child pornographers and terrorists a big enough threat to justify
taking this away?

I hate to end with a physical analogy, but this is a great way to explain the
issue to someone less tech-savvy. Imagine you are a manufacturer of the locks
used in bank and casino vaults. You take great pride in your craft, and your
lock is secure against all but the most extreme attempts to break it. Now,
suppose one day the FBI comes and tells you, “it’s fine and all that you built
this vault, but we need you to install a second keyhole so we can open the vault
and see if there are terrorists hiding inside.” The key they want to use is no
more sophisticated than a house key, and can be opened by the most pedestrian of
criminals. Is this really a good idea? 