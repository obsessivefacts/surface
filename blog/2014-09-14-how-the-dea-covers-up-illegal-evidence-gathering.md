---
title: 'How the DEA covers up illegal evidence-gathering'
subtitle: 'Secret phone records database used for “parallel construction” of evidence'
image: 'hemisphere.png'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2014-09-14 04:19:00 PST'
tags: 'dea, surveillance, unconstitutional, constitution, EFF, law enforcement, warrantless, spying, metadata, parallel construction, justice'
---

According to [slides released by EFF](https://www.eff.org/files/2014/09/12/7-3-14_mr6608_res.pdf),
law enforcement agencies have been using Hemisphere, a secret phone records
monitoring database, to build criminal cases against defendants and then
cover it up by “fortuitously” happening across other evidence gained through
legitimate channels. The 24-page slide deck describes the program, along with
the elaborate techniques used to conceal the true source of evidence from
judges, prosecutors and criminal defendants.

Funded by the Office of National Drug Control Policy (ONDCP), the Hemisphere
program is powered by a massive phone metadata monitoring database with advanced
pattern-recognition algorithms designed to track individual targets, including
location. Features include:

- No need for a warrant! Near realtime-access to phone records and metadata

- Pattern recognition to identify individuals, even when they change phones

- Location information for “tracking targets and placing them in certain areas
  at certain times.”

Sounds great, right? The only problem, which the presentation skillfuly dances
around without explicitly acknowledging, is that it’s most likely illegal and
unconstitutional. That’s why you “DO NOT mention Hemisphere in any official
reports or court documents.” Instead, you use Hemisphere to gather the evidence
you need, and then, by sheer luck, get the documents you need through official
channels, or pull the right car over at the right place and right time. This is
an evidence-laundering technique known as
[parallel construction](https://en.wikipedia.org/wiki/Parallel_construction), or
as the Hemisphere presentation puts it, “Parallel Subpoenaing.” The presentation
goes to great lengths to describe this, emphasizing how the program must remain
secret.

#### It's illegal.

Under the U.S. Constitution, criminal defendants are entitled to due process of
law, which means both that evidence against them must be obtained through
legitimate means, and that they must be given a chance to challenge it in court.
The Hemisphere program flies in the face of both of these requirements.
Obtaining evidence through warrantless mass surveillance clearly violates the
Fourth Amendment. Parallel construction conceals the true origin of evidence
(illegally obtained evidence), making it impossible for defendants to challenge
the practices of law enforcement agents.

Under [“Fruit of the poisonous tree”](https://en.wikipedia.org/wiki/Fruit_of_the_poisonous_tree)
doctrine, if criminal evidence is gathered through illegal means, it’s
inadmissible, and any further evidence obtained as a result of that
evidence is also inadmissible. This is a legal precedent designed to prevent
exactly what the government is doing with the Hemisphere program. On paper, our
criminal justice system realizes that it’s better to let a few criminals walk
free than allow the Constitutional rights of everyone to be systematically
violated by shady law enforcement practices. Unfortunately, when evidence is
concealed from the courts, it’s impossible for them to put a stop to this, and
justice cannot be served.