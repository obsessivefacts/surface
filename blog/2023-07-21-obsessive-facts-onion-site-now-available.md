---
title: 'Obsessive Facts hidden service now available on TOR'
author: 'henriquez'
image: 'red-handed-lofi.png'
date: '2023-07-21 14:21:00 CST'
tags: 'TOR, privacy, encryption, gemini, security, fingerprinting, device fingerprinting, javascript, gemini'
---

Lately I've been playing with some alternative web protocols, specifically [Project Gemini](https://gemini.circumlunar.space/).
But I realized before I create a ["Smol Web"](https://zserge.com/posts/small-web/)
site, I've been missing the opportunity to release a "Dark Web" version of this site
accessible to people using the [TOR Browser](https://www.torproject.org/). So I
put my cores to work [brute forcing](https://github.com/cathugger/mkp224o)
the perfect [vanity address](https://community.torproject.org/onion-services/advanced/vanity-addresses/),
and a couple days later I'm happy with the result.

**Find us at http://obsessivecto5al3kdoe24cyt77np4w4owew7sm66qb7kwhlpzsgyuyd.onion**

This link only works if you have the TOR Browser, or another browser capable of
loading onion addresses (but please don't use [Brave](https://brave.com/) for high
security use-cases).

In the spirit of the higher-security nature of TOR, I've disabled all JavaScript
on the onion site. I've been [bitching about](/blog/2020-04-04-the-javascript-black-hole.html)
JavaScript device fingerprinting for awhile now, so it was time to do this anyway. JavaScript is a
[progressive enhancement](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement)
for most of the functionality on this site, so if you want the fancy animations and
media streaming you can use the Clearnet version. And if you care more about security
over all else, use the Darknet version. Mostly everything still works either way.