---
title: 'updates galore!'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-08-22 00:00:00 PST'
---

Hellohello!

The unemployment is going wonderfully. I've had a nice amount of time to work on
the site in this short window of time before life gets serious again. So here it
is: a brand new game for your gaming pleasure. Actually, two games in one. Check
out the "focus" page, and be sure to notice the buttons in the lower corners.

Oh, and I couldn't get that stupid Men Without Hats "We can dance if you want
to" song out of my head, so I did a cover of it to mock it. [Check it out on the
music page](/music). Sadly, the damn thing is still stuck in my head.

Believe me, more updates are soon to come. The upcoming project should be VERY
cool when it's ready.

Peace out!