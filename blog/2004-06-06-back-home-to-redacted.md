---
title: 'back home to undefined'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-06-06 00:00:00 PST'
tags: 'deprecated'
---

Yes it's true... I'm back in _undefined_ for the summer. It's good to be
home, although I already miss all my friends in Santa Cruz. My travels went well
except for this $25 overweight luggage fee cuz my bag was 15 pounds over the 50
pound limit. Oh well. Worse things have happened to better people. Like poor old
Ronald. Hopefully this summer will be nonstop excitement and entertainment. The
Insplackible _undefined_ and I will be making lots of awesome music, the
likes of which the world has never heard. Music page. There shall be lots of
time to make _undefined_.

Speaking of that, I've made a couple of minor modifications to a few pages to
make them run better. Most notably the homepage now has a "loading" screen while
it's loading. The damn thing is 40 kilobytes of source code, and often the page
would render before the scripts were loaded leading to Javascript glitches. No
more. Other minor stuff will go unnoticed in the vast sea of human
consciousness, much like the rest of this site. BAH.

I have enclosed a picture of myself looking cracked out.


![](/images/blog/2004-06-06-back-home-to-redacted/blog1.gif)

Ok, bye bye!