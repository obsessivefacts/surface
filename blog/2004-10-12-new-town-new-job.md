---
title: 'new town, new job'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-10-12 00:00:00 PST'
tags: 'DNS, domain, hosting, deprecated'
---

Hmm, it's been awhile since I updated... nowadays I'm living in Santa Cruz, have
a tiny room all to myself and work at a computer company doing tech support and
programming. Life couldn't be better. So whatever.

When this entry becomes visible on _undefined_, it will mean that the DNS
change for the domain has successfully propagated and that the site is now
hosted on my company's server.

My friend _undefined_ had been giving me free hosting before, and for that I
am eternally greatful. My employers are offering the same, and since they're an
ISP, I have a lot more control over my setup and it's a bit closer to home.
(The old server was in England)

As for updates, no guarantees for the time being. I'll try to get back to
_undefined_ when I have another break from reality. If you're reading this
message and you aren't me, then you're about one of ten people who regularly
visits this site... so ... congratulations. I just wish this had caught on more
for all the hundreds of hours I put into it.

Um... whatever.