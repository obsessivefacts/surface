---
title: 'Lenovo ThinkPad P1 Gen 7 first impression: Faulty GPU'
subtitle: 'Beautiful laptop gets instant RMA :('
author: 'henriquez'
date: '2024-07-01 15:55:00 CST'
image: 'lenovo.jpg'
unlisted: true
---

I purchased a Lenovo ThinkPad P1 Gen 7 laptop for a side project I've been
working on. This was directly from Lenovo's online store and one of the first
available for sale in the United States.

The P1 Gen 7 boasts impressive specifications, including an Intel Core Ultra 9
CPU and an NVIDIA RTX 4070. Lenovo markets it as an "AI"-ready laptop thanks to
available pro-level GPUs and Intel's built-in "neural processing unit" (NPU).
And physically, the P1 Gen 7 makes a strong first impression, rivaling the
precision and build quality of Apple's MacBook laptops.

Unfortunately, as soon as I powered it on things went downhill. My particular
unit had graphical corruption and display flickering from the moment the
Windows 11 setup wizard popped up. I tried updating the BIOS and drivers, but
nothing improved the artifacting. From my experience with other machines, this
indicates either bad memory or a bad GPU and requires replacement hardware.

I needed a working laptop immediately for my project, so I authorized an RMA for
refund with Lenovo and purchased an Alienware M16 R2 with the same specs for
$400 cheaper in-store at Best Buy. I hope Lenovo will improve the quality
control on these laptops because people don't have time to go through support
chatbots and inevitably wait for replacement hardware. RIP.