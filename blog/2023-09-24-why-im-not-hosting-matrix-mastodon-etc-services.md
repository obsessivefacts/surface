---
title: 'Why I'm not hosting Matrix / Mastodon / etc. services'
author: 'henriquez'
date: '2023-09-24 01:42:00 CST'
tags: 'Mastodon, Matrix, inevitability'
---

This is a frequently asked question, and here's the answer. I love decentralized
and federated protocols, but for security reasons I won't host Matrix or
Mastodon services on this domain. If I had the time and patience, I'd love to
create my own Matrix implementation, but it's a tall order. Matrix has always
been a very complicated protocol to implement, and with
[the recent release of Matrix 2.0](https://matrix.org/blog/2023/09/matrix-2-0/)
it got even more complicated. Similarly, I'd love for the social features on
this site to interoperate with Mastodon, but it has a very _particular_
implementation of the ActivityPub protocol which would be a ton of work to
recreate.

The vast majority of individuals hosting these services appear to be using
publicly released Docker containers, or similar, which is great for spinning
servers up quickly but bad (IMO) from a security standpoint. Trusting other
people with the specifics of your packages is one thing, but spinning up other
people's virtual machines on your own network(s) is a dangerous game.
[Attack surface](https://www.sentinelone.com/cybersecurity-101/what-is-cyber-security-attack-surface/)
is a big deal in cybersecurity, which is why I prefer to roll my own protocol
implementations when possible. At least that way, if I fuck up, I know who to
blame when shit goes south.

Matrix, Mastodon, and the Fediverse in general are amazing innovations, and
likely the future of social interactions online. There are very smart people
with domain expertise running and hosting these services, but I'm not one of
them, nor do I care to be.

For people with shared goals and vision, don't fret. Obsessive Facts is a small
operation, and I am likely not the threat actor you're looking for. Again, there
are smart people with domain expertise working on the problems listed on the
[About Us](https://www.obsessivefacts.com/about/x) page. You can either trust
their implementation or roll your own, but sending toots on this domain won't
change the inevitable outcome.
