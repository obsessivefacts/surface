---
title: 'learning flash'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-05-15 00:00:00 PST'
tags: 'deprecated'
---

Hello. This post serves as a test of my improved blogging system which stores
entries in a database and allows people to quickly jump from entry to entry with
the little arrows on the lower left corner. Big whoop. Eventually I hope to put
something in that allows people to comment. As if anyone even comes to this
site.

Not much is new in Santa Cruz. I'm (guiltily) enjoying my lighter class load
this quarter. Since I had to withdraw from precalculus I've had so much more
time to work on _undefined_. Speaking of that, a little earlier this week,
after I posted the *secret page* (ooh), I realized that I had run into a wall:
the design in my head couldn't be fully expressed with the tools I've been using
to build this site. So far, _undefined_ has been a testament to the true
power of combining DHTML, Javascript, CSS and PHP. But these have their limits,
and now I've been feeling somewhat constrained by them. So although I've wanted
to remain a raw-code purist, this will no longer be possible: I'm a designer,
not a programmer. In order to increase my design potential, I'm gonna have to
break down and start using Macromedia Flash to create some of my newer designs.

That also means I'm going to have to learn how to use Macromedia Flash. I've
already got a decent start on it, and after I receive *Macromedia Flash MX 2004
For Dummies*, I'll become a lot more proficient. Keep an eye on the new "flash"
page (the A key) ... as I learn more, it should get flashier :-D All right,
that's all for now. Peace out.