---
title: 'Install Stable Diffusion in WSL with AMD Radeon ROCm'
subtitle: 'Recently released Adrenalin 24.12.1 driver unlocks new AI-related potential!'
author: 'henriquez'
date: '2024-12-21 12:02:00 CST'
tags: 'AMD, ROCm, Radeon, Stable Diffusion, AI, WSL, Linux, Windows'
---

Recently when upgrading my AMD Adrenalin driver, a line in the release notes piqued my interest:

> Official support for Windows Subsystem for Linux (WSL 2) enables users with supported hardware to develop with AMD ROCm™ software on a Windows system, eliminating the need for dual boot set ups.

Historically, [AMD ROCm](https://www.amd.com/en/products/software/rocm.html) support has been pretty limited compared to [NVIDIA CUDA](https://developer.nvidia.com/cuda-toolkit), which has worked in Windows Subsystem for Linux (WSL 2) for awhile. So this new driver seemed like kind of a big deal, and I thought I'd check it out!

- **See article: [Install Radeon software for WSL with ROCm](https://rocm.docs.amd.com/projects/radeon/en/latest/docs/install/wsl/install-radeon.html)**

AMD's article is short and sweet. Obviously you'll need the latest [AMD Adrenalin Edition GPU driver](https://www.amd.com/en/products/software/adrenalin.html) installed, and also [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/). Microsoft's official documentation is good, and I've gone through [my own installation experience](/blog/2023-09-12-how-to-mount-a-luks-encrypted-linux-drive-in-windows.html) here.

Once you have the amdgpu driver installed, you can run `rocminfo` to confirm everything is working. You should see output like this:

```text
*******
Agent 2
*******
  Name:                    gfx1100
  Marketing Name:          AMD Radeon RX 7900 XTX
  Vendor Name:             AMD
  Feature:                 KERNEL_DISPATCH
  Profile:                 BASE_PROFILE
```

Installing the [Stable Diffusion Web UI](https://github.com/AUTOMATIC1111/stable-diffusion-webui) is also easy. You'll need Python 3.10 and Git installed (`sudo apt install python3 git`) if you don't already. Then just pick an installation folder and clone the stable-diffusion-webui repository to your local machine: `git clone https://github.com/AUTOMATIC1111/stable-diffusion-webui.git`

#### Fixing AMD-specific problems in Stable Diffusion

Once you have the Stable Diffusion code, you should be able to run `./webui-sh` to start the Web UI. However, more than likely you'll run into a couple of specific errors that prevent it from starting:

- **Torch is not able to use GPU; add --skip-torch-cuda-test to COMMANDLINE_ARGS variable to disable this check'**
  
  By default, PyTorch is trying to talk to the NVIDIA CUDA driver. Obviously on an AMD GPU, that's not going to work. Helpfully, this error message tells us how to fix the problem.

- **RuntimeError: "addmm_impl_cpu" not implemented for 'Half'**
  
  I'm not sure if this is a driver bug or what, but apparently half-precision mode isn't working under ROCm. You can fix this by adding `--precision full --no-half` to your COMMANDLINE_ARGS.

To fix both problems, simply edit your webui-user.sh file, find and _un_-comment the line (remove the leading `#`) with `export COMMANDLINE_ARGS`, and customize it like so:

```text
export COMMANDLINE_ARGS="--skip-torch-cuda-test --precision full --no-half"
```

![Edit webui-user.sh with GNU nano](/images/blog/2024-12-21-installing-stable-diffusion-in-wsl-with-amd-radeon-rocm-support/webui-user.png)

Save the file, and now you should be able to `./webui-sh` to start the Web UI and begin generating images with your AMD Radeon GPU! Once the Web UI is running, you can open it in your browser by navigating to http://127.0.0.1:7860

![A shiba inu sitting at a table, no methamphetamines](/images/blog/2024-12-21-installing-stable-diffusion-in-wsl-with-amd-radeon-rocm-support/shiba-inu.png)
