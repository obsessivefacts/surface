---
title: 'My disappearing act'
subtitle: 'Why the site went dark (and why it's back)'
author: 'henriquez'
image: 'car.jpg'
author_email: 'henriquez@protonmail.com'
date: '2023-04-26 04:20:00 EST'
tags: 'life, divorce, unemployment'
---

I remember the feeling of compulsive resolve last summer as I disconnected and pulled my web
server out of my closet. Along with my laptop, a monitor, some clothes, and [my
dog](/fmiffel/ShibaInuFacts), I hastily packed everything into my Challenger and drove north.
Despite having barely three hours of sleep I continued driving for twelve hours,
through a tropical storm and into the mountains. When I reached Nashville,
Tennessee, I called it a night and booked a room for myself and my dog at a 
plush hotel that caters to traveling recording artists. All day I had
appreciated the opportunity to drive and think, and that night I appreciated the
bed as I sank into a restful sleep.

My marriage of 18 years was over, my life was permanently altered. I was
reminded of a semi-lucid dream decades ago when I discovered the ability to move
so fast that I could tear myself through the fabric of the universe itself into
a new spacetime, a parallel universe. The only downside? You can never go back,
no matter how fast you move, how desperately you tear. Nothing would ever be the
same.

Since then, my web server stayed packed away as the rest of my old life began to
fall apart; divorce, the commercial failure of my company, mass layoffs of the
team I had built, deep depression, the recent loss of my own job being just
icing on the cake. A pessimist would say I am ruined, or that I wasted nearly
two decades of my life on a failed marriage. But I'm not a pessimist and I think
I learned some valuable lessons.

**1. You cannot change who another person is.**

You should try to be a better person, a more loving person, a better
communicator, a better lover. You should try to improve all of the things you
can control, but some things are impossible to control, and you shouldn't try.
Instead, think about what you want and what you need out of a relationship and
figure out if the other person is willing or able to meet you there. And note
that being willing != being able.

**2. Human beings can take a lot of damage.**

["They say"](https://rushkoff.com/books/coercion/) that moving is one of the
most stressful things a person can do. And also divorce. Oh also losing
your job. I experienced all of these things in rapid succession and it was very
stressful, it completely sucked, but it also wasn't the end of me. After rolling
with enough punches one becomes an expert at rolling. _In for a penny, in for a
pound._ That mentality almost made it fun.

**3. A chance to start over is a gift.**

It's easy to fall into ruts, personally, professionally, etc. A runner or 
weight-lifter would call this "hitting a plateau." In my case it was
[feeling like a lizard](/blog/2021-04-24-one-of-these-days.html). A sense of
extreme inertia can make it hard to self-examine, much less achieve the volition
necessary to break away from patterns of mediocrity. Having the rug pulled out
from under your life conveniently forces these issues to the forefront; when
forced to adapt or perish, there's really only one constructive path forward. A
chance to start over is a gift from fate, whether you wanted it or believe in
fate.

In that spirit, I've revived this web site. It's been going in one form or
another since 2004 or earlier and I want to keep it going. The old web server is
still packed away (RIP); that thing was powerful but sucked down electricity
like it was going out of style with its 140 watt TDP. I've put together a much
faster server that sips power, and took the opportunity to upgrade the operating
system, the Ansible deployment system for this site, and the node version for
[the code.](https://gitlab.com/obsessivefacts/surface)

I still have to figure out what's next for me, but I'm feeling optimistic about
my future.