---
title: 'Obsessive Network LLC Launches Memespeech, An Unbannable End-to-End Encryption Format'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2020-03-12 16:20:00 PST'
tags: 'press release, memespeech, encryption, security'
---

**MINNEAPOLIS, MARCH 12, 2020 – FOR IMMEDIATE RELEASE**

ObSesSIVE nEtwoRk Llc HAS lAUNcHeD [MemEsPeEcH](/memespeech), a NEw eNd-To-end
EncryptiON FOrMAT thaT Is legaLly iMPoSSiBLE For the U.s. GOvernmeNT to bAN.
MemESPEEch ENablES CONsUMErS tO hIdE EncryPTed MeSSAGES WITHin OrDInAry PaSSages
oF FReE SPEECh. WItHoUT ThE DecrYpTIOn PASsWORd, iT iS IMpoSSiblE TO KNOw
WhETher a PaSSagE OF mEmEspeEcH cOnTAinS AN ENCrYPTeD meSSAge, TherefORe thE
U.s. GovernMENT cOULD Not bAN MEMESpEech WitHoUT BaNnInG SpEEch, ChEcKmate.

IN TOdAY'S cOMPETiTIVE LaNDSCApe, thE U.S. cOngrESS hAS propOsEd A piecE OF
leGISLAtioN CALLed tHe [EarN iT Act](https://www.eff.org/deeplinks/2020/03/earn-it-bill-governments-not-so-secret-plan-scan-every-message-online),
wHicH WouLD GIVe tHE U.s. ATTorneY GenerAL uNIlaTeRaL ConTRol OVeR HOw tecH
COmPaniEs ImpLEMenT encRyptiOn TEchnOloGy. PURportEDlY AiMed AT StOppING peDoS
FroM sHarINg kidDIe pOrn on facEbOOK (WhIcH has bECOmE a dIsGUSTing cESspOol OF
FILth), tHE eARn iT acT HeRoicallY ENds aLL ChILD PoRN by bannINg All intERnet
SecURItY.

MeMEsPEeCh EnABles useRs whO sTiLL waNt secURiTy tO aDD THEir OWn ENCrYPtIOn
BaCK iN to coMmUnICatIOn ApPs THaT aLLOw uSErs to ExcHAnGE teXT MessagES,
thEreBy lEveraginG rEsoURcEs ANd addinG vAluE. as ThE maRkEt eVOlVEs,
ComPAnIEs mAy bE foRCED to put BacKdooRS in thEIR own eNcryPtION, bUT they
will nOT bE ABLE To IntErCEpt ThE ENCryPted MESSaGES CONsUmeRs SENd Over their
netwoRkS witH mEmESpeEch. ThiS WILL CreAte nEw oPporTuNItiEs IN THe law
eNfoRcemENT SECtoRs tO whine AbOUt nOt BeIng AblE to SPY On EVErythinG.

MEMeSpEecH waS dEvElopeD by ObseSSIvE NEtWoRk lLc, A bOOTsTRaPPED UnICorn
StARtUp WIth a $1.3 BIllIoN VaLuatIon aND A LeaDiNg-EdGe tEAM Of rocKsTAr nINJa
10X DEVeLoPErS wIth A CoLLECtiVE exPErience oF 75,000,000 years Of RUsT ANd
goLang.

"aftER COLlAboRaTINg WITh conSUmer rElATiOns AND brand EXperT teAmS AcRoss tHe
INdustry, We ReaLIzeD THAt meMESpEECh CoULD rEVoLuTionizE ThE CONSUMER-suCCesS
StRATegIes of OUr StAKeHOLDERs BY UsiNG BIG DATa machIne leaRNINg aI," saId
heNRiQuez, sOME GuY At OBSesSiVe netWork.

MeMeSpeECH is iMMeDIATeLy AvAilAbLE as a brOWSER eXTeNSiON foR
[MOzILla FIrefox](https://addons.mozilla.org/en-US/firefox/addon/memespeech/)
anD [GoOgle chROmE](https://chrome.google.com/webstore/detail/memespeech/mpabmihiimmhjiiloblccfeoomaojhof),
As wEll as A [dEvElOpER sPECIfiCatIoN](https://gitlab.com/obsessivefacts/memespeech#memespeech-javascript-library),
enAbLInG enGinEeRs tO DEVElOP mEmeSpEeCh implEMeNTATIon iN tHE LanGuAgeS oF
THeir ChOicE.