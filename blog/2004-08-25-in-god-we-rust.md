---
title: 'In god we rust.'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-08-25 00:00:00 PST'
tags: 'music, rant, deprecated'
---

If you look around you see a simple world cuz it's too easy.

You dislike gray and I dislike you not since I am too lazy.

It's just you must consume, absorb, have every

Thing that you do not need, but want, sickening me.

You pull me in, absorb my will, instigating

My life in this charade of your humanity.


Program us to tune out, drop in for a function

Of yours. Keep us fearing the worst every second

That you herd us into this cage you call freedom

[It's not too late to break away from an awful pattern](/music)


Binary has a place--oh yes but its not perception.

Strangling someone else takes place through a disconnection.

Fueling the war machine keeps us in a misdirection.

Dividing us and them. (oh we're so much better)


Break it, smash it, throw it away:

Your dominated mind,

Or take all you see from an evil on top;

You might as well be blind


When the world bites back and it's time to pay

This hypertrophic charge,

It's a pretty price and I hate to say

"In god we rust at large."