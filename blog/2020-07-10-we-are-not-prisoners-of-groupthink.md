---
title: 'We are not prisoners of groupthink.'
subtitle: 'How I stopped worrying about "cancel culture" with this one weird tip.'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2020-07-10 11:40:00 CST'
tags: 'groupthink, cancel culture, censorship, reeeee'
---

_This is a response to the Gareth Roberts essay titled
["We are all prisoners of groupthink".](https://unherd.com/2020/07/why-the-prisoner-is-more-accurate-than-orwell/)_

A common theme on the Internet is selection bias. We seek out content and
interactions that fit our sensibilities, beliefs and emotional disposition.
Social networks have exploited this tendency, drawing us into
[filter bubbles](https://en.wikipedia.org/wiki/Filter_bubble) where we are
algorithmically bombarded with content designed to maximize our "engagement"
with no regard to damage done in terms of our psychological well-being or
intellectual isolation. This makes us better consumers, but reinforces
divisions between individuals and poisons any possibility of meaningful
discourse, instead favoring shit-flinging competitions between so-called
["keyboard warriors."](https://twitter.com/realdonaldtrump/status/1261126114799468549?lang=en)
This is well-documented, the social media companies are aware of it,
and they don't care because [division makes money.](https://www.wsj.com/articles/facebook-knows-it-encourages-division-top-executives-nixed-solutions-11590507499)

Our filter bubbles are designed to comfort and placate us while we're force-fed
promoted content and offers and idealized imagery. Our collective ability
to think critically has been siphoned away; anything that remotely challenges
our beliefs is seen as a threat or an attack. Over time this has lead to the
ridiculous notion that "words are violence," and from this, the rise of
[cancel culture](https://en.wikipedia.org/wiki/Online_shaming#Call-outs_and_cancellation),
the First World pastime of mobbing, doxing, and socially destroying
anyone who dares to voice an unpopular opinion or do something stupid. This cancel
culture was born of social media. Sure, some might blame other factors like liberal
arts education but really they're nothing new to society. But I'll tell you what changed.

******

Back in the good old days (when you didn't need 32gb of
memory to browse the web), Twitter and Facebook used to display a chronological
feed of your friends' posts. This was very functional, but ended up creating a problem
for the social media companies: in order to maximize the amount of time you spend on
their sites they needed you to friend/follow tons of people (even people you aren't really
friends with). But if you did that, your feed would turn into a shitshow, with too
much content for any person keep up with. So Facebook and Twitter went back to the
drawing board and came up with a fantastic innovation: the algorithmic content feed
(aka. the death of society and end of the Internet). The feed algorithm could "get
into your head" with [psychographic microtargeting](https://en.wikipedia.org/wiki/Psychographics),
allowing the machine to recommend content and advertisements that _you_ are likely to
"engage" with—click, like, share,
anything to keep you on the site and viewing more ads a little longer.

The deprecation of the chronological timeline in favor of the
machine-curated content feed had major psychological side effects for everyone involved.
When the machine decided what content to recommend it would favor content that is most
likely to create engagement; this trends toward content that creates an emotional
response, which on the Internet is most often outrageous content that evokes fear or
anger. By bombarding people with upsetting shit and exposing them to "communities" of other
people [circlejerking](https://knowyourmeme.com/memes/circle-jerk) about how upsetting
everything is, the machine dialed the filter
bubble effect up to 11, essentially dividing people into groups and then radicalizing
them with increasingly extremist content. And in the process,
**Facebook and Twitter radicalized the actual publishers of content.**

Remember when you could pick up a newspaper or turn on
your television and get news coverage that seemed at least superficially factual
and unbiased? Obviously those days are gone. Newspapers are mostly out of business,
TV viewership is down, and the dying husk of our mainstream media is increasingly
obsessed with a contrived "culture war." Traditional media has been superceded by
the Internet, and with social media dominating peoples' time spent on
the Internet, Facebook and Twitter have become gatekeepers between the
publishers and their viewers. Factual and unbiased reporting is simply not engaging
enough and will not appear on peoples' feeds. Only breathless hyperbolic
fear-mongering and rage porn will break through the algorithm and get clicks.
Mainstream publishers have been forced to shift their entire media strategies
around _online engagement_ as they desperately attempt to stay relevant on the Internet.
And what's more engaging than a controversy? Thus,
mainstream publishers have "picked sides" that resonate with their
audiences'
filter bubbles in the artifical culture war, promoting non-newsworthy events
into manufactured controversies, and sparking mob action with headlines like
["Here's why [food CEO's] meeting with [world leader] is prObLeMaTiC."](https://www.cnn.com/2020/07/11/us/goya-foods-unanue-trump-hispanic-market/index.html)


And that brings us to recent months, where
after locking every person of fighting age in closet-sized apartments, where peoples' only
access to the outside world was filtered through the toxic lens of social media, and
where the only information available was underpinned by fear and outrage, people
lost their shit. And now we're collectively hand-wringing about cancel culture (but
being real careful not to upset the mob.)

But here's the thing: cancel culture is irrelevant if you don't give a fuck.
You are not a prisoner of groupthink. You might be a prisoner of your belief
that you should give a fuck. But that is under your control. We are not living
a George Orwell hellscape, memes like
_"[1984](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four) was a warning, not
an instruction manual"_ fall flat. The premise that we should care what a bunch
of larpy wannabe do-gooders think on Twitter and Facebook is false.
It doesn't matter—you can't lose a game you don't play. Cancel culture was born
of social media. **If we cancel Facebook and Twitter**, we can break the cycle of
extreme division and hyperbolic microtargeting, shatter the filter bubbles,
and reclaim our access to information from monopolistic ad targeting algorithms.
By divorcing our attention from these toxic echo chambers, manufactured controversies
will become less profitable,
people will be able to think more critically and talk to each other more sincerely,
and cancel culture will end organically. There are really no major drawbacks.

So much of our time online is _wasted_ creating, curating, and "defending"
these perfectly plastic personas, avatars, idealized identities that represent
some vague notion of a persistent sense of self on the Internet. And for what?
Do you really talk to your 600 Facebook friends? Do you even give a shit about
who your 10,000 Instagram followers are? Do they give a shit about you? No.
People waste so much of their lives trying to stake out an online identity that
they start to believe it actually matters, _but it doesn't._
**A persistent online identity is a liability, not an asset.**

We are all tempted by the lie that social networks base their existence on:
that we need to put our "selves" online for all to see. This lie is a mental hack,
exploiting our human need for meaningful interactions with other people,
as well as the dark aspects of human nature: ego, anger and trauma. As society
increasingly isolates and divides humans from one another physically and
socially, we are tempted by the lie that our online personas form a meaningful
extension of our real-world selves.

Sadly, the vast majority of our interactions on social media are hollow, and the
few glimmers of meaningful connections with others that _do_ occur
instill a Pavlovian-style hope, an addictive draw to keep us infinitely scrolling
through our algorithmically-curated content feeds in the vain hope that the
machine will bring meaning to the emptiness of our lives. But social media is little more
than mental masturbation. The service is free but the price we pay is dear.

When you put your real self online, you open yourself up to attack.
Like a federal indictment, the mob can come for anyone at any time. Whether or
not you are a good person is irrelevant, and trying to craft your online persona
to appease the mob is a loser's game. As the filter bubbles increasingly divide-and-circlejerk
people into more extreme viewpoints, what passes for acceptable
behavior today could be heresy tomorrow. And when your name, your employer, and
your family are all connected to your social media presence, you put yourself in
real world danger for very little real world benefit.

So what can you do? **Cancel yourself.** Delete your social media presence.
Sever the link between your online self and your real-world self. Seriously.
_You can't lose a game you don't play._

But wait, _isn't this extreme?_ Maybe it is, or maybe you're just addicted to
social media. I've talked to a lot of people about this and heard some common
excuses people use to rationalize addictive behavior to themselves.

- **"But social media is an important part of my professional network."**

I can only speak anecdotally. I've built a successful career without using LinkedIn
or other social networks to promote myself. My work ethic, skill and reputation have
carried me
as far as I care to go in my field. Also anecdotally, when I hired a contractor
to remodel my basement, I didn't check her Facebook page; I saw her work at a
neighbor's house and asked them to put me in touch. She did a great job on
my basement (and later posted photos of it to her Instagram). Good work
promotes itself.

But what doesn't work is when the line between personal and professional gets
blurred, which is almost inevitable on a medium designed around social
interaction. The tension between _being a professional_ and _having an opinion_ is
overwhelming for some people. The person who lists in their Twitter bio that
they "work for Google" and "bash the fash" isn't doing themself or their employer
any favors. Such tact only works for those who stay in the good graces of the mob,
which is, again, a loser's game.

- **"But I use social media to keep in touch with my school friends."**

No you don't. You "friend" or "follow" people that you will likely never talk to
again, and every now and then the algorithm surfaces some photo or blurb from
a distant acquaintance that you are likely to engage with, you click "like" and
go back to never talking again. This is little more than voyeurism, a creepy window
into peoples' lives where you keep tabs on their activities but don't otherwise
interact with them. What are you, some kind of stalker?

Your real friends you talk to via SMS or numerous other mediums. You will hang
out in person and text photos to each other. You don't use Facebook or Twitter for
this. None of this changes when you delete your account.

- **"But I'm on a crusade to spread my political opinion."**

This is the most entertaining and misguided excuse. Whether it's
"libtards owned" or "drumpf is hitler," your opinion as it pertains to
some contrived holy war between forces of good and evil is little more than farting
in the wind. You aren't changing anything by arguing with people on the Internet.
You're just digging further into the addictive abyss of your filter bubble. You
are literally the problem.

One thing that people across the political spectrum can agree on, from
socialist/communists to alt-right Neo-Nazis, is that Facebook is fucking
unethical. Whether your jam is that they [interfered](https://www.theatlantic.com/technology/archive/2017/10/what-facebook-did/542502/)
in a U.S. election, or
they're [censoring conservative viewpoints](https://www.wsj.com/articles/why-conservatives-dont-trust-facebook-11566309603),
or they performed
[secret psychological experiments](https://www.theatlantic.com/technology/archive/2014/06/everything-we-know-about-facebooks-secret-mood-manipulation-experiment/373648/)
on people, we can all agree that Facebook is an evil company and Mark Zuckerberg
is a creepy android sociopath. Twitter is also _problematic._ They allow
[@realdonaldtrump](https://twitter.com/realdonaldtrump) to spread "hate speech"
and propaganda, but also [they add propaganda to his propaganda](https://www.cnbc.com/2020/05/26/twitter-fact-checks-trump-slaps-warning-labels-on-his-tweets-about-mail-in-ballots.html),
they [censor Free Speech](https://www.foxnews.com/politics/nunes-twitter-google-facebook-are-tech-tyrants-that-censor-conservatives),
and [enabled mass surveillance](https://theintercept.com/2020/07/09/twitter-dataminr-police-spy-surveillance-black-lives-matter-protests/)
of George Floyd protestors.
No matter where you stand on these issues, it's hard to deny: the damage
these companies have done to society is incaculable. And yet people continue to
use their social networks, often
to bash the social networks themselves with no sense of irony.

**If you're politically aware at all then you know that the social media companies are
unethical af,** you probably know that they are wrecking the Internet, and you
should know better than to continue using them. Your attention is their
commodity, and by continuing to click, share, and like, you continue to enrich
them while surrendering a part of your soul to the machine. But you can break the cycle.

You are not a prisoner of groupthink. You have a choice about how you spend your
limited time on this planet. There is much more to the Internet than social media,
and there's much more to
life (and being social) than the Internet. Go outside, read a book, lawyer up,
hit the gym. You don't have to be Zuckerberg's little bitch. You can delete your
social media. If enough people do, the world will be a much calmer place.
If this idea scares you, then it's because you're a follower. You know what
you're doing is wrong and you're only doing it because other people are doing it.
But you know who else did that?
Nazis. So do yourself a favor. Cancel yourself before the mob decides you're
a Nazi and does it for you. Free yourself from the tyranny of your persistent
persona, and remember the age-old truth:

![On the Internet nobody knows you're a dog](/images/blog/2020-07-10-we-are-not-prisoners-of-groupthink/on-the-internet-nobody-knows-you-are-a-dog.jpg)

