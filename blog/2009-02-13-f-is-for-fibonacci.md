---
title: 'F is for Fibonacci'
subtitle: '(the synthesizer is actually a password entry form)'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2009-02-13 00:00:00 PST'
tags: 'fibonacci, design, math, deprecated'
---

I have posted a new section about Fibonacci numbers. It was supposed to be about
how Fibonacci numbers relate to design, but in the process of doing research, I
learned that a lot of everything having to do with Fibonacci numbers and design
is questionable at best and bullshit at the worst. Instead I ended up exposing
some of these misconceptions and focusing instead on the math-related aspects of
the sequence. It's much easier to prove things that are mathematical facts. I
hope you find my project to be entertaining and informative. This may be the
last major update I will ever make to _undefined_.