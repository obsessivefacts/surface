---
title: 'chess update'
author: 'henriquez'
author_email: 'henriquez@protonmail.com'
date: '2004-07-14 00:00:00 PST'
tags: 'deprecated, coming soon'
---

Per the request of the illustrious _undefined_ (who totally kicks my ass at
chess) I've made it so that each player starts out with their pieces on the
bottom. Before, white would always be on the bottom, and black would always be
on top, but no more!

I had to modify quite a bit of code to achieve this, so new bugs may have been
introduced. So, as always, be on the lookout!

I hope this makes the game feel more like natural to whoever plays as black. I
really appreciate comments, suggestions and feedback, and hope to get more of
them!

Blah, it's 5:40 AM and I have to go to work soon. Which sucks (more than it
normally would) because I can't sleep because I've been having nightmares about
how I hate this job. I have to stand slightly hunched over for 8 hours in this
120 degrees Farenheit laundry room and fold napkins and towels as quickly as
possible as they're spat out of this giant industrial drying machine that blasts
hot air in my face. It's so much fun, the nightmares were about how I wish I
could work their 24 hours a day, 7 days a week but CAN'T.

Whatever, at least it's $$$. Peace out.