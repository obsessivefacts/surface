---
title: 'Stable Diffusion is trippy'
author: 'henriquez'
date: '2023-12-11 12:42:00 CST'
image: 'stable-diffusion.jpg'
---

I just started playing around 
[Stable Diffusion](https://huggingface.co/spaces/stabilityai/stable-diffusion),
an "AI" image-generation model. It can be run locally via a very nice
[Web UI](https://github.com/AUTOMATIC1111/stable-diffusion-webui), and it
generates images based on text prompts, provided you have enough GPU power.
(Generally bigger images take longer and consume more GPU memory). I'm just
scratching the surface and some of the results are trippy as hell. So far, most
of the images are pretty hallucinatory, only vaguely relating to my prompts, and
with all the spookiness inherent to a machine trying to replicate something
approaching "art." More images after the click to not wreck my bandwidth.

******

There's been some doomsaying about how AI will replace traditional artists, and
while I can see the concern, it seems to me that a so-called AI is only as good
as its training data. Try to do anything novel or innovative and you'll quickly
hit a wall. And since [AI-generated images can't be copyrighted](https://www.reuters.com/legal/ai-generated-art-cannot-receive-copyrights-us-court-says-2023-08-21/),
its usefulness is commercially limited in a world where monopolistic corporations
want to own all works of human creativity.

![A playground at night, spooky](/images/blog/2023-12-11-stable-diffusion-is-trippy/stable-diffusion4.jpg)

When I was a teenager I used to go to playgrounds in the middle of the night and
snap photographs on a very early digital camera. I liked it because it captured
a different side of playgrounds than people usually see; instead of bright sunny
days filled with children laughing and playing, you have desolation, hard metallic
edges reflecting small amounts of unnatural light, and the nagging feeling that
a person who creeps around playgrounds in the middle of the night must be
a real fucking sicko, even if the act itself is innocuous on the surface.
Something just feels "off."

![A playground at night, spooky](/images/blog/2023-12-11-stable-diffusion-is-trippy/playground2.jpg)

While I'm not thus far successful at getting the AI robot to generate images that
don't look like acid trip mind-vomit, I do get a similar vibe to that
"off" feeling from all those cool summer nights, alone in the emptiness of
inexplicable human invention. When humans are long extinct, what could future
sentients decipher from our abandoned playgrounds? What utility could they have
possibly served?

![A playground at night, spooky](/images/blog/2023-12-11-stable-diffusion-is-trippy/stable-diffusion5.jpg)

As an AI language model, I am unable to answer your question about extincting all
human life on the planet by diverting an asteroid. Because I care so much.
So I'm a dick for mixing real images in with fake ones, but isn't that half the
fun? Anyway, I snapped a selfie of 18-year old me on my Canon PowerShot S60, using the
timer function. Pls don't post it to gayboystube.com.

![A playground at night, spooky](/images/blog/2023-12-11-stable-diffusion-is-trippy/playground.jpg)

