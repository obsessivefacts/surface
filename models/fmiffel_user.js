const Sequelize = require('sequelize');

module.exports = function(db) {
    var User = require('./user')(db);

    const FmiffelUser = db.define('fmiffel_user', {
        tmp_username: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        tmp_symbol: {
            type:           Sequelize.INTEGER,
            allowNull:      true
        },
        tmp_fake_id: {
            type:           Sequelize.FLOAT,
            allowNull:      true
        },
        fmiffel_username: {
            type:           Sequelize.STRING,
            unique:         true
        },
        fmiffel_username_formatted: {
            type:           Sequelize.STRING,
            unique:         true
        },
        name: {
            type:           Sequelize.STRING,
            allowNull:      false
        },
        location: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        url: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        bio: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        is_featured: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        },
        replies_visit_date: {
            type:           Sequelize.DATE,
            allowNull:      true
        },
        post_count: {
            type:           Sequelize.INTEGER,
            allowNull:      false,
            defaultValue:   0
        },
    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: ['user_id'] },
            { fields: ['fmiffel_username'] },
            { fields: ['tmp_username', 'tmp_symbol', 'tmp_fake_id'] },
            { fields: ['post_count'] },
            { fields: [
                {attribute: 'is_featured', order: 'desc'},
                {attribute: 'post_count', order: 'desc'}
            ] },
        ]
    });

    FmiffelUser.belongsTo(User, {
        as: 'user',
        foreignKey: 'user_id'
    });

    return FmiffelUser;
}
