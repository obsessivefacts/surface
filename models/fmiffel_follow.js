const Sequelize = require('sequelize');

module.exports = function(db) {
    var FmiffelUser = require('./fmiffel_user')(db);

    const FmiffelFollow = db.define('fmiffel_follow', {
    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: ['fmiffel_user_id'] },
            { fields: ['fmiffel_user_followed_id'] },
        ]
    });
    FmiffelFollow.belongsTo(FmiffelUser, {
        as: 'fmiffel_user',
        foreignKey: 'fmiffel_user_id'
    });
    FmiffelFollow.belongsTo(FmiffelUser, {
        as: 'fmiffel_user_followed',
        foreignKey: 'fmiffel_user_followed_id'
    });

    return FmiffelFollow;
}
