const Sequelize = require('sequelize');

module.exports = function(db) {
    const Guestbook = db.define('guestbook', {
        commands: {
            type:           Sequelize.TEXT,
            allowNull:      false
        },
        timestamp: {
            type:           Sequelize.BIGINT,
            allowNull:      false
        },
        censored: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        }
    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: ['timestamp', 'censored'] },
        ]
    });

    return Guestbook;
}
