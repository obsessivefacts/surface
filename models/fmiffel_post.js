const Sequelize = require('sequelize');

module.exports = function(db) {
    var FmiffelUser = require('./fmiffel_user')(db);

    const FmiffelPost = db.define('fmiffel_post', {
        text: {
            type:           Sequelize.TEXT,
            allowNull:      true
        },
        hashed_ip: {
            type:           Sequelize.STRING,
            allowNull:      false
        },

    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: [
                'fmiffel_user_id',
                {attribute: 'created_at', order: 'desc'}
            ] },
            { fields: [
                'reply_to_post_id',
                {attribute: 'created_at', order: 'asc'}
            ] },
            { fields: [
                'reply_to_user_id',
                {attribute: 'created_at', order: 'desc'}
            ] },
            { fields: [{ attribute: 'created_at', order: 'desc'}] },
            { 
                name: 'fmiffel_post_lowercase_text',
                fields: [
                    Sequelize.fn('lower', Sequelize.col('text')),
                    {attribute: 'created_at', order: 'desc'}
                ]
            }
        ]
    });

    FmiffelPost.belongsTo(FmiffelUser, {as: 'fmiffel_user', foreignKey: 'fmiffel_user_id'});
    FmiffelPost.belongsTo(FmiffelPost, {as: 'reply_to_post', foreignKey: 'reply_to_post_id'});
    FmiffelPost.belongsTo(FmiffelUser, {as: 'reply_to_user', foreignKey: 'reply_to_user_id'});

    return FmiffelPost;
}