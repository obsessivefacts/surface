const Sequelize = require('sequelize');

module.exports = function(db) {
    var Role = require('./role')(db);

    const User = db.define('user', {
        username: {
            type:           Sequelize.STRING,
            unique:         true
        },
        username_formatted: {
            type:           Sequelize.STRING,
        },
        password: {
            type:           Sequelize.STRING,
            allowNull:      false
        },
        email: {
            type:           Sequelize.STRING,
            unique:         true
        },
        title: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        access_token: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        subscribe: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        },
        verified: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        },
        avatar: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        bio: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        public_email: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        },
    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: ['access_token'] }
        ]
    });

    User.belongsToMany(Role, {through: 'user_role'});
    Role.belongsToMany(User, {through: 'user_role'});

    return User;
}
