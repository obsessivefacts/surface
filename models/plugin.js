const Sequelize = require('sequelize');

module.exports = function(db) {
    const Plugin = db.define('plugin', {
        name:       { type: Sequelize.STRING, unique: true },
        code:       { type: Sequelize.STRING, unique: true },
        path:       { type: Sequelize.STRING },
        plugins:    { type: Sequelize.STRING },
    }, { freezeTableName: true, underscored: true });

    return Plugin;
}
