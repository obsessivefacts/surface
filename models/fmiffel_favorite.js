const Sequelize = require('sequelize');

module.exports = function(db) {
    var FmiffelUser = require('./fmiffel_user')(db);
    var FmiffelPost = require('./fmiffel_post')(db);

    const FmiffelFavorite = db.define('fmiffel_favorite', {
    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: ['fmiffel_user_id'] },
            { fields: ['fmiffel_post_id'] },
        ]
    });

    FmiffelFavorite.belongsTo(FmiffelUser, {as: 'fmiffel_user', foreignKey: 'fmiffel_user_id'});
    FmiffelFavorite.belongsTo(FmiffelPost, {as: 'fmiffel_post', foreignKey: 'fmiffel_post_id'});

    return FmiffelFavorite;
}
