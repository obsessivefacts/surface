const Sequelize = require('sequelize');

module.exports = function(db) {
    var User = require('./user')(db);

    const Post = db.define('post', {
        type: {
            type:           Sequelize.STRING,
            allowNull:      false
        },
        ref_id: {
            type:           Sequelize.STRING,
            allowNull:      false
        },
        tmp_username: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        tmp_symbol: {
            type:           Sequelize.INTEGER,
            allowNull:      true
        },
        tmp_fake_id: {
            type:           Sequelize.FLOAT,
            allowNull:      true
        },
        hashed_ip: {
            type:           Sequelize.STRING,
            allowNull:      false
        },
        text: {
            type:           Sequelize.TEXT,
            allowNull:      false
        },
        is_deleted: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        },
        is_edited: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        },
        is_spam: {
            type:           Sequelize.BOOLEAN,
            defaultValue:   false
        },
    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: ['type', 'ref_id', 'is_spam', 'created_at'] },
            { fields: ['reply_to_id'] },
            {
                name: 'post_type_ref_id_created_at_desc',
                fields: ['type', 'ref_id', 'is_spam',
                {attribute: 'created_at', order: 'desc'}]
            },
        ]
    });

    Post.belongsTo(User, {as: 'user', foreignKey: 'user_id'});
    Post.belongsTo(Post, {as: 'reply_to', foreignKey: 'reply_to_id'});
    Post.hasMany(Post, {as: 'replies', foreignKey: 'reply_to_id', hooks: true, onDelete: 'cascade'})


    return Post;
}
