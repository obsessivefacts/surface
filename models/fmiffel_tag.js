const Sequelize = require('sequelize');

module.exports = function(db) {
    var FmiffelUser = require('./fmiffel_user')(db);
    var FmiffelPost = require('./fmiffel_post')(db);

    const FmiffelTag = db.define('fmiffel_tag', {
        tag: {
            type:           Sequelize.STRING,
            allowNull:      false
        },
    }, {
        freezeTableName:    true,
        underscored:        true,
        indexes: [
            { fields: ['created_at'] },
            {
                name: 'fmiffel_tag_created_at_desc',
                fields: [{attribute: 'created_at', order: 'desc'}]
            },
            { fields: ['fmiffel_post_id'] }
        ]
    });

    FmiffelTag.belongsTo(FmiffelUser, {
        as: 'fmiffel_user',
        foreignKey: 'fmiffel_user_id'
    });
    FmiffelTag.belongsTo(FmiffelPost, {
        as: 'fmiffel_post',
        foreignKey: 'fmiffel_post_id'
    });

    return FmiffelTag;
}
