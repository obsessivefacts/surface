const Sequelize = require('sequelize');

module.exports = function(db) {
    
    const Role = db.define('role', {
        name: {
            type:           Sequelize.STRING,
            unique:         true
        },
        description: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        icon: {
            type:           Sequelize.STRING,
            allowNull:      true
        },
        html: {
            type:           Sequelize.TEXT,
            allowNull:      true
        },
        url: {
            type:           Sequelize.STRING,
            allowNull:      true
        }
    }, {
        freezeTableName:    true,
        underscored:        true
    });

    return Role;
}
