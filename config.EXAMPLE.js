// This file must be renamed to config.js and placed in the parent directory.
module.exports = {
    ugly: false, // whether to uglify front-end javascript code

    baseURL: 'https://www.obsessivefacts.com',
    onionURL: 'http://obsessivecto5al3kdoe24cyt77np4w4owew7sm66qb7kwhlpzsgyuyd.onion',

    cdn: {
        // surface: 'https://static.yourdomain.com',    // optional cdn urls
        // plugins: 'https://basement.yourdomain.com'   // for front-end caching
    },

    db: {
        db: 'my_database',
        user: 'root',
        pass: 'p@ssw0rd',
        options: {
            host: 'localhost',
            dialect: 'postgres',
            logging: true,
            operatorsAliases: false
        }
    },

    redis: {
        host: '127.0.0.1',
        port: 6379,
        db:   0
    },

    storage: {
        local: {
            basePath: '/path/to/obsessivefacts/static/',
            typePaths: {
                avatar: 'avatars/'
            }
        }
    },

    deimos: { url: 'http://hostname' },

    security: {
        // be sure to white list any CDN domains in your content security policy
        csp: 'default-src \'self\'; style-src \'self\' \'unsafe-inline\'; frame-ancestors \'none\'; object-src \'none\'; connect-src \'self\'',
        // leave hsts blank or undefined for testing, keep on in production
        hsts: 'max-age=63072000; includeSubDomains; preload',
        xFrameOptions: 'DENY',
        cookieSecret: '1234',
        accessControlAllowOrigin: 'www.obsessivefacts.com',
        tlsCert: 'cert.pem',
        tlsPrivKey: 'key.pem'
    },

    cookie: {
        signed: true,
        httpOnly: true,
        maxAge: 2592000000 // 30 days
    },

    users: {
        // what to call users if they have no title set
        defaultTitle: 'User',

        // passphrase to encrypt temporary user identity cookies
        tmpEncryptionPassphrase: 'this is my password',
    }
}
