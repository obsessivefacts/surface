var fs      = require('fs'),
    license = fs.readFileSync('client/LICENSE').toString(),
    config  = require('../config');

const readDir = path => fs.readdirSync(path).filter(f => f != '.DS_Store')

module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  var standalonePages = {
    '404': {},
    index: {
      extraControllers: ['keyboard'],
    },
    music: {},
    guestbook: {
      extraControllers: ['posts'],
    },
    fmiffel: {},
    access_granted: {},
    user_profile: {},
    blog: {
      extraControllers: ['posts'],
    },
    about: {},
    contact: {},
    memespeech: {},
    volition: {},
    sitemap: {},
    privacy: {},
    terms: {},
    dmca: {},
    facts: {},
    'gemini-proxy': {}
  };

  var concat = {
    options: {
      separator: '\n',
      stripBanners: false
    },
    main: {
      src: [
        'client/LICENSE',
        'client/core/util.js',
        'client/core/fade.js',
        'client/core/notifier.js',
        'client/core/loader.js',
        'client/core/socket.js',
        'client/obsessivefacts.js',
        'client/core/header.js',
        'client/controllers/_page.js',
      ],
      dest: 'static/js/main.js'
    }
  };
  var terser = {
    main: {
      files: {
        'static/js/main.min.js': ['static/js/main.js']
      }
    }
  }

  for (var key in standalonePages) {
    if (standalonePages.hasOwnProperty(key)) {
      var staticFilename = 'static/js/'+key+'.js',
          page = {
            src: ['client/LICENSE'],
            dest: staticFilename
          };
      if (standalonePages[key].extraControllers) {
        for (var i=0; i<standalonePages[key].extraControllers.length; i++) {
          page.src.push('client/controllers/'+standalonePages[key].extraControllers[i]+'.js');
        }
      }
      page.src.push('client/controllers/'+key+'.js');
      concat[key] = page;

      terser[key] = {files: {['static/js/'+key+'.min.js']: [staticFilename]}};
    }
  }

  var standaloneFiles = readDir("less/_standalone/"),
      lessFiles = {"static/css/main.css": "less/main.less"};
  
  for (let file of standaloneFiles) {
    var savePath = "static/css/"+file.replace('.less', '.css');
    lessFiles[savePath] = "less/_standalone/"+file;
  }

  var facts = ['fake', 'hate', 'shibainu', 'true'];

  for (let fact of facts) {
    let factList = readDir("views/facts/"+fact),
        jsList = readDir("client/controllers/facts/"+fact),
        lessList = readDir("less/facts/"+fact);

    for (let matched of factList) {
      let jsIndex = jsList.indexOf(matched+'.js');
      if (jsIndex !== -1) {
        concat['facts-'+fact+'-'+matched] = {
          src: ['client/LICENSE', 'client/controllers/facts/'+fact+'/'+matched+'.js'],
          dest: 'static/js/facts/'+fact+'/'+matched+'.js'
        }
        terser['facts-'+fact+'-'+matched] = {
          files: {
            ['static/js/facts/'+fact+'/'+matched+'.min.js']: ['static/js/facts/'+fact+'/'+matched+'.js']
          }
        }
      }
      let lessIndex = lessList.indexOf(matched+'.less');
      if (lessIndex !== -1) {
        lessFiles['static/css/facts/'+fact+'/'+matched+'.css'] = 'less/facts/'+fact+'/'+lessList[lessIndex];
      }
    }
  }

  // make a no-cdn version of the css
  for (var key in lessFiles) {
    if (lessFiles.hasOwnProperty(key)) {
      lessFiles[key.replace('static/css', 'static/css-nocdn')] = lessFiles[key];
    }
  }
  
  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: lessFiles
      }
    },
    concat: concat,
    cdn: {
      options: {
        cdn: config.cdn.surface
      },
      dist: {
        cwd: './static/css/',
        dest: './static/css/',
        src: ['*.css']
      }
    },
    copy: {
      main: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['client/_standalone/**'],
            dest: 'static/js/_standalone/',
            filter: 'isFile'
          }
        ]
      },
      lib: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['client/lib/**'],
            dest: 'static/js/_lib/',
            filter: 'isFile'
          }
        ]
      }
    },
    terser: terser,
    sri: {
      generate: {
        src: [
          'static/js/**/*.js',
          'static/css/**/*.css'
        ],
        options: {
          dest: 'sri.json',
          algorithms: [ 'sha256' ],
          pretty: true
        }
      }
    },
    watch: {
      styles: {
        files: ['less/**/*.less'], // which files to watch
        tasks: ['less', 'sri'],
        options: {
          nospawn: true
        }
      },
      javascript: {
        files: ['client/**/*.js'],
        tasks: ['concat', 'copy', 'terser', 'sri']
      },
    },
    clean: {
      javascript: [
        'static/js/*/',
        'static/js/*.js'
      ],
      styles: [
        'static/css/*/',
        'static/css/*.css',
        'static/css-nocdn/*/',
        'static/css-nocdn/*.css'
      ]
    }
  });

  grunt.registerTask('default', ['less', 'copy', 'watch']);
  grunt.registerTask('build', ['concat', 'less', 'copy', 'terser', 'sri']);
  grunt.registerTask('deploy', ['concat', 'less', 'copy', 'terser', 'sri']);
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-cdn');
};
