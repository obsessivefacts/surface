const Promise   = require('bluebird');
const util      = require('../lib/util'),
    config      = require('../../config'),
    view        = require('../lib/view'),
    cache       = require('../lib/cache'),
    response    = require('../lib/response'),
    shitlib     = require('../lib/shitlib'),
    marked      = require('marked'),
    hilite      = require('highlight.js'),
    moment      = require('moment');

var models      = {},
    app         = null,
    db          = null,
    redis       = null;

const PAGE_SIZE = 20;

marked.setOptions({
    sanitize: true,
    highlight: function(code, type) {
        if (type == 'text') return code;
        var language_subset = ['javascript', 'json', 'python'];
        return hilite.highlightAuto(code, language_subset).value;
    }
});

module.exports = function(providers) {
// ==

app   = providers.app;
db    = providers.db;
redis = providers.redis;

cache.redisInit(redis);

models.User = require('../models/user')(db);
models.Post = require('../models/post')(db);

const challenges = {
    'BLOMPF': {
        question: 'Who was the forty fifth president of the United States of America?',
        answers: ['Trump']
    },
    'FIGHTCLUB': {
        question: 'Who penned the 1995 essay ‘Industrial Society and Its Future’?',
        answers: ['Unabomber', 'Kacyznski']
    },
    'PRIVATIZED': {
        question: 'What illegal human experimentation program was: A) run by the CIA, B) involved the use of drugs and other coercive techniques to control peoples\' minds, and C) was supposedly halted in 1973?',
        answers: ['MKULTRA', 'MK-ULTRA']
    },
    'THERMITE': {
        question: 'Two airplanes hit two towers in New York City on September 11, 2001. How many towers fell?',
        answers: ['three', '3']
    },
    'PLAYPEN': {
        question: 'TRUE or FALSE: The FBI ran a website sharing thousands of child porn images.',
        answers: ['t']
    },
    'MOCKINGBIRD': {
        question: 'At a Feb. 1981 meeting in the West Wing of the White House, William Casey infamously said: “We’ll know our disinformation program is complete when everything the American public believes is false.” Which federal agency was Casey the director of?',
        answers: ['CIA', 'Central Intelligence']
    },
    'DUNKIN': {
        question: '“Enhanced interrogation” is a euphemism for _______.',
        answers: ['torture']
    }
};


app.get(/^\/posts\/delete\/([a-f0-9]{16})([0-9]+)a([a-f0-9]+)$/, function(req, res, next) {
    const postId = parseInt(req.params[1]),
          securityToken = req.params[2],
          json = util.isJSON(req),
          user = req.user;

    var postInfo, post, replyCount, context;

    return cache.redisGet(req.params[0])
        .then(function(encryptedCallbackData) {
            try {
                postInfo = util.decrypt(encryptedCallbackData);
            } catch(err) {
                throw { _msg: 'Access denied.', _code: 401 };
            }
            if (
                !(user.id && (securityToken == util.sha256(user.access_token)))
                &&
                !(user.tmpIdentity && securityToken == util.sha256(user.tmpIdentityEncrypted))
            ) {
                throw { _msg: 'Access denied.', _code: 401 };
            }

            return models.Post.findByPk(postId)
        })
        .then(function(_post) {
            post = _post;
            if (!post || (!api.isMyPost(post, user) && !user.hasRole('angel'))) {
                throw { _msg: 'Access denied.', _code: 401 };
            }
            return models.Post.count({ where: { reply_to_id: postId } });
        })
        .then(function(_replyCount) {
            replyCount = _replyCount;

            return api.getContext(postInfo.type, postInfo.refId, postId, postInfo.descending)
        })
        .then(function(_context) {
            context = _context;

            if (replyCount) {
                return models.Post.update({
                    is_deleted: true,
                    // text: '' // hold off for now while debugging
                }, {
                    where: { id: postId }
                });
            } else {
                return models.Post.destroy({
                    where: { id: postId }
                })
            }
        })
        .then(function() {
            return api.countPosts(postInfo.type, postInfo.refId, true);
        })
        .then(function() {
            return dumpCache(postInfo.type, postInfo.refId);
        })
        .then(function() {
            var splitPath = postInfo.path.split('#'),
                newPath = splitPath[0] + '?page=' + context[context.length-1]._page,
                finalPath;

            if (replyCount) {
                var finalPath = newPath + '#POST-' + postId;
            } else if (context.length > 1) {
                var finalPath = newPath + '#POST-' + context[context.length-2].id;
            } else {
                var finalPath = newPath + (splitPath.length > 1 ? '#' + splitPath[1] : '');
            }
            return response.redirect(finalPath, req, res);
        })
        .catch(function(err) {
            err || (err = {});
            console.log('post deletion error: ', err);
            return response.redirect('/access_denied', req, res);
        });
       
});

var loadPostTree = function(type, refId, page, user, options) {
    if (!options) options = {};
    let includeSpam = options.includeSpam ? true : false;

    const cacheKey = 'loadPostTree:' + page + ':' + (includeSpam ? 1 : 0) + ':1';
    const cacheNamespace = 'posts:' + type + ':' + refId;
    const sort = options.descending ? 'DESC' : 'ASC';

    var userFields = ['id', 'username', 'avatar', 'title'];
    var posts, postCount;
    var query = {
        where: {
            type: type,
            ref_id: refId,
            reply_to_id: null
        },
        include: [
            { model: models.User, as: 'user', attributes: userFields },
            { model: models.Post, as: 'replies', include: [
                { model: models.User, as: 'user', attributes: userFields },
                { model: models.Post, as: 'replies', include: [
                    { model: models.User, as: 'user', attributes: userFields },
                    { model: models.Post, as: 'replies', include: [
                        { model: models.User, as: 'user', attributes: userFields },
                        { model: models.Post, as: 'replies', include: [
                            { model: models.User, as: 'user', attributes: userFields },
                        ]}
                    ]}
                ]}
            ]}
        ],
        order: [
            [ 'created_at', sort ],
            [ 'replies', 'created_at', sort ],
            [ 'replies', 'replies', 'created_at', sort ],
            [ 'replies', 'replies', 'replies', 'created_at', sort ],
            [ 'replies', 'replies', 'replies', 'replies', 'created_at', sort ],
        ]
    }
    if (page) {
        query.offset = (page - 1) * PAGE_SIZE;
        query.limit = PAGE_SIZE;
    }
    if (!includeSpam) {
        query.where.is_spam = false;
    }

    return cache.redisGet(cacheKey, cacheNamespace)
        .then(function(cachedData) {
            if (cachedData) {
                // console.log('CACHE HIT: ', cacheKey, cacheNamespace);
                return cachedData;
            }
            // console.log('no cache hit: ', cacheKey, cacheNamespace);
            return models.Post.findAndCountAll(query);
        })
        .then(function(result) {
            if (typeof result === 'string') {
                var decoded = JSON.parse(result);
                posts = decoded.posts;
                postCount = decoded.postCount;
            } else {
                posts = result.rows.map(function(post) { return post.toJSON() });
                postCount = result.count;
            }

            var _recursiveMarkdownParse = function(posts, depth) {
                for (post of posts) {
                    post._markdown = marked(post.text).replace(/\<a\s/img, '<a rel="nofollow" ');
                    post._depth = depth;
                    post._ago_time = moment(post.createdAt).fromNow();
                    if (post.replies) {
                        post.replies = _recursiveMarkdownParse(post.replies, depth + 1);
                    }
                }
                return posts;
            }
            posts  = _recursiveMarkdownParse(posts, 1);

            const cacheObj = { posts: posts, postCount: postCount };

            return cache.redisSet(cacheKey, JSON.stringify(cacheObj), 86400, cacheNamespace)
        })
        .then(function() {
            return {
                posts: user ? treeUserCompare(posts, user) : posts,
                count: postCount
            };
        })
}

var treeUserCompare = function(posts, user) {
    var _recursiveUserCompare = function(posts) {
        for (post of posts) {
            post._is_me = api.isMyPost(post, user);
            if (post._is_me || user.hasRole('angel')) {
                if (user.id && (!post.is_deleted || user.hasRole('angel'))) {
                    post._delete_token = util.sha256(user.access_token);
                } else {
                    post._delete_token = util.sha256(user.tmpIdentityEncrypted);
                }
            }
            if (post.replies) {
                post.replies = _recursiveUserCompare(post.replies);
            }
        }
        return posts;
    }
    return _recursiveUserCompare(posts);
}

var compressCallbackData = function(callbackData) {
    return cache.redisGet(callbackData)
        .then(function(existing) {
            if (existing) return existing;

            var compressed;

            return util.makeId()
                .then(function(hex) {
                    compressed = hex;
                    return cache.redisSet(compressed, callbackData, 86400)
                })
                .then(function() {
                    return cache.redisSet(callbackData, compressed, 86400);
                })
                .then(function() {
                    return compressed;
                })
        })
}

const dumpCache = (type, refId) => {
    const namespace = 'posts:' + type + ':' + refId;
    return cache.redisDestroyNamespace(namespace);
};

var api = {
    // options.typeSingular
    // options.typePlural
    // options.basePath
    // options.highlight
    // options.descending
    // options.suppressPostActions
    renderPosts: function(type, refId, req, page, options) {
        const user = req.user;
        var dntNotice = false;
        var dntOverrideToken = req.user.tmpIdentityEncrypted;

        if (!options) options = {};

        var path = req.path + '#' + (options.typePlural || 'posts').toUpperCase() + '-' + refId,
            cbRaw = {
                path: path,
                type: type,
                refId: refId
            };

        if (options.descending) cbRaw.descending = true;

        var callbackData = util.encrypt(cbRaw), shortenedCallbackData;

        if (
            req.headers.dnt
            &&
            req.headers.dnt.toString() === "1"
            &&
            !req.user.id
            &&
            (!req.signedCookies || !req.signedCookies.insidious_tracking_cookie)
        )
        {
            var dntNotice = true;
        }

        var compiledPosts, paginationPostCount, postCount;

        return compressCallbackData(callbackData)
            .then(function(compressed) {
                shortenedCallbackData = compressed;
                return loadPostTree(type, refId, page, user, {
                    descending: options.descending,
                    includeSpam: req.query.hilite ? true : false
                });
            })
            .then(function(result) {
                const posts = result.posts;
                paginationPostCount = result.count;

                if (options.suppressPostActions && !req.user.hasRole('angel'))
                    var suppressPostActions = true;
                else
                    var suppressPostActions = false;
                
                // todo cache this
                return view.renderComponent(req, 'posts/posts', {
                    posts: posts,
                    type: type,
                    refId: refId,
                    typePlural: options.typePlural || 'posts',
                    highlight: options.highlight || req.query.hilite || 0,
                    callbackData: shortenedCallbackData,
                    suppressPostActions: suppressPostActions,
                })
            })
            .then(function(_compiledPosts) {
                compiledPosts = _compiledPosts;
                return api.countPosts(type, refId)
            })
            .then(function(_postCount) {
                postCount = _postCount;
                return api.renderForm(req, type, refId, user, callbackData, {
                    placeholder: 'Write your '+(options.typeSingular || 'post'),
                    collapsing: true
                })
            })
            .then(function(composeHtml) {
                let basePath = options.basePath ? options.basePath : '/' + type + '/' + refId;

                return view.renderComponent(req, 'posts/index', {
                    compiledPosts: compiledPosts,
                    composeHtml: composeHtml,
                    postCount: postCount,
                    type: type,
                    refId: refId,
                    dntNotice: dntNotice,
                    dntOverrideToken: dntOverrideToken,
                    callbackData: shortenedCallbackData,
                    typeSingular: options.typeSingular || 'post',
                    typePlural: options.typePlural || 'posts',
                    numPages: Math.ceil(paginationPostCount / PAGE_SIZE),
                    basePath: basePath,
                    page: page,
                })
            })
            .then(function(html) {
                return { html: html };
            });
    },

    getContext: function(type, refId, postId, descending, includeSpam) {
        return loadPostTree(type, refId, 0, null, {includeSpam})
            .then(function(result) {
                const posts = result.posts;
                if (!posts) return null;

                var context;

                var _search = function(posts, postId, parents, idx) {
                    var i = descending ? posts.length + 1 : 0;
                    for (p of posts) {
                        if (descending) i--;
                        else i++;
                        if (idx) p._pagination_idx = idx;
                        else p._pagination_idx = i;
                        p._page = Math.ceil(p._pagination_idx / PAGE_SIZE);
                        if (p.id == postId) {
                            context = parents.concat(p);
                            break;
                        }
                        if (p.replies) {
                             _search(p.replies, postId, parents.concat(p), p._pagination_idx);
                        }
                    }
                }
                _search(posts, postId, []);

                if (!context) return null;
                else for (post of context) post.replies = null;

                return context;
            });
    },

    renderForm: function(req, type, refId, user, callbackData, options) {
        if (!options) options = {};

        return view.renderComponent(req, 'posts/compose', {
            placeholder: options.placeholder || 'Write your post',
            buttonText: options.buttonText || 'Post',
            prefill: options.prefill || null,
            msg: options.msg || null,
            isError: options.isError || false,
            collapsing: options.collapsing || false,
            editPostId: options.editPostId || null,
            spamChallengeIdx: options.spamChallengeIdx || null,
            spamChallengeText: options.spamChallengeText || null,
            callbackData: callbackData,
            securityToken: user.id ? user.encryptedAccessToken() : user.tmpIdentityEncrypted,
            user: {
                id: user.id,
                avatar: user.avatar,
                username: user.username,
                title: user.title,
                _is_angel: user.hasRole('angel'),
            },
            tmp_username: user.tmpIdentity ? user.tmpIdentity.name : null,
            tmp_symbol: user.tmpIdentity ? user.tmpIdentity.symbol : null,
            tmp_fake_id: user.tmpIdentity ? user.tmpIdentity.snowflake : null,
        })
    },

    create: function(req, postInfo, text) {
        var instance = null;
        var post = {
            type: postInfo.type,
            ref_id: postInfo.refId,
            text: text,
            hashed_ip: req.headers['x-forwarded-for'] ?
                util.sha256(req.headers['x-forwarded-for'])
                :
                util.sha256(req.connection.remoteAddress),
        }
        if (postInfo.replyToId) {
            post.reply_to_id = postInfo.replyToId;
        }
        if (req.user.isLoggedIn()) {
            console.log('post.user_id = ', req.user.id);
            post.user_id = req.user.id;
        } else {
            post.tmp_username = req.user.tmpIdentity.name;
            post.tmp_symbol = req.user.tmpIdentity.symbol;
            post.tmp_fake_id = req.user.tmpIdentity.snowflake;
        }
        return shitlib.isSpam(text, post.hashed_ip)
            .then(function(isSpam) {
                if (isSpam) {
                    post.is_spam = true;
                    console.warn('IS SPAM!: ', post.hashed_ip);
                } else {
                    console.log('post: ', post);
                }
                return models.Post.create(post)
            })
            .then(function(_instance) {
                instance = _instance;
                return api.countPosts(postInfo.type, postInfo.refId, true);
            })
            .then(function() {
                return dumpCache(postInfo.type, postInfo.refId);
            })
            .then(function() {
                if (instance.is_spam) {
                    shitlib.isBannedHashedIP(instance.hashed_ip).then(banned => {
                        if (!banned) return;
                        setTimeout(() => {
                            console.warn('DELETING: ', instance.id, '(bad IP)');
                            dumpCache(instance.type, instance.ref_id)
                            return models.Post.destroy({where: {id: instance.id}});
                        }, 5000);
                    });
                }
                return instance;
            });
    },

    update: function(postId, text) {
        var instance;
        return models.Post.update({
            text: text,
            is_edited: true,
        }, {
            where: { id: postId },
            returning: true
        })
        .then(function(_instance) {
            instance = _instance;
            const post = instance[1][0];
            return dumpCache(post.type, post.ref_id);
        })
        .then(function() {
            return instance;
        });
    },

    getById: function(postId) {
        return models.Post.findByPk(postId);
    },

    isMyPost: function(post, user) {
        return (
            (
                user.id
                &&
                post.user_id == user.id
            )
            ||
            (
                user.tmpIdentity
                &&
                post.tmp_username == user.tmpIdentity.name
                &&
                post.tmp_symbol == user.tmpIdentity.symbol
                &&
                post.tmp_fake_id
                &&
                post.tmp_fake_id == user.tmpIdentity.snowflake
                // you don't stand a ... GHOST ... of a chance!
                // get it?
                // GHOST?!
            )
        )
    },

    resolvePostCounts: function(posts, type, refIdKey) {
        return Promise.each(posts, function(post) {
            return api.countPosts(type, post[refIdKey])
                .then(function(count) {
                    post._post_count = count;
                })
        }).then(function(posts) {
            return posts;
        });
    },

    countPosts: function(type, refId, force) {
        var cacheKey = 'posts:' + type + ':' + refId + ':COUNT',
            cachedVal = cache.memoryGet('posts', cacheKey);

        if (cachedVal !== null && !force) {
            return Promise.resolve(cachedVal);
        }

        return models.Post.count({ where: {
            type: type,
            ref_id: refId
        } })
        .then(function(count) {
            cache.memorySet('posts', cacheKey, count);
            return count;
        })
    },

    getSpamChallenge: function(idx) {

        if (idx) return {key: idx, ...challenges[idx]};

        const keys = Object.keys(challenges),
            key = keys[Math.floor(Math.random() * keys.length)];

        return {key, ...challenges[key]};
    },

    validateSpamChallenge: (idx, userResponse) => {
        let challenge = challenges[idx],
            response  = userResponse.trim().toLowerCase();

        if (!challenge) return false;

        for (let i=0; i<challenge.answers.length; i++) {
            if (response.indexOf(challenge.answers[i].toLowerCase()) !== -1) {
                return true;
            }
        }

        return false;
    }
}

return api;

// ==
}