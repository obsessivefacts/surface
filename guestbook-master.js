const HARD_SYNC_INTERVAL = 60000,
      CLEANUP_INTERVAL = 3600000,
      FS_IMG_PATH = __dirname + '/static/images/guestbook/',
      Sequelize = require('sequelize'),
      Op = require('sequelize').Op,
      config = require('../config'),
      NodeRedisPubsub = require('node-redis-pubsub'),
      redis = require('redis'),
      fs = require('fs'),
      nrp = new NodeRedisPubsub({
        emitter: redis.createClient(config.redis),
        receiver: redis.createClient(config.redis)
      }),
      db = new Sequelize(
        config.db.db,
        config.db.user,
        config.db.pass,
        config.db.options
      ),
      { createCanvas, loadImage } = require('canvas'),
      canvas = createCanvas(1440, 900),
      ctx = canvas.getContext('2d');

var commandQueue = [],
    syncedAndListening = false,
    mostRecentPNGTimestamp = 0,
    models = {};

models.Guestbook = require('./models/guestbook')(db);

nrp.on('guestbook', function(data) {
    if (!syncedAndListening)
        return console.warn('guestbook not synced. ignoring command: ', data);

    if (!data.command.length)
        return;

    var command = data.command.split(';'),
        subscriber = data.subscriber;

    switch (command[0]) {
        case 'd':
            draw(command, subscriber);
            break;
        case 'q':
            recapQueue(command, subscriber);
            break;
        default:
            console.warn('GUESTBOOK UNSUPPORTED COMMAND: ', command);
            break;
    }
});

const draw = function(command, subscriber, internal) {
    if (command.length < 7)
        return console.warn('GUESTBOOK DRAW BAD FORMAT: ', command);

    var isValid = true,
        coords = [];

    const radius = parseInt(command[1]),
          blur = parseInt(command[2]),
          r = parseInt(command[3]),
          g = parseInt(command[4]),
          b = parseInt(command[5]);

    // never trust the client.
    if (isNaN(radius) || radius < 1 || radius > 10) isValid = false;
    if (isNaN(blur) || blur < 0 || blur > 100) isValid = false;
    if (isNaN(r) || r < 0 || r > 255) isValid = false;
    if (isNaN(g) || g < 0 || g > 255) isValid = false;
    if (isNaN(b) || b < 0 || b > 255) isValid = false;
    for (var i=6; i<command.length; i++) {
        var xy = command[i].split(',');
        if (xy.length != 2 || !parseInt(xy[0]) || !parseInt(xy[1])) {
            isValid = false;
            break;
        }
        coords.push(xy);
    }

    if (!isValid)
        return console.warn('GUESTBOOK DRAW BAD FORMAT: ', command);

    for (var i=6; i<command.length; i++) {
        var xy = coords[i-6];
        brush(ctx, xy[0], xy[1], radius, blur, r, g, b);
        console.log('draw: ', xy);
    }

    if (!internal) {
        var serialized = command.join(';');
        commandQueue.push(serialized);
        nrp.emit('socket', {
            provider: 'guestbook',
            exclude: [subscriber],
            command: 'bd:'+serialized
        });
        console.log('commandQueue: ', commandQueue);
    }
    console.log('radius: ', radius, '; blur: ', blur, '; r: ', r, '; g: ', g, '; b: ', b);
}

const brush = function(context, x, y, radius, blur, r, g, b) {
    var blurSize = radius - (((blur/100) * radius) || (radius == 1 ? .001 : 1)),
        radgrad = context.createRadialGradient(x, y, blurSize, x, y, radius);
        radgrad.addColorStop(0, 'rgba('+r+','+g+','+b+',1)');
        radgrad.addColorStop(1, 'rgba('+r+','+g+','+b+',0)');
        radgrad.addColorStop(1, 'rgba(255,255,255,0)');
        context.fillStyle = radgrad;
        context.fillRect(x - radius, y - radius, radius * 2, radius * 2);
}

const syncToStorage = function() {
    if (!commandQueue.length) return;

    const commands = commandQueue.join(':'),
          timestamp = new Date().getTime();

    commandQueue = [];

    writeToFile(timestamp);
    models.Guestbook.create({ commands: commands, timestamp: timestamp })
}

const writeToFile = function(timestamp) {
    const path = FS_IMG_PATH + timestamp + '.png';

    return new Promise(function(resolve, reject) {
        fs.access(path, fs.F_OK, function(err) {
            if (err) resolve(false);
            resolve(true);
        })
    })
    .then(function(exists) {
        if (exists) return true;

        return new Promise(function(resolve, reject) {
            const out = fs.createWriteStream(path)
            const stream = canvas.createPNGStream({compressionLevel: 9});
            stream.pipe(out);
            out.on('finish', function() { 
                setMostRecentPNGTimestamp(timestamp);
                resolve(true);
            });
            out.on('error', function(err) { reject(err) });
        });
    })
}

const recapQueue = function(command, subscriber) {
    var timestamp = parseInt(command[1]) || mostRecentPNGTimestamp,
        commands = '';

    return loadMessagesFromDb(timestamp)
        .then(function(records) {
            for (var i=0; i<records.length; i++) {
                commands += (i ? ':' : '') + records[i].commands;
            }
            var queue = commandQueue.join(':');
            if (queue.length) {
                commands += (commands.length ? ':' : '') + queue;
            }
            if (!commands.length) return;
            nrp.emit('socket', {
                provider: 'guestbook',
                to: [subscriber],
                command: 'bd:'+commands
            });
        });
}

const loadMessagesFromDb = function(timestamp) {
    return models.Guestbook.findAll({
        where: {
            timestamp: {[Op.gt]: timestamp},
            censored: false
        }
    })
}

const syncFromStorage = function() {
    console.log('guestbook syncing from storage...');

    return readStorageDirectory()
        .then(function(files) {
            for (file of files) {
                if (file.indexOf('.png') === -1) continue;
                const fileTimestamp = parseInt(file.replace('.png', ''));

                if (fileTimestamp > mostRecentPNGTimestamp) {
                    setMostRecentPNGTimestamp(fileTimestamp);
                }
            }
            if (mostRecentPNGTimestamp) {
                return new Promise(function(resolve, reject) {
                    loadImage(FS_IMG_PATH + mostRecentPNGTimestamp + '.png')
                        .then(
                            function(image) {
                                ctx.drawImage(image, 0, 0, 1440, 900);
                                resolve(true);
                            },
                            function(err) { reject(err) }
                        );
                })
            }
        })
        .then(function() {
            return loadMessagesFromDb(mostRecentPNGTimestamp)
        })
        .then(function(records) {
            for (var i=0; i<records.length; i++) {
                var commands = records[i].commands.split(':');
                for (var j=0; j<commands.length; j++) {
                    var command = commands[j].split(';');
                    draw(command, null, true);
                }
            }
            if (records.length) {
                return writeToFile(records[i-1].timestamp)
            }
            return false;
        })
        .then(function(writeResult) {
            console.log('guestbook synced from storage. listening for commands.');
            syncedAndListening = true;
        })
}

const cleanup = function() {
    console.log('guestbook cleanup starting...');

    return readStorageDirectory()
        .then(function(files) {
            const now = new Date().getTime();

            for (var i = 0; i < files.length - 4; i++) {
                var file = files[i];
                
                if (file.indexOf('.png') === -1) continue;

                var timestamp = parseInt(file.replace('.png', '')),
                    diff = now - timestamp;
                
                if (diff > 86400000) {
                    fs.unlink(FS_IMG_PATH + file, function(err) {
                        if (err) {
                            return console.error('FAILED TO DELETE: ', err);
                        }
                    });
                }
            }
        })
}

const readStorageDirectory = function() {
    return new Promise(function(resolve, reject) {
        fs.readdir(FS_IMG_PATH, function(err, files) {
            if (err) return reject(err);
            resolve(files)
        });
    });
}

const setMostRecentPNGTimestamp = function(timestamp) {
    mostRecentPNGTimestamp = timestamp;
    nrp.emit('guestbookTimestamp', { timestamp: mostRecentPNGTimestamp });
}

syncFromStorage();
setInterval(syncToStorage, HARD_SYNC_INTERVAL);
setInterval(cleanup, CLEANUP_INTERVAL);

