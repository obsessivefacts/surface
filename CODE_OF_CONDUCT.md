# Contributor Code of Conduct

We hold ourselves to the following rules and ask that you do as
well if you wish to play with us. If not, then you are encouraged
to play by your own rules, though we may not associate with you.
The benefit of playing with us boils down to the talent and
collective experience within our network, and how we can create
a force multiplier toward our mutual goals.

## USE PSEUDONYMS

For the vast majority of online interactions, who you are is
irrelevant. The only thing that matters is what you do.
A persistent persona is only baggage that weighs you down
and makes you easier to compromise.

## BE READY TO BURN YOUR PSEUDONYMS

A pseudonym that lives too long is just an alter ego. Make
peace with starting anew. In rare cases a pseudonym may be
preserved as a Nexus Alias if it takes on external-facing
responsibilities, but these aliases may be shared by many.

A handshake may be coordinated with relevant players when
continued association is required after a burn; this is
done on a privileged basis.

## RESPECT OTHERS' PRIVACY

In some cases you may know or think you know someone's IRL
identity. Please keep these thoughts to yourself both in and
out of the game.

## AVOID PERSONAL ATTACKS

We gain nothing by tearing others down. It may be necessary
to criticize a person who is acting as a representative of an
opponent in an official capacity, but it's important to limit
any attacks to the body of work of the opponent.

Personal attacks should generally be avoided and in no case
is it acceptable to attack someone personally via an alias.

## BE INCLUSIVE

In the context of the game, a person's political affiliation
or opinion on any particular wedge issue is usually not
relevant. Do not fall prey to the divide-and-conquer tactics
of our opponents; these have been used throughout history
to prevent people from forming coalitions against systems of
control. We do not have to like or agree with everyone we
play the game with, but it is crucial to be respectful of one
another so we can play together and achieve our shared goals.

## BE RELIABLE

We are human and have limitations. It's better for a person
to temper their ambitions than to promise more than they can
deliver and then burn out. This is a side project for most
involved and we appreciate each others' work, even when we
can't always contribute as much as we'd like.

Communication is crucial if factors arise that could interfere
with a prior commitment. Don't leave people high and dry.

## "COMMUNITY STANDARDS"

While as an organization we advocate for Free Speech as a human
right, this project and its related websites are not a forum for
speech that violates this code of conduct. In other words: if
you come into our house and act like an ass, we will show you
the door (though you would still be encouraged to start your own
anti-social network, we did).