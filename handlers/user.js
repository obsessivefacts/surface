var view     = require('../lib/view'),
    response = require('../lib/response'),
    util     = require('../lib/util'),
    multi    = require('multiparty'),
    sharp    = require('sharp'),
    bcrypt   = require('bcryptjs'),
    models   = {},
    app      = null,
    db       = null,
    redis    = null,
    files    = null;

module.exports = function(providers) {
// ==

app           = providers.app;
db            = providers.db;
redis         = providers.redis;
files         = providers.files;
models.Plugin = require('../models/plugin')(db);
models.User   = require('../models/user')(db);
models.Role   = require('../models/role')(db);

app.get(/^\/user\/([A-Za-z0-9_]+)$/, function(req, res, next) {
    return models.User.findOne({
        where: { username: req.params[0] },
        include: [ models.Role ]
    })
    .then(function(user) {
        if (!user) throw 404;
        var isAdmin = req.user.hasRole('angel'),
            isMe = req.user.id == user.id;
        return view.render(req, 'user/profile', {
            user: user,
            can_edit: isAdmin || isMe,
            me: req.user
        });
    })
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) {
        if (err === 404) return response.show404(req, res);
        response.error(err, req, res);
    });
});

app.get('/user/auth/logout', function(req, res, next) {
    return models.User.findOne({
        where: { username: req.user.username }
    })
    .then(function(user) {
        if (!user) return response.redirect('/', req, res);
        res.clearCookie('access_token');
        console.log('clearing access_token: ', user.access_token);
        redis.del(user.access_token);
        return user.update({access_token: null});
    })
    .then(function() {
        return response.redirect('/', req, res);
    })
    .catch(function(err) {
        response.error(err, req, res);
    });
});

app.get(/^\/user\/edit\/([A-Za-z0-9_]+)$/, function(req, res, next) {
    return models.User.findOne({
        where: { username: req.params[0] },
        include: [ models.Role ]
    })
    .then(function(user) {
        if (!user) throw 404;
        var isAdmin = req.user.hasRole('angel'),
            isMe = req.user.id == user.id;
        if (!isAdmin && !isMe)
            return response.redirect('/access_denied', req, res);
        user.access_token = util.sha256(user.access_token);
        return view.render(req, 'user/edit', {
            user: user,
            me: req.user
        });
    })
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) {
        if (err === 404) return response.show404(req, res);
        response.error(err, req, res);
    });
});

app.get(/^\/user\/delete-avatar\/([A-Za-z0-9_]+)\/(.+)$/, function(req, res, next) {
    return models.User.findOne({
        where: { username: req.params[0] },
        include: [ models.Role ]
    })
    .then(function(user) {
        if (!user) throw 404;
        var isAdmin = req.user.hasRole('angel'),
            isMe = req.user.id == user.id;
        if ((!isAdmin && !isMe) || util.sha256(user.access_token) != req.params[1])
            return response.redirect('/access_denied', req, res);
        return deleteAvatar(user);
    })
    .then(function() {
        return response.redirect('/user/edit/'+req.params[0], req, res);
    });
});

app.post(/^\/user\/edit\/([A-Za-z0-9_]+)$/, function(req, res, next) {
    var fields, user, errors = [], json = false;
    return models.User.findOne({
        where: { username: req.params[0] },
        include: [ models.Role ]
    })
    .then(function(_user) {
        if (!_user) throw 404;
        user = _user;
        var isAdmin = req.user.hasRole('angel'),
            isMe = req.user.id == user.id;
        if (!isAdmin && !isMe)
            return response.redirect('/access_denied', req, res);
        return new Promise(function(resolve, reject) {
            (new multi.Form()).parse(req, function(err, fields, files) {
                if (err) return reject(err);
                return resolve([fields, files]);
            });
        });
    })
    .then(function(fieldsAndFiles) {
        fields = util.form.flatten(fieldsAndFiles[0]);
        json = util.form.truthy(fields.json) || false;
        var files  = fieldsAndFiles[1];
        if (fields.access_token != util.sha256(user.access_token))
            return response.redirect('/access_denied', req, res);
        var promise = Promise.resolve();
        if (files.avatar && files.avatar.length && files.avatar[0].size)
            promise = promise.then(function() {
                return processAvatarUpload(user, files.avatar[0])
            });
        return promise;
    })
    .then(function() {
        if (!util.form.isEmail(fields.email)) {
            errors.push('Please enter a valid email address.');
            return "bogus email";
        }
        return models.User.findOne({ where: { email: fields.email }});
    })
    .then(function(_exists) {
        if (_exists && _exists !== "bogus email" && _exists.id != user.id)
            errors.push('That email is already registered.');

        var update = {
            email: fields.email,
            bio: fields.bio,
            public_email: fields.public_email ? true : false,
            subscribe: fields.subscribe ? true : false,
        };

        var promise = Promise.resolve();
        if (parseInt(fields.change_password) == 1)
            promise = promise.then(function() {
                return doChangePassword(fields, update, errors, user);
            });
        else if (errors.length == 0)
            promise = promise.then(function() {
                return updateUser(user, update);
            });
        return promise;
    }).then(function() {
        if (json) {
            if (errors.length) res.status(400);
            return res.json({
                status: errors.length == 0 ? 'ok' : 'error',
                errors: errors
            });
        }
        if (errors.length) {
            user = user.toJSON();
            user.email = fields.email;
            user.bio = fields.bio;
            user.public_email = fields.public_email ? true : false;
            user.subscribe = fields.subscribe ? true : false;
            user.access_token = util.sha256(user.access_token);
            return view.render(req, 'user/edit', {
                user: user,
                errors: errors,
                me: req.user
            })
            .then(function(data) { response.send(data, req, res); });
        }
        return response.redirect('/user/'+req.params[0], req, res);
    })
    .catch(function(err) {
        if (err === 404) return response.show404(req, res);
        response.error(err, req, res);
    });
});

var doChangePassword = function(fields, update, errors, user) {
    return new Promise(function(resolve, reject) {
        bcrypt.compare(fields.old_password, user.dataValues.password,
            function(err, res) {
                if (err) return reject(err);
                resolve(res);
            });
    })
    .then(function(passwordIsCorrect) {
        if (!passwordIsCorrect) errors.push('Wrong password, genius.');
        if (!fields.new_password || fields.new_password.length < 4)
            errors.push('Please enter a new password that isn\'t idiotic.');
        else if (fields.new_password_2 != fields.new_password)
            errors.push('Please double check your new password.');
        if (errors.length == 0)
            return new Promise(function(resolve, reject) {
                bcrypt.hash(fields.new_password, 12, function(err, hash) {
                    if (err) return reject(err);
                    return resolve(hash);
                });
            })
            .then(function(hash) {
                update.password = hash;
            })
        return Promise.resolve();
    })
    .then(function() {
        var promise = Promise.resolve();
        if (errors.length == 0)
            promise = promise.then(function() {
                return updateUser(user, update);
            });
        return promise;
    });
};

var updateUser = function(user, update) { return user.update(update); };

var deleteAvatar = function(user) {
    if (user.avatar)
        return files.delete('avatar', user.avatar).then(function() {
            return user.update({avatar: null});
        });
    else
        return Promise.resolve();
};

var processAvatarUpload = function(user, avatar) {
    var filename, promise = Promise.resolve();
    if (user.avatar)
        promise = promise.then(function() { return deleteAvatar(user, true) });
    promise = promise.then(function() {
        return util.makeId(16)
            .then(function(bytes) {
                filename = bytes + '.jpg';
                return sharp(avatar.path)
                    .rotate()
                    .resize(256, 256)
                    .jpeg()
                    .toBuffer()
            })
            .then(function(buffer) {
                console.log('uploading avatar image...');
                return files.uploadBuffer('avatar', filename, buffer);
            })
            .then(function() {
                return updateUser(user, {avatar: filename});
            });
    });
    return promise;
};

return { blank: true };

// ==
}
