var shibainufacts = require('../lib/shibainufacts'),
    view          = require('../lib/view'),
    response      = require('../lib/response'),
    config        = require('../../config'),
    app           = null,
    sitemap       = null;

const inus = shibainufacts.getTheFacts();

module.exports = function(providers) {
// ==

app = providers.app;
sitemap = providers.sitemap;

app.get('/facts', function(req, res) {
    return view.render(req, 'facts', {})
        .then(function(data) { response.send(data, req, res); })
        .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/facts');

app.get('/facts/shibainu/profile', function(req, res) {
    return view.render(req, 'facts/shibainu/profile', {
        inus: inus,
        viewMode: 'profile'
    }, {
        title: 'Shiba Inu Facts',
        meta: [
            { property: 'og:title', content: 'Shiba Inu Facts' },
            { property: 'og:description', content: 'Facts about shiba inus' },
            { 
                property: 'og:image',
                content:  (config.cdn_surface ? config.cdn_surface : 'https://www.obsessivefacts.com') + '/images/facts/shibainu/profile/banner2.jpg'
            },
            { property: 'og:url', content: 'https://www.obsessivefacts.com/facts/shibainu/profile' },
            { name: 'twitter:card', content: 'summary_large_image' },
            { property: 'og:site_name', content: 'Obsessive Facts' },
            { name: 'twitter:image:alt', content: 'Picture of shiba inu' }
        ]
    })
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/facts/shibainu/profile');

app.get(/^\/facts\/shibainu\/([0-9]+)$/, function(req, res) {
    let fact = inus.filter(inu => inu.id == req.params[0]);
    return view.render(req, 'facts/shibainu/profile', {
        inus: fact,
        viewMode: 'standalone'
    }, {
        title: 'Shiba Inu Facts',
        meta: [
            { property: 'og:title', content: 'Shiba Inu Facts' },
            { property: 'og:description', content: fact[0].text || 'Facts about shiba inus' },
            { 
                property: 'og:image',
                content: fact[0].attachedImages.length ?
                    (config.cdn_surface ? config.cdn_surface : 'https://www.obsessivefacts.com') + '/images/facts/shibainu/profile/' + fact[0].id + '_0.jpg'
                    :
                    (config.cdn_surface ? config.cdn_surface : 'https://www.obsessivefacts.com') + '/images/facts/shibainu/profile/banner2.jpg'
            },
            { property: 'og:url', content: 'https://www.obsessivefacts.com/facts/shibainu/' + fact[0].id },
            { name: 'twitter:card', content: 'summary_large_image' },
            { property: 'og:site_name', content: 'Obsessive Facts' },
            { name: 'twitter:image:alt', content: 'Picture of shiba inu' }
        ]
    })
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});

app.get(/^\/facts\/([a-zA-Z]+)\/([a-zA-Z0-9_\-+]+)$/, function(req, res) {
    return view.render(req, 'facts/'+req.params[0]+'/'+req.params[1], {})
        .then(function(data) { response.send(data, req, res); })
        .catch(function(err) { response.error(err, req, res); });
});


return { blank: true };

// ==
}
