var app      = null
    ws       = null,
    nrp      = null;

const crypto  = require('crypto');

module.exports = function(providers) {
// ==

app = providers.app;
ws = providers.ws;
nrp = providers.nrp;

app.ws('/socket', function(wsc, req) {
    
    wsc._meta = {
        id: crypto.randomBytes(16).toString('hex'),
        alive: true,
        user: req.user,
        subscriptions: [],
    }
    
    wsc.on('message', function(msg) {
        // wsc.send('received : ' + msg);

        var components = msg.toString().split(/:(.+)/);
        if (components.length < 2)
            return console.log('SOCKET RECEIVED BAD MESSAGE: ', msg);

        var provider = components[0],
            msg = components[1];

        switch (provider) {
            case 'subscribe':
                subscribe(msg, wsc._meta.subscriptions);
                break;
            case 'unsubscribe':
                unsubscribe(msg, wsc._meta.subscriptions);
                break;
            case 'heartbeat':
                wsc.send('heartbeat:pong');
                break;
            case 'guestbook':
                // wsc.send('guestbook:received:'+msg);
                nrp.emit('guestbook', {command: msg, subscriber: wsc._meta.id});
                break;
            case 'sillyputty':
                // wsc.send('sillyputty:received:'+msg);
                nrp.emit('sillyputty', {command: msg, subscriber: wsc._meta.id});
                break;
            default:
                console.log('no handler for provider: ', provider);
        }
    });
    wsc.on('pong', function() {
        wsc._meta.alive = true;
    })
    wsc.on('close', function(e) {
        // console.log('SOCKET DICONNECT: ', wsc._meta);
    });

});

nrp.on('socket', function(data) {
    ws.getWss().clients.forEach(function(wsc) {
        if (wsc._meta.subscriptions.indexOf(data.provider) === -1) {
            return;
        }
        if (data.to && data.to.indexOf(wsc._meta.id) === -1) {
            return;
        }
        if (data.exclude && data.exclude.indexOf(wsc._meta.id) !== -1) {
            return;
        }
        wsc.send(data.provider + ':' + data.command);
    })
})

const subscribe = function(provider, subscriptions) {
    if (subscriptions.indexOf(provider) === -1) {
        subscriptions.push(provider);
        console.log('subscribed: ', provider, subscriptions)
    }
}

const unsubscribe = function(provider, subscriptions) {
    const subscriptionIdx = subscriptions.indexOf(provider);
    if (subscriptionIdx !== -1) {
        subscriptions.splice(subscriptionIdx, 1);
        console.log('unsubscribed: ', provider, subscriptions)
    }
}

const heartbeat = setInterval(function() {
    ws.getWss().clients.forEach(function(wsc) {
        if (!wsc._meta || wsc._meta.alive === false) {
            console.log('SOCKET TERMINATE (no heartbeat): ', wsc._meta);
            return wsc.terminate();
        }
        wsc._meta.alive = false;
        wsc.ping(function() { })
    });
}, 30000);

return { blank: true };

// ==
}
