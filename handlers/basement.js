var config   = require('../../config'),
    view     = require('../lib/view'),
    response = require('../lib/response'),
    util     = require('../lib/util'),
    cellar   = require('../lib/cellar'),
    multi    = require('multiparty'),
    url      = require('url'),
    app      = null,
    db       = null,
    redis    = null;

module.exports = function(providers) {
// ==

app           = providers.app;
db            = providers.db;
redis         = providers.redis;


app.get(/^\/basement\/([a-f0-9]+)\/(.*)$/, function(req, res) {
    var plug = null;
    return new Promise(function(resolve, reject) {
        redis.get(req.params[0], function(err, path) {
            if (err || !path || path.indexOf(req.params[1]) !== 0) reject(err);
            else resolve(path);
        })
    })
    .then(function(path) {
        var search = url.parse(req.url).search,
            path   = req.params[1];
        return cellar.getPlugin(path, search, req.headers.cookie, util.isOnionRequest(req));
    })
    .then(function(_plugin) {
        plug = _plugin;
        var pageType = plug.pageType.replace(/\//g, '-')
        return view.layout(req, plug.content, plug.meta, pageType);
    })
    .then(function(html) {
        plug.html = html;
        return response.send(plug, req, res);
    })
    .catch(function(err) {
        return res.redirect('/access_denied');
    });
});

app.post(/^\/basement\/([a-f0-9]+)\/(.*)$/, function(req, res) {
    var plug = null,
        json = false;
    return new Promise(function(resolve, reject) {
        redis.get(req.params[0], function(err, path) {
            if (err || !path || path.indexOf(req.params[1]) !== 0) reject(err);
            else resolve(path);
        })
    })
    .then(function(path) {
        return new Promise(function(resolve, reject) {
            (new multi.Form()).parse(req, function(err, fields, files) {
                if (err) return reject(err);
                return resolve(fields);
            });
        });
    })
    .then(function(fields) {
        if (util.form.truthy(fields.json)) json = true;

        var search = url.parse(req.url).search,
            path   = req.params[1];

        return cellar.postPlugin(path, search, fields, req.headers.cookie, util.isOnionRequest(req));
    })
    .then(function(_plugin) {
        plug = _plugin;
        if (plug.redirect || json) return;
        var pageType = plug.pageType.replace(/\//g, '-')
        return view.layout(req, plug.content, plug.meta, pageType);
    })
    .then(function(html) {
        if (plug.accessToken) {
            res.cookie('access_token', plug.accessToken, config.cookie);
            res.clearCookie('insidious_tracking_cookie');
        }
        if (plug.redirect && !json)
            return response.redirect(plug.redirect, req, res);
        plug.html = html;
        return response.send(plug, req, res, {json: json});
    })
    .catch(function(err) {
        if (err && err.pluginErr)
            return res.status(err.status ? err.status : 500).json(err.pluginErr)
        
        return response.redirect('/access_denied', req, res);
    });
});

app.get(/^\/basement\/plugins\/(.*)$/, function(req, res) {
    return cellar.pipe(req, res, 'plugins', req.params[0]);
});

app.get(/^\/basement\/images\/(.*)$/, function(req, res) {
    return cellar.pipe(req, res, 'images', req.params[0]);
});

app.get(/^\/basement\/videos\/(.*)$/, function(req, res) {
    return cellar.pipe(req, res, 'videos', req.params[0]);
});

app.get(/^\/basement\/audio\/(.*)$/, function(req, res) {
    return cellar.pipe(req, res, 'audio', req.params[0]);
});


return { blank: true };

// ==
}
