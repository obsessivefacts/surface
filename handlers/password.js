var view     = require('../lib/view'),
    response = require('../lib/response'),
    util     = require('../lib/util'),
    multi    = require('multiparty'),
    models   = {},
    app      = null,
    db       = null,
    redis    = null,
    sitemap  = null;

module.exports = function(providers) {
// ==

app           = providers.app;
db            = providers.db;
redis         = providers.redis;
sitemap       = providers.sitemap;
models.Plugin = require('../models/plugin')(db);

app.get('/password', function(req, res) {
    var code = req.query.code,
        key  = null;
    if (code) {
        var keys = code.split(',');
        key = parseInt(keys[keys.length - 1]);
        console.log('key: ', key);
    }
    return view.render(req, 'index', {password: true, code: code, key: key},
        key ? {
            // this used to work, in 1999
            // meta: [{
            //     httpEquiv: "REFRESH",
            //     content:   "0;URL='/audio/key-"+key+".ogg?autostart=true"
            // }]
        } : null
    )
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/password');

app.post('/password', function(req, res) {
    var json = false,
        path = null;

    return new Promise(function(resolve, reject) {
        (new multi.Form()).parse(req, function(err, fields, files) {
            if (err) return reject(err);
            return resolve(fields);
        });
    }).then(function(formFields) {
        if (util.form.truthy(formFields.json)) json = true;
        return models.Plugin.findOne({ where: { code: formFields.code[0] }});
    }).then(function(plugin) {
        if (!plugin) throw { _msg: 'Access denied.', _code: 401 };
        path = plugin.dataValues.path;
        return util.makeId();
    }).then(function(id) {
        return new Promise(function(resolve, reject) {
            redis.set(id, path, function(err, reply) {
                if (err) reject(err);
                redis.expire(id, 86400);
                resolve('/basement/' + id  + '/' + path);
            });
        });
    }).then(function(_goto) {
        if (json) res.json({ url: _goto });
        else res.redirect(_goto);
    }).catch(function(err) {
        err || (err = {});
        console.log('password error: ', err);
        if (json) res.status(err._code ? err._code : 500).json({err: err._msg});
        else res.redirect('/access_denied');
    });
})

return { blank: true };

// ==
}
