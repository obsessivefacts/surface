var config   = require('../../config'),
    view     = require('../lib/view'),
    response = require('../lib/response'),
    cache    = require('../lib/cache'),
    util     = require('../lib/util'),
    multi    = require('multiparty'),
    app      = null,
    redis    = null;

const MAX_REPLY_DEPTH = 4;

module.exports = function(providers) {
// ==

app     = providers.app;
redis   = providers.redis;

cache.redisInit(redis);

const postLib = require('../components/posts')(providers)

const renderReplyForm = function(req, res, postInfo, options) {
    if (!options) options = {};

    const postId = options.postId || postInfo.postId;
    var context, replyHTML;

    return postLib.getContext(postInfo.type, postInfo.refId, postId, postInfo.descending)
        .then(function(_context) {
            context = _context;
            if (context && context.length < MAX_REPLY_DEPTH) {
                return view.renderComponent(req, 'posts/post', {
                    post: context[context.length-1],
                    suppressPostActions: true,
                    suppressHighlight: true,
                })
            }
            return null;
        })
        .then(function(_replyHTML) {
            if (_replyHTML) {
                replyHTML = _replyHTML;
            }
            var prefill = null,
                msg = null,
                replyToId = null;

            if (context) replyToId = context[context.length-1].id;

            if (context && context.length == MAX_REPLY_DEPTH) {
                var post = context[context.length-1],
                    text = post.text;

                var username = post.user ? post.user.username : post.tmp_username;

                text = text.replace(/\r\n/ig, "\n").split("\n");
                prefill = '> **Posted by '+username+':**\n> \n';
                for (line of text) {
                    // if (line.indexOf('>') !== 0)
                        prefill += '> ' + line + '\n';
                }
                prefill += '\n';
                msg = 'Please remove any quoted text that is not relevant to your reply.';

                replyToId = context[context.length-2].id
            }
            postInfo.postId = postId;
            postInfo.replyToId = replyToId;
            postInfo.lulz = Math.random();

            var callbackData = util.encrypt(postInfo);

            return postLib.renderForm(req, postInfo.type, postInfo.refId, req.user, callbackData, {
                placeholder: 'Write your reply',
                buttonText: 'Reply',
                prefill: options.prefill || prefill,
                msg: options.msg || msg,
                isError: options.isError || false,
                spamChallengeIdx: options.spamChallengeIdx || null,
                spamChallengeText: options.spamChallengeText || null,
            })
        })
        .then(function(composeHtml) {
            if (!context) {
                return response.show404(req, res);
            }
            return view.render(req, 'write', {
                replyHTML: replyHTML,
                composeHtml: composeHtml
            })
            .then(function(data) { response.send(data, req, res); })
        })
        .catch(function(err) { response.error(err, req, res); });
}

const renderPostForm = function(req, res, postInfo, options) {
    if (!options) options = {};

    var callbackData = util.encrypt(postInfo);

    return postLib.renderForm(req, postInfo.type, postInfo.refId, req.user, callbackData, {
        prefill: options.prefill || null,
        msg: options.msg || null,
        isError: options.isError || false,
        editPostId: options.editPostId || null,
        buttonText: options.buttonText || null,
        spamChallengeIdx: options.spamChallengeIdx || null,
        spamChallengeText: options.spamChallengeText || null,
    })
    .then(function(composeHtml) {
        return view.render(req, 'write', {
            composeHtml: composeHtml
        })
        .then(function(data) { response.send(data, req, res); })
    })
    .catch(function(err) { response.error(err, req, res); });
}

app.get(/^\/write\/([a-f0-9]{16})([0-9]+)$/, function(req, res, next) {
    const postId = parseInt(req.params[1]);
    var postInfo;

    return cache.redisGet(req.params[0])
        .then(function(encryptedCallbackData) {
            try {
                postInfo = util.decrypt(encryptedCallbackData);
            } catch(err) {
                throw { _msg: 'Access denied.', _code: 401 };
            }

            return renderReplyForm(req, res, postInfo, {postId: postId});
        })
        .catch(function(err) {
            err || (err = {});
            return response.redirect('/access_denied', req, res);
        });;
});

app.get(/^\/write\/([a-f0-9]{16})e([0-9]+)$/, function(req, res, next) {
    const postId = parseInt(req.params[1]);
    var postInfo;

    return cache.redisGet(req.params[0])
        .then(function(encryptedCallbackData) {
            try {
                postInfo = util.decrypt(encryptedCallbackData);
            } catch(err) {
                throw { _msg: 'Access denied.', _code: 401 };
            }

            return postLib.getById(postId);
        })
        .then(function(post) {
            return renderPostForm(req, res, postInfo, {
                prefill: post.text,
                editPostId: postId,
                buttonText: 'Edit',
            });
        })
        .catch(function(err) {
            err || (err = {});
            return response.redirect('/access_denied', req, res);
        });;
});

app.post(/^\/write$/, function(req, res, next) {
    var fields, errors = [], json = false, postInfo, securityToken,
        securityError = false, editPostId;

    var _securityError = function() {
        if (json) {
            errors.push('LEGALIZE METH')
            return _jsonResponse(401);
        }
        return res.redirect('/access_denied');
    }

    var _jsonResponse = function(statusCode, result) {
        if (errors.length) res.status(statusCode);
        return res.json({
            status: errors.length == 0 ? 'ok' : 'error',
            errors: errors,
            result: result
        });
    }
    
    return new Promise(function(resolve, reject) {
        (new multi.Form()).parse(req, function(err, fields, files) {
            if (err) return reject(err);
            return resolve([fields, files]);
        });
    })
    .then(function(fieldsAndFiles) {
        fields = util.form.flatten(fieldsAndFiles[0]);
        json = util.form.truthy(fields.json) || false;

        if (fields.shibainu) {
            editPostId = parseInt(fields.shibainu) || 0;
        }

        try {
            postInfo = util.decrypt(fields.legalize);
            securityToken = util.decrypt(fields.meth);
        } catch(err) {
            return _securityError();
        }

        if (!securityCheck(securityToken, req.user, req.headers)) {
            return _securityError();
        }

        if (!fields.post || fields.post.length < 4) {
            errors.push('Please actually write something.');
        }

        let spamChallengeIdx = null,
            spamChallengeText = '';

        // if not a javascript post and the body contains 'http', '.com', etc.
        // then there is a high probability of spam. let's make sure the user
        // is actually a human without google advanced persistent malware WEI
        if (
            (
                fields.post.toLowerCase().indexOf('http') !== -1
                ||
                fields.post.toLowerCase().indexOf('.com') !== -1
                ||
                fields.post.toLowerCase().indexOf('.net') !== -1
                ||
                fields.post.toLowerCase().indexOf('.ru') !== -1
            )
            && 
            !json
        ) {
            const challengeIdx = fields.caveat || null,
                  userResponse = fields.emptor || null,
                  challenge    = postLib.getSpamChallenge(challengeIdx);

            // if we didn't get a challenge back then the client gave us a bogus
            // value for the hidden field. fail.
            if (!challenge) {
                return _securityError();
            }
            if (userResponse) {
                if (!postLib.validateSpamChallenge(challengeIdx, userResponse)) {
                    errors.push('Wrong, try again lol');
                }
            } else {
                errors.push('Please humor us and answer this simple question.');
            }

            spamChallengeIdx = challenge.key;
            spamChallengeText = challenge.question;
        }

        var promise = Promise.resolve();
        if (!errors.length && !editPostId) {
            promise = promise.then(function() {
                return postLib.create(req, postInfo, fields.post);
            });
        } else if (!errors.length && editPostId) {
            promise = promise.then(function() {
                return postLib.getById(editPostId)
            })
            .then(function(post) {
                if (!postLib.isMyPost(post, req.user)) {
                    throw Error('user cannot edit ' + editPostId);
                }
                return postLib.update(editPostId, fields.post);
            })
            .catch(function(err) {
                console.log('error editing post: ', err);
                errors.push('LEGALIZE METH');
                return null;
            });
        }
        return promise.then(function(result) {
            if (errors.length) {
                if (json) {
                    return _jsonResponse(401);
                }
                else if (postInfo.replyToId) {
                    return renderReplyForm(req, res, postInfo, {
                        postId: postInfo.postId,
                        prefill: fields.post,
                        msg: errors[0],
                        isError: true,
                        spamChallengeIdx,
                        spamChallengeText
                    });
                }
                else {
                    return renderPostForm(req, res, postInfo, {
                        prefill: fields.post,
                        msg: errors[0],
                        isError: true,
                        editPostId: editPostId,
                        buttonText: editPostId ? 'Edit' : null,
                        spamChallengeIdx,
                        spamChallengeText
                    });
                }
            }
            if (editPostId) {
                result = result[1][0].dataValues;
            }
            return postLib.getContext(postInfo.type, postInfo.refId, result.id, postInfo.descending, result.is_spam)
                .then(function(context) {
                    var post = context[context.length - 1],
                        cleanPath = postInfo.path.replace(/\#.*/, ''),
                        spamHint = result.is_spam ? 'hilite' : 'highlight',
                        finalPath = cleanPath + '?page=' + post._page + '&' + spamHint + '=' + result.id + '#POST-' + result.id;

                    if (json) {
                        return _jsonResponse(200, {
                            cleanPath: cleanPath,
                            finalPath: finalPath,
                            page: post._page,
                            id: result.id
                        });
                    }

                    return res.redirect(finalPath);
                });
        });
    })
    .catch(function(err) {
        if (err === 404) return response.show404(req, res);
        response.error(err, req, res);
    });
});

const securityCheck = function(securityToken, user, headers) {
    if
    (
        user.isLoggedIn()
        &&
        (
            !securityToken.access_token
            ||
            user.access_token != securityToken.access_token
        )
    )
    {
        return false;
    }
    else if
    (
        !user.isLoggedIn()
        &&
        (
            !securityToken.name
            ||
            !securityToken.snowflake
            ||
            (
                (
                    !headers.dnt
                    ||
                    headers.dnt.toString() !== "1"
                    ||
                    securityToken.optIn
                )
                &&
                (
                    securityToken.name != user.tmpIdentity.name
                    ||
                    securityToken.symbol != user.tmpIdentity.symbol
                    ||
                    securityToken.snowflake != user.tmpIdentity.snowflake
                )
            )
        )
    )
    {
        return false;
    }
    else
    {
        return true;
    }
}


return { blank: true };

// ==
}
