var view        = require('../lib/view'),
    response    = require('../lib/response'),
    fs          = require('fs'),
    multi       = require('multiparty'),
    Memespeech  = require('memespeech'),
    util        = require('../lib/util'),
    app         = null,
    sitemap     = null;

const billOfRights = `# Amendments to the Constitution of the United States of America

## Amendment I.

Congress shall make no law respecting an establishment of religion, or prohibiting the free exercise thereof; or abridging the freedom of speech, or of the press; or the right of the people peaceably to assemble, and to petition the government for a redress of grievances.

## Amendment II.

A well regulated militia, being necessary to the security of a free state, the right of the people to keep and bear arms, shall not be infringed.

## Amendment III.

No soldier shall, in time of peace be quartered in any house, without the consent of the owner, nor in time of war, but in a manner to be prescribed by law.

## Amendment IV.

The right of the people to be secure in their persons, houses, papers, and effects, against unreasonable searches and seizures, shall not be violated, and no warrants shall issue, but upon probable cause, supported by oath or affirmation, and particularly describing the place to be searched, and the persons or things to be seized.

## Amendment V.

No person shall be held to answer for a capital, or otherwise infamous crime, unless on a presentment or indictment of a grand jury, except in cases arising in the land or naval forces, or in the militia, when in actual service in time of war or public danger; nor shall any person be subject for the same offense to be twice put in jeopardy of life or limb; nor shall be compelled in any criminal case to be a witness against himself, nor be deprived of life, liberty, or property, without due process of law; nor shall private property be taken for public use, without just compensation.

## Amendment VI.

In all criminal prosecutions, the accused shall enjoy the right to a speedy and public trial, by an impartial jury of the state and district wherein the crime shall have been committed, which district shall have been previously ascertained by law, and to be informed of the nature and cause of the accusation; to be confronted with the witnesses against him; to have compulsory process for obtaining witnesses in his favor, and to have the assistance of counsel for his defense.

## Amendment VII.

In suits at common law, where the value in controversy shall exceed twenty dollars, the right of trial by jury shall be preserved, and no fact tried by a jury, shall be otherwise reexamined in any court of the United States, than according to the rules of the common law.

## Amendment VIII.

Excessive bail shall not be required, nor excessive fines imposed, nor cruel and unusual punishments inflicted.

## Amendment IX.

The enumeration in the Constitution, of certain rights, shall not be construed to deny or disparage others retained by the people.

## Amendment X.

The powers not delegated to the United States by the Constitution, nor prohibited by it to the states, are reserved to the states respectively, or to the people.`;


module.exports = providers => {
// ==

app = providers.app;
sitemap = providers.sitemap;

app.get('/memespeech', (req, res) => {
  return view.render(req, 'memespeech', {})
    .then(data => response.send(data, req, res))
    .catch(err => response.error(err, req, res));
});
sitemap.add('/memespeech');

app.post('/memespeech', (req, res, next) => {
    let fields;

    return new Promise((resolve, reject) => {
        (new multi.Form()).parse(req, (err, fields, files) => {
            if (err) return reject(err);
            return resolve([fields, files]);
        });
    })
    .then(async function(fieldsAndFiles) {
        fields = util.form.flatten(fieldsAndFiles[0]);

        let ms = new Memespeech({ carrierText: billOfRights });

        let budgets = ms.computeAESByteBudget(),
            plaintext = fields.plaintext,
            plaintextLen = Memespeech.DataUtils.stringToBytes(plaintext).length,
            charactersAvailable = budgets.byteBudgetCompact - plaintextLen,
            mode = 'compact',
            responseObj = {};

        if (charactersAvailable < 0) {
            responseObj.error = 'COULD NOT ENCRYPT—Please shorten your text by '
                              + (-1 * charactersAvailable) + ' bytes.';
        } else if (plaintextLen == 0) {
            responseObj.error = 'Please actually enter a message to encrypt!';
        } else {
            if (budgets.byteBudgetStandard - plaintextLen >= 0)
                mode = 'standard'

            await ms.initCipherFromPassphrase('SANIC', mode);

            try {
                await ms.encrypt(plaintext);
                responseObj.memespeech = ms.toString();
                responseObj.msg = 'Your message was encrypted into the '
                                + 'Bill of Rights with password: SANIC';
            } catch(err) {
                responseObj.error = 'An error occurred during encryption, WTF';
            }
        }
        return view.render(req, 'memespeech', responseObj)
    })
    .then(data => response.send(data, req, res))
    .catch(err => response.error(err, req, res));
});

return { blank: true };

// ==
}
