var view     = require('../lib/view'),
    response = require('../lib/response'),
    child    = require('child_process'),
    app      = null,
    sitemap  = null;

module.exports = function(providers) {
// ==

app = providers.app;
sitemap = providers.sitemap;

app.get('/', function(req, res) {
  return new Promise(function(resolve, reject) {
    child.exec('uptime', function (error, stdout, stderr) {
      if (error) reject(error);
      resolve(stdout.trim());
    });
  })
  .then((uptime) => {
    return view.render(req, 'index', { uptime })
  })
  .then(function(data) { response.send(data, req, res); })
  .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/');

return { blank: true };

// ==
}
