var view     = require('../lib/view'),
    response = require('../lib/response'),
    app      = null,
    sitemap  = null;


module.exports = function(providers) {
// ==

app = providers.app;
sitemap = providers.sitemap;

app.get('/volition', function(req, res) {
  return view.render(req, 'volition', {})
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/volition');

app.get('/volition/noscript', function(req, res) {
  return view.render(req, 'volition', {no_js: true})
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});

return { blank: true };

// ==
}
