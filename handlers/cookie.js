var config   = require('../../config'),
    view     = require('../lib/view'),
    response = require('../lib/response'),
    util     = require('../lib/util'),
    cache    = require('../lib/cache'),
    crypto   = require('crypto'),
    app      = null,
    redis    = null;

module.exports = function(providers) {
// ==

app     = providers.app;
redis   = providers.redis;

cache.redisInit(redis);

app.get(/^\/cookie\/dnt\-override\/([a-f0-9\-]+)\/([a-f0-9\-]+)$/, function(req, res, next) {
    var tmpCookie = req.params[0],
        callbackEnc = req.params[1],
        callbackDec = null,
        callbackPath = null;

    return cache.redisGet(callbackEnc)
        .then(function(encryptedCallbackData) {
            try {
                let notMasked = util.decrypt(tmpCookie);
                notMasked.optIn = true;

                var callbackPath = util.decrypt(encryptedCallbackData).path;
                let encrypted = util.encrypt(notMasked);

                req.user.tmpIdentityEncrypted = encrypted;
                res.cookie('insidious_tracking_cookie', encrypted, config.cookie);
                res.set({'Tk': 'C'});
            }
            catch(err) {

                if (util.isJSON(req)) res.status(err._code ? err._code : 500).json({err: 'FAIL'});
                else res.redirect('/access_denied');
                return;
            }

            if (util.isJSON(req))
                res.json({yay: true});
            else
                res.redirect(callbackPath);
        });
});

app.get('/cookie/destroy', function(req, res) {
    res.clearCookie('insidious_tracking_cookie');
    res.redirect('/access_granted');
})

return { blank: true };

// ==
}
