var view          = require('../lib/view'),
    util          = require('../lib/util'),
    response      = require('../lib/response'),
    config        = require('../../config'),
    tls           = require('tls'),
    stream        = require('stream'),
    ColorScheme   = require('color-scheme'),
    app           = null,
    sitemap       = null;

const SOCKET_DESTROY_TIMEOUT = 10000,
      DEBUG = true;

const GEMINI_ERRORS = {
    // -------------------------------------------------------------------------
    // Internal errors
    // -------------------------------------------------------------------------
    'INVALID_URI':          '0x Invalid URI.',
    'FORBIDDEN_URI':        '0x Forbidden URI.',
    'INVALID_PROTOCOL':     '0x Invalid protocol.',
    'REFUSED':              '0x Connection refused with remote server.',
    'TIMEOUT':              '0x Failed to communicate with remote server.',
    'TOO_MANY_REDIRECTS':   '0x Too many redirects.',
    'INVALID_RESPONSE':     '0x Invalid response format.',
    'UNSUPPORTED':          '0x Unsupported response format.',
    'UNSUPPORTED_10':       '0x Unsupported input response: 10',
    'UNSUPPORTED_11':       '0x Unsupported input response: 11',

    // -------------------------------------------------------------------------
    // From spec (https://gemini.circumlunar.space/docs/specification.gmi)
    // -------------------------------------------------------------------------
    '40':                   '40 TEMPORARY FAILURE',
    '41':                   '41 SERVER UNAVAILABLE',
    '42':                   '42 CGI ERROR',
    '43':                   '43 PROXY ERROR',
    '44':                   '44 SLOW DOWN',
    '50':                   '50 PERMANENT FAILURE',
    '51':                   '51 NOT FOUND',
    '52':                   '52 GONE',
    '53':                   '53 PROXY REQUEST REFUSED',
    '59':                   '59 BAD REQUEST',
    '60':                   '60 CLIENT CERTIFICATE REQUIRED',
    '61':                   '61 CERTIFICATE NOT AUTHORIZED',
    '62':                   '62 CERTIFICATE NOT VALID'
}

module.exports = function(providers) {
    // ==

    app = providers.app;
    sitemap = providers.sitemap;

    app.get('/gemini-proxy', function(req, res) {
        let viewParams = {
            internal: req.query.internal ? true : false
        };

        let uri = 'gemini://obsessivefacts.com/gemini-proxy',
            start = new Date();

        if (req.query.uri) uri = req.query.uri;

        return getGeminiUri(uri)
            .then(function(geminiResponse) {

                if (req.query.download) {
                    let file = geminiResponse.uri.pathname.replace(/.*\//, '');
                    res.writeHead(200, {
                        'Content-Type': geminiResponse.mimeType,
                        'Content-Disposition': 'attachment; filename="'+file+'"'
                    });
                    return res.end(geminiResponse.binaryBuffer);
                }

                viewParams.uri = geminiResponse.uri.href.replace(':1965', '')
                                                        .replace('<', '&lt;');
                viewParams.elapsed = (((new Date()) - start) / 1000).toFixed(2);
                viewParams.header  = getHeaderProps(geminiResponse.uri);
                viewParams.encURI  = encodeURIComponent(viewParams.uri);

                switch (geminiResponse.mimeType) {
                    case 'text/gemini':
                        viewParams.gemtext = true;
                        viewParams.data = parseGemtext(geminiResponse);
                        break;
                    case 'image/jpeg':
                    case 'image/apng':
                    case 'image/gif':
                    case 'image/png':
                    case 'image/webp':
                        viewParams.image = true;
                        viewParams.data = binaryToBase64(geminiResponse);
                        break;
                    case 'application/pgp-signature':
                    case 'application/x-yaml':
                    case 'text/plain':
                    case 'text/yaml':
                    case 'text/x-yaml':
                    case 'text/csv':
                    case 'text/javascript':
                    case 'text/css':
                    case 'text/plain':
                    case 'text/markdown':
                    case 'text/x-markdown':
                        viewParams.rawtext = true;
                        viewParams.data = geminiResponse.data;
                        break;
                    default:
                        viewParams.unsupported = true;
                        viewParams.mimeType = geminiResponse.mimeType;
                        break;
                }
                
                return view.render(req, 'gemini-proxy', viewParams)
                           .then((data) => {
                                data.viewParams = cleanViewParams(viewParams);
                                return response.send(data, req, res)
                            });
            })
            .catch(function(err) {
                viewParams.uri     = uri;
                viewParams.error   = err;
                viewParams.elapsed = (((new Date()) - start) / 1000).toFixed(2);

                return view.render(req, 'gemini-proxy', viewParams)
                           .then((data) => {
                                data.viewParams = cleanViewParams(viewParams);
                                return response.send(data, req, res)
                            });
            });
    });
    sitemap.add('/gemini-proxy');

    return { blank: true };

    // ==
}

/**
 * Fetch a Gemini Protocol URI
 * @param {string} uri URI string in format gemini://domain.com[:port][/path]
 * @param {number} redirectCount used internally for recursive redirecting
 * @returns {object} An object containing data from the Gemini Response
 */
const getGeminiUri = async (uri, redirectCount) => {

    return new Promise((resolve, reject) => {

        if (!redirectCount)
            redirectCount = 0;
        
        if (redirectCount == 3) 
            return reject(GEMINI_ERRORS['TOO_MANY_REDIRECTS']);
        
        try {
            var url = new URL(uri);
        } catch (err) {
            return reject(GEMINI_ERRORS['INVALID_URI']);
        }

        if (
            url.hostname.indexOf('localhost') === 0 ||
            url.hostname.indexOf('127.')      === 0 ||
            url.hostname.indexOf('10.')       === 0 ||
            url.hostname.indexOf('172.')      === 0 ||
            url.hostname.indexOf('192.168.')  === 0
            )
            return reject(GEMINI_ERRORS['FORBIDDEN_URI']);

        if (url.protocol !== 'gemini:')
            return reject(GEMINI_ERRORS['INVALID_PROTOCOL']);            

        if (!url.port)
            url.port = '1965';

        if (!url.pathname)
            url.pathname = '/';

        let buffers = [];

        const options = {
            rejectUnauthorized: false,
            servername:         url.hostname
        };

        const socket = tls.connect(url.port, url.hostname, options, () => {

            socket.on('data', (data) => {
                buffers.push(data);
            });

            socket.on('end', () => {
                clearTimeout(socketDestroyTimer);

                const buffer = Buffer.concat(buffers),
                      gmiData = buffer.toString('utf8');

                const headerRegExp = new RegExp(/^(\d{2}\s+.*)\r?\n/);

                let headerMatch = gmiData.match(headerRegExp);
                if (!headerMatch || headerMatch.length < 2) {
                    return reject(GEMINI_ERRORS['INVALID_RESPONSE']);
                }
                const headerStr     = headerMatch[1],
                      headerSplit   = headerStr.split(/\s+/, 2),
                      responseCode  = parseInt(headerSplit[0]),
                      responseMeta  = headerSplit[1], 
                      responseData  = gmiData.replace(headerRegExp, ''),
                      binaryBuffer  = buffer.slice(headerMatch[0].length);

                switch (responseCode) {
                    case 10:
                        return reject(GEMINI_ERRORS['UNSUPPORTED_10']);
                    case 11:
                        return reject(GEMINI_ERRORS['UNSUPPORTED_11']);
                    case 20:
                        if (responseMeta.indexOf(';') !== -1) {
                            var metas    = responseMeta.split(';'),
                                mimeType = metas[0],
                                lang     = metas[1].replace('lang=', '');
                        } else {
                            var mimeType = responseMeta,
                                lang     = 'en';
                        }
                        return resolve({
                            status:         responseCode,
                            mimeType:       mimeType,
                            lang:           lang,
                            data:           responseData,
                            uri:            url, 
                            binaryBuffer
                        })
                    case 30:
                    case 31:
                        console.warn('redirecting to ' + responseMeta);
                        let redirect = responseMeta.indexOf('/') === 0
                            ?
                            'gemini://'+url.hostname+':'+url.port+responseMeta
                            :
                            responseMeta;
                        return resolve(getGeminiUri(redirect, redirectCount+1));
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                        return reject(GEMINI_ERRORS[responseCode]);
                    default:
                        console.warn('unsupported response: ', responseMeta);
                        return reject(GEMINI_ERRORS['UNSUPPORTED']);
                }
            });
            socket.write('gemini://' + url.hostname + url.pathname + '\r\n');
        });
        socket.on('error', (err) => {
            console.log('gemini error: ', err);
            reject(GEMINI_ERRORS['REFUSED']);
        });
        const socketDestroyTimer = setTimeout(() => {
            socket.destroy();
            reject(GEMINI_ERRORS['TIMEOUT']);
        }, SOCKET_DESTROY_TIMEOUT);
    });
}

/**
 * Convert gemtext into HTML markup
 * @param {object} geminiResposne the response object passed from @getGeminiUri
 * @returns {string} HTML markup
 */
const parseGemtext = (geminiResponse) => {
    let html     = '',
        open     = '',
        baseUri  = geminiResponse.uri;

    const isOpen = (tag) => open === tag;

    const doOpen = (tag) => {
        doClose();
        open = tag;
        html += '<'+open+'>\r\n'
    };

    const doClose = () => {
        if (open) {
            html += '</'+open+'>\r\n';
            open = '';
        }
    };

    const sanitize = (str) => str.trim().replaceAll('<', '&lt;');

    geminiResponse.data.split(/\r?\n/).forEach( (line) => {
        // handle pre blocks first and foremost
        if (line.startsWith('```')) {
            if (isOpen('pre')) {
                return doClose();
            }
            else {
                return doOpen('pre');
            }
        }
        if (isOpen('pre')) {
            return html += line.replaceAll('<', '&lt;') + '\r\n';
        }

        // handle quote blocks
        if (line.startsWith('>')) {
            if (!isOpen('blockquote')) {
                doOpen('blockquote');
                html += '<div class="q">“</div>';
            }
            return html += '<p>' + sanitize(line.substr(1)) + '&nbsp;</p>\r\n';
        } else if (isOpen('blockquote')) {
            doClose();
        }

        // handle bulleted lists
        if (line.startsWith('*')) {
            if (!isOpen('ul')) {
                doOpen('ul');
            }
            return html += '<li>' + sanitize(line.substr(1)) + '</li>\r\n';
        } else if (isOpen('ul')) {
            doClose();
        }

        // handle headers in reverse order of treachery
        if (line.startsWith('###')) {
            return html += '<h3>' + sanitize(line.substr(3)) + '</h3>\r\n';
        } else if (line.startsWith('##')) {
            return html += '<h2>' + sanitize(line.substr(2)) + '</h2>\r\n';
        } else if (line.startsWith('#')) {
            return html += '<h1>' + sanitize(line.substr(1)) + '</h1>\r\n';
        }

        // handle links, ugh
        if (line.startsWith('=>')) {
            let link = sanitize(line.substr(2)).match(/^(\S+)(?:\s+(.+))?$/);

            if (!link || link.length < 3) return;

            let isImg = !!link[1].match(/\.jpg$|\.jpeg$|\.gif$|\.png$|\.webp$/);

            if (link[1].startsWith('http:')   ||
                link[1].startsWith('https:')  ||
                link[1].startsWith('gopher:') ||
                link[1].startsWith('ftp:') ) {
                return html += '<p class="a"><a href="' + link[1] + '" '
                            +  'target="_blank" class="external">'
                            +  (link[2] ? link[2] : link[1]) + '</a></p>\r\n';
            } else if (link[1].startsWith('mailto:')) {
                return html += '<p class="a"><a href="' + link[1] + '" '
                            +  'class="mail">' + (link[2] ? link[2] : link[1])
                            +  '</a></p>\r\n';
            } else {
                if (link[1].startsWith('gemini:')) {
                    var uri = link[1],
                        isLink = true; // like, a link to another site
                } else {
                    var file = geminiResponse.uri.pathname,
                        path = file.substr(0, file.lastIndexOf('/') + 1),
                        uri  = 'gemini://' + baseUri.hostname
                             + (baseUri.port != '1965' ? ':'+baseUri.port : '')
                             + (link[1].startsWith('/') ? link[1] 
                             : path + link[1]);
                }
                return html += '<p class="a"><a href="/gemini-proxy?uri='
                            +  encodeURIComponent(uri)+ '" ' + (isLink ? 
                               'class="link">' : isImg ? 'class="img">' : '>')
                            +  (link[2] ? link[2] : link[1]) + '</a></p>\r\n';
            }
        }

        // everything else is just a paragraph lol
        return html += '<p>'+sanitize(line)+'&nbsp;</p>\r\n';
    });
    doClose();

    return html;
}

/**
 * Convert binary data in the Gemini response to a base64-encoded data string
 * @param {object} geminiResponse the response object passed from @getGeminiUri
 * @returns {string} A data string
 */
const binaryToBase64 = (geminiResponse) => {
    let b64 = geminiResponse.binaryBuffer.toString('base64');
    return 'data:' + geminiResponse.mimeType + ';base64, ' + b64;
}

/**
 * Gets a color scheme from the domain name
 * @param {string} domain name of domain
 * @param {string} variation color-scheme variation
 * @returns {array} color scheme hex values
 */
const getColorSchemeFromDomain = (domain, variation) => {
    // domain = domain + 'lolol';  // randomize
    if (!variation) variation = 'light';
    let val = 0;
    for (let i = 0; i < domain.length; i++) val += domain.charCodeAt(i);
    val = val % 360;
    let scheme = new ColorScheme;
    scheme.from_hue(val).scheme('tetrade').variation(variation);
    return scheme.colors();
};

/**
 * Gets a SVG filename from domain (totally specific to obsessivefacts.com)
 * @param {string} domain name of domain
 * @returns {string} 
 */
const getSvgFromDomain = (domain) => {
    // domain = domain + 'fadsssssssas';  // randomize
    let val = 0;
    for (let i = 0; i < domain.length; i++) val += domain.charCodeAt(i);
    return '#dos' + (val % 33 < 10 ? '0' + val % 33 : val % 33);
};

/**
 * Render properties for a page header (specific to obsessivefacts.com lol)
 * @param {string} base URI of gemini capsule
 * @returns {string} HTML markup
 */
const getHeaderProps = (baseUri) => {

    if (baseUri.pathname.startsWith('/~')) {
        var site = baseUri.pathname.substr(1).replace(/\/.*$/, '');
    } else {
        var site = baseUri.hostname;
    }
    const colors = getColorSchemeFromDomain(site, 'light'),
          svg = getSvgFromDomain(site),
          uri = 'gemini://' + baseUri.hostname
                            + (baseUri.port != '1965' ? ':' + baseUri.port : '')
                            + '/' 
                            + (baseUri.pathname.startsWith('/~') ? site : ''),
          proxyUri = '/gemini-proxy?uri=' + encodeURIComponent(uri),
          siteName = site;

    return { colors, svg, proxyUri, siteName }
};

/**
 * Writes to the console if DEBUG mode is turned on
 * @param {string} string to write
 */
const debugWrite = (str) => DEBUG ? console.log(str) : null;

/**
 * "Cleans" viewParams for inclusion in JSON response data.
 * @param {object} viewParams object
 * @returns {object} viewParams object with unnecessary info removed
 */
const cleanViewParams = (viewParams) => {
    let cleaned = {
        config:     viewParams.config,
        elapsed:    viewParams.elapsed,
        uri:        viewParams.uri,
        encURI:     viewParams.encURI,
        header:     viewParams.header
    };
    if (viewParams.gemtext)     cleaned.gemtext = true;
    if (viewParams.image)       cleaned.image = true;
    if (viewParams.rawtext)     cleaned.rawtext = true;
    if (viewParams.unsupported) cleaned.unsupported = true;
    if (viewParams.mimeType)    cleaned.mimeType = viewParams.mimeType;
    if (viewParams.error)       cleaned.error = viewParams.error;

    return cleaned;
};