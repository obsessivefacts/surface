var sri      = require('../sri.json'),
    app      = null;


module.exports = function(providers) {
// ==

app = providers.app;

app.get('/sri/standalone', function(req, res) {
    let sriMatch = sri['@static/js/_standalone/' + req.query.script + '.js'],
        response = {};
        
    if (sriMatch) {
        response['js/' + req.query.script] = {
            hashes: sriMatch.hashes,
            integrity: sriMatch.integrity
        }
    }
    return res.json(response);
});

return { blank: true };

// ==
}
