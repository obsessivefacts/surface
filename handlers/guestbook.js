var view     = require('../lib/view'),
    response = require('../lib/response'),
    util     = require('../lib/util'),
    shitlib  = require('../lib/shitlib'),
    fs       = require('fs'),
    models   = {},
    app      = null
    nrp      = null,
    sitemap  = null,
    FS_IMG_PATH = __dirname + '/../static/images/guestbook/';


var mostRecentPNGTimestamp = 0;

module.exports = function(providers) {
// ==

app = providers.app;
nrp = providers.nrp;
sitemap = providers.sitemap;

const postLib = require('../components/posts')(providers)

app.get('/guestbook', function(req, res) {
    var page = 1,
        highlight = 0;

    if (req.query.page) {
        page = parseInt(req.query.page) || 1;
        if (page < 1) page = 1;
    }
    if (req.query.highlight) {
        highlight = parseInt(req.query.highlight) || 0;
    }

    let hashedIP = util.getHashedIPFromReq(req),
        promise = Promise.resolve();

    if (shitlib.isSpammyRequest(req)) {
        promise = promise.then(() => {
            return shitlib.isBannedHashedIP(hashedIP).then(isBanned => {
                // NOTE ~ disable shitlib for now
                // if (isBanned) throw new Error(hashedIP)
                console.warn('spam?', hashedIP);
                return false;
            })
        })
    }
    return promise.then(() => {
        console.log('proceeding anyway');
        return postLib.renderPosts('guestbook', 'index', req, page, {
            typeSingular: 'post',
            typePlural: 'posts',
            basePath: '/guestbook',
            highlight: highlight,
            descending: true,
            suppressPostActions: true
        })
        .then(function(commentsData) {
            if (req.query.posts_ajax) {
                return res.json(commentsData);
            }

            return view.render(req, 'guestbook', {
                mostRecentPNGTimestamp: mostRecentPNGTimestamp,
                commentsHTML: commentsData.html
            })
            .then(function(data) { response.send(data, req, res); })
            .catch(function(err) { response.error(err, req, res); });
        })
    })
    .catch(err => {
        console.error('!BANNED!: ', hashedIP);
        response.HTTPError(req, res, 403);
    });
});
sitemap.add('/guestbook');

nrp.on('guestbookTimestamp', function(data) {
    // console.log('guestbook setting most recent timestamp: ', data);
    mostRecentPNGTimestamp = data.timestamp;
})

const getInitialMostRecentPNGTimestamp = function() {
    return new Promise(function(resolve, reject) {
        fs.readdir(FS_IMG_PATH, function(err, files) {
            if (err) return reject(err);
            resolve(files)
        });
    })
    .then(files => {
        for (file of files) {
            if (file.indexOf('.png') === -1) continue;
            const fileTimestamp = parseInt(file.replace('.png', ''));

            if (fileTimestamp > mostRecentPNGTimestamp) {
                mostRecentPNGTimestamp = fileTimestamp;
            }
        }
        console.log('guestbook initial PNG timestamp is: ', mostRecentPNGTimestamp);

    })
}
getInitialMostRecentPNGTimestamp();

return { blank: true };

// ==
}
