var view     = require('../lib/view'),
    response = require('../lib/response'),
    app      = null,
    sitemap  = null;


module.exports = function(providers) {
// ==

app = providers.app;
sitemap = providers.sitemap;

app.get('/contact', function(req, res) {
  return view.render(req, 'contact', {})
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/contact');

app.get('/contact/faq', function(req, res) {
  return view.render(req, 'contact', { faq_active: true })
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/contact/faq');

return { blank: true };

// ==
}
