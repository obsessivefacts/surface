var view     = require('../lib/view'),
    response = require('../lib/response'),
    fs       = require('fs'),
    marked   = require('marked'),
    hilite   = require('highlight.js'),
    models   = {},
    app      = null,
    db       = null,
    redis    = null,
    files    = null,
    sitemap  = null,
    posts    = [],
    tags     = '';

const PER_PAGE = 5;
const BLOG_TITLE = 'Obsessive Facts Blog';
const BLOG_URL = 'https://obsessivefacts.com/blog';
const BLOG_DESCRIPTION = 'Posts about shiba inus and meth';

marked.setOptions({
    highlight: function(code, type) {
        if (type == 'text') return code;
        var language_subset = ['javascript', 'json', 'python'];
        return hilite.highlightAuto(code, language_subset).value;
    }
});

module.exports = function(providers) {
// ==

app           = providers.app;
db            = providers.db;
redis         = providers.redis;
files         = providers.files;
sitemap       = providers.sitemap;

const postLib = require('../components/posts')(providers)

app.get(/^\/blog\/([a-zA-Z0-9_\-+]+)\.html$/, function(req, res, next) {

    var page = 1,
        highlight = 0;

    if (req.query.page) {
        page = parseInt(req.query.page) || 1;
        if (page < 1) page = 1;
    }
    if (req.query.highlight) {
        highlight = parseInt(req.query.highlight) || 0;
    }

    return postLib.renderPosts('blog', req.params[0], req, page, {
        typeSingular: 'comment',
        typePlural: 'comments',
        basePath: '/blog/' + req.params[0] + '.html',
        highlight: highlight
    })
    .then(function(commentsData) {
        if (req.query.posts_ajax) {
            return res.json(commentsData);
        }
        return list(req, res, {
            page: 1,
            slug: req.params[0],
            showComments: true,
            viewingSlug: true,
        }, commentsData);
    })
    .catch(function(err) {
        console.log('caught error');
        if (err._code && err._code === 404) return response.show404(req, res);
        response.error(err, req, res);
    });
});

app.get(/^\/blog\/tagged\/([a-zA-Z0-9_\-+]+)$/, function(req, res, next) {
    return list(req, res, {page: 1, tag: req.params[0]});
});

app.get(/^\/blog\/tagged\/([a-zA-Z0-9_\-+]+)\/(\d*)$/, function(req, res, next) {
    return list(req, res, {page: req.params[1], tag: req.params[0]});
});

app.get(/^\/blog$/, function(req, res, next) {
    return list(req, res, {page: 1});
});

app.get(/^\/blog\/(\d*)$/, function(req, res, next) {
    return list(req, res, {page: req.params[0]});
});

app.get(/^\/blog\/rss.xml$/, function(req, res, next) {
    var feed = `<?xml version="1.0"?>
<rss version="2.0">
<channel>
\t<title>${BLOG_TITLE}</title>
\t<link>${BLOG_URL}</link>
\t<description>${BLOG_DESCRIPTION}</description>
    `;
    for (var i=0; i<posts.length && i<50; i++) {
        var post = posts[i];
        feed += '\n\t<item>\n\t\t<link>https://obsessivefacts.com/blog/' + post.slug + '.html</link>';
        if (post.title) feed += '\n\t\t<title>' + post.title + '</title>';
        if (post.subtitle) feed += '\n\t\t<description>' + post.subtitle + '</description>';
        feed += '\n\t</item>';
    }
    feed += '\n</channel>\n</rss>';
    res.send(feed);
})

const list = function(req, res, params, commentsData) {
    var meta;

    var filtered = filter(params),
        viewParams = {
            posts: filtered.posts,
            page: filtered.page,
            numPages: filtered.numPages,
            tagCloud: tags,
            showComments: params.showComments ? params.showComments : false
        }
    if (filtered.page > 1)
        viewParams.prevPage = filtered.page - 1;

    if (filtered.page < filtered.numPages)
        viewParams.nextPage = filtered.page + 1;

    if (params.tag) {
        viewParams.taggedPretty = params.tag.replace(/\+/g, ' ');
        viewParams.tagged = params.tag;
    }

    if (commentsData) {
        viewParams.commentsHTML = commentsData.html;
    }

    if (params.viewingSlug) {
        if (!filtered.posts.length) {
            throw { _msg: 'file not found', _code: 404 };
        }
        var post = filtered.posts[0];
        var meta = { meta: [] }

        if (post.title)
            meta.title = post.title;

        if (post.subtitle)
            meta.meta.push({ name: 'description', content: post.subtitle });

        viewParams.viewingSlug = true;
    }

    return view.render(req, 'blog', viewParams, meta)
        .then(function(data) {
            response.send(data, req, res);
        })
        .catch(function(err) {
            response.error(err, req, res);
        });
}

const filter = function(params) {
    var filtered = [],
        page = parseInt(params.page) || 1,
        page = page < 1 ? 1 : page,
        sliceIdx = (page-1) * PER_PAGE;

    for (var post of posts) {
        if (params.slug) {
            if (post.slug !== params.slug) {
                continue;
            }
        } else if (post.unlisted) {
            continue;
        }
        if (params.tag) {
            if (!post.tags) {
                continue;
            }
            var found = false;
            for (var tag of post.tags) {
                if (tag.replace(/\+/g, ' ') === params.tag.replace(/\+/g, ' ')) {
                    found = true;
                }
            }
            if (found == false) {
                continue;
            }
        }
        filtered.push(post);
    }

    return {
        posts: filtered.slice(sliceIdx, sliceIdx + PER_PAGE),
        page: page,
        numPages: Math.ceil(filtered.length / PER_PAGE),
    };
}

fs.readdir(__dirname + '/../blog', function(err, files) {
    var tagCounts = {},
        minTagCount = 1,
        maxTagCount = 1;
    for (var i=files.length-1; i>=0; i--) {
        var post = fs.readFileSync(__dirname + '/../blog/' + files[i], 'utf8'),
            regex = /^-{3}\s*[\r\n]+([\s\S]*)[\r\n]+[^-]-{3}[\r\n]+([\s\S]*)$/ig,
            regexLinux = /^-{3}\s*[\n]+([\s\S]*)[\n]+-{3}[\n]+([\s\S]*)$/ig,
            parsed = {
                slug: files[i].replace('.md', '')
            },
            matches = regex.exec(post),
            matchesLinux = regexLinux.exec(post);

        if (matchesLinux) matches = matchesLinux;

        if (matches) {
            headerData = matches[1].split(matchesLinux ? '\n' : '\r');
            for (var j=0; j<headerData.length; j++) {
                var regex = /\s*(\w+):\s*'*(.*[^'])'*/ig
                var keyval = regex.exec(headerData[j]);
                if (keyval) parsed[keyval[1]] = keyval[2];
            }
            parsed.raw = matches[2];
            var markdown = marked(matches[2]).split('<hr>');
            parsed.content = markdown.shift();
            parsed.contentReadMore = markdown.join('<hr>');
            if (parsed.tags) {
                parsed.tags = parsed.tags.split(/,\s*/);
                for (tag of parsed.tags) {
                    if (typeof tagCounts[tag] === "undefined") {
                        tagCounts[tag] = 1;
                    } else {
                        const ret = tagCounts[tag]++;
                        if (ret >= maxTagCount) {
                            maxTagCount++;
                        }
                    }
                }
            }
            posts.push(parsed);
        }
    }
    postLib.resolvePostCounts(posts, 'blog', 'slug');
    for (var tag in tagCounts) {
        if (tagCounts.hasOwnProperty(tag)) {
            var val = {
                hits: tagCounts[tag],
                scale: 100
            };
            if (minTagCount != maxTagCount) {
                val.scale = 100 + (( Math.log(val.hits) / (Math.log(maxTagCount) - Math.log(minTagCount)) ) * 200);
            }
            tags += '\n<li style="font-size: ' + val.scale + '%"><a href="/blog/tagged/' + tag.replace(/\s/g, '+') + '" title="' + val.hits + ' Posts">' + tag + '</a></li>';
        }
    }
    if (posts.length) {
        sitemap.add('/blog', posts[0].date)
        for (let post of posts) {
            sitemap.add('/blog/'+post.slug+'.html', post.date)
        }
    }
})

return { blank: true };

// ==
}
