var view     = require('../lib/view'),
    response = require('../lib/response'),
    util     = require('../lib/util'),
    cache    = require('../lib/cache'),
    marked   = require('marked'),
    hilite   = require('highlight.js'),
    moment   = require('moment'),
    multi    = require('multiparty'),
    Promise  = require('bluebird'),
    app      = null,
    db       = null,
    redis    = null,
    sitemap  = null,
    models   = {};

const Op = require('sequelize').Op;

const FMIFFEL_CACHE_TIMEOUT = 5; // seconds
const FMIFFEL_TRENDING_TAG_COMPUTE_INTERVAL = 3600000; // milliseconds
const FMIFFEL_PAGE_SIZE = 15;

var trendingTags = [];

marked.setOptions({
    sanitize: true,
    highlight: function(code, type) {
        if (type == 'text') return code;
        var language_subset = ['javascript', 'json', 'python'];
        return hilite.highlightAuto(code, language_subset).value;
    }
});

module.exports = function(providers) {
// ==

app           = providers.app;
db            = providers.db;
redis         = providers.redis;
sitemap       = providers.sitemap;

sitemap.add('/fmiffel');
sitemap.add('/fmiffel/henriquez');
sitemap.add('/fmiffel/shibainufacts');

cache.redisInit(redis);

models.FmiffelUser = require('../models/fmiffel_user')(db);
models.FmiffelFollow = require('../models/fmiffel_follow')(db);
models.FmiffelPost = require('../models/fmiffel_post')(db);
models.FmiffelFavorite = require('../models/fmiffel_favorite')(db);
models.FmiffelTag = require('../models/fmiffel_tag')(db);
models.User = require('../models/user')(db);

app.get('/fmiffel', function(req, res) {
    const replyToPostId = parseInt(req.query.replyToPostId) || null,
          searchParams = {
            userId: null, // overridden after the user loads
            showUserPosts: true,
            showFollowedPosts: true,
            showAtPosts: false,
            showFavorites: false,
            searchString: '',
            limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
            pageSize: FMIFFEL_PAGE_SIZE
          };

    return promiseCurrentUserPage(req, res, searchParams, replyToPostId, 'home');
});

app.get('/fmiffel/faq', function(req, res) {
    return view.render(req, 'fmiffel', { viewMode: 'faq' })
               .then(function(data) { response.send(data, req, res); })
});

app.get('/fmiffel/search', function(req, res) {
    return search(req, res);
});

app.get('/fmiffel/replies', function(req, res) {
    const replyToPostId = parseInt(req.query.replyToPostId) || null,
          searchParams = {
            userId: null, // overridden after the user loads
            showUserPosts: false,
            showFollowedPosts: false,
            showAtPosts: true,
            showFavorites: false,
            searchString: '',
            limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
            pageSize: FMIFFEL_PAGE_SIZE
          };

    return promiseCurrentUserPage(req, res, searchParams, replyToPostId, 'replies');
});

app.get('/fmiffel/favorites', function(req, res) {
    const replyToPostId = parseInt(req.query.replyToPostId) || null,
          searchParams = {
            userId: null, // overridden after the user loads
            showUserPosts: false,
            showFollowedPosts: false,
            showAtPosts: false,
            showFavorites: true,
            searchString: '',
            limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
            pageSize: FMIFFEL_PAGE_SIZE
          };

    return promiseCurrentUserPage(req, res, searchParams, replyToPostId, 'favorites');
});

app.get('/fmiffel/settings', function(req, res) {
    return settings(req, res);
});

app.get(/^\/fmiffel\/tag\/([a-zA-Z0-9_\-+]+)$/, function(req, res) {
    req.query.searchString = req.params[0];
    return search(req, res);
});

app.get(/^\/fmiffel\/([a-zA-Z0-9\-\'+]+)$/, function(req, res) {
    const searchParams = {
            userId: null, // overridden after the profile loads
            showUserPosts: true,
            showFollowedPosts: false,
            showAtPosts: false,
            showFavorites: false,
            searchString: '',
            limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
            pageSize: FMIFFEL_PAGE_SIZE
          };

    return promiseProfilePage(req, res, searchParams, req.params[0], 'profile');
});

app.get(/^\/fmiffel\/([a-zA-Z0-9\-\'+]+)\/(\d+)$/, function(req, res) {
    const searchParams = {
            userId: null, // overridden after the profile loads
            showUserPosts: false,
            showFollowedPosts: false,
            showAtPosts: false,
            showFavorites: false,
            searchString: '',
            showPostThread: req.params[1],
            limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
            pageSize: FMIFFEL_PAGE_SIZE
          };

    return promiseProfilePage(req, res, searchParams, req.params[0], 'post');
});

app.get(/^\/fmiffel\/([a-zA-Z0-9\-\'+]+)\/favorites$/, function(req, res) {
    const searchParams = {
            userId: null, // overridden after the profile loads
            showUserPosts: false,
            showFollowedPosts: false,
            showAtPosts: false,
            showFavorites: true,
            searchString: '',
            limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
            pageSize: FMIFFEL_PAGE_SIZE
          };

    return promiseProfilePage(req, res, searchParams, req.params[0], 'favorites');
});

app.get(/^\/fmiffel\/([a-zA-Z0-9\-\'+]+)\/following$/, function(req, res) {
    return promiseFollowPage(req, res, 'following', req.params[0])
})

app.get(/^\/fmiffel\/([a-zA-Z0-9\-\'+]+)\/followers$/, function(req, res) {
    return promiseFollowPage(req, res, 'followers', req.params[0])
})

app.get(/^\/fmiffel\/follow\/f([0-9]+)a([a-f0-9]{64})$/, function(req, res) {
    return promiseChangeFollow(req, res, 'follow', parseInt(req.params[0]), req.params[1]);
})

app.get(/^\/fmiffel\/unfollow\/f([0-9]+)a([a-f0-9]{64})$/, function(req, res) {
    return promiseChangeFollow(req, res, 'unfollow', parseInt(req.params[0]), req.params[1]);
})

app.get(/^\/fmiffel\/favorite\/fa([0-9]+)a([a-f0-9]{64})$/, function(req, res) {
    return promiseChangeFavorite(req, res, 'favorite', parseInt(req.params[0]), req.params[1]);
})

app.get(/^\/fmiffel\/favorite\/fd([0-9]+)a([a-f0-9]{64})$/, function(req, res) {
    return promiseChangeFavorite(req, res, 'unfavorite', parseInt(req.params[0]), req.params[1]);
})

app.get(/^\/fmiffel\/delete\/d([0-9]+)a([a-f0-9]{64})$/, function(req, res) {
    return promiseDelete(req, res, parseInt(req.params[0]), req.params[1]);
})

app.post(/^\/fmiffel\/post$/, function(req, res, next) {
    var fields, json = false, atUsername, atUserId, replyInfo, post = {};

    return new Promise(function(resolve, reject) {
        (new multi.Form()).parse(req, function(err, fields, files) {
            if (err) return reject(err);
            return resolve([fields, files]);
        });
    })
    .then(function(fieldsAndFiles) {
        fields = util.form.flatten(fieldsAndFiles[0]);
        json = util.form.truthy(fields.json) || false;

        return bootstrapUser(req)
    })
    .then(function(_fmlUser) {
        fmlUser = _fmlUser;
        if (fields.csrfToken != getCSRFToken(req) && fmlUser.fmiffel_username != 'npc') {
            throw 'METH IS NOT A CRIME';
        }
        if (!fields.twought) {
            throw 'Please write a post.';
        }
        if (fields.twought.length <= 140) {
            throw 'Your post must be more than 140 characters.';
        }
        post.fmiffel_user_id = fmlUser.id;

        atUsername = fields.twought.match(/^@([a-zA-Z0-9\-\'+]+)/i);

        if (atUsername && atUsername.length == 2) {
            atUsername = atUsername[1];
            return getProfileUserId(atUsername);
        } else {
            return null;
        }
    })
    .then(function(_atUserId) {
        atUserId = _atUserId;
        if (atUserId) {
            post.reply_to_user_id = atUserId;
            post.text = fields.twought.substr(atUsername.length + 2);
        } else {
            post.text = fields.twought;
        }
        if (fields.replyToUserId && fields.replyToPostId) {
            return getReplyInfo(fields.replyToPostId);
        } else {
            return null;
        }
    })
    .then(function(_replyInfo) {
        replyInfo = _replyInfo;
        if (replyInfo &&
            replyInfo.replyToUserId == atUserId &&
            replyInfo.replyToPostId == parseInt(fields.replyToPostId)) {
            post.reply_to_post_id = replyInfo.replyToPostId;
        }
        post.hashed_ip = req.headers['x-forwarded-for']
            ?
            util.sha256(req.headers['x-forwarded-for'])
            :
            util.sha256(req.connection.remoteAddress);
        return models.FmiffelPost.create(post);
    })
    .then(function(instance) {
        models.FmiffelUser.increment('post_count', {where: {id: fmlUser.id}});
        savePostTags(fmlUser, instance, post.text.match(/\s#(\w+)/img));
        var path = null;
        if (post.reply_to_post_id) {
            path = '/fmiffel/'+replyInfo.fmiffelUsernameFormatted+'/'+post.reply_to_post_id+'#FMIFFEL-'+instance.id;
        }
        if (json) { return res.json({path: path}); }
        if (path) { return res.redirect(path); }
        return res.redirect('/fmiffel/' + fmlUser.fmiffel_username_formatted);
    })
    .catch(function(msg) {
        console.error('fmiffel post error: ', msg);
        if (json) { return res.json({error: msg}); }
        const replyToPostId = parseInt(fields.replyToPostId) || null,
              searchParams = {
                userId: null,
                showUserPosts: true,
                showFollowedPosts: true,
                showAtPosts: false,
                showFavorites: false,
                searchString: '',
                limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
                pageSize: FMIFFEL_PAGE_SIZE
              };

        return promiseCurrentUserPage(req, res, searchParams, replyToPostId, 'home', {
            error: msg,
            text: fields.twought
        });
    });
})

app.post('/fmiffel/settings', function(req, res, next) {
    var fields, errors = {}, json = false;

    return new Promise(function(resolve, reject) {
        (new multi.Form()).parse(req, function(err, fields, files) {
            if (err) return reject(err);
            return resolve([fields, files]);
        });
    })
    .then(function(fieldsAndFiles) {
        fields = util.form.flatten(fieldsAndFiles[0]);
        json = util.form.truthy(fields.json) || false;
        return bootstrapUser(req)
    })
    .then(function(_fmlUser) {
        fmlUser = _fmlUser;
        if (fields.csrfToken != getCSRFToken(req) || fmlUser.fmiffel_username == 'npc') {
            throw 'npc';
        }
        let hasErrors = false;

        if (fields.url && (!util.form.isUrl(fields.url) || fields.url.length > 255)) {
            errors.url = 'Please enter a valid URL';
            hasErrors = true;
        }
        if (fields.bio && fields.bio.length > 160) {
            errors.bio = 'Please shorten your bio.';
            hasErrors = true;
        }
        if (fields.location && fields.location.length > 128) {
            errors.location = 'Please shorten your location.';
            hasErrors = true;
        }
        if (hasErrors) { throw 'error(s)'; }

        let update = {
            url: fields.url,
            bio: fields.bio,
            location: fields.location
        };
        return models.FmiffelUser.update(update, {where: {id: fmlUser.id}});
    })
    .then(function() {
        var cacheKey = 'fmiffel:profile:user:' + fmlUser.id;
        return cache.redisDelete(cacheKey);
    })
    .then(function() {
        let path = '/fmiffel/' + fmlUser.fmiffel_username_formatted;
        if (json) { return res.json({path: path}); }
        return res.redirect(path);
    })
    .catch(function(code) {
        console.error('fmiffel settings error(s): ', code, errors);
        if (json) { return res.json({ errors: errors, npc: code === 'npc' }); }
        req._postErrors = errors;
        if (code == 'npc') { req._npcFail = true; }
        return settings(req, res); 
    });
});

const settings = function(req, res) {
    return bootstrapUser(req)
        .then(function(_fmlUser) { return getProfile(_fmlUser.id, _fmlUser) })
        .then(function(_profile) {
            return view.render(req, 'fmiffel', {
                buttonId: 'b_' + util.makeIdCheap(),
                profile: _profile,
                viewMode: 'settings',
                csrfToken: getCSRFToken(req),
                postErrors: req._postErrors ? req._postErrors : {},
                npcFail: req._npcFail ? true : false
            })
        })
        .then(function(data) { response.send(data, req, res); })
}

const search = function(req, res) {
    const json = util.form.truthy(req.query.json) || false,
          searchParams = {
            userId: parseInt(req.query.userId) || 0,
            showUserPosts: util.form.truthy(req.query.showUserPosts) || false,
            showFollowedPosts: util.form.truthy(req.query.showFollowedPosts) || false,
            showAtPosts: util.form.truthy(req.query.showAtPosts) || false,
            showFavorites: util.form.truthy(req.query.showFavorites) || false,
            searchString: req.query.searchString || '',
            limit: parseInt(req.query.pageSize) || FMIFFEL_PAGE_SIZE,
            pageSize: FMIFFEL_PAGE_SIZE,
            afterId: parseInt(req.query.afterId) || 0
          };

    var fmlUser, profile;

    return bootstrapUser(req)
        .then(function(_fmlUser) {
            fmlUser = _fmlUser;
            return getProfile(fmlUser.id, fmlUser);
        })
        .then(function(_profile) {
            profile = _profile;
            return getPosts(fmlUser, searchParams);
        })
        .then(function(posts) {
            if (json) {
                return view.renderPartial(req, 'fmiffel/post_list', {
                    profile: profile,
                    posts: posts,
                    searchParams: searchParams,
                    hideListContainer: true,
                    _is_angel: req.user.hasRole('angel')
                })
                .then(function(html) {
                    return res.json({posts: html, count: posts.count});
                });
            } else {
                return view.render(req, 'fmiffel', {
                    pageType: 'search',
                    viewMode: 'user',
                    profile: profile,
                    posts: posts,
                    searchParams: searchParams,
                    _is_angel: req.user.hasRole('angel'),
                    _unreadReplyCount: fmlUser._unreadReplyCount,
                    trendingTags: trendingTags,
                    csrfToken: getCSRFToken(req),
                })
                .then(function(data) { response.send(data, req, res); })
            }
        })
        .catch(function(err) { response.error(err, req, res); });
}

const promiseCurrentUserPage = function(req, res, searchParams, replyToPostId, pageType, persistState) {
    var fmlUser, profile, replyInfo;
    
    return bootstrapUser(req, pageType == 'replies' ? true : false)
        .then(function(_fmlUser) {
            fmlUser = _fmlUser;
            searchParams.userId = fmlUser.id;
            return getProfile(fmlUser.id, fmlUser)
        })
        .then(function(_profile) {
            profile = _profile;
            if (replyToPostId) {
                return getReplyInfo(replyToPostId)
            } else {
                return null;
            }
        })
        .then(function(_replyInfo) {
            if (_replyInfo) {
                replyInfo = _replyInfo;
            }
            return getPosts(fmlUser, searchParams);
        })
        .then(function(posts) {
            return view.render(req, 'fmiffel', {
                pageType: pageType,
                viewMode: 'user',
                profile: profile,
                posts: posts,
                replyInfo: replyInfo,
                searchParams: searchParams,
                _is_angel: req.user.hasRole('angel'),
                _unreadReplyCount: fmlUser._unreadReplyCount,
                csrfToken: getCSRFToken(req),
                persistState: persistState,
                trendingTags: trendingTags
            })
        })
        .then(function(data) { response.send(data, req, res); })
        .catch(function(err) { response.error(err, req, res); });
}

const promiseProfilePage = function(req, res, searchParams, username, pageType) {
    var fmlUser, profile;

    return bootstrapUser(req)
        .then(function(_fmlUser) {
            fmlUser = _fmlUser;
            return getProfileUserId(username);
        })
        .then(function(profileUserId) {
            searchParams.userId = profileUserId;
            return getProfile(profileUserId, fmlUser);
        })
        .then(function(_profile) {
            profile = _profile;
            return getPosts(fmlUser, searchParams);
        })
        .then(function(posts) {
            return view.render(req, 'fmiffel', {
                pageType: pageType,
                viewMode: 'profile',
                profile: profile,
                posts: posts,
                searchParams: searchParams,
                _is_angel: req.user.hasRole('angel'),
                csrfToken: getCSRFToken(req),
                npcFail: req.query.npcFail ? true : false,
            })
        })
        .then(function(data) { response.send(data, req, res); })
        .catch(function(err) { response.error(err, req, res); });
}

const promiseFollowPage = function(req, res, type, username) {
    var fmlUser, profile;

    return bootstrapUser(req)
        .then(function(_fmlUser) {
            fmlUser = _fmlUser;
            return getProfileUserId(username);
        })
        .then(function(profileUserId) {
            return getProfile(profileUserId, fmlUser);
        })
        .then(function(_profile) {
            profile = _profile;
            const _fn = type == 'following' ? getFollowingList : getFollowerList;
            return _fn(profile.user.id, fmlUser.id);
        })
        .then(function(followList) {
            return view.render(req, 'fmiffel', {
                pageType: type,
                viewMode: 'profile',
                profile: profile,
                followList: followList,
                fmlUserId: fmlUser.id,
                csrfToken: getCSRFToken(req),
                _is_angel: req.user.hasRole('angel')
            })
        })
        .then(function(data) { response.send(data, req, res); })
        .catch(function(err) { response.error(err, req, res); });
}

const promiseChangeFollow = function(req, res, action, profileUserId, csrfToken) {
    var fmlUser, profile;
    const json = util.form.truthy(req.query.json) || false;

    return bootstrapUser(req)
        .then(function(_fmlUser) {
            fmlUser = _fmlUser;
            if (csrfToken != getCSRFToken(req) && fmlUser.fmiffel_username != 'npc') {
                throw { _msg: 'Access denied.', _code: 401 };
            }
            return getProfile(profileUserId, fmlUser);
        })
        .then(function(_profile) {
            profile = _profile;
            return toggleFollow(fmlUser, profile.user, action);
        })
        .then(function(result) {
            if (json) { return res.json({msg: 'ok'}); }
            const path = '/fmiffel/' + profile.user.fmiffel_username_formatted;
            return response.redirect(path, req, res);
        })
        .catch(function(err) {
            err || (err = {});
            if (err._code == 403) {
                if (json) {
                    return res.json({error: 'The NPC user is not allowed to unfollow anyone.'});
                } else {
                    const path = '/fmiffel/' + profile.user.fmiffel_username_formatted + '?npcFail=true';
                    return response.redirect(path, req, res);
                }
            }
            return response.redirect('/access_denied', req, res);
        });
}

const promiseChangeFavorite = function(req, res, action, postId, csrfToken) {
    var fmlUser, profile;
    const json = util.form.truthy(req.query.json) || false;

    return bootstrapUser(req)
        .then(function(_fmlUser) {
            fmlUser = _fmlUser;
            if (csrfToken != getCSRFToken(req) && fmlUser.fmiffel_username != 'npc') {
                throw { _msg: 'Access denied.', _code: 401 };
            }
            return toggleFavorite(fmlUser, postId, action);
        })
        .then(function(result) {
            if (json) { return res.json({msg: 'ok'}); }
            const path = '/fmiffel/' + fmlUser.fmiffel_username_formatted + '/favorites';
            return response.redirect(path, req, res);
        })
        .catch(function(err) {
            console.error('favorite error: ', err);
            err || (err = {});
            return response.redirect('/access_denied', req, res);
        });
}

const promiseDelete = function(req, res, postId, csrfToken) {
    var fmlUser;
    const json = util.form.truthy(req.query.json) || false;

    return bootstrapUser(req)
        .then(function(_fmlUser) {
            fmlUser = _fmlUser;
            if (csrfToken != getCSRFToken(req) && fmlUser.fmiffel_username != 'npc') {
                throw { _msg: 'Access denied.', _code: 401 };
            }
            return deletePost(postId, fmlUser, req.user.hasRole('angel'));
        })
        .then(function(result) {
            if (json) { return res.json({msg: 'ok'}); }
            const path = '/fmiffel/' + fmlUser.fmiffel_username_formatted;
            return response.redirect(path, req, res);
        })
        .catch(function(err) {
            console.error('error: ', err);
            err || (err = {});
            if (err._code == 403) {
                if (json) {
                    return res.json({error: 'The NPC user is not allowed to unfollow anyone.'});
                } else {
                    const path = '/fmiffel/' + fmlUser.fmiffel_username_formatted + '?npcFail=true';
                    return response.redirect(path, req, res);
                }
            }
            return response.redirect('/access_denied', req, res);
        });
}

// pretend everything below is broken out into model files

const getCSRFToken = function(req) {
    return util.sha256(req.user.access_token || req.user.tmpIdentityEncrypted);
}

const bootstrapUser = function(req, resetUnreadReplyCount) {
    if (req.user.id)
    {
        return models.FmiffelUser.findOne({where: {
            user_id: req.user.id
        }})
        .then(function(fmiffelUser) {
            if (fmiffelUser) {
                return populateUnreadReplyCount(fmiffelUser, resetUnreadReplyCount);
            }
            return getClosestAvailableUsernameFormatted(req.user.username_formatted)
                .then(function(availableUsernameFormatted) {
                    var user = {
                        user_id: req.user.id,
                        fmiffel_username: availableUsernameFormatted.toLowerCase(),
                        fmiffel_username_formatted: availableUsernameFormatted,
                        name: req.user.username_formatted,
                        location: 'the internet',
                        url: 'https://obsessivefacts.com',
                        bio: req.user.bio,
                    };
                    return models.FmiffelUser.create(user)
                        .then(function(instance) {
                            return bootstrapFollowRecommendedAccounts(instance);
                        })
                });
        })            
    }
    else if (
        isBot(req)
        ||
        (
            req.headers.dnt
            &&
            req.headers.dnt.toString() === "1"
            &&
            (!req.signedCookies || !req.signedCookies.insidious_tracking_cookie)
        )
    )
    {
        return models.FmiffelUser.findOne({where: {fmiffel_username: 'npc'}})
            .then(function(fmiffelUser) {
                return populateUnreadReplyCount(fmiffelUser, resetUnreadReplyCount);
            });
    }
    else {
        var strippedUsername = req.user.tmpIdentity.name.replace(/\s/g, '');

        return models.FmiffelUser.findOne({where: {
            tmp_username: req.user.tmpIdentity.name,
            tmp_symbol: req.user.tmpIdentity.symbol,
            tmp_fake_id: req.user.tmpIdentity.snowflake
        }})
        .then(function(fmiffelUser) {
            if (fmiffelUser) {
                return populateUnreadReplyCount(fmiffelUser, resetUnreadReplyCount);
            }
            return getClosestAvailableUsernameFormatted(strippedUsername)
                .then(function(availableUsernameFormatted) {
                    var user = {
                        tmp_username: req.user.tmpIdentity.name,
                        tmp_symbol: req.user.tmpIdentity.symbol,
                        tmp_fake_id: req.user.tmpIdentity.snowflake,
                        fmiffel_username: availableUsernameFormatted.toLowerCase(),
                        fmiffel_username_formatted: availableUsernameFormatted,
                        name: req.user.tmpIdentity.name,
                        location: 'the internet',
                        url: 'https://obsessivefacts.com',
                        bio: 'I didn\'t fill out my bio',
                    };
                    return models.FmiffelUser.create(user)
                        .then(function(instance) {
                            return bootstrapFollowRecommendedAccounts(instance);
                        })
                })
        });
    }
}

const isBot = req => {
    let userAgent = req.headers['user-agent'];
    if (userAgent) {
        if (
            userAgent.indexOf('Googlebot') !== -1
            || userAgent.indexOf('Bytespider') !== -1
            || userAgent.toLowerCase().indexOf('bot') !== -1
        ) {
            return true;
        }
    }
    return false;
}

const getClosestAvailableUsernameFormatted = (usernameFormatted, c) => {
    if (!c) c = 0;

    return models.FmiffelUser.findAll({
        where: { fmiffel_username: usernameFormatted.toLowerCase() },
        limit: 1,
        order: [ [ 'created_at', 'DESC' ] ]
    })
    .then(function(existUser) {
        if (existUser.length == 0)
            return usernameFormatted;
        else
            c++;

        return getClosestAvailableUsernameFormatted(usernameFormatted + c, c);
    })
}

const bootstrapFollowRecommendedAccounts = function(fmlUser) {
    return models.FmiffelUser.findAll({
        where: {id: {[Op.not]: fmlUser.id}},
        order: [ ['is_featured', 'DESC'], ['post_count', 'DESC'] ],
        attributes: ['id'],
        limit: 24,
        raw: true
    })
    .then(function(accounts) {
        return models.FmiffelFollow.bulkCreate(accounts.map(function(account) {
            return {
                fmiffel_user_id: fmlUser.id,
                fmiffel_user_followed_id: account.id
            }
        }))
    })
    .then(function() {
        return fmlUser;
    });
}

const populateUnreadReplyCount = function(fmlUser, reset) {
    if (reset) {
        return fmlUser.update({
            replies_visit_date: db.fn('NOW')
        })
        .then(function() {
            fmlUser._unreadReplyCount = 0;
            return fmlUser;
        });
    } else {
        var where = {
            reply_to_user_id: fmlUser.id,
            fmiffel_user_id: {[Op.not]: fmlUser.id}
        };
        if (fmlUser.replies_visit_date) {
            where.created_at = {[Op.gt]: fmlUser.replies_visit_date};
        }
        return models.FmiffelPost.count({
            where: where
        })
        .then(function(count) {
            fmlUser._unreadReplyCount = count;
            return fmlUser;
        })
    }
}

const getProfile = function(userId, currUser) {
    var profile = {},
        cacheKey = 'fmiffel:profile:following:' + userId;

    return cache.redisGet(cacheKey)
        .then(function(cachedData) {
            if (cachedData) return cachedData;

            return models.FmiffelFollow.findAndCountAll({
                where: { fmiffel_user_id: userId },
                limit: 0
            });
        })
        .then(function(result) {
            if (typeof result === 'string') {
                profile.following = JSON.parse(result);
            } else {
                profile.following = result.count;
                cache.redisSet(cacheKey, JSON.stringify(result.count), FMIFFEL_CACHE_TIMEOUT);
            }

            cacheKey = 'fmiffel:profile:followers:' + userId;
            return cache.redisGet(cacheKey)
        })
        .then(function(cachedData) {
            if (cachedData) return cachedData;

            return models.FmiffelFollow.findAndCountAll({
                where: { fmiffel_user_followed_id: userId },
                limit: 0
            });
        })
        .then(function(result) {
            if (typeof result === 'string') {
                profile.followers = JSON.parse(result);
            } else {
                profile.followers = result.count;
                cache.redisSet(cacheKey, JSON.stringify(result.count), FMIFFEL_CACHE_TIMEOUT);
            }

            cacheKey = 'fmiffel:profile:updates:' + userId;
            return cache.redisGet(cacheKey)
        })
        .then(function(cachedData) {
            if (cachedData) return cachedData;

            return models.FmiffelPost.findAndCountAll({
                where: { fmiffel_user_id: userId },
                limit: 0
            });
        })
        .then(function(result) {
            if (typeof result === 'string') {
                profile.updates = JSON.parse(result);
            } else {
                profile.updates = result.count;
                cache.redisSet(cacheKey, JSON.stringify(result.count), FMIFFEL_CACHE_TIMEOUT);
            }
            cacheKey = 'fmiffel:profile:user:' + userId;
            return cache.redisGet(cacheKey)
        })
        .then(function(cachedData) {
            if (cachedData) return cachedData;

            return models.FmiffelUser.findOne({
                where: { id: userId },
                include: [
                    { model: models.User, as: 'user', attributes: ['avatar'] },
                ]
            });
        })
        .then(function(result) {
            if (typeof result === 'string') {
                profile.user = JSON.parse(result);
            } else {
                profile.user = result.toJSON();
                cache.redisSet(cacheKey, JSON.stringify(profile.user), FMIFFEL_CACHE_TIMEOUT);
            }
            profile.user._is_me = userId == currUser.id;
            if (profile.user._is_me) {
                return getFollowingList(userId, userId, false, 1, 24)
            }
            cacheKey = 'fmiffel:profile:followLink:' + currUser.id + ':' + userId;
            return cache.redisGet(cacheKey)
                .then(function(cachedData) {
                    if (cachedData) return cachedData;

                    return models.FmiffelFollow.findOne({
                        where: {
                            fmiffel_user_id: currUser.id,
                            fmiffel_user_followed_id: userId
                        }
                    });
                })
                .then(function(result) {
                    if (typeof result === 'string') {
                        profile.user._i_am_following = JSON.parse(result);
                    } else {
                        profile.user._i_am_following = !!result;
                        cache.redisSet(cacheKey, JSON.stringify(profile.user._i_am_following), FMIFFEL_CACHE_TIMEOUT);
                    }
                    return getFollowingList(userId, userId, false, 1, 24);
                });
        })
        .then(function(followedUsers) {
            profile.followedUsers = followedUsers;                    
            return profile;
        });
}

const getProfileUserId = function(fmiffelUsernameFormatted) {
    var cacheKey = 'fmiffel:profile:userId:' + fmiffelUsernameFormatted;
    return cache.redisGet(cacheKey)
        .then(function(cachedData) {
            if (cachedData) return cachedData;

            return models.FmiffelUser.findOne({
                where: { fmiffel_username: fmiffelUsernameFormatted.toLowerCase() },
                attributes: ['id']
            })
        })
        .then(function(result) {
            if (typeof result === 'string') {
                return JSON.parse(result);
            }
            if (!result || !result.id) {
                throw { _msg: 'Not found.', _code: 404 };
            }
            cache.redisSet(cacheKey, JSON.stringify(result.id), FMIFFEL_CACHE_TIMEOUT);
            return result.id;
        });
}

const generateFollowQuery = function(type) {
    if (type == 'following') {
        var hingeColumn = 'ff.fmiffel_user_id',
            userLookupColumn = 'ff.fmiffel_user_followed_id';
    } else {
        var hingeColumn = 'ff.fmiffel_user_followed_id',
            userLookupColumn = 'ff.fmiffel_user_id';
    }
    return `
        SELECT
            fu.id, fu.tmp_username, fu.tmp_symbol, fu.tmp_fake_id,
            fu.fmiffel_username, fu.fmiffel_username_formatted, fu.name,
            fu.user_id, u.avatar, ff2.id AS already_followed
        FROM
            fmiffel_follow ff
        INNER JOIN
            fmiffel_user fu
        ON
            fu.id = ` + userLookupColumn + `
        LEFT OUTER JOIN
            fmiffel_follow ff2
        ON
            ff2.fmiffel_user_followed_id = ` + userLookupColumn + `
        AND
            ff2.fmiffel_user_id = :alreadyFollowing
        LEFT OUTER JOIN
            "user" u
        ON
            u.id = fu.user_id
        WHERE
            ` + hingeColumn + ` = :userId
        LIMIT
            :perPage
        OFFSET
            :offset
        `;
}

const getFollowingList = function(userId, userIdToSeeIfFollowingAlready, returnArray, page, perPage) {
    if (!page) page = 1;
    if (!perPage) perPage = 99999;
    const cacheNamespace = 'fmiffel:following:' + userId,
          cacheKey = 'fmiffel:following:'
            + (userIdToSeeIfFollowingAlready ? 1 : 0) + ':'
            + (returnArray ? 1 : 0) + ':'
            + page + ':'
            + perPage;

    return cache.redisGet(cacheKey, cacheNamespace)
        .then(function(cachedData) {
            if (cachedData) return cachedData;

            return db.query(
                generateFollowQuery('following'),
                {
                    replacements: {
                        userId: userId,
                        alreadyFollowing: userIdToSeeIfFollowingAlready,
                        offset: (page-1)*perPage,
                        perPage: perPage
                    },
                }
            );
        })
        .then(function(result) {
            if (typeof result === 'string') {
                return JSON.parse(result);
            }
            result = !returnArray ? result[0] : result[0].map(function(r) {
                return r.id;
            });
            cache.redisSet(cacheKey, JSON.stringify(result), FMIFFEL_CACHE_TIMEOUT, cacheNamespace);
            return result;
        });
}

const getFollowerList = function(userId, userIdToSeeIfFollowingAlready, page, perPage) {
    if (!page) page = 1;
    if (!perPage) perPage = 99999;
    const cacheNamespace = 'fmiffel:followers:' + userId,
          cacheKey = 'fmiffel:followers:'
            + (userIdToSeeIfFollowingAlready ? 1 : 0) + ':'
            + page + ':'
            + perPage;

    return cache.redisGet(cacheKey, cacheNamespace)
        .then(function(cachedData) {
            if (cachedData) return cachedData;

            return db.query(
                generateFollowQuery('followers'),
                {
                    replacements: {
                        userId: userId,
                        alreadyFollowing: userIdToSeeIfFollowingAlready,
                        offset: (page-1)*perPage,
                        perPage: perPage
                    },
                }
            );
        })
        .then(function(result) {
            if (typeof result === 'string') {
                return JSON.parse(result);
            }
            cache.redisSet(cacheKey, JSON.stringify(result[0]), FMIFFEL_CACHE_TIMEOUT, cacheNamespace);
            return result[0];
        });
}

const getPosts = function(currUser, searchParams) {
    // Initialize query expanders
    var qry = '',
        extra = '',
        result = {},
        replacements = {
            userId: searchParams.userId,
            currUserId: currUser.id
        };

    // Get followed user list for this user
    return getFollowingList(searchParams.userId, currUser.id, true)
        .then(function(followedUserList) {
            // flatten the follower array to a list
            followedUserList = followedUserList.join(',') || "-1";

            // Build the selection query
            qry = `
                SELECT
                    fp.id, fp.text, fp.created_at, fp.fmiffel_user_id,
                    fp.reply_to_post_id, fp.reply_to_user_id, fu.tmp_username,
                    fu.tmp_symbol, fu.tmp_fake_id, fu.fmiffel_username,
                    fu.fmiffel_username_formatted, fu.user_id, u.avatar,
                    fu2.fmiffel_username_formatted AS reply_to_username,
                    ff.id AS is_favorite, ff2.id AS is_viewed_user_favorite
                FROM
                    fmiffel_post fp
                INNER JOIN
                    fmiffel_user fu
                ON
                    fu.id = fp.fmiffel_user_id
                LEFT OUTER JOIN
                    "user" u
                ON
                    u.id = fu.user_id
                LEFT OUTER JOIN
                    fmiffel_favorite ff
                ON
                    ff.fmiffel_user_id = :currUserId
                AND
                    ff.fmiffel_post_id = fp.id
                LEFT OUTER JOIN
                    fmiffel_favorite ff2
                ON
                    ff2.fmiffel_user_id = :userId
                AND
                    ff2.fmiffel_post_id = fp.id
                LEFT OUTER JOIN
                    fmiffel_user fu2
                ON
                    fu2.id = fp.reply_to_user_id
                WHERE
                    (1=0
                `;

            if (searchParams.showUserPosts) { 
                extra += "OR fp.fmiffel_user_id = :userId ";
            }
            // It's a lot simpler to just show extended posts for everyone
            if (searchParams.showFollowedPosts) { 
                extra += "OR fp.fmiffel_user_id IN ("+followedUserList+") ";
            }
            if (searchParams.showAtPosts) { 
                extra += "OR fp.reply_to_user_id = :userId ";
            }
            if (searchParams.showFavorites) { 
                extra += "OR ff2.id IS NOT NULL ";
            }
            if (searchParams.searchString) {
                extra += "OR lower(fp.text) LIKE :searchString ";
                extra += "OR fu.fmiffel_username LIKE :searchString ";
                replacements.searchString = '%' + searchParams.searchString.toLowerCase() + '%';
            }
            if (typeof searchParams.showPostThread !== "undefined") {
                extra += "OR fp.id = :showPostThread ";
                extra += "OR fp.reply_to_post_id = :showPostThread ";
                replacements.showPostThread = parseInt(searchParams.showPostThread);
            }
            extra += ') ';
            var afterId = '';
            if (searchParams.afterId) {
                afterId = 'AND fp.id < :afterId ';
                replacements.afterId = searchParams.afterId;
            }
            if (typeof searchParams.showPostThread !== "undefined") {
                var order = 'ASC';
            } else {
                var order = 'DESC';
            }
            qry += extra + afterId + 'ORDER BY fp.created_at ' + order + ' LIMIT :limit';
            replacements.limit = searchParams.limit;

            // Run our query
            return db.query(qry, { replacements: replacements });
        })
        .then(function(response) {
            var posts = response[0];

            for (post of posts) {
                post.text = postprocessPostText(post.text);
                post._ago_time = moment(post.created_at).fromNow();
                post._is_me = post.fmiffel_user_id == currUser.id;
            }
            result.posts = posts;

            // Now run a simpler version to get record count for pagination
            // perhaps all of this could be done with sequelize findAndCountAll.
            // perhaps i need you to hold my dick when i pee because i'm scared.
            qry = `
                SELECT
                    COUNT(fp.fmiffel_user_id) AS record_count
                FROM
                    fmiffel_post fp
                INNER JOIN
                    fmiffel_user fu
                ON
                    fu.id = fp.fmiffel_user_id
                LEFT OUTER JOIN
                    fmiffel_favorite ff
                ON
                    ff.fmiffel_user_id = :currUserId
                AND
                    ff.fmiffel_post_id = fp.id
                LEFT OUTER JOIN
                    fmiffel_favorite ff2
                ON
                    ff2.fmiffel_user_id = :userId
                AND
                    ff2.fmiffel_post_id = fp.id
                WHERE
                    (1=0
                `;
            return db.query(qry + extra, { replacements: replacements });
        })
        .then(function(response) {
            result.count = parseInt(response[0][0].record_count);
            return result;
        });
}

const toggleFollow = function(currUser, followUser, action) {
    if (
        currUser.fmiffel_username == 'npc' && action == 'unfollow'
        ||
        currUser.id == followUser.id
    ) {
        throw { _msg: 'ARE YOU KIDDING ME?!', _code: 403 };
    }
    return models.FmiffelFollow.destroy({ where: {
        fmiffel_user_id: currUser.id,
        fmiffel_user_followed_id: followUser.id
    }})
    .then(function(res) {
        const cacheKeys = [
            'fmiffel:profile:following:' + currUser.id,
            'fmiffel:profile:followers:' + followUser.id,
            'fmiffel:profile:followLink:' + currUser.id + ':' + followUser.id
        ];
        return Promise.each(cacheKeys, function(cacheKey) {
            return cache.redisDelete(cacheKey)
        });
    })
    .then(function() {
        const namespaces = [
            'fmiffel:following:' + currUser.id,
            'fmiffel:followers:' + followUser.id
        ];
        return Promise.each(namespaces, function(namespace) {
            return cache.redisDestroyNamespace(namespace)
        });
    })
    .then(function() {
        if (action == 'unfollow') { return true; }

        return models.FmiffelFollow.create({
            fmiffel_user_id: currUser.id,
            fmiffel_user_followed_id: followUser.id
        });
    });
}

const toggleFavorite = function(fmlUser, postId, action) {
    return models.FmiffelFavorite.destroy({ where: {
        fmiffel_user_id: fmlUser.id,
        fmiffel_post_id: postId
    }})
    .then(function() {
        if (action == 'unfavorite') { return true; }

        return models.FmiffelPost.findOne({
            where: { id: postId }
        })
        .then(function(post) {
            if (!post) { throw { _msg: '4040404', _code: 404 }; }

            return models.FmiffelFavorite.create({
                fmiffel_user_id: fmlUser.id,
                fmiffel_post_id: postId
            });
        })
    })
}

const deletePost = function(postId, fmlUser, isAngel) {
    if (fmlUser.fmiffel_username == 'npc') {
        throw { _msg: 'ARE YOU KIDDING ME?!', _code: 403 };
    }
    return models.FmiffelPost.findOne({where: {id: postId}})
        .then(function(post) {
            if (!post) {
                throw { _msg: '4040404', _code: 404 };
            }
            if (post.fmiffel_user_id != fmlUser.id && !isAngel) {
                throw { _msg: 'User does not have access to delete post', _code: 401 };
            }
            return models.FmiffelFavorite.destroy({where: {fmiffel_post_id: postId}})
        })
        .then(function() {
            return models.FmiffelTag.destroy({where: {fmiffel_post_id: postId}})
        })
        .then(function() {
            return models.FmiffelPost.destroy({where: {id: postId}})
        })
        .then(function() {
            models.FmiffelUser.decrement('post_count', {where: {id: fmlUser.id}});
            return models.FmiffelPost.update(
                { reply_to_post_id: null },
                { where: { reply_to_post_id: postId } }
            );
        })
}

const postprocessPostText = function(text) {
    text = marked(text)
           .replace(/\<a\s/img, '<a rel="nofollow" ')
           .replace(/([>\s])(#)(\w+)/img, '$1<a href="/fmiffel/tag/$3">$2$3</a>');
    return text;
}

const getReplyInfo = function(replyToPostId) {
    return models.FmiffelPost.findOne({
        where: {id: parseInt(replyToPostId)},
        include: [
            {
                model: models.FmiffelUser,
                as: 'fmiffel_user',
                attributes: ['fmiffel_username_formatted']
            }
        ]
    })
    .then(function(post) {
        if (!post) { throw 'whatever'; }

        const replyInfo = {
            replyToUserId: post.fmiffel_user_id,
            replyToPostId: post.id,
            fmiffelUsernameFormatted: post.fmiffel_user.fmiffel_username_formatted,
            text: postprocessPostText(post.text)
        }
        return replyInfo;
    })
    .catch(function(err) {
        console.error('getReplyInfo failed: the client is an ass.');
        return null;
    });
}

const savePostTags = function(fmlUser, instance, tagMatches) {
    if (!tagMatches) return Promise.resolve(false);
    var dupes = {};
    return models.FmiffelTag.bulkCreate(
        tagMatches.filter(function(tag) {
            if (typeof dupes[tag] === "undefined") {
                dupes[tag] = true;
                return true;
            }
            return false;
        }).map(function(tag) {
            return {
                tag: tag.substr(2, 32),
                fmiffel_user_id: fmlUser.id,
                fmiffel_post_id: instance.id
            }
        }
    ));
}

const computeTrendingTags = function() {
    var tagObj = {}, tagArr = [];

    return models.FmiffelTag.findAll({
        limit: 1000,
        raw: true,
        order: [ ['created_at', 'DESC'] ]
    })
    .then(function(tags) {
        if (!tags || !tags.length) return;

        var lastTimestamp = new Date(tags[tags.length-1].createdAt).getTime();

        for (tag of tags) {
            let key = tag.tag.toLowerCase(),
                timestamp = new Date(tag.createdAt).getTime(),
                timeScore = timestamp - lastTimestamp;

            if (typeof tagObj[key] === "undefined") {
                tagObj[key] = { count: 1, timeScore: timeScore, variances: {} };
                tagObj[key].variances[tag.tag] = 1;
            } else {
                tagObj[key].count++;
                tagObj[key].timeScore += timeScore;
                if (typeof tagObj[key].variances[tag.tag] === "undefined") {
                    tagObj[key].variances[tag.tag] = 1;
                } else {
                    tagObj[key].variances[tag.tag]++;
                }
            }
        }
        for (var key in tagObj) {
            if (!tagObj.hasOwnProperty(key)) continue;

            var varianceArray = [];
            for (var subKey in tagObj[key].variances) {
                if (!tagObj[key].variances.hasOwnProperty(subKey)) continue;

                varianceArray.push({
                    variance: subKey,
                    count: tagObj[key].variances[subKey]
                })
            }
            varianceArray.sort(function(a, b) { return b.count - a.count; });
            tagObj[key].winner = varianceArray[0].variance;
            tagArr.push({
                tag: tagObj[key].winner,
                timeScore: tagObj[key].timeScore
            });
        };
        tagArr.sort(function(a, b) { return b.timeScore - a.timeScore; });
        trendingTags = tagArr.slice(0, 4);
    });
}

computeTrendingTags();
setInterval(computeTrendingTags, FMIFFEL_TRENDING_TAG_COMPUTE_INTERVAL);

return { blank: true };

// ==
}
