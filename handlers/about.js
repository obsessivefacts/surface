var view     = require('../lib/view'),
    response = require('../lib/response'),
    fs       = require('fs'),
    marked   = require('marked'),
    app      = null,
    sitemap  = null,
    about    = '';


module.exports = function(providers) {
// ==

app = providers.app;
sitemap = providers.sitemap;

app.get('/about', function(req, res) {
  return view.render(req, 'about', { about: about })
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/about');

app.get('/about/x', function(req, res) {
  return view.render(req, 'about', { rotate_checked: true, about: about })
    .then(function(data) { response.send(data, req, res); })
    .catch(function(err) { response.error(err, req, res); });
});
sitemap.add('/about/x');

about = marked(fs.readFileSync(__dirname + '/../README.md', 'utf8'));

return { blank: true };

// ==
}
